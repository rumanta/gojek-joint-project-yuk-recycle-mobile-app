Yuk Recycle v.1.0
=====
> Deskripsi Projek


Staging coverage:
- ![Back End Coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2019/PPLA4/badges/staging/coverage.svg?job=backend:test) Backend
- ![Mitra Coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2019/PPLA4/badges/staging/coverage.svg?job=mitra:unit-test) Mitra app
- ![Customer Coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2019/PPLA4/badges/staging/coverage.svg?job=customer:unit-test) Customer app

Development coverage:
- ![Back End Coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2019/PPLA4/badges/development/coverage.svg?job=backend:test) Backend
- ![Mitra Coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2019/PPLA4/badges/development/coverage.svg?job=mitra:unit-test) Mitra app
- ![Customer Coverage](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2019/PPLA4/badges/development/coverage.svg?job=customer:unit-test) Customer app


Member :
- Misael Jonathan
- Donny Samuel
- Endrawan Andika Wicaksana
- Muhammad Yusuf Sholeh
- R. Azka Ali Fazagani
- William Rumanta

Technology Used :
- Flutter
- Golang
- PosgreSQL


Feature in this version :
