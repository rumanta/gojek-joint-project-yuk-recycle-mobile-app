import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:flutter/foundation.dart';
import 'package:customer_app/location_map.dart' as location;
import 'package:customer_app/make_order.dart' as makeorder;
import 'package:customer_app/login_provider.dart';
import 'package:http/http.dart' show Client;
import 'package:customer_app/models/api_provider.dart';

void main() {

  testWidgets('Test Location Map Page Loaded', (WidgetTester tester) async {

    makeorder.OrderItems passedOrderItemDummy = new makeorder.OrderItems();

    // Init
    await tester.pumpWidget(location.LocationMap(passedOrderItems: passedOrderItemDummy,));

    // Check Map Widget Exists
    expect(find.byKey(Key('locationMap')), findsOneWidget);

  });

  test("Api Provider Test : Place Detail HTTP Request Response = 200", () async {
    final apiProvider = ApiProvider();

    // Mock HTTP
    apiProvider.client = MockClient((request) async {
      final mapJson = {'result' : {'id': 'abcdefg', 'geometry' : {'location' : {'lat' : 30.0, 'lng' : 40.0}}}};
      return Response(json.encode(mapJson), 200);
    });

    // API Call
    String placeId = 'abcdefg';
    final item = await apiProvider.placeDetailHttpRequest(placeId);

    // Value Test
    expect(item.latitude, 30.0);
    expect(item.longitude, 40.0);
  });

  test("Api Provider Test : Place Detail HTTP Request Response != 200", () async {
    final apiProvider = ApiProvider();

    //Mock Http
    apiProvider.client = MockClient((request) async {
      final mapJson = {'result' : {'id': 'abcdefg', 'geometry' : {'location' : {'lat' : 30.0, 'lng' : 40.0}}}};
      return Response(json.encode(mapJson), 400);
    });

    //Value Test
    String placeId = 'abcdefg';
    final item = await apiProvider.placeDetailHttpRequest(placeId);

    expect(item, null);
  });

  test("Api Provider Test : Get address HTTP Request Response = 200", () async {
    final apiProvider = ApiProvider();

    //Mock Http
    apiProvider.client = MockClient((request) async {
      final mapJson = {'results': [{'formatted_address' : 'Jalan Kelapa Kopyor'}]};
      return Response(json.encode(mapJson), 200);
    });

    //Simulate Camera Animation
    GoogleMapController controller;
    final latlng = LatLng(30.0, 40.0);

    location.CurrentAddress address = await apiProvider.httpRequest(controller, '123456789', latlng.latitude.toString(), latlng.longitude.toString());

    expect(address.address, 'Jalan Kelapa Kopyor');
  });

  test("Api Provider Test : Get address HTTP Request Response != 200", () async {
    final apiProvider = ApiProvider();

    //Mock Http
    apiProvider.client = MockClient((request) async {
      final mapJson = {'results': [{'formatted_address' : 'Jalan Kelapa Kopyor'}]};
      return Response(json.encode(mapJson), 400);
    });

    //Simulate Camera Animation
    GoogleMapController controller;
    final latlng = LatLng(30.0, 40.0);

    location.CurrentAddress address = await apiProvider.httpRequest(controller, '123456789', latlng.latitude.toString(), latlng.longitude.toString());

    expect(address, null);
  });
  testWidgets('Test Floating Button Click Show Marker', (WidgetTester tester) async {
    makeorder.OrderItems passedOrderItemDummy = new makeorder.OrderItems();
    LatLng dummy = LatLng(6.666, 7.7777);
    await tester.pumpWidget(
        new MaterialApp(
          home: new location.MapSample(passedOrderItems: passedOrderItemDummy, currentLocation: dummy,),
          ),
        );

    // Check Floating Button Exists
    expect(find.byKey(Key('floating-button')), findsOneWidget);

    location.MapSampleState state = tester.state<location.MapSampleState>(find.byType(location.MapSample));

    expect(state.markers.isEmpty, true);
    expect(state.addressContainerVisibility, false);

    location.CurrentAddress address = new location.CurrentAddress(address: 'Jalan Kelapa Kopyor');
    state.setMarkerAndContainerAvailibility(address, 30.0, 40.0);
    await tester.pump();

    expect(state.markers.isEmpty, false);
    expect(state.addressContainerVisibility, true);
  });

  test('Check Detail Order Only Display Non-Zero Quantity',() async {
    Map<String, makeorder.Sampah> listSampah = Map<String, makeorder.Sampah>();
    makeorder.Sampah dummySampahPlastik = new makeorder.Sampah(
      namaSampah: "Sampah Plastik",
      id: '1',
      quantity: 1,
      satuan: 'kg',
      pic: 'qwertyuiopasdfghjklzxcvbnm',
      desc: 'ini sampah plastik'
    );
    makeorder.Sampah dummySampahKertas = new makeorder.Sampah(
      namaSampah: "Sampah Kertas",
      id: '2',
      quantity: 0,
      satuan: 'kg',
      pic: 'qwertyuiopasdfghjklzxcvbnm',
      desc: 'ini sampah kertas'
    );
    makeorder.Sampah dummySampahBotol = new makeorder.Sampah(
      namaSampah: "Botol Plastik",
      id: '3',
      quantity: 0,
      satuan: 'botol',
      pic: 'qwertyuiopasdfghjklzxcvbnm',
      desc: 'ini botol plastik'
    );
    listSampah['Sampah Plastik'] = dummySampahPlastik;
    listSampah['Botol Plastik'] = dummySampahBotol;
    listSampah['Sampah Kertas'] = dummySampahKertas;
    
    makeorder.OrderItems orderItem = new makeorder.OrderItems(itemSampah: listSampah);
    location.OrderDetail orderDetail = new location.OrderDetail(orderItems: orderItem);

    Map<String, dynamic> json = orderDetail.toJson();
    expect(json['order_detail'].length, 1);
  });
  test('Create Order HTTP Request', () async {
    final apiProvider = ApiProvider();

    // Order Detail Object
    Map<String, makeorder.Sampah> listSampah = Map<String, makeorder.Sampah>();
    makeorder.Sampah dummySampahPlastik = new makeorder.Sampah(
      namaSampah: "Sampah Plastik",
      id: '1',
      quantity: 1,
      satuan: 'kg',
      pic: 'qwertyuiopasdfghjklzxcvbnm',
      desc: 'ini sampah plastik'
    );
    makeorder.Sampah dummySampahKertas = new makeorder.Sampah(
      namaSampah: "Sampah Kertas",
      id: '2',
      quantity: 0,
      satuan: 'kg',
      pic: 'qwertyuiopasdfghjklzxcvbnm',
      desc: 'ini sampah kertas'
    );
    makeorder.Sampah dummySampahBotol = new makeorder.Sampah(
      namaSampah: "Botol Plastik",
      id: '3',
      quantity: 0,
      satuan: 'botol',
      pic: 'qwertyuiopasdfghjklzxcvbnm',
      desc: 'ini botol plastik'
    );
    listSampah['Sampah Plastik'] = dummySampahPlastik;
    listSampah['Botol Plastik'] = dummySampahBotol;
    listSampah['Sampah Kertas'] = dummySampahKertas;
    
    makeorder.OrderItems orderItem = new makeorder.OrderItems(itemSampah: listSampah);
    location.OrderDetail orderDetail = new location.OrderDetail(orderItems: orderItem);

    // Mock HTTP
    apiProvider.client = MockClient((request) async {
      final mapJson = {
        "mitra_id": 0,
        "status": "order has been processed"
      };
      return Response(json.encode(mapJson), 200);
    });

    // API Call
    String token = 'initoken';
    Map response = await apiProvider.createOrderHttpRequest(orderDetail, token);

    // Value Test    
    expect(response['status'], true);
    expect(response['errorMessage'], null);
  });
}
