import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:customer_app/register.dart';

void main (){
  testWidgets('Renders', (WidgetTester tester) async {
    await tester.pumpWidget(new MaterialApp(home: Register(),));

    expect(find.text('Name'), findsOneWidget);
    expect(find.text('Email Address'), findsOneWidget);
    expect(find.text('Phone Number'),findsOneWidget);
    expect(find.text('Password'), findsOneWidget);
    expect(find.text('Konfirmasi Password'), findsOneWidget);
    expect(find.byType(TextFormField), findsNWidgets(5));
    expect(find.byType(RaisedButton), findsNWidgets(1));

    await tester.tap(find.byKey(Key('SignUpBackEnd')));
    await tester.pump();


});
}