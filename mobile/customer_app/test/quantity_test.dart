// import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// import 'package:customer_app/main.dart';
// import 'package:customer_app/make_order.dart';
// import 'package:flutter/foundation.dart';
// import 'package:quiver/testing/async.dart';


void main() {
  
}
// void main() {

//   testWidgets('Test Quantity Widget', (WidgetTester tester) async {
    
//     List<String> keySet = ['quantMinus', 'quantPlus'];
    
//     // Init
//     await tester.pumpWidget(CustMakeOrder());
  
//     // Check Card Number
//     expect(find.byIcon(Icons.add), findsNWidgets(5));
    
//     var satuan = 'kg';
//     // Check Each Card Functionality
//     for (var cardSampah = 1; cardSampah <= 5; cardSampah++) {
//       if(cardSampah == 2 || cardSampah == 3) {
//         satuan = 'btl';
//       } else {
//         satuan = 'kg';
//       }

//       // Decrease Quantity Bad State : Non-Negative Number Prevention
//       await tester.tap(find.byKey(Key('quantMinus' + cardSampah.toString())));
//       await tester.pump();
//       expect((find.byKey(Key('quantItem'+cardSampah.toString())).evaluate().first.widget as Text).data, '0 '+satuan);


//       // Increase Quantity Good State
//       await tester.tap(find.byKey(Key('quantPlus' + cardSampah.toString())));
//       await tester.pump();

//       expect((find.byKey(Key('quantItem'+cardSampah.toString())).evaluate().first.widget as Text).data, '1 '+satuan);

//       // Decrease Quantity Good State
//       await tester.tap(find.byKey(Key('quantMinus'+cardSampah.toString())));
//       await tester.pump();

//       expect((find.byKey(Key('quantItem'+cardSampah.toString())).evaluate().first.widget as Text).data, '0 '+satuan);
//     }
//   });
// }
