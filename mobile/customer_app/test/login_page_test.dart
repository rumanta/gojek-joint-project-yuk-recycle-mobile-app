import 'package:flutter_test/flutter_test.dart';
import 'package:customer_app/login_page.dart';

void main() {

  test('Empty email returns error string', () {
    var result = EmailFieldValidator.validate('');
    expect(result, 'Email tidak boleh kosong');
  });

  test('Non-valid email format returns error string', () {
    var result = EmailFieldValidator.validate('will.com');
    expect(result, 'Silahkan isi email dengan format yang benar');
  });

  test('Numeric email returns null', () {
    var result = EmailFieldValidator.validate('will@host.com');
    expect(result, null);
  });

  test('Empty password returns error string', () {
    var result = PasswordFieldValidator.validate('');
    expect(result, 'Password tidak boleh kosong');
  });

  test('Non-empty password returns null', () {
    var result = PasswordFieldValidator.validate('asdasd');
    expect(result, null);
  });
}