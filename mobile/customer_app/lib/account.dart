import 'package:flutter/material.dart';
import 'login_provider.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:package_info/package_info.dart';
import 'dart:async';
import 'const.dart';
import 'edit_account.dart';

Client client = new Client();
String HOME_URL = '$BASE_URL/account';

class API {
  static Future APICall() {
    var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    return client.get(HOME_URL, headers: header);
  }
}

class UserData {
  String name;
  String email;
  int phone;
  int login_type;

  UserData(String name, String email, int phone, int login_type) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.login_type = login_type;
  }

  UserData.fromJson(Map json)
      : name = json['name'],
        email = json['email'],
        phone = json['phone'],
        login_type = json['login_type'];

  Map toJson() {
    return {
      'name': name,
      'email': email,
      'phone': phone,
      'login_type': login_type
    };
  }
}

GoogleSignIn _googleSignIn = new GoogleSignIn(
    scopes: [
      'https://www.googleapis.com/auth/userinfo.profile',
    ],
    serverClientId:
        "97555802072-tq4dfs9kft22l1du58oin0sh7umk2meg.apps.googleusercontent.com");

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'My Account',
          style: TextStyle(
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w900,
            fontSize: 20,
            color: Color(0xFF1D1D27),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          AccountEdit(),
          PrivacyPolicyInfo(),
          TermsConditionInfo(),
          AppVersion(),
          LogOutButton(),
        ],
      ),
    );
  }
}

class LogOutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: RaisedButton(
            color: Color(0xFFDC1714),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Text(
              'LOGOUT',
              style: TextStyle(
                color: Color(0xFFF2F2F2),
                fontSize: 15,
                fontWeight: FontWeight.w700,
              ),
            ),
            onPressed: () async {
              while (Navigator.canPop(context)) {
                Navigator.of(context).pop();
              }

              LoginProvider.logout();
              _googleSignIn.signOut();
              Navigator.of(context).pushReplacementNamed('/');

              while (Navigator.canPop(context)) {
                Navigator.of(context).pop();
              }
            }));
  }
}

class AccountEdit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      decoration: new BoxDecoration(
        boxShadow: [
          new BoxShadow(
            offset: new Offset(0, 1),
            blurRadius: 4,
            color: const Color.fromRGBO(29, 29, 29, 0.15),
          ),
        ],
      ),
      child: Card(
        shape: const RoundedRectangleBorder(
          borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
        ),
        child: Container(
          margin: EdgeInsets.all(13.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 8,
                child: AccountInfo(),
              ),
              Expanded(
                flex: 2,
                child: AccountEditButton(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AccountInfo extends StatefulWidget {
  @override
  _AccountInfoState createState() => _AccountInfoState();
}

class _AccountInfoState extends State<AccountInfo> {
  String name;
  String email;
  String phone;
  int login_type;

  bool verified = false;
  _getData() {
    API.APICall().then((response) {
      setState(() {
        Map map = json.decode(response.body);
        map = map['customer_data'];
        name = map['name'];
        email = map['email'];
        phone = map['phone'] == null ? "" : map['phone'];
        login_type = map['login_type'];
        verified = (email != null) && (email != "") && (phone != "");
      });
    });
  }

  initState() {
    _getData();
    super.initState();
  }

  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text(
            name,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 15,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            name,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 10,
              fontStyle: FontStyle.italic,
            ),
          ),
          Text(
            phone,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 10,
              fontStyle: FontStyle.italic,
            ),
          ),
          Row(children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 5.0),
              child: (verified)
                  ? Icon(
                      Icons.done,
                      color: Color(0xff007e24),
                      size: 10.0,
                    )
                  : Icon(
                      Icons.cancel,
                      color: Color(0xffdc1714),
                      size: 10.0,
                    ),
            ),
            Text(
              '${verified ? '' : 'un'}verified customer',
              style: const TextStyle(
                fontFamily: 'Roboto',
                fontSize: 8,
                fontStyle: FontStyle.italic,
              ),
            ),
          ]),
        ]));
  }
}

class InfoItemList extends StatelessWidget {
  final Text text;
  final String route;

  InfoItemList({@required this.text, @required this.route});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        onPressed: () {
          Navigator.of(context).pushNamed(this.route);
        },
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 14.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: this.text,
                    margin: EdgeInsets.symmetric(vertical: 9.0),
                  ),
                  Container(
                    height: 35.0,
                    child: const Icon(
                      Icons.arrow_forward,
                      color: Color(0xFF1D1D27),
                    ),
                  ),
                ],
              ),
            ),
            Divider(height: 8),
          ],
        ),
      ),
    );
  }
}

class PrivacyPolicyInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoItemList(
        text: Text(
          'Privacy Policy',
          style: const TextStyle(
            fontFamily: 'Roboto',
            color: Color(0xFF1D1D1D),
            fontSize: 15,
            fontWeight: FontWeight.w700,
          ),
        ),
        route: '/account/privacy_policy');
  }
}

class TermsConditionInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoItemList(
        text: Text(
          'Terms & Conditions',
          style: const TextStyle(
            fontFamily: 'Roboto',
            color: Color(0xFF1D1D1D),
            fontSize: 15,
            fontWeight: FontWeight.w700,
          ),
        ),
        route: '/account/terms_and_condition');
  }
}

class AccountEditButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: RaisedButton(
        onPressed: () => Navigator.pushNamed(context, '/editAccount'),
        color: const Color(0xff007e24),
        child: Text(
          'Edit',
          style: const TextStyle(
            fontFamily: 'Roboto',
            fontSize: 15,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

class AppVersion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FutureBuilder(
            future: PackageInfo.fromPlatform(),
            builder: (context, snapshot) {
              String version;
              switch (snapshot.connectionState) {
                case ConnectionState.done:
                  version = snapshot.data.version;
                  break;
                default:
                  version = '-';
              }
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 14.0, vertical: 7.0),
                child: Text(
                  'v$version',
                  style: const TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 14,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              );
            },
          ),
          Divider(height: 8),
        ],
      ),
    );
  }
}
