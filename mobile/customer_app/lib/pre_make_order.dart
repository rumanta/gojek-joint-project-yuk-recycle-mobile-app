import 'package:flutter/material.dart';
import 'package:customer_app/location_map.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'location_map.dart';

import 'account.dart';
import 'package:customer_app/login_provider.dart';
import 'dart:io';
import 'package:http/http.dart' show Client;
import 'const.dart';
import 'package:customer_app/models/api_provider.dart';
import 'make_order.dart';
import 'ui/nav_menu.dart';

GlobalKey globalKey = GlobalKey();
GlobalKey globalKey3 = GlobalKey();
class RecyclePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new HowToOrderPage()
    );
  }
}

class HowToOrderPage extends StatefulWidget {
  final myController = TextEditingController();
  _HowToOrderPageState createState() => new _HowToOrderPageState();
}

class AppDimensionControl {
  double height;
  TextStyle textStyleBold;
  TextStyle textStyleRegular;
  TextStyle textStyleMedium;
  TextStyle textStyleButtonMedium;
  TextStyle textStyleButtonBold;
  
  AppDimensionControl({
    this.textStyleBold, this.height,
    this.textStyleRegular, this.textStyleMedium,
    this.textStyleButtonBold, this.textStyleButtonMedium
  });

  factory AppDimensionControl.dimensionChange(double ratio) {
    if (ratio < 4.0) {
      return AppDimensionControl(
        height: 210,
        textStyleMedium: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w500,
          fontSize: 16,
          color: Color(0xFF1D1D1D),
        ),
        textStyleRegular: TextStyle(
          fontFamily: "Neo_Sans",
          fontSize: 13,
          color: Color(0xFF1D1D1D),
        ),
        textStyleBold: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w700,
          fontSize: 20,
          color: Color(0xFF1D1D1D),
        ),
        textStyleButtonMedium: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w500,
          fontSize: 10,
          color: Colors.white,
        ),
        textStyleButtonBold: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w700,
          fontSize: 13,
          color: Colors.white,
        ),
      );
    } else {
      return AppDimensionControl(
        height: 130,
        textStyleMedium: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w500,
          fontSize: 18,
          color: Color(0xFF1D1D1D),
        ),
        textStyleRegular: TextStyle(
          fontFamily: "Neo_Sans",
          fontSize: 15,
          color: Color(0xFF1D1D1D),
        ),
        textStyleBold: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w700,
          fontSize: 22,
          color: Color(0xFF1D1D1D),
        ),
        textStyleButtonMedium: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w500,
          fontSize: 10,
          color: Colors.white,
        ),
        textStyleButtonBold: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w700,
          fontSize: 15,
          color: Colors.white,
        ),
      );
    }
  }
}

class _HowToOrderPageState extends State<HowToOrderPage> { 
  Map<String, double> loc1;
  Map<String, double> loc2;
  Map<String, double> loc3;
  AppDimensionControl dimension;

  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
    super.initState();
  }
  _afterLayout(_) {
    loc1 = _getPositions();
    loc3 = _getPositions3();
    setState(() {});
  }

  Map<String, double> _getPositions() {
    final RenderBox renderBox = globalKey.currentContext.findRenderObject();
    final position = renderBox.localToGlobal(Offset.zero);
    Map<String, double> coor = {
      'x': position.dx,
      'y': position.dy,
    };
    return coor;
  }
  Map<String, double> _getPositions3() {
    final RenderBox renderBox = globalKey3.currentContext.findRenderObject();
    final position = renderBox.localToGlobal(Offset.zero);
    Map<String, double> coor = {
      'x': position.dx,
      'y': position.dy,
    };
    return coor;
  }

  Widget _getSampahNameIcon(double ratio) {
    List<Widget> sampahIcon;
    if (ratio >= 4.0) {
      return Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              sampahButtonWidget('Sampah Plastik'),
              sampahButtonWidget('Sampah Kertas'),
              sampahButtonWidget('Sampah Kardus'),
            ],
          ),
          Row(
            children: <Widget>[
              sampahButtonWidget('Sampah Plastik'),
              sampahButtonWidget('Sampah Kertas'),
              sampahButtonWidget('Sampah Kardus'),
            ],
          )
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              sampahButtonWidget('Sampah Plastik'),
              sampahButtonWidget('Sampah Kertas'),
            ],
          ),
          Row(
            children: <Widget>[
              sampahButtonWidget('Sampah Kardus'),
              sampahButtonWidget('Botol Plastik'),
            ],
          ),
          Row(
            children: <Widget>[
              sampahButtonWidget('Botol Kaca'),
            ],
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {  
    double ratio = MediaQuery.of(context).devicePixelRatio;
    dimension = AppDimensionControl.dimensionChange(ratio);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: Key('pageMakeOrder'),
      appBar: AppBar(
        title: Text(
          'Make Order',
          style: dimension.textStyleBold,
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            child: CustomPaint(
              painter: ShapesPainter(
                pos1: loc1,
                pos3: loc3,
              ),
              child: Container(height: 700),
              ),
            ),
          Container(
            padding: EdgeInsets.only(left: 15.0, top: 15.0),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 20.0),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'How to order?',
                    style: dimension.textStyleBold,
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      height: dimension.height,
                      child: Row(
                        children: <Widget>[
                          Container(
                            key: globalKey,
                            alignment: Alignment.topLeft,
                            child: circlePointWidget(1),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width-39,
                            padding: EdgeInsets.only(top: 0, left: 15.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      'Pilih jenis sampah yang ingin kamu recycle.',
                                      style: dimension.textStyleMedium,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(children: <Widget>[
                                  Expanded(
                                    flex: 0,
                                    child: Text(
                                      'apa saja yang dapat kamu recycle:',
                                      style: dimension.textStyleRegular,
                                      ),
                                    )
                                  ],
                                ),
                                _getSampahNameIcon(ratio),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 30.0),
                      child: Row(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.topLeft,
                            child: circlePointWidget(2),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(top: 0.0, left: 15.0),
                              child: 
                                Text(
                                  'Masukkan alamat pengambilan sampah',
                                  style: dimension.textStyleMedium,
                                ),
                            )
                          ),
                        ],
                      )
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 30.0),
                      child: Row(
                        children: <Widget>[
                          Container(
                            key: globalKey3,
                            alignment: Alignment.topLeft,
                            child: circlePointWidget(3),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 0.0, left: 15.0),
                            child: 
                              Text(
                                'Checkout dan lakukan order',
                                style: dimension.textStyleMedium,
                              ),
                          ),
                        ],
                      )
                    )
                  ],
                ),

                Container(
                  padding: EdgeInsets.only(right: 15.0),
                  child: RaisedButton(
                    color: Color(0xff293542),
                    onPressed: () {
                      NavMenu.of(context).navigateToRecyclePage();
                    },
                    child: Text('Order Now',
                      style: dimension.textStyleButtonBold,
                    ),
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ClipOval circlePointWidget(content) {
    return ClipOval(
      child: Container(
        color: Color(0xFF0645AD),
        height: 24.0,
        width: 24.0,
        child: Center(
          child: Text('$content',
            style: TextStyle(
              color: Colors.white,
              fontSize: 15.0,
            ),
          ),
        )
      ),
    );
  }
  
  Container sampahButtonWidget(content) {
    return Container(
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.circular(10.0)
      ),
      margin: EdgeInsets.only(right: 5.0),
      child: ButtonTheme(
        buttonColor: Color(0xFF0645AD),
        minWidth: 90.0,
        height: 30.0,
        child: RaisedButton(
          onPressed: () {},
          child: Text('$content',
            style: dimension.textStyleButtonMedium
          ),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0)
          ),
        )
      ),
    );
  }
}

class ShapesPainter extends CustomPainter {
  Map<String, double> pos1;
  Map<String, double> pos3;

  ShapesPainter({this.pos1, this.pos3});
  @override
  void paint(Canvas canvas, Size size) {
    double distance1to3 = pos3['y'] - pos1['y'];
    final paint = Paint();
    // set the paint color to be white
    paint.color = Color(0xFF0645AD);
    // Create a rectangle with size and width same as the canvas
    var rect = Rect.fromLTWH(pos1['x']+9.0, pos1['y']-75.0, 5.0, distance1to3);
    // draw the rectangle using the paint
    canvas.drawRect(rect, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}


