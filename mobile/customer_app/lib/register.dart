import 'package:flutter/material.dart';
import 'package:validate/validate.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';
import 'const.dart';
import 'login_page.dart';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' show Client;

class Register extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterState();
}

Future<bool> sendToBackEndAPIRegister(Map body, BuildContext context) async {
  Client client = new Client();
  String BASE_URL = 'http://152.118.201.222:21414/api/v1/customer/new';
  // BASE_URL = BASE_URL + '/?code=' + body['code'];
  await client.post(BASE_URL,body: json.encode(body)).then((response){
    Map result = json.decode(response.body);
    if (response.statusCode == HttpStatus.ok) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('Success Message'),
            content: new Text('Akun berhasil didaftarkan!'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('Error'),
            content: new Text(result['error']['message']),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  });
}


class _LoginData {
  String email = '';
  String password = '';
  String phoneNumber ='';
  String name = '';
}


class _RegisterState extends State<Register> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  MediaQueryData queryData;

  TextEditingController _emailController;
  TextEditingController _phoneNumberController;
  TextEditingController _passwordController;
  TextEditingController _retypePasswordController;
  TextEditingController _nameController;
  
  _LoginData _data = new _LoginData();

  bool _autoValidate =false;
  String _email;
  String _phoneNumber;
  String _password;
  String _rePassword;
  String _name;


  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if(value.isEmpty)
      return 'Email is empty';
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    return Scaffold(
      backgroundColor: Color(0xFF293542),
      body: new ListView(
        key: Key("RegisterPage"),
        padding: EdgeInsets.only(top: 100.0),
        children: <Widget>[
          new Container(
            child: new Image.asset('assets/logo.png', fit: BoxFit.fitWidth,),
            padding: EdgeInsets.fromLTRB(100, 0, 100, 0),
          ),
          new Container(
            padding: EdgeInsets.all(40),
            child: new Form(
              key: _formKey,
              autovalidate: _autoValidate,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    controller: _nameController,
                    onSaved: (value){
                      this._name = value;
                      this._data.name = value;
                    },
                    keyboardType: TextInputType.text,
                    validator: (value){
                      if(value.isEmpty){
                        return 'Name cannot be empty';
                      }
                      else{
                        _name = value;
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Name',
                      hintText: 'Insert name here',
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 15,
                      ),
                      contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 10)
                    ),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),

                  TextFormField(
                    controller: _emailController,
                    onSaved: (value){
                      this._email = value;
                      this._data.email = value;
                    },
                    keyboardType: TextInputType.emailAddress,
                    validator: validateEmail,
                    decoration: InputDecoration(
                      labelText: 'Email Address',
                      hintText: 'ex@mple.com',
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 15,
                      ),
                      contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 10)
                    ),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),


                  TextFormField(
                    onFieldSubmitted: (value){
                      this._phoneNumber = value;
                    },
                    controller: _phoneNumberController,
                    keyboardType: TextInputType.phone,
                    onSaved: (value){
                      this._phoneNumber = value;
                      this._data.phoneNumber = value;
                    },
                    validator: (value) {
                      if(value.isEmpty) {
                        return 'Phone number cannot be empty';
                      }
                      if(value.substring(0,3) != '+62'){
                        return 'Phone number harus dimulai dengan +62';
                      }
                      if(value.length < 5) {
                        return 'Phone number must consist more than 5 numbers';
                      }
                      _phoneNumber = value;
                    },
                    decoration: const InputDecoration(
                      labelText: 'Phone Number',
                      hintText: 'Dimulai dengan +62',
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 15,
                      ),
                      contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 10)
                    ),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),


                  TextFormField(
                    onSaved: (value){
                      _password = value;
                      this._data.password = value;
                    },
                    obscureText: true,
                    controller: _passwordController,
                    validator: (value) {
                      if(value.isEmpty) {
                        return 'Password cannot be empty';
                      }
                    },
                    decoration: const InputDecoration(
                      labelText: 'Password',
                      hintText: 'Masukan Password',
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 15,
                      ),
                      contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 10)
                    ),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),

                  TextFormField(
                    onSaved: (value){
                      this._rePassword = value;
                    },
                    obscureText: true,
                    controller: _retypePasswordController,
                    validator: (value) {
                      if(value.isEmpty) {
                        return 'field cannot be empty';
                      }

                      // if(value.toString() != _password){
                      //   return 'password not match';
                      // }
                    },
                    decoration: const InputDecoration(
                      labelText: 'Konfirmasi Password',
                      hintText: 'Masukkan kembali passwordmu',
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 15,
                      ),
                      contentPadding: const EdgeInsets.fromLTRB(0, 20, 0, 10)
                    ),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),

                  const SizedBox(height: 16.0),

                  ButtonTheme(
                    buttonColor: Color(0xFFF2F2F2),
                    minWidth: 285,
                    height: 40,
                    child: RaisedButton(
                      key: Key('SignUpBackEnd'),
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                      onPressed: () {
                        if (this._formKey.currentState.validate()) {
                          _formKey.currentState.save(); 
                          Map body = {
                            'email': _data.email,
                            'password': _data.password,
                            'name': _data.name,
                            'phone': _data.phoneNumber
                          };
                          sendToBackEndAPIRegister(body, context);
                        }
                      },
                      child: Text('Sign Up', style: TextStyle(color: Color(0xFF293542)),),
                    ),
                  ),

                  const SizedBox(height: 5),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
  


}