import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'const.dart';
import 'login_provider.dart';
import 'home_detail.dart';

class Data {
  String title;
  String body;

  Data(String title, String body) {
    this.title = title;
    this.body = body;
  }

  Data.fromJson(Map json)
      : title = json['title'],
        body = json['body'];

  Map toJson() {
    return {'title': title, 'body': body};
  }
}

String HOME_URL = '$BASE_URL/home';
var listImage = [
  'assets/recycle_logo.png',
  'assets/trash_pict.png',
  'assets/go_green.png',
  'assets/help.png',
];

class Home extends StatefulWidget {
  VoidCallback gotoHelp;
  Home({@required this.gotoHelp, Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _HomePageHelperState();
}

class _HomePageHelperState extends State<Home> {
  Future<List<Data>> homePageDatafuture;
  List<Data> homepageData;
  bool alreadyFetch = false;

  @override
  void initState() {
    homePageDatafuture = _requestHomepageData();
    super.initState();
  }

  List<Data> listData;

  Future<List<Data>> _requestHomepageData() async {
    var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    final response = await http.get(HOME_URL, headers: header);

    if (response.statusCode == HttpStatus.ok) {
      var result = jsonDecode(response.body);
      Iterable list = result['data'];
      listData = list.map((model) => Data.fromJson(model)).toList();

      return listData;
    } else {
      print('Error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Data>>(
      future: homePageDatafuture,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (!alreadyFetch) {
            alreadyFetch = true;
            homepageData = snapshot.data;
          }
          return buildAfterFetch(context);
        }
        return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  Widget buildAfterFetch(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Image.asset('assets/home_black.png',
              scale: 3.0, width: 85.0, height: 85.0),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0.0,
        ),
        body: ListView.builder(
          itemCount: homepageData.length + 1,
          itemBuilder: (context, index) {
            if (index == homepageData.length) {
              return Stack(
                children: <Widget>[
                  Image.asset(listImage[index]),
                  InkWell(
                    onTap: widget.gotoHelp,
                    child: Container(
                      height: 168,
                      child: Center(
                        child: new Text(
                          'Help',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
            return Stack(
              children: <Widget>[
                Image.asset(listImage[index]),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomeDetail(
                            title: homepageData[index].title,
                            body: homepageData[index].body),
                      ),
                    );
                  },
                  child: Container(
                    height: 168,
                    child: Center(
                      child: new Text(
                        homepageData[index].title,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        )
        // body: ListView.builder(
        //   itemCount: listData.length,
        //   itemBuilder: (context,index){
        //     return ListTile(
        //       contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        //       title: Text(
        //         listData[index].title,
        //         style: TextStyle(
        //           color: Color(0xFF007E24),
        //           fontWeight: FontWeight.w700,
        //           fontSize: 20,
        //         ),
        //       ),

        //       subtitle: Text(
        //         listData[index].body,
        //         style: TextStyle(
        //           fontSize: 15,
        //           fontWeight: FontWeight.w500,
        //           color: Color(0xFF1D1D1D),
        //         ),
        //       ),
        //     );
        //   },
        // )
        );
  }
}
