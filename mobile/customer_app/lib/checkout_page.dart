import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'ui/nav_menu.dart';
import 'login_provider.dart';
import 'location_map.dart';
import 'home.dart';
import 'models/api_provider.dart';

ApiProvider apiProvider = ApiProvider();

class Checkout extends StatelessWidget {
  OrderDetail orderDetail;

  Checkout({this.orderDetail});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CheckoutPage(orderDetail: orderDetail, bodyContext: context,),
    );
  }
}


class CheckoutPage extends StatefulWidget{
  OrderDetail orderDetail;
  BuildContext bodyContext;
  CheckoutPage({Key key, this.orderDetail, this.bodyContext}) : super(key:key);
  CheckoutPageState createState() => CheckoutPageState();
}

class CheckoutPageState extends State<CheckoutPage> {
  int itemCount;
  void doCheckout() {
    var token = LoginProvider.getTokenCustomer();
    apiProvider.createOrderHttpRequest(orderDetail, token).then((status) =>
        _navigateToSearchingPage(status, context));
  }

  TextStyle textStyleMedium = TextStyle(
    fontFamily: "Neo_Sans",
    fontWeight: FontWeight.w500,
    fontSize: 18,
    color: Color(0xFF1D1D1D),
  );
  TextStyle textStyleRegular = TextStyle(
    fontFamily: "Neo_Sans",
    fontSize: 15,
    color: Color(0xFF1D1D1D),
  );
  TextStyle textStyleBold = TextStyle(
    fontFamily: "Neo_Sans",
    fontWeight: FontWeight.w700,
    fontSize: 20,
    color: Color(0xFF1D1D1D),
  );
  TextStyle textStyleButtonMedium = TextStyle(
    fontFamily: "Neo_Sans",
    fontWeight: FontWeight.w500,
    fontSize: 10,
    color: Colors.white,
  );
  TextStyle textStyleButtonBold = TextStyle(
    fontFamily: "Neo_Sans",
    fontWeight: FontWeight.w700,
    fontSize: 15,
    color: Colors.white,
  );

  Widget headline(String headline) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5.0),
      child: Text("$headline",
        style: textStyleBold),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF2F2F2),
      child: Container(
        margin: EdgeInsets.only(top: 40.0, bottom: 30.0, left: 20.0, right: 20.0),
        child: Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            border: new Border.all(
              color: Colors.grey,),
          ),
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: Color(0xfff2f2f2),
              title: Text('Checkout Page',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w700,
                  color: Color(0xff1d1d1d)
                )),
              leading: IconButton(
                icon: const Icon(Icons.arrow_back, color: Color(0xff1d1d27),),
                  color: Colors.black,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
              )
            ),
            backgroundColor: Color(0xfff2f2f2),
            body: ListView(
              children:<Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(15.0),
                      alignment: Alignment.topLeft,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          headline('Alamat'),
                          Text(widget.orderDetail.address,
                            style: textStyleRegular),
                        ],
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(bottom: 15.0),
                      margin: EdgeInsets.only(left: 15.0, right: 15.0),
                      alignment: Alignment.topLeft,
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          headline('Order Item(s)'),
                          InkWell(
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return SimpleDialog(
                                    title: const Text('Select item to add'),
                                    children: orderDetail.orderItems.getZeroQuantityItems()
                                      .map((element) =>
                                        new SimpleDialogOption(
                                          onPressed: () {
                                            setState(() {
                                              element.quantity = 1; 
                                            });
                                            Navigator.pop(context);
                                          },
                                          child: Text(element.namaSampah,
                                            style: textStyleRegular ,),
                                        )).toList()
                                  );
                                }
                              );
                            },
                            child: Text(
                              '+ Add Item',
                              style: TextStyle(
                                fontFamily: 'Neo_Sans',
                                color: Colors.blue,
                                fontSize: 18,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 15.0, right: 15.0),
                      alignment: Alignment.topLeft,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: 
                          widget.orderDetail.orderItems
                          .getNonZeroQuantityItems().map((element) => 
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(element.namaSampah.toString()
                                ,style: textStyleRegular,),
                                Container(
                                  width: MediaQuery.of(context).size.width/3,
                                  padding: EdgeInsets.only(bottom: 10.0),
                                  alignment: Alignment.centerRight,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                element.quantity = element.quantity - 1;
                                              });
                                            },
                                            child: new Image.asset(
                                              'assets/round_minus.png',
                                              width: 24,
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                                            child: Text(element.quantity.toString()
                                              ,style: textStyleRegular,
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                element.quantity = element.quantity + 1;
                                              });
                                            },
                                            child: new Image.asset(
                                              'assets/round_plus.png',
                                              width: 24,
                                            ),
                                          ),
                                        ] 
                                      ),
                                      Text(element.satuan.toString()
                                      ,style: textStyleRegular,),],
                                  ),
                                )
                              ],
                            )
                          ).toList()
                        ),
                      ),
                    Container(
                      margin: EdgeInsets.all(15.0),
                      alignment: Alignment.topLeft,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          headline('Notes'),
                          Text(widget.orderDetail.notes == '' ? '-' : widget.orderDetail.notes,
                            style: textStyleRegular),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 15.0, right: 15.0),
                      alignment: Alignment.bottomCenter,
                      child: RaisedButton(
                        child: Text('Confirm'),
                        onPressed: () {
                          var token = LoginProvider.getTokenCustomer();
                          apiProvider.createOrderHttpRequest(orderDetail, token).then((status) =>
                            _navigateToSearchingPage(status, context));
                        },
                      ),
                    )
                  ]
                ),
              ]
            ),
          ),
        ),
      ),
    );
  }
  
  @visibleForTesting
  void _navigateToSearchingPage(Map response, BuildContext context) {
    CircularProgressIndicator();
    if (response['status']) {      
      _showSuccessCreateOrder(context);
    } else {
      _showFailedFindMitra(context);
    }
  }

  void _showSuccessCreateOrder(BuildContext context) {
     showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
              "Your Order Has Been Processed",
              style: TextStyle(fontSize: 28.0),
            ),
          content: new Text(
            "Please wait until your order picked by our Mitra",
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Color(0xffDC1714),
                    )
                  ),
                  onPressed: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    NavMenu.of(context).triggerNavigateToIndex(0);
                  },
                ),
              ],
            )
          ],
        );
      }
    );
  }
  void _showFailedFindMitra(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
              "SORRY",
              style: TextStyle(fontSize: 28.0),
            ),
          content: new Text(
            "Our service is not available in your area",
            style: TextStyle(fontSize: 20.0),
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Color(0xffDC1714),
                    )
                  ),
                  onPressed: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    NavMenu.of(context).triggerNavigateToIndex(0);
                  },
                ),
              ],
            )
          ],
        );
      }
    );
  }
}

