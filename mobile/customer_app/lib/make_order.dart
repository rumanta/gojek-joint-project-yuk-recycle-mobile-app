import 'package:flutter/material.dart';
import 'package:customer_app/location_map.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'location_map.dart';

import 'account.dart';
import 'package:customer_app/login_provider.dart';
import 'dart:io';
import 'package:http/http.dart' show Client;
import 'const.dart';
import 'package:customer_app/models/api_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

OrderItems orderItem;
ApiProvider apiProvider = ApiProvider();

class Sampah {
  String id;
  String namaSampah;
  String satuan;
  String pic;
  String desc;
  int quantity;

  Sampah({this.namaSampah, this.id, this.quantity, this.satuan, this.pic, this.desc});
  factory Sampah.fromJson(Map<String, dynamic> json, String id) {
    return Sampah(
      namaSampah: json['title'],
      desc: json['description'],
      satuan: json['quantity_type'],
      pic: json['image'],
      quantity: 0,
      id: id,
    );
  }
}

class OrderItems {
  Map<String, Sampah> itemSampah = {};

  OrderItems({this.itemSampah});

  bool isNotEmpty() {
    if (itemSampah['Sampah Plastik'].quantity != 0) return true;
    if (itemSampah['Sampah Kertas'].quantity != 0) return true;
    if (itemSampah['Kardus/Karton'].quantity != 0) return true;
    if (itemSampah['Botol Plastik'].quantity != 0) return true;
    if (itemSampah['Botol Kaca'].quantity != 0) return true;
    return false;
  }

  factory OrderItems.fromJson(List<dynamic> json) {
    Map<String, Sampah> listSampah = Map<String, Sampah>();
    for (int i = 0; i < json.length; i++){
      String id = (i+1).toString();
      String namaSampah = json[i]['title'];
      listSampah[namaSampah] = Sampah.fromJson(json[i], id);
    }
    
    return OrderItems(
      itemSampah: listSampah
    );
  }

  List<Sampah> getNonZeroQuantityItems() {
    List<Sampah> listSampah = [];
    itemSampah.forEach((k, v) {
      if (v.quantity != 0) {
        listSampah.add(v);
      }
    });
    return listSampah;
  }

  List<Sampah> getZeroQuantityItems() {
    List<Sampah> listSampah = [];
    itemSampah.forEach((k, v) {
      if (v.quantity == 0) {
        listSampah.add(v);
      }
    });
    return listSampah;
  }
}

class CustMakeOrder extends StatelessWidget {
  OrderItems passedOrderItem;
  CustMakeOrder({this.passedOrderItem});

  @override
  Widget build(BuildContext context) {
    if (passedOrderItem != null) {
      orderItem = passedOrderItem;
      return MakeOrder();
    } else { 
      return FutureBuilder(
        future: initOrderItem(),
        builder: (context,snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              return new Container();
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator()
              );
            case ConnectionState.done:
              return MakeOrder();
            default:
              return Container();
          }
        }
      );
    }
  }

  Future<OrderItems> initOrderItem() async {
    var token = LoginProvider.getTokenCustomer();
    return await apiProvider.garbageTypeHttpRequest(token);
  }
}

class MakeOrder extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new MakeOrderPage()
    );
  }
}

class AppDimensionControl {
  double pic_size;
  TextStyle textStyleBold;
  TextStyle textStyleRegular;
  double cardwidth;

  AppDimensionControl({
    this.pic_size, this.textStyleBold, this.textStyleRegular, this.cardwidth
  });

  factory AppDimensionControl.dimensionChange(double ratio, BuildContext context) {
    if (ratio < 4.0) {
      return AppDimensionControl(
        cardwidth: MediaQuery.of(context).size.width-90,
        pic_size: 48.0,
        textStyleRegular: TextStyle(
          fontFamily: "Neo_Sans",
          fontSize: 10,
          color: Color(0xFF1D1D1D),
        ),
        textStyleBold: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w700,
          fontSize: 12,
          color: Color(0xFF1D1D1D),
        ),
      );
    } else {
      return AppDimensionControl(
        cardwidth: MediaQuery.of(context).size.width-110,
        pic_size: 48.0,
        textStyleRegular: TextStyle(
          fontFamily: "Neo_Sans",
          fontSize: 15,
          color: Color(0xFF1D1D1D),
        ),
        textStyleBold: TextStyle(
          fontFamily: "Neo_Sans",
          fontWeight: FontWeight.w700,
          fontSize: 16,
          color: Color(0xFF1D1D1D),
        ),
      );
    }
  }
}

class MakeOrderPage extends StatelessWidget {
  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print("UKURAN WIDTH");
    print(MediaQuery.of(context).size.width);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: Key('pageMakeOrder'),
      appBar: AppBar(
        title: Text(
          'Make Order',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Color(0xff1d1d1d),),
            color: Colors.black,
            onPressed: () {
              Navigator.of(context).pop();
            },
        )
      ),
      body: ListView(
        children: [
          Column(
            children: orderItem.itemSampah.values.toList()
                .map((element) => new JenisSampah(
              id: element.id,
              jenisSampah: element.namaSampah,
              satuan: element.satuan,
              pic: element.pic,
              quantity: element.quantity,
              desc: element.desc,
            )).toList(),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 20),
            child: RaisedButton(
              key: Key('orderButton'),
              color: Color(0xff007E24),
              onPressed: () {
                checkMakeOrderValidity(orderItem, context);
              },
              child: Text('Next', style: TextStyle(color: Colors.white)),
            ),
          ),
        ],
        padding: const EdgeInsets.all(5.0),
      ),
    );
  }

  void checkMakeOrderValidity(orderItem, context) {
    if (checkOrderEmpty(orderItem, context)) {
      showItemEmptyWarning(context);
    }
    else {
      bool check = checkPhoneNumberNotExist(context);
      if(check == true) {
        showPhoneNotExistWarning(context);
      }
      else {
        Navigator.push(context, new MaterialPageRoute(
          builder: (context) =>
          new LocationMap(
            passedOrderItems: orderItem,
          )
        ));
      }
    }
  }

  bool checkPhoneNumberNotExist(BuildContext context) {
    Client client = new Client();
    var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    client.get(getCustomerDataApi, headers: header).then((response) {
      Map<String, dynamic> responseBody = json.decode(response.body); // NO RESPONSE
      if (response.statusCode == HttpStatus.ok) {
        var customerData = responseBody['customer_data'];
        return customerData['phone'] == null ? true : false;
      } else {
        return true;
      }
    });
  }

  bool checkOrderEmpty(orderItem, context) {
    return orderItem.isNotEmpty() ? false : true;
  }

  showPhoneNotExistWarning(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text('Phone Number Doesn\'t Exist!'),
          content: new Text('Please fill your phone number in Account page before Make an Order'),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Go to Account page"),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/account');
              },
            ),
          ],
        );
      },
    );
  }

  void showItemEmptyWarning(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            "Item Cannot be Empty",
            style: TextStyle(fontSize: 26.0),
          ),
          content: new Text(
            "Please change the quantity of the garbage",
            style: TextStyle(fontSize: 18.0),
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                FlatButton(
                  child: Text(
                      'Okay',
                      style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xffDC1714),
                      )
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            )
          ],
        );
      }
    );
  }
}

class JenisSampah extends StatefulWidget {
  String jenisSampah;
  String id;
  String pic;
  String satuan;
  String desc;
  int quantity;
  JenisSampah({this.jenisSampah, this.pic, Key key, this.id, this.satuan, this.quantity, this.desc}) : super(key: key);
  _JenisSampahState createState() => new _JenisSampahState();
}

class _JenisSampahState extends State<JenisSampah> {
  int itemCount;
  AppDimensionControl dimension;
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).devicePixelRatio;
    dimension = AppDimensionControl.dimensionChange(ratio, context);
    print(dimension.pic_size);
    if (widget.satuan == 'Botol') {
      widget.satuan = 'Btl';
    }
    itemCount =  widget.quantity;
    double heightPerCard = (MediaQuery.of(context).size.height - 220)/5;
    return Card(
      child: Container(
        padding: EdgeInsets.only(left: 15, top: (heightPerCard-64)/2, bottom: (heightPerCard-64)/2),
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.all(
              const Radius.circular(10.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SvgPicture.memory(
              base64Decode(widget.pic),
              width: dimension.pic_size,
              height: dimension.pic_size,
            ),
            Container(
              width: dimension.cardwidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(widget.jenisSampah,
                              style: dimension.textStyleBold,
                            ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 5),
                          child: Text(widget.desc,
                              style: TextStyle(fontSize: 12.0)),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      children: [
                        Row(
                          children: <Widget>[
                            new IconButton(
                                key: Key('quantMinus' + widget.id),
                                icon: new Icon(Icons.remove, size: 10),
                                onPressed: ()
                                    {
                                      setState(() {
                                        itemCount > 0 ? itemCount-- : itemCount;
                                        widget.quantity = itemCount;
                                        orderItem.itemSampah[widget.jenisSampah].quantity = itemCount;
                                      });
                                    }),
                            new Text(
                              itemCount.toString() + " " + widget.satuan,
                              style: dimension.textStyleRegular,
                              key: Key('quantItem' + widget.id),
                            ),
                            new IconButton(
                                key: Key('quantPlus' + widget.id),
                                icon: new Icon(Icons.add, size: 10),
                                onPressed: ()
                                {
                                  setState(() {
                                    itemCount++;
                                    widget.quantity = itemCount;
                                    orderItem.itemSampah[widget.jenisSampah].quantity = itemCount;
                                  });
                                }),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


