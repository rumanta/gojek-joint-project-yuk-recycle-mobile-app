import 'package:flutter/material.dart';
import 'login_page.dart';
import 'ui/nav_menu.dart';
import 'register.dart';
import 'make_order.dart';
import 'login_provider.dart';
import 'edit_account.dart';
import 'checkout_page.dart';
import 'account.dart';
import 'terms_condition.dart';
import 'privacy_policy.dart';
import 'home_detail.dart';
import 'package:flutter/services.dart';

void main() async {
  await LoginProvider.init();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
    runApp(CustomerApp());
  });
}

class CustomerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yuk Recycle - Customer',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          textTheme: TextTheme(
              body1: TextStyle(fontSize: 30.0, fontFamily: 'Roboto_Medium'))),
      routes: <String, WidgetBuilder>{
        '/': (_) => LoginPageCustomer(),
        '/home': (_) => NavMenu(),
        '/register': (_) => Register(),
        '/account': (_) => AccountPage(),
        '/editAccount': (_) => AccountEditPage(),
        '/account/terms_and_condition': (_) => TermsCondition(),
        '/account/privacy_policy': (_) => PrivacyPolicy(),
        '/home/detail': (_) => HomeDetail(),
      },
    );
  }
}
