import 'package:flutter/material.dart';
import 'package:customer_app/login_provider.dart';
import 'package:customer_app/ui/nav_menu.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';

class EmailFieldValidator {
  static String validate(String value) {
    final RegExp emailRegExp = RegExp(r'^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

    if (value.isEmpty) {
      return 'Email tidak boleh kosong';
    } else if (!emailRegExp.hasMatch(value)) {
      return 'Silahkan isi email dengan format yang benar';
    }

    return null;
  }
}

class PasswordFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Password tidak boleh kosong' : null;
  }
}

final colorBlue = Color(0xFF293542);

class LoginPageCustomer extends StatefulWidget {
  @override
  State createState() => new _LoginPageState();
}

GoogleSignIn _googleSignIn = new GoogleSignIn(
  scopes: [
    'https://www.googleapis.com/auth/userinfo.profile',
  ],
  serverClientId: "97555802072-tq4dfs9kft22l1du58oin0sh7umk2meg.apps.googleusercontent.com"
);

final FirebaseAuth _auth = FirebaseAuth.instance;

Future<FirebaseUser> _handleSignIn(BuildContext context) async {
    try{
      _googleSignIn.signOut();
    final GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn().catchError((onError){
      print(onError.toString());
      return null;
    });
    final authorizationCode = googleSignInAccount.serverAuthCode;
    final googleKey = await googleSignInAccount.authentication;
    LoginProvider.sendToBackEndAPIGoogle(googleSignInAccount, context).then((loggedIn) {
      if (loggedIn == true) {
        Navigator.of(context).pushReplacementNamed('/home');
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text('Error Message'),
              content: new Text('Login gagal, Silahkan ulangi'),
              actions: <Widget>[
                new FlatButton(
                  child: new Text("Close"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    });
    }catch(e){
      print(e.toString());
    };
  
}


class _LoginPageState extends State<LoginPageCustomer> {

  final _formKey = GlobalKey<FormState>();
  static final _emailValue = new TextEditingController();
  static final _passwordValue = new TextEditingController();
  Future<bool> _loginState;

  static var _loginBtnState = 0;

  Future<bool> getLoginState() async {
    bool loginState = LoginProvider.checkIsLoggedIn();

    return loginState;
  }

  @override
  void initState() {
    _loginState = getLoginState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    void _resetLoginForm() {
      _emailValue.clear();
      _passwordValue.clear();

      setState(() {
        _loginBtnState = 0;
      });
    }

    void _showErrorDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('Login Failed'),
            content: new Text('Username or Password does not match!'),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                }
              ),
            ],
          );
        },
      );
    }

    void _handlePressLoginBtn() {
      setState(() {
        _loginBtnState = 1;
      });
      LoginProvider.login(_emailValue.text, _passwordValue.text).then((loggedIn) {
        if (loggedIn == true) {
          _resetLoginForm();
          Navigator.of(context).pushReplacementNamed('/home');
        } else {
          setState(() {
            _loginBtnState = 0;
          });
          _showErrorDialog();
        }
      });
    }

    Widget _buildLogo = new Container(
      child: new Image.asset(
        'assets/yuk_recycle_white.png', scale: 3.0, width: 200.0, height: 100.0
      ),
    );

    var _buildFormEmail = Row (
      children: [
        Container(
          padding: const EdgeInsets.only(right: 10),
          child: new Image.asset(
            'assets/email_icon.png', scale: 3.0, width: 25.0, height: 25.0
          ),
        ),
        Expanded(
          child: new TextFormField(
            decoration: new InputDecoration(
              hintText: 'Email',
            ),
            controller: _emailValue,
            validator: EmailFieldValidator.validate,
            keyboardType: TextInputType.emailAddress,
          ),
        )
      ],
    );

    var _buildFormPassword = Row (
      children: [
        Container(
          padding: const EdgeInsets.only(right: 10),
          child: new Image.asset(
            'assets/lock_icon.png', scale: 3.0, width: 25.0, height: 25.0
          ),
        ),
        Expanded(
          child: new TextFormField(
            decoration: new InputDecoration(
              hintText: 'Password',
            ),
            controller: _passwordValue,
            obscureText: true,
            keyboardType: TextInputType.text,
            validator: PasswordFieldValidator.validate,
          ),
        ),
      ],
    );

    var _buildFormButton = (BuildContext context) {
      if (_loginBtnState == 0) {
        return Container(
          padding: EdgeInsets.fromLTRB(0, 50, 0, 10),
          child: ButtonTheme(
            buttonColor: Color(0xFFF2F2F2),
            minWidth: 400.0,
            height: 42,
            child: RaisedButton(
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _handlePressLoginBtn();
                }
              },
              child: Text('Login',
                style: TextStyle(color: colorBlue, fontSize: 20.0),
              ),
            ),
          ),
        );
      } else {
          return Container(
            padding: EdgeInsets.fromLTRB(0, 30, 0, 20),
            child: CircularProgressIndicator()
          );
        }
      };

    var _buildGoogleBtn = (context) => ButtonTheme(
      buttonColor: Color(0xFFDC1714),
      minWidth: 246,
      height: 40,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: RaisedButton(
        padding: EdgeInsets.all(0),
        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
        onPressed: () {
          _handleSignIn(context);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              width: 50,
              height: 40,
              decoration: BoxDecoration(
                color: Color(0xFFF2F2F2),
                borderRadius: new BorderRadius.only(
                  topLeft: new Radius.circular(10.0),
                  bottomLeft: new Radius.circular(10.0),
                  topRight: new Radius.circular(10.0)
                )
              ),
              child: ImageIcon(AssetImage('assets/google_logo.png'))
              ),

              Container(
                margin: EdgeInsets.fromLTRB(45, 0, 0, 0),
                child: Text('Login with Google', style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                ),)
              )
          ],
        )
      ),
    );

    var _buildRegisterTextLink = (context) => new Container(
      padding: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
      child: InkWell(
        child: new Text(
          'don\'t have an account? register here',
          style: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
            fontStyle: FontStyle.italic,
            decoration: TextDecoration.underline,
          )
        ),
        onTap: () => Navigator.of(context).pushNamed('/register')
      )
    );

    Form _buildLoginForm(BuildContext context) => new Form(
      key: _formKey,
      child: new Theme(
        data: ThemeData(
          brightness:Brightness.dark,
          inputDecorationTheme: new InputDecorationTheme(
            hintStyle: TextStyle(
              color: Colors.grey, fontSize: 14,
            )
          )
        ),
        child: Container(
          padding: const EdgeInsets.only(top: 40.0, left: 40, right: 40),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _buildFormEmail,
              _buildFormPassword,
              _buildFormButton(context),
              _buildGoogleBtn(context),
              _buildRegisterTextLink(context),
            ]
          ),
        ),
      )
    );

    return new FutureBuilder<bool> (
      future: _loginState,
      builder: (context,snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            return new Container();
          case ConnectionState.waiting:
            return CircularProgressIndicator();
          case ConnectionState.done:
            if (snapshot.hasData) {
              var loginState = snapshot.data;
              var loginPage = new Scaffold(
                backgroundColor: colorBlue,
                body: new Stack (
                  fit: StackFit.expand,
                  children: <Widget>[
                    new ListView(
                      children: [
                        SizedBox(height: 90.0),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            _buildLogo,
                            _buildLoginForm(context),
                          ],
                        )
                      ]
                    )
                  ]
                  )
              );
              if (loginState) {
                return NavMenu();
              } else {
                return loginPage;
              }
            } else {
              return Container(child: Text('Error..'));
            }
          break;
          default:
            return Container();
        }
      },
    );
  }
}

