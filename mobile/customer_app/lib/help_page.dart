import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'models/faq.dart';
import 'login_provider.dart';
import 'const.dart' show BASE_URL;

const faqUrl = '$BASE_URL/FAQ';

class HelpPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HelpPageState();
}

class HelpPageState extends State<HelpPage> {
  Faq faq;

  @override
  void initState() {
    setState(() {
      faq = Faq.fromJson({});
    });
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> faqWidgets = [];
    for (FaqDetail faqDetail in faq.faqDetails) {
      Widget answerWidget = faqDetail.isExpanded ? Text(
        faqDetail.body,
        style: TextStyle(
          fontFamily: 'Roboto',
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
          fontSize: 13,
          color: Color(0xFF1D1D27),
        ),
      ) : Container();
      String clickableTextText = faqDetail.isExpanded ? "Hide Answer" : "View Answer";
      Color clickableTextColor = faqDetail.isExpanded ? Color(0xFFDC1714) : Color(0xFF007E24);
      faqWidgets.add(Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: Text(
                      faqDetail.title,
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                        color: Color(0xFF1D1D27),
                      ),
                    ),
                  ),
                  answerWidget,
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: GestureDetector(
                      child: Text(
                        clickableTextText,
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          fontSize: 9,
                          color: clickableTextColor,
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          faqDetail.isExpanded = !faqDetail.isExpanded;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Divider(),
          ],
        ),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('Help / FAQ', style: TextStyle(
          fontFamily: 'Roboto',
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.w900,
          fontSize: 20,
          color: Color(0xFF1D1D27),
        ),),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 15, left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: faqWidgets,
            ),
          ),
        ],
      ),
    );
  }

  void _getData() {
    var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    http.get(faqUrl, headers: header).then((response) {
      setState(() {
        faq = Faq.fromJson(json.decode(response.body));     
      });
    });
  }
}
