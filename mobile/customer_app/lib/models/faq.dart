import '../util.dart';

class Faq {
  final List<FaqDetail> faqDetails;

  Faq({this.faqDetails});

  factory Faq.fromJson(Map<String, dynamic> data) {
    var faqList = getOrDefaultValue(data['data'], []) as List;
    return Faq(
      faqDetails: faqList.map((faq) => FaqDetail.fromJson(faq)).toList(),
    );
  }
}

class FaqDetail {
  String title;
  String body;
  bool isExpanded;

  FaqDetail({this.title, this.body, this.isExpanded});

  factory FaqDetail.fromJson(Map<String, dynamic> data) {
    return FaqDetail(
      title: getOrDefaultValue(data['title'], ''),
      body: getOrDefaultValue(data['body'], ''),
      isExpanded: false,
    );
  }
}
