class PushNotification {
  final DataMessage dataMessage;

  PushNotification({this.dataMessage});

  factory PushNotification.fromJson(Map<String, dynamic> data) {
    return PushNotification(
      dataMessage: DataMessage.fromJson(data['data']),
    );
  }
}

class DataMessage {
  String type;
  String response;

  DataMessage({this.type, this.response});

  factory DataMessage.fromJson(dynamic data) {
    return DataMessage(
      type: data['type'],
      response: data['response'],
    );
  }
}
