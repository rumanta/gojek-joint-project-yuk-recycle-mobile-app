import '../util.dart';

class OrderDetail {
  int id;
  String name;
  String address;
  String distance;
  int price;
  List<GarbageDetail> garbageDetail;
  String status;
  String time;
  String reason;
  String phone;
  double latitude;
  double longitude;
  String notes;

  OrderDetail({this.id, this.name, this.address, this.distance, this.price, 
  this.garbageDetail, this.status, this.time, this.reason, this.phone, this.latitude, this.longitude, this.notes});

  factory OrderDetail.fromJson(Map<String, dynamic> data) {
    var garbageDetails = getOrDefaultValue(data['garbage_detail'], []) as List;
    double distanceInDouble = getOrDefaultValue(data['distance'], 0.0);
    return OrderDetail(
      id: getOrDefaultValue(data['id'], 0),
      name: getOrDefaultValue(data['name'], ''),
      address: getOrDefaultValue(data['address'], ''),
      distance: distanceInDouble.toStringAsFixed(2),
      price: getOrDefaultValue(data['price'], 0),
      garbageDetail: garbageDetails.map((detail) => GarbageDetail.fromJson(detail)).toList(),
      status: getOrDefaultValue(data['status'], ''),
      time: getOrDefaultValue(data['time'], ''),
      reason: getOrDefaultValue(data['reason'], ''),
      phone: getOrDefaultValue(data['phone'], ''),
      latitude: getOrDefaultValue(data['latitude'], 0.0),
      longitude: getOrDefaultValue(data['longitude'], 0.0),
      notes: getOrDefaultValue(data['notes'], ""),
    );
  }
}

class GarbageDetail {
  String title;
  int quantity;
  String quantityType;

  GarbageDetail({this.title, this.quantity, this.quantityType});

  factory GarbageDetail.fromJson(Map<String, dynamic> data) {
    return GarbageDetail(
      title: data['title'],
      quantity: data['quantity'],
      quantityType: data['quantity_type'],
    );
  }
}
