import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:location/location.dart' as locationManager;
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:customer_app/make_order.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart' show Client;
import 'dart:convert';
import 'package:customer_app/login_provider.dart';
import 'package:customer_app/ui/nav_menu.dart';
import 'package:customer_app/const.dart';
import 'package:customer_app/location_map.dart';

class ApiProvider {
  Client client = Client();

  Future<OrderItems> garbageTypeHttpRequest(token) async {
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization': 'Bearer $token',
    };
    final response = await client.get(
      "http://152.118.201.222:21414/api/v1/customer/garbage-types",
      headers: headers,
    );
    if (response.statusCode == 200) {
      var result = jsonDecode(response.body);
      var garbageList = result['garbage_types'];
      orderItem = OrderItems.fromJson(garbageList);
      return orderItem;

    } else {
      print("Google Place API Fails, Cannot Return LatLng");
    }
  }

  Future<Map> createOrderHttpRequest(OrderDetail orderDetail, String token) async {
    var url = '$BASE_API/api/v1/order';
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization': 'Bearer $token',
    };
    print(orderDetail.toJson().toString());
    final response = await client.post(
        url,
        headers: headers,
        body: jsonEncode(orderDetail.toJson())
    );

    var result = jsonDecode(response.body);
    if (response.statusCode == 200) {
      print(result.toString());
      var output = {
        'status' : makeOrderResponseStatusSuccess,
        'errorMessage' : null
      };
      return output;
    } else {
      var output = {
        'status' : makeOrderResponseStatusSuccess,
        'errorMessage' : result['error']['message']
      };
      return output;
    }
  }
  

  Future<LatLng> placeDetailHttpRequest(placeId) async {
    final response = await client.get("https://maps.googleapis.com/maps/api/place/details/json?placeid="+placeId+"&fields=name,geometry,formatted_phone_number&key="+kGoogleApiKey);

    if (response.statusCode == 200) {
      var result = jsonDecode(response.body);
      var latitude = result["result"]["geometry"]["location"]["lat"];
      var longitude = result["result"]["geometry"]["location"]["lng"];

      return LatLng(latitude, longitude);
    } else {
      print("Google Place API Fails, Cannot Return LatLng");
    }
  }
  
  Future<CurrentAddress> httpRequest(GoogleMapController controller, String kGoogleApiKey, String lat, String lng) async {
    final response = await client.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+ lat +','+ lng +'&key='+kGoogleApiKey);

    if (response.statusCode == 200) {
      return CurrentAddress.fromJson(jsonDecode(response.body));
    } else {
      print("Failed : Status Code is not 200!");
    }
  }
}