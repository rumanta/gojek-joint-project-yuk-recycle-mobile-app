T getOrDefaultValue<T>(T value, T defaultValue) {
  return value == null ? defaultValue : value;
}
