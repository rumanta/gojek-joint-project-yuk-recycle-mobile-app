import 'package:flutter/material.dart';


class HomeDetail extends StatefulWidget {
  final String title;
  final String body;

  HomeDetail({Key key, @required this.title, this.body}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageHelperState(title: title, body: body);
}

class _HomePageHelperState extends State<HomeDetail> {
  final String title;
  final String body;

  _HomePageHelperState({Key key, @required this.title, this.body});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/home_black.png', scale: 3.0, width: 85.0, height: 85.0
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () {
              Navigator.of(context).pop();
            },
        ),
      ),
      body: ListTile(
        contentPadding: EdgeInsets.fromLTRB(15, 20, 15, 10),
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 12),
          child: new Text(
            title ?? '',
            style: TextStyle(
              color: Color(0xFF007E24),
              fontWeight: FontWeight.w700,
              fontSize: 20,
            ),
          ),
        ),
        subtitle: new Text(
          body ?? '',
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w500,
            color: Color(0xFF1D1D1D),
          ),
        ),
      ) 
    );
  }
}
