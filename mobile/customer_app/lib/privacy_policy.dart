import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'const.dart';
import 'login_provider.dart';


Client client = new Client();
String privacyPolicyApiUrl = '$BASE_URL/privacy-policy';
class API{
  static Future APICall(){
    var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    return client.get(privacyPolicyApiUrl, headers: header);
  }
}

class Data {
  String title;
  String body;

  Data(String title, String body) {
    this.title = title;
    this.body = body;
  }

  Data.fromJson(Map json)
      : title = json['title'],
        body = json['body'];

  Map toJson() {
    return {'title': title, 'body': body};
  }
}

class PrivacyPolicy extends StatefulWidget {
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  
  var listData = new List<Data>();

  _getData() {
    API.APICall().then((response) {
      setState(() {
        Map map = json.decode(response.body);
        Iterable list = map['data'];
        listData = list.map((model) => Data.fromJson(model)).toList();
      });
    }); 
  }

  initState() {
    super.initState();
    _getData();
  }

  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text('Privacy and Policies',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w900,
                fontSize: 20,
                color: Color(0xFF1D1D27),
              ),
            ),
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
                color: Colors.black,
              onPressed: () {
                  Navigator.of(context).pop();
                },
            ),
          ),
      body: ListView.builder(
        itemCount: listData.length,
        itemBuilder: (context,index){
          return ListTile(
            contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            title: Text(
              listData[index].title,
              style: TextStyle(
                color: Color(0xFF1D1D1D),
                fontWeight: FontWeight.w700,
                fontSize: 20,
              ),
            ),

            subtitle: Text(
              listData[index].body,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w500,
                color: Color(0xFF1D1D1D),
              ),
            ),
          );
        }, 
      )
    );
  }
}
