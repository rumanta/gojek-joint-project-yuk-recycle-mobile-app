import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:customer_app/const.dart' show BASE_URL, BASE_API;
import 'dart:async';
import 'package:http/http.dart' show Client;

var updateFCMTokenUrl = "$BASE_API/api/v1/notification/token";

class LoginProvider {

  LoginProvider();

  static SharedPreferences _prefs;
  static Client client;
  static FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  static const TOKEN_CUSTOMER = 'token_customer';
  static const IS_LOGGED_IN = 'isLoggedIn';

  static Future init() async {
    _prefs = await SharedPreferences.getInstance();
    client = new Client();
  }

  static sendFCMTokenToBackend(String tokenFcm) {
    var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    client.put(updateFCMTokenUrl, headers: header, body: json.encode({
      'token': tokenFcm,
    }));
  }

  static login(String email, String password) async {

    final loginUrl = '$BASE_URL/login';
    Map body = {
      "email": email,
      "password": password,
    };

    await client.post(loginUrl, body: json.encode(body)).then((response) {
      Map<String, dynamic> responseBody = json.decode(response.body);
      if (response.statusCode == HttpStatus.ok) {
        var token = responseBody['token']['value'];
        var status = responseBody['status'];
        _prefs.setString(TOKEN_CUSTOMER, token);
        _prefs.setBool(IS_LOGGED_IN, status == 'logged_in');
        _firebaseMessaging.getToken().then((token) {
          sendFCMTokenToBackend(token);
        });
      } else {
        print(responseBody['error']['message']);
      }
    });

    return _prefs.get(IS_LOGGED_IN);
  }

  static Future<bool> sendToBackEndAPIGoogle(GoogleSignInAccount googlekey, BuildContext context) async {
    Client client = new Client();
    final loginGoogleUrl = '$BASE_URL/oauth/google';
    Map body = {
      'code': googlekey.serverAuthCode,
    };
    final response = await client.post(loginGoogleUrl, body: json.encode(body));
    print(response);
    Map result = json.decode(response.body);
    if (response.statusCode == HttpStatus.ok) {
      var token = result['token']['value'];
      var status = result['status'];
      _prefs.setString(TOKEN_CUSTOMER, token);
      _prefs.setBool(IS_LOGGED_IN, status == 'logged_in');
      _firebaseMessaging.getToken().then((token) {
          sendFCMTokenToBackend(token);
        });
    } else {
      print(result['error']['message']);
    }

    return _prefs.get(IS_LOGGED_IN);
  }

  static checkIsLoggedIn() {

    if (_prefs.getBool(IS_LOGGED_IN) == null) {
      _prefs.setBool(IS_LOGGED_IN, false);
    }

    return _prefs.getBool(IS_LOGGED_IN);
  }

  static getTokenCustomer() {
    if (_prefs.getString(TOKEN_CUSTOMER) == null) {
      _prefs.setString(TOKEN_CUSTOMER, '');
    }

    return _prefs.getString(TOKEN_CUSTOMER);
  }

  static logout() {
    sendFCMTokenToBackend("a");
    _prefs.setBool(IS_LOGGED_IN, false);
    _prefs.remove(TOKEN_CUSTOMER);
  }

}




