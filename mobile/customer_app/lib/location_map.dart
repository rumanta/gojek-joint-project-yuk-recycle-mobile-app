
          // Padding(
          //   padding: EdgeInsets.only(top: 20),
          //   child: TextField(
          //     key: Key('note'),
          //     controller: myController,
          //     textCapitalization: TextCapitalization.sentences,
          //     decoration: InputDecoration(
          //       border: new OutlineInputBorder(
          //           borderRadius: new BorderRadius.all(
          //             const Radius.circular(10.0),
          //           ),
          //           borderSide: new BorderSide(color: Colors.black)),
          //       hintText: 'Masukkan Notes',
          //       labelText: 'Notes',
          //       labelStyle: TextStyle(fontSize: 14),
          //     ),
          //     maxLines: 2,
          //     style: TextStyle(fontSize: 16, color: Colors.black),
          //   ),
          // ),

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:location/location.dart' as locationManager;
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:customer_app/make_order.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart' show Client;
import 'dart:convert';
import 'const.dart';
import 'login_provider.dart';
import 'ui/nav_menu.dart';
import 'checkout_page.dart';
import 'models/api_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
const kGoogleApiKey = "AIzaSyDycUBHdtst7K2Z5uRKFRn3Gsmu2cVImWY";

OrderDetail orderDetail;
ApiProvider apiProvider = new ApiProvider();

bool makeOrderResponseStatusSuccess = true;

class CurrentAddress {
  final String address;

  CurrentAddress({this.address});

  factory CurrentAddress.fromJson(Map<String, dynamic> json) {
    return CurrentAddress(
      address: json['results'][0]['formatted_address'],
    );
  }
}

class OrderDetail {
  OrderItems orderItems;
  String notes = '';
  double latitude;
  double longitude;
  String address;

  OrderDetail({this.orderItems});
  
  Map<String, dynamic> toJson() {
    var orderDetails = [];
    orderItems.itemSampah.forEach((title, objSampah) {
      if (objSampah.quantity != 0) {
        orderDetails.add({
          'garbage_type_id': int.parse(objSampah.id),
          'quantity': objSampah.quantity,
          'garbage_type_name': objSampah.namaSampah,
          'quantity_type' : objSampah.satuan,
        });
      }
    });

    return {
      'customer_lat': latitude,
      'customer_lon': longitude,
      'notes': notes,
      'address': address,
      'order_detail': orderDetails,
    };
  }
}

class LocationMap extends StatefulWidget {
  OrderItems passedOrderItems;
  LocationMap({Key key, @required this.passedOrderItems}) : super(key: key);
  LocationMapState createState() => LocationMapState(passedOrderItems: passedOrderItems);
}

class LocationMapState extends State<LocationMap> {
  OrderItems passedOrderItems;
  LocationMapState({Key key, @required this.passedOrderItems});
  var location = new locationManager.Location();
  Future<LatLng> currentLocation;

  initState() {
    super.initState();
    currentLocation = goToMyLocation();
    orderDetail = new OrderDetail(orderItems: passedOrderItems);
  }

  Widget build(BuildContext context) {
    return FutureBuilder(
      key: Key('locationMap'),
      future: currentLocation,
      builder: (context,snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            return new Container();
          case ConnectionState.waiting:
            return Center(
              child: CircularProgressIndicator()
            );
          case ConnectionState.done:
            return LocationMapBuilder(passedOrderItems: passedOrderItems, currentLocation: snapshot.data);
          default:
            return Container();
        }
      }
    );
  }
  
  Future<LatLng> goToMyLocation() async {
    var currentLocation = <String, double>{};
    try {
      currentLocation = await location.getLocation();
    } catch (e) {
      currentLocation = null;
    }
    LatLng target = LatLng(currentLocation['latitude'], currentLocation['longitude']);

    return target;
  }
}

class LocationMapBuilder extends StatelessWidget {
  OrderItems passedOrderItems;
  LatLng currentLocation;

  LocationMapBuilder({this.passedOrderItems, this.currentLocation});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key('location-map'),
      body: MapSample(passedOrderItems: passedOrderItems, currentLocation: currentLocation,),
    );
  }
}

class MapSample extends StatefulWidget {
  OrderItems passedOrderItems;
  LatLng currentLocation;
  MapSample({Key key, @required this.passedOrderItems, this.currentLocation }) : super(key: key);

  @override
  State<MapSample> createState() => MapSampleState(passedOrderItems: passedOrderItems, currentLocation: currentLocation);
}

class MapSampleState extends State<MapSample> {
  List<PlacesSearchResult> places = [];
  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  final Set<Marker> markers = Set();
  var location = new locationManager.Location();
  CurrentAddress currentAddress;
  
  Map<String, double> userLocation;

  Completer<GoogleMapController> _controller = Completer();
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

  OrderItems passedOrderItems;
  LatLng currentLocation;
  CameraPosition _kGooglePlex;
  MapSampleState({@required this.passedOrderItems, @required this.currentLocation});

  var addressContainerVisibility = false;
  var notesVisible = false;

  void initState() {
    super.initState();
    _kGooglePlex = CameraPosition(
      target: currentLocation,
      zoom: 14.4746,
    );
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Pilih Lokasi Pengambilan',
          style: TextStyle(color: Color(0xff1d1d27)),
        ),
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Color(0xff1d1d27),),
            color: Colors.black,
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              Navigator.push(context, new MaterialPageRoute(
                  builder: (context) =>
                  new CustMakeOrder(passedOrderItem: passedOrderItems,)
                ),
              );
            },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Color(0xff1d1d27)),
            onPressed: () {
              _handlePressButton();
            },
          ),
        ],
      ),

      body: Stack(
        children: <Widget> [
          GestureDetector(
            onPanDown: (DragDownDetails details) {
              setState(() {
                addressContainerVisibility = false;
              });
            },
            child:
            GoogleMap(
              gestureRecognizers: Set()
                    ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
                    ..add(Factory<VerticalDragGestureRecognizer>( () => VerticalDragGestureRecognizer()))
                    ..add(Factory<HorizontalDragGestureRecognizer>( () => HorizontalDragGestureRecognizer())),
              markers: markers,
              initialCameraPosition: _kGooglePlex,
              trackCameraPosition: true,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              myLocationEnabled: true,
            )
          ),
          Align(
            alignment: Alignment.center,
            child:
            Container(
              margin: EdgeInsets.only(bottom: 20.0),
                child:Image.asset(
              'assets/pin.png',
              width: 20.0,
              height: 20.0,
              )
            )
          ),
          Container(
            child: Visibility(
              visible: addressContainerVisibility,
              child:
                Container(
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.all(const Radius.circular(10.0))
                  ),
                  
                  margin: EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0),
                  height: 180.0,
                  padding: EdgeInsets.all(10.0),
                  child: 
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                      Text(
                        'Address:',
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        )
                      ),
                      Text(
                        (currentAddress != null) ? currentAddress.address.toString():'',
                        style: TextStyle(
                          fontSize: 16.0
                        )
                      ),
                      Row(
                        children: <Widget>[
                          Visibility(
                            visible: notesVisible,
                            child: new Text(
                              orderDetail.notes,
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 8),
                            child: InkWell(
                              child: new Text(
                                notesButton(),
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.blue,
                                  decoration: TextDecoration.underline,
                                )),
                              onTap: () {
                                pickLocationAddNotes();
                              }
                            ),
                          ),
                        ],
                      ),
                      ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width,
                        height: 35.0,
                        child: RaisedButton(
                          color: Colors.green,
                          child: Text('Order Now', style: TextStyle(color: Colors.white)),
                          onPressed: () {
                            if (markers.isNotEmpty) {
                              Navigator.push(context, new MaterialPageRoute(
                                  builder: (context) =>
                                  new Checkout(orderDetail: orderDetail,)
                                ),
                              );
                            } else {
                              print("Marker Empty");
                            }
                          },
                        ),
                      )
                    ],
                  )
                )
              )
            ),
        ],
      ),

      floatingActionButton:
        Visibility(
          visible: !addressContainerVisibility,
          child: FloatingActionButton(          
              key: Key('floating-button'),
              onPressed: _onAddMarkerButtonPressed,
              materialTapTargetSize: MaterialTapTargetSize.padded,
              backgroundColor: Colors.green,
              child: const Icon(Icons.add_location, size: 36.0),
            )
        )
    );
  }

  String notesButton() {
    return notesVisible == true ? 'Change notes' : 'Add Notes';
  }

  void pickLocationAddNotes() {
    String writtenNotes = '';
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add Notes'),
          content: new Row(
            children: <Widget>[
              new Expanded(
                child: new TextField(
                autofocus: true,
                decoration: new InputDecoration(
                  labelText: 'Notes', hintText: 'eg. Blok/No, etc (max 30 characters)'),
                  onChanged: (value) {
                    writtenNotes = value;
                },
              ))
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Add'),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  orderDetail.notes = writtenNotes.length <= 30 ?
                  writtenNotes.substring(0, writtenNotes.length):
                  writtenNotes.substring(0, 30);                
                  notesVisible = orderDetail.notes != '' ? true : false;
                });
              },
            ),
          ],
        );
      }
    );
  }

  @visibleForTesting
  void _onAddMarkerButtonPressed() async {
    final GoogleMapController controller = await _controller.future;
    currentAddress = await apiProvider.httpRequest(controller, kGoogleApiKey,
        controller.cameraPosition.target.latitude.toString(),
        controller.cameraPosition.target.longitude.toString());
    setMarkerAndContainerAvailibility(currentAddress, controller.cameraPosition.target.latitude, controller.cameraPosition.target.longitude);
  }

  void setMarkerAndContainerAvailibility(currentAddress, latitude, longitude) {
    orderDetail.latitude = latitude;
    orderDetail.longitude = longitude;
    orderDetail.address = currentAddress.address;

    setState(() {
      addressContainerVisibility = true;
      markers.clear();
      markers.add(Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId((LatLng(latitude, longitude)).toString()),
        position: LatLng(latitude, longitude),
        infoWindow: InfoWindow(
          title: currentAddress.address,
          snippet: LatLng(latitude, longitude).toString(),
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  void onError(PlacesAutocompleteResponse response) {
    homeScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  @visibleForTesting
  Future<void> _handlePressButton() async {
    final GoogleMapController controller = await _controller.future;
    try {
      final center = await controller.cameraPosition.target;
      Prediction p = await PlacesAutocomplete.show(
          context: context,
          strictbounds: center == null ? false : true,
          apiKey: kGoogleApiKey,
          onError: onError,
          mode: Mode.fullscreen,
          language: "en",
          location: center == null
              ? null
              : Location(center.latitude, center.longitude),
          radius: center == null ? null : 10000);
      CameraPosition newLocation = CameraPosition(
      target: await apiProvider.placeDetailHttpRequest(p.placeId),
      zoom: 19,
      );
      controller.animateCamera(CameraUpdate.newCameraPosition(newLocation));
    } catch (e) {
      return;
    }
  }
}