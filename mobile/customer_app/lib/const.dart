const BASE_API = 'http://152.118.201.222:21414';
// const BASE_API = 'localhost:8000';

const BASE_URL = BASE_API + '/api/v1/customer';

const ORDER_STATUS_SEARCHING_DRIVER = "Searching driver";
const ORDER_STATUS_SCHEDULED = "Scheduled";
const ORDER_STATUS_PICKED_UP = "Picked up";
const ORDER_STATUS_COMPLETED = "Completed";
const ORDER_STATUS_CANCELLED_BY_MITRA = "Cancelled by Mitra";
const ORDER_STATUS_CANCELLED_BY_CUSTOMER = "Cancelled by Customer";
const ORDER_STATUS_NO_SERVICE_AVAILABLE = "No service available";
const ORDER_STATUS_NO_MITRA_TAKE_THE_ORDER = "No mitra take the order";
const getCustomerDataApi = "$BASE_URL/account";
