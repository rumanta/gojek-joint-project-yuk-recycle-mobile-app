import 'package:flutter/material.dart';
import 'package:customer_app/make_order.dart';
import 'package:customer_app/home.dart';
import 'package:customer_app/account.dart';
import 'package:customer_app/help_page.dart';
import 'package:customer_app/order_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:customer_app/models/push_notification.dart';
import 'package:flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math';
import 'dart:ui';
import 'dart:async';
import 'package:customer_app/location_map.dart';
import 'package:customer_app/pre_make_order.dart';

const ORDER_RESPONSE_MAKE_ORDER_SCHEDULED = 'ORDER SUCCEDED';
const ORDER_RESPONSE_MAKE_ORDER_FAIL = 'YOUR LAST ORDER HAS FAILED';
const ORDER_RESPONSE_NO_SERVICE = 'NO_SERVICE';
const ORDER_TYPE_MAKE_ORDER = 'MAKE_ORDER';
const ORDER_TYPE_COMPLETE_ORDER = 'COMPLETE_ORDER';
const ORDER_TYPE_PICKUP_ORDER = 'PICKUP_ORDER';
const ORDER_TYPE_CANCEL_ORDER = 'CANCEL_ORDER';

class InheritedBottomBar extends InheritedWidget {
  _NavMenuBar data;

  InheritedBottomBar({Key key, this.data, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedBottomBar old) => true;
}

class NavMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NavMenuBar();

  static _NavMenuBar of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(InheritedBottomBar)
            as InheritedBottomBar)
        .data;
  }
}

class _NavMenuBar extends State<NavMenu> with TickerProviderStateMixin {
  AnimationController _controller;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  int _bottomBarNavigationIndex = 0;
  bool isSearchingForMitra = false;
  bool isFloatingButtonVisible = true;
  static SharedPreferences _prefs;

  void gotoHelp() {
    setState(() {
      _bottomBarNavigationIndex = 3;
    });
  }

  List<Widget> _children;

  @override
  void initState() {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      _prefs = sp;
      isSearchingForMitra = getPrefsIsSearchingStatus();
    });

    super.initState();
    configureFirebaseCloudMessaging();

    _controller = AnimationController(
      duration: Duration(days: 1),
      vsync: this,
    );
    _children = [
      Home(gotoHelp: gotoHelp),
      OrderPage(),
      HowToOrderPage(),
      HelpPage(),
      AccountPage(),
    ];
  }

  void navigateToRecyclePage() {
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new CustMakeOrder()),
    );
  }

  void startAnimation() {
    _controller.forward(from: 0.0);
    Timer(new Duration(seconds: 20), () {
      _controller.stop();
      _controller.reset();
    });
  }

  static getPrefsIsSearchingStatus() {
    if (_prefs.getBool('is_searching') == null) {
      _prefs.setBool('is_searching', false);
    }

    return _prefs.getBool('is_searching');
  }

  static setPrefsIsSearchingStatusTrue() {
    return _prefs.setBool('is_searching', true);
  }

  static setPrefsIsSearchingStatusFalse() {
    return _prefs.setBool('is_searching', false);
  }

  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void configureFirebaseCloudMessaging() {
    _firebaseMessaging.getToken().then((token) {
      print("fcm token: $token");
    });

    var callback = (Map<String, dynamic> message) => messagingCallback(message);
    _firebaseMessaging.configure(
      onMessage: callback,
      onResume: callback,
      onLaunch: callback,
    );
  }

  Future<dynamic> messagingCallback(Map<String, dynamic> message) async {
    PushNotification pushNotification =
        await PushNotification.fromJson(message);
    DataMessage dataMessage = pushNotification.dataMessage;
    String type = dataMessage.type;
    String resp = dataMessage.response;

    if (type == ORDER_TYPE_MAKE_ORDER) {
      setState(() {
        setPrefsIsSearchingStatusFalse();
        isSearchingForMitra = false;
        _controller.stop();
        _controller.reset();
      });
      if (resp == ORDER_RESPONSE_MAKE_ORDER_SCHEDULED) {
        _showDialogMakeOrder();
      } else if (resp == ORDER_RESPONSE_MAKE_ORDER_FAIL) {
        _showDialogMitraNotFound();
      } else if (resp == ORDER_RESPONSE_NO_SERVICE) {
        _showDialogNoService();
      } else {
        print("Error : Make Order with this response not registered");
      }
    } else if (type == ORDER_TYPE_COMPLETE_ORDER) {
      _showDialogCompleteOrder();
    } else if (type == ORDER_TYPE_PICKUP_ORDER) {
      _showDialogPickUpOrder();
    } else if (type == ORDER_TYPE_CANCEL_ORDER) {
      _showDialogCancelledOrder();
    } else {
      print("Cannot Recognize Response Type");
    }
  }

  void _showDialogCancelledOrder() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("We are Sorry!"),
            content: new Text("Your order has been cancelled by our Mitra"),
            actions: <Widget>[
              new ButtonTheme(
                  minWidth: 80,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Back',
                        style: TextStyle(
                            color: Color(0xFF1D1D1D), fontSize: 16.0)),
                  )),
            ],
          );
        });
  }

  void _showDialogNoService() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("We are Sorry!"),
            content: new Text("Our services is not available in your area"),
            actions: <Widget>[
              new ButtonTheme(
                  minWidth: 80,
                  child: FlatButton(
                    color: Color(0xFF293542),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Okay',
                        style: TextStyle(color: Colors.white, fontSize: 16.0)),
                  )),
            ],
          );
        });
  }

  void _showDialogMitraNotFound() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("We are Sorry!"),
            content: new Text("We cannot find any Mitra for you now"),
            actions: <Widget>[
              new ButtonTheme(
                  minWidth: 80,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Back',
                        style: TextStyle(
                            color: Color(0xFF1D1D1D), fontSize: 16.0)),
                  )),
            ],
          );
        });
  }

  void _showDialogMakeOrder() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("Mitra Found!"),
            content: new Text("Your order has been received by our Mitra"),
            actions: <Widget>[
              new ButtonTheme(
                  minWidth: 80,
                  child: FlatButton(
                    color: Color(0xff007E24),
                    onPressed: () {
                      Navigator.pop(context);
                      triggerNavigateToIndex(1);
                    },
                    child: Text('Order Detail',
                        style: TextStyle(color: Colors.white, fontSize: 16.0)),
                  )),
              new ButtonTheme(
                  minWidth: 80,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Back',
                        style: TextStyle(
                            color: Color(0xFF1D1D1D), fontSize: 16.0)),
                  )),
            ],
          );
        });
  }

  void _showDialogPickUpOrder() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("Pick Up Time!"),
            content: new Text("Our Mitra is on the way to your Location!"),
            actions: <Widget>[
              new ButtonTheme(
                  minWidth: 80,
                  child: FlatButton(
                    color: Color(0xff007E24),
                    onPressed: () {
                      Navigator.pop(context);
                      triggerNavigateToIndex(1);
                    },
                    child: Text('Order Detail',
                        style: TextStyle(color: Colors.white, fontSize: 16.0)),
                  )),
              new ButtonTheme(
                  minWidth: 80,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Back',
                        style: TextStyle(
                            color: Color(0xFF1D1D1D), fontSize: 16.0)),
                  )),
            ],
          );
        });
  }

  void _showDialogMakeOrderDisabled() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("There's an Active Order"),
          content: new Text("Are you sure you want to make another order?"),
          actions: <Widget>[
            new ButtonTheme(
                minWidth: 80,
                child: FlatButton(
                  color: Color(0xff007E24),
                  onPressed: () {
                    Navigator.pop(context);
                    triggerNavigateToIndex(1);
                  },
                  child: Text('Order Detail',
                      style: TextStyle(color: Colors.white, fontSize: 16.0)),
                )),
            new ButtonTheme(
                minWidth: 80,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    triggerNavigateToIndex(2);
                  },
                  child: Text('Continue',
                      style: TextStyle(color: Colors.white, fontSize: 16.0)),
                )),
          ],
        );
      },
    );
  }

  void _showDialogCompleteOrder() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Yay! Your Order is Complete"),
          content: new Text(
              "Thank you for using our service and put your trust on our Mitra"),
          actions: <Widget>[
            ButtonTheme(
                minWidth: 100,
                child: FlatButton(
                  color: Color(0xff007E24),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Okay',
                      style: TextStyle(color: Colors.white, fontSize: 20.0)),
                )),
          ],
        );
      },
    );
  }

  void setIsSearchingForMitraTrue() {
    setState(() {
      this.isSearchingForMitra = true;
      setPrefsIsSearchingStatusTrue();
      startAnimation();
    });
  }

  void triggerNavigateToIndex(int index) {
    setState(() {
      _bottomBarNavigationIndex = index;
    });
  }

  bool checkFloatingButtonVisible() {
    if (MediaQuery.of(context).viewInsets.bottom != 0) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return InheritedBottomBar(
      data: this,
      child: Scaffold(
          body: _children[_bottomBarNavigationIndex],
          backgroundColor: Color(0xFFFFFFFF),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Visibility(
            visible: checkFloatingButtonVisible(),
            child: FloatingActionButton(
              child: RotationTransition(
                turns: Tween(begin: 0.0, end: 25000.5).animate(_controller),
                child: ImageIcon(AssetImage('assets/recycle.png')),
              ),
              elevation: 4.0,
              backgroundColor: _bottomBarNavigationIndex == 2
                  ? Color(0xFF007E24)
                  : Color(0xFF293542),
              onPressed: () {
                if (_bottomBarNavigationIndex != 2) {
                  isSearchingForMitra == true
                      ? _showDialogMakeOrderDisabled()
                      : setState(() {
                          _bottomBarNavigationIndex = 2;
                        });
                }
              },
            ),
          ),
          bottomNavigationBar: new Theme(
              data: Theme.of(context).copyWith(
                canvasColor: const Color(0xFFFFFFFF),
              ),
              child: BottomNavigationBar(
                key: Key("CustomerNavBar"),
                type: BottomNavigationBarType.fixed,
                fixedColor: Color(0xFFFFFFFF),
                currentIndex: _bottomBarNavigationIndex,
                onTap: (int index) {
                  if (index != 2) {
                    setState(() {
                      _bottomBarNavigationIndex = index;
                    });
                  }
                },
                items: [
                  new BottomNavigationBarItem(
                    icon: new ImageIcon(AssetImage('assets/home.png'),
                        color: _bottomBarNavigationIndex == 0
                            ? Color(0xFF007E24)
                            : Color(0xFF293542)),
                    title: new Text(
                      "Home",
                      key: Key('HomeButton'),
                      style: new TextStyle(
                        color: _bottomBarNavigationIndex == 0
                            ? Color(0xFF007E24)
                            : Color(0xFF293542),
                        fontSize: 10.0,
                      ),
                    ),
                  ),
                  new BottomNavigationBarItem(
                    icon: new ImageIcon(AssetImage('assets/list.png'),
                        color: _bottomBarNavigationIndex == 1
                            ? Color(0xFF007E24)
                            : Color(0xFF293542)),
                    title: new Text(
                      "Order",
                      key: Key('OrderButton'),
                      style: new TextStyle(
                        color: _bottomBarNavigationIndex == 1
                            ? Color(0xFF007E24)
                            : Color(0xFF293542),
                        fontSize: 10.0,
                      ),
                    ),
                  ),
                  new BottomNavigationBarItem(
                    icon: new ImageIcon(AssetImage('assets/account.png'),
                        color: _bottomBarNavigationIndex == 2
                            ? Color(0x00007E24)
                            : Color(0x00293542)),
                    title: new Text(
                      "RECYCLE",
                      key: Key('RecycleButton'),
                      style: new TextStyle(
                          color: _bottomBarNavigationIndex == 2
                              ? Color(0xFF007E24)
                              : Color(0xFF293542),
                          fontSize: 13.0,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  new BottomNavigationBarItem(
                    icon: new ImageIcon(AssetImage('assets/info.png'),
                        color: _bottomBarNavigationIndex == 3
                            ? Color(0xFF007E24)
                            : Color(0xFF293542)),
                    title: new Text(
                      "Help",
                      key: Key('HelpButton'),
                      style: new TextStyle(
                        color: _bottomBarNavigationIndex == 3
                            ? Color(0xFF007E24)
                            : Color(0xFF293542),
                        fontSize: 10.0,
                      ),
                    ),
                  ),
                  new BottomNavigationBarItem(
                    icon: new ImageIcon(
                      AssetImage('assets/account.png'),
                    ),
                    activeIcon: ImageIcon(
                      AssetImage('assets/account-active.png'),
                      color: const Color(0xFF007E24),
                    ),
                    title: new Text(
                      "Account",
                      key: Key('AccountButton'),
                      style: new TextStyle(
                        color: _bottomBarNavigationIndex == 4
                            ? Color(0xFF007E24)
                            : Color(0xFF293542),
                        fontSize: 10.0,
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }
}
