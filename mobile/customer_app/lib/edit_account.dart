import 'dart:async';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'const.dart';
import 'login_provider.dart';

Client client = new Client();
String HOME_URL = '$BASE_URL/account';
class API{
  static Future APICall(){
    var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    return client.get(HOME_URL, headers: header);
  }
}
class UserData {
  String name;
  String email;
  int phone;
  int login_type;


  UserData(String name, String email, int phone, int login_type){
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.login_type = login_type;
    }

  UserData.fromJson(Map json)
      : name = json['name'],
        email = json['email'],
        phone = json['phone'],
        login_type = json['login_type'];

  Map toJson() {
    return {'name': name, 'email': email, 'phone': phone, 'login_type': login_type};
  }
}

Future<Map<dynamic, dynamic>> sendToBackEndAPIUpdate(Map body, BuildContext context) async {
  Client client = new Client();;
  // BASE_URL = BASE_URL + '/?code=' + body['code'];

  var token = LoginProvider.getTokenCustomer();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

  final response = await client.post(HOME_URL, headers: header, body: json.encode(body)).then((value){
    Map result =json.decode(value.body);
    if (value.statusCode != 200) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('Error Message'),
            content: new Text(result['error']['message']),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('Profile Updated'),
            content: new Text('Your profile has been updated'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    return result;
  });
}


class AccountEditPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('Edit Account', style: TextStyle(
          fontFamily: 'Roboto',
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.w900,
          fontSize: 20,
          color: Color(0xFF1D1D27),
        ),),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () {
              Navigator.of(context).pop();
            },
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 4.0),
        child: AccountEditForm(),
      ),
    );
  }
}

class AccountEditForm extends StatefulWidget {
  @override
  _AccountEditFormState createState() => _AccountEditFormState();
}

class _AccountEditFormState extends State<AccountEditForm> {
  String name;
  String email;
  String phone;
  int login_type;
  _getData() {
    API.APICall().then((response) {
      setState(() {
        Map map = json.decode(response.body);
        map = map['customer_data'];
        _emailController.text = map['email'];
        _phoneController.text = map['phone'];
        _nameController.text = map['name'];
        login_type = map['login_type'];
      });
    }); 
  }

  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _oldPasswordController = TextEditingController();
  TextEditingController _newPasswordController = TextEditingController();
  TextEditingController _retypePasswordController = TextEditingController();
  TextEditingController _nameController = TextEditingController();

  @override
  initState() {
    _getData();
    super.initState();
  }

  dispose() {
    _controllers.forEach((_, controller) => controller.dispose());
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  final _NAME = 'Name';
  final _PHONE = 'Phone';
  final _EMAIL = 'Email';
  final _OLDPASSWORD = 'Old Password';
  final _NEWPASSWORD = 'New Password';
  final _RETYPEPASSWORD = 'Confirm New Password';

  var _controllers;

  _AccountEditFormState() {
    this._controllers = {
      _NAME: TextEditingController(),
      _PHONE: TextEditingController(),
      _EMAIL: TextEditingController(),
      _OLDPASSWORD: TextEditingController(),
      _NEWPASSWORD: TextEditingController(),
      _RETYPEPASSWORD: TextEditingController(),
    };
  }

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    final inputDecoration = InputDecoration(
      contentPadding: EdgeInsets.only(bottom: 5.0),
    );

    final space = Container(margin: EdgeInsets.only(top: 15));

    Widget formWidget = Container();
    if(login_type == 1){
      formWidget = Form(
            key: _formKey,
            child: Container(
              margin: EdgeInsets.all(15.0),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Text(_NAME, style: textStyle),
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      hintText: 'Please insert name'
                    ),
                  ),
                  space,
                  Text(_PHONE, style: textStyle),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    controller: _phoneController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      hintText: 'Please insert phone'                      
                    ),
                  ),
                  space,
                  Text(_EMAIL, style: textStyle),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      hintText: 'Please insert email'                      
                    ),
                  ),
                  space,
                  Text(_OLDPASSWORD, style: textStyle),
                  TextFormField(
                    controller: _oldPasswordController,
                    decoration: inputDecoration,
                    obscureText: true,
                  ),
                  space,
                  Text(_NEWPASSWORD, style: textStyle),
                  TextFormField(
                    controller: _newPasswordController,
                    decoration: inputDecoration,
                    obscureText: true,
                  ),
                  space,
                  Text(_RETYPEPASSWORD, style: textStyle),
                  TextFormField(
                    controller: _retypePasswordController,
                    decoration: inputDecoration,
                    obscureText: true,
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                  ),
                  RaisedButton(
                    onPressed: () {
                      if (_newPasswordController.text == _retypePasswordController.text){
                        if (_emailController.text == ''){ _emailController.text = email;}
                        Map body = {
                                'email': _emailController.text,
                                'old_password': _oldPasswordController.text,
                                'new_password': _newPasswordController.text,
                                'name': _nameController.text,
                                'phone': _phoneController.text
                              };
                        sendToBackEndAPIUpdate(body, context);
                      } else if(_newPasswordController.text != '' && _oldPasswordController.text == ''){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: new Text('Error Message'),
                              content: new Text('Fill old password first'),
                              actions: <Widget>[
                                new FlatButton(
                                  child: new Text("Close"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }else if (_newPasswordController.text == '' && _oldPasswordController.text != ''){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: new Text('Error Message'),
                              content: new Text('Fill new password first'),
                              actions: <Widget>[
                                new FlatButton(
                                  child: new Text("Close"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }
                    },
                    color: const Color(0xff007e24),
                    child: Text(
                      'Save',
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
          }else if(login_type == 2){
          formWidget = Form(
            key: _formKey,
            child: Container(
              margin: EdgeInsets.all(15.0),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Text(_NAME, style: textStyle),
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      hintText: 'Please insert name',
                      enabled: false,
                    ),
                  ),
                  space,
                  Text(_PHONE, style: textStyle),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    controller: _phoneController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      hintText: 'Please insert phone',
                    ),
                  ),
                  space,
                  Text(_EMAIL, style: textStyle),
                  TextFormField(
                    controller: _emailController,
                    enabled: false,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      hintText: 'Please insert email',
                    ),
                  ),
                  space,
                  Text(_OLDPASSWORD, style: textStyle),
                  TextFormField(
                    controller: _oldPasswordController,
                    obscureText: true,
                    enabled: false,
                    decoration: InputDecoration(
                      hintText: 'Cannot be modified',
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      enabled: false,
                    ),
                  ),
                  space,
                  Text(_NEWPASSWORD, style: textStyle),
                  TextFormField(
                    controller: _newPasswordController,
                    obscureText: true,
                    enabled: false,
                    decoration: InputDecoration(
                      hintText: 'Cannot be modified',
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      enabled: false,
                    ),
                  ),
                  space,
                  Text(_RETYPEPASSWORD, style: textStyle),
                  TextFormField(
                    controller: _retypePasswordController,
                    obscureText: true,
                    enabled: false,
                    decoration: InputDecoration(
                      hintText: 'Cannot be modified',
                      contentPadding: EdgeInsets.only(bottom: 5.0),
                      enabled: false,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                  ),
                  RaisedButton(
                    onPressed: () {
                      if(_newPasswordController.text == _retypePasswordController.text){
                        if (_emailController.text == ''){ _emailController.text = email;}
                        Map body = {
                                'email': _emailController.text,
                                'password': _newPasswordController.text,
                                'name': _nameController.text,
                                'phone': _phoneController.text
                              };
                        sendToBackEndAPIUpdate(body, context);
                      }else if(_newPasswordController.text != '' && _oldPasswordController.text == ''){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: new Text('Error Message'),
                              content: new Text('Fill old password first'),
                              actions: <Widget>[
                                new FlatButton(
                                  child: new Text("Close"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }else if (_newPasswordController.text == '' && _oldPasswordController.text != ''){
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: new Text('Error Message'),
                              content: new Text('Fill new password first'),
                              actions: <Widget>[
                                new FlatButton(
                                  child: new Text("Close"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }
                    },
                    color: const Color(0xff007e24),
                    child: Text(
                      'Save',
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
            
          );
        }

    return Scaffold(
      body: ListView(
        children: <Widget>[
          formWidget,
          Container(
            child: Padding(
              padding: EdgeInsets.all(12.0),
              
            ),
          )
        ],
      ),
    );
  }
}