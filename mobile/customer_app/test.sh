#!/bin/bash

if [[ $1 == "flutter" ]]; then
    flutter test --coverage
    genhtml coverage/lcov.info --output=coverage/
fi
