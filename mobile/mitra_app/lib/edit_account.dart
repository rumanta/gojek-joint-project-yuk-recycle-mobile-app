import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

import 'account_page.dart';

final _NAME = 'Name';
final _PHONE = 'Phone';
final _EMAIL = 'Email';
final _ADDRESS = 'Address';
final _LOCATION = 'Location';
final kGoogleApiKey = "AIzaSyDycUBHdtst7K2Z5uRKFRn3Gsmu2cVImWY";

class AccountEditPage extends StatefulWidget {
  @override
  _AccountEditPage createState() => _AccountEditPage();
}

class _AccountEditPage extends State<AccountEditPage> {
  AccountData data;
  Completer<GoogleMapController> _controller = Completer();

  void updateData(BuildContext context) {
    var snackbarDuration = Duration(seconds: 3);
    AccountProvider.updateAccountData(data).then((response) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Your data successfully updated'),
        duration: snackbarDuration,
      ));
    }, onError: (e) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(e.toString()),
        duration: snackbarDuration,
      ));
    });
  }

  Widget layout(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Padding(
            padding: EdgeInsets.all(12.0),
            child: const Icon(
              Icons.arrow_back,
              color: Color(0xFF1D1D27),
            ),
          ),
        ),
        title: Text(
          'Manage Account',
          style: TextStyle(
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w900,
            fontSize: 20,
            color: Color(0xFF1D1D27),
          ),
        ),
        actions: <Widget>[
          Container(
            child: Padding(
              padding: EdgeInsets.all(12.0),
              child: Builder(
                builder: (BuildContext context) {
                  return RaisedButton(
                    onPressed: () => updateData(context),
                    color: const Color(0xff007e24),
                    child: Text(
                      'Save',
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 4.0),
        child: AccountEditForm(data: data, controller: _controller),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: AccountProvider.fetchAccountData(),
      builder: (BuildContext context, AsyncSnapshot<AccountData> snapshot) {
        // defaults to jakarta coordinate
        AccountData placeholderData = AccountData(
          lat: -6.210783,
          lon: 106.845306,
        );
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            data = placeholderData;
            break;
          case ConnectionState.done:
            if (snapshot.data == null) {
              data = placeholderData;
              break;
            }
            data = snapshot.data;
            _controller.future.then((controller) {
              controller.animateCamera(
                  CameraUpdate.newLatLng(LatLng(data.lat, data.lon)));
            });
            break;
          default:
            data = placeholderData;
        }

        return Container(
          child: this.layout(context),
        );
      },
    );
  }
}

class Maps extends StatelessWidget {
  AccountData data;
  Completer<GoogleMapController> controller;
  MapType _currentMapType = MapType.normal;

  Maps({@required this.data, @required this.controller, Key key})
      : super(key: key);

  void _onCameraMove(CameraPosition position) {
    data.setLatLon(position.target.latitude, position.target.longitude);
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: LatLng(data.lat, data.lon),
        zoom: 15.0,
      ),
      onMapCreated: (GoogleMapController controller) {
        this.controller.complete(controller);
      },
      onCameraMove: _onCameraMove,
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
    );
  }
}

class AccountEditForm extends StatefulWidget {
  AccountData data;
  Completer<GoogleMapController> controller;
  Map<String, dynamic> controllers = {
    _NAME: TextEditingController(),
    _PHONE: TextEditingController(),
    _EMAIL: TextEditingController(),
    _ADDRESS: TextEditingController(),
  };

  AccountEditForm({@required this.data, @required this.controller, Key key})
      : super(key: key);

  @override
  _AccountEditFormState createState() => _AccountEditFormState();
}

class _AccountEditFormState extends State<AccountEditForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    widget.controllers.forEach((_, controller) => controller.dispose());
    super.dispose();
  }

  void setAddress() {
    var lat = widget.data.lat;
    var lng = widget.data.lon;
    http
        .get(
            'https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=$kGoogleApiKey')
        .then((response) {
      if (response.statusCode == 200) {
        var json_response = jsonDecode(response.body);
        widget.controllers[_ADDRESS].text =
            json_response['results'][0]['formatted_address'];
        widget.data.address = json_response['results'][0]['formatted_address'];
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Cant get location address'),
          duration: Duration(seconds: 2),
        ));
      }
    });
  }

  static final textStyle = TextStyle(
    fontFamily: 'Roboto',
    fontWeight: FontWeight.bold,
    fontSize: 15,
  );

  static final inputDecoration = InputDecoration(
    contentPadding: EdgeInsets.only(bottom: 5.0),
  );

  static final space = Container(margin: EdgeInsets.only(top: 15));

  @override
  Widget build(BuildContext context) {
    widget.controllers[_NAME].text = widget.data.fullName;
    widget.controllers[_PHONE].text = widget.data.phone;
    widget.controllers[_EMAIL].text = widget.data.email;
    widget.controllers[_ADDRESS].text = widget.data.address;

    return Container(
      margin: EdgeInsets.all(15.0),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Text(_NAME, style: textStyle),
          TextField(
            onChanged: (text) {
              widget.data.fullName = text;
            },
            controller: widget.controllers[_NAME],
            decoration: inputDecoration,
          ),
          space,
          Text(_PHONE, style: textStyle),
          TextField(
            onChanged: (text) {
              widget.data.phone = text;
            },
            controller: widget.controllers[_PHONE],
            decoration: inputDecoration,
          ),
          space,
          Text(_EMAIL, style: textStyle),
          TextField(
            onChanged: (text) {
              widget.data.email = text;
            },
            controller: widget.controllers[_EMAIL],
            decoration: inputDecoration,
          ),
          space,
          Text(_ADDRESS, style: textStyle),
          TextField(
            onChanged: (text) {
              widget.data.address = text;
            },
            controller: widget.controllers[_ADDRESS],
            keyboardType: TextInputType.multiline,
            maxLines: 2,
            decoration: inputDecoration,
          ),
          space,
          Text(_LOCATION, style: textStyle),
          RaisedButton(
            onPressed: setAddress,
            color: const Color(0xff007e24),
            child: Text(
              'Set Address',
              style: const TextStyle(
                fontFamily: 'Roboto',
                fontSize: 15,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            height: 300.0,
            width: 200.0,
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Maps(
                    data: widget.data,
                    controller: widget.controller,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20.0),
                    child: Image.asset(
                      'assets/pin.png',
                      width: 20.0,
                      height: 20.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
