import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'edit_account.dart';

import 'login_page_mitra.dart';
import 'login_provider.dart';
import 'const.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'My Account',
          style: TextStyle(
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w900,
            fontSize: 20,
            color: Color(0xFF1D1D27),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          AccountEdit(),
          AddWarehouse(),
          PrivacyPolicyInfo(),
          TermsConditionInfo(),
          AppVersion(),
          LogoutButton(),
        ],
      ),
    );
  }
}

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        color: Color(0xFFDC1714),
        shape: new RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          'LOGOUT',
          style: TextStyle(
            color: Color(0xFFF2F2F2),
            fontSize: 15,
            fontWeight: FontWeight.w700,
          ),
        ),
        onPressed: () async {
          while (Navigator.canPop(context)) {
            Navigator.of(context).pop();
          }

          LoginProvider.logout();
          Navigator.of(context).pushReplacementNamed('/');

          while (Navigator.canPop(context)) {
            Navigator.of(context).pop();
          }
        },
      ),
    );
  }
}

class AccountEdit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      decoration: new BoxDecoration(
        boxShadow: [
          new BoxShadow(
            offset: new Offset(0, 1),
            blurRadius: 4,
            color: const Color.fromRGBO(29, 29, 29, 0.15),
          ),
        ],
      ),
      child: Card(
        shape: const RoundedRectangleBorder(
          borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
        ),
        child: Container(
          margin: EdgeInsets.all(13.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 8,
                child: AccountInfo(),
              ),
              Expanded(
                flex: 2,
                child: AccountEditButton(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AccountProvider {
  static http.Client client = new http.Client();
  static SharedPreferences _prefs;

  static Future<Map> getHeaders() async {
    _prefs = await SharedPreferences.getInstance();
    var JWT = _prefs.getString('token_mitra');
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $JWT',
    };
  }

  static Future<AccountData> fetchAccountData() async {
    final accountDataURL = '$BASE_URL/account';
    var header = await getHeaders();

    http.Response response =
        await client.get(accountDataURL, headers: await getHeaders());

    if (response.statusCode != HttpStatus.ok) {
      throw ('Failed to fetch data');
    }

    return AccountData.fromJson(json.decode(response.body)['mitra']);
  }

  static Future<Map> updateAccountData(AccountData mitraData) async {
    final accountDataURL = '$BASE_URL/account';
    var header = await getHeaders();
    Map<String, dynamic> data = {
      "mitra": {
        "name": mitraData.fullName,
        "email": mitraData.email,
        "phone": mitraData.phone,
        "location": {
          "address": mitraData.address,
          "lat": mitraData.lat,
          "lon": mitraData.lon,
        }
      }
    };

    http.Response response = await client.post(accountDataURL,
        body: json.encode(data), headers: header);

    if (response.statusCode != HttpStatus.ok) {
      throw (json.decode(response.body)['error']['message']);
    }

    return null;
  }

  static Future<Map> fetchTermsAndAgreement() async {
    final termsAndAgreementURL = '$BASE_URL/TNC';
    var headers = await getHeaders();

    http.Response response =
        await client.get(termsAndAgreementURL, headers: headers);

    if (response.statusCode != HttpStatus.ok) {
      throw ("Failed to fetch Terms and Agreement!");
    }

    return json.decode(response.body);
  }

  static Future<Map> fetchPrivacyPolicy() async {
    final privacyPolicyURL = '$BASE_URL/privacy-policy';
    var headers = await getHeaders();

    http.Response response =
        await client.get(privacyPolicyURL, headers: headers);

    if (response.statusCode != HttpStatus.ok) {
      throw ("Failed to fetch Privacy Policy!");
    }

    return json.decode(response.body);
  }
}

class AccountData {
  String fullName;
  String email;
  String phone;
  String address;
  double lat;
  double lon;
  bool verified;

  AccountData({
    this.fullName = '-',
    this.email = '-',
    this.phone = '-',
    this.address = '-',
    this.lat = 0.0,
    this.lon = 0.0,
    this.verified = false,
  });

  void setLatLon(double lat, double lon) {
    this.lat = lat;
    this.lon = lon;
  }

  factory AccountData.fromJson(Map<String, dynamic> json) {
    return AccountData(
      fullName: json['name'],
      email: json['email'],
      phone: json['phone'],
      address: json['location']['address'],
      lat: json['location']['lat'],
      lon: json['location']['lon'],
      verified: json['verified'],
    );
  }

  @override
  String toString() {
    return "${this.fullName} ${this.lat} ${this.lon}";
  }
}

class AccountInfo extends StatelessWidget {
  Widget layout(AccountData data) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            data.fullName,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 15,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            data.email,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 12,
              fontStyle: FontStyle.italic,
            ),
          ),
          Text(
            data.phone,
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 12,
              fontStyle: FontStyle.italic,
            ),
          ),
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 5.0),
                child: (data.verified)
                    ? Icon(
                        Icons.done,
                        color: Color(0xff007e24),
                        size: 10.0,
                      )
                    : Icon(
                        Icons.cancel,
                        color: Color(0xffdc1714),
                        size: 10.0,
                      ),
              ),
              Text(
                '${data.verified ? '' : 'un'}verified account',
                style: const TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 10,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: AccountProvider.fetchAccountData(),
      builder: (BuildContext context, AsyncSnapshot<AccountData> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return this.layout(AccountData());
          case ConnectionState.done:
            if (snapshot.hasError) {
              return this.layout(AccountData());
            }
            return this.layout(snapshot.data);
        }
      },
    );
  }
}

class AddWarehouse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class InfoItemList extends StatelessWidget {
  final Text text;
  final Function onTap;

  InfoItemList({@required this.text, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        onPressed: () => this.onTap(),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 14.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: this.text,
                    margin: EdgeInsets.symmetric(vertical: 9.0),
                  ),
                  Container(
                    height: 35.0,
                    child: const Icon(
                      Icons.arrow_forward,
                      color: Color(0xFF1D1D27),
                    ),
                  ),
                ],
              ),
            ),
            Divider(height: 8),
          ],
        ),
      ),
    );
  }
}

class PrivacyPolicyInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoItemList(
      text: Text(
        'Privacy Policy',
        style: const TextStyle(
          fontFamily: 'Roboto',
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () => Navigator.pushNamed(context, '/home/account/privacy_policy'),
    );
  }
}

class TermsConditionInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InfoItemList(
      text: Text(
        'Terms & Conditions',
        style: const TextStyle(
          fontFamily: 'Roboto',
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {
        Navigator.pushNamed(context, '/home/account/terms_condition');
      },
    );
  }
}

class AccountEditButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: RaisedButton(
        onPressed: () =>
            Navigator.pushNamed(context, '/home/account/edit_account'),
        color: const Color(0xff007e24),
        child: Text(
          'Edit',
          style: const TextStyle(
            fontFamily: 'Roboto',
            fontSize: 15,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

class AppVersion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FutureBuilder<PackageInfo>(
            future: PackageInfo.fromPlatform(),
            builder: (context, snapshot) {
              String version;
              switch (snapshot.connectionState) {
                case ConnectionState.done:
                  version = snapshot.data.version;
                  break;
                default:
                  version = '-';
              }
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 14.0, vertical: 7.0),
                child: Text(
                  'v$version',
                  style: const TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 14,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              );
            },
          ),
          Divider(height: 8),
        ],
      ),
    );
  }
}
