import 'package:flutter/material.dart';

class AppInfo extends StatelessWidget {
  final String title;
  final Function fetchData;

  AppInfo({this.title, this.fetchData});

  Future<void> showToastError(context, String error) async {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(error),
      duration: Duration(seconds: 2),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Padding(
            padding: EdgeInsets.all(12.0),
            child: const Icon(
              Icons.arrow_back,
              color: Color(0xFF1D1D27),
            ),
          ),
        ),
        title: Text(
          title,
          style: TextStyle(
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w900,
            fontSize: 20,
            color: Color(0xFF1D1D27),
          ),
        ),
      ),
      body: FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
          List listData = [];
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Container(
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  backgroundColor: Colors.green,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                showToastError(context, snapshot.error.toString());
                return Container(
                  alignment: Alignment.center,
                  child: Text("Failed to fetch data"),
                );
              }
              print("done");
              print(snapshot.data);
              listData = snapshot.data['data'];
              break;
          }
          return ListView.builder(
            itemCount: listData.length,
            itemBuilder: (context, index) {
              return ListTile(
                contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                title: Text(
                  listData[index]['title'],
                  style: TextStyle(
                    color: Color(0xFF1D1D1D),
                    fontWeight: FontWeight.w700,
                    fontSize: 20,
                  ),
                ),
                subtitle: Text(
                  listData[index]['body'],
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: Color(0xFF1D1D1D),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
