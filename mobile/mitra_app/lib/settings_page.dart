import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models/mitra_settings.dart';
import 'const.dart' show BASE_URL;

var mitraSettingsUrl = "$BASE_URL/settings";

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: _SettingsPageHelper(),
      ),
    );
  }
}

class _SettingsPageHelper extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SettingsPageHelperState();
}

class _SettingsPageHelperState extends State<_SettingsPageHelper> {
  MitraSettings mitraSettings;
  bool isCreatePickUpSchedule;
  List<TextEditingController> minimumPickUpController;
  List<TextEditingController> priceController;
  TextEditingController distanceController;
  Future<MitraSettings> mitraSettingsFuture;
  bool alreadyFetch = false;

  @override
  void initState() {
    mitraSettingsFuture = _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<MitraSettings>(
      future: mitraSettingsFuture,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (!alreadyFetch) {
            alreadyFetch = true;
            mitraSettings = snapshot.data;
            isCreatePickUpSchedule = !mitraSettings.schedules.isAllSelected();

            priceController = [];
            minimumPickUpController = [];
            distanceController = TextEditingController(
                text: mitraSettings.maximumPickUpDistance.toString());
            for (final garbageSetting in mitraSettings.garbageSettings) {
              priceController.add(
                  TextEditingController(text: garbageSetting.price.toString()));
              minimumPickUpController.add(TextEditingController(
                  text: garbageSetting.minimumQuantity.toString()));
            }
          }
          
          mitraSettings.maximumPickUpDistance = int.parse(distanceController.text);
          for (int i = 0; i < mitraSettings.garbageSettings.length; i++) {
            mitraSettings.garbageSettings[i].minimumQuantity =
                int.parse(minimumPickUpController[i].text);
            mitraSettings.garbageSettings[i].price =
                int.parse(priceController[i].text);
          }

          return buildAfterFetch(context);
        }
        return CircularProgressIndicator();
      },
    );
  }

  Widget buildAfterFetch(BuildContext context) {
    TextStyle buttonTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w900,
      fontSize: 12,
      color: Color(0xFFFFFFFF),
    );
    TextStyle sectionHeaderTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w900,
      fontSize: 15,
      color: Color(0xFF000000),
    );
    TextStyle normalTextStyle12px = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 13,
      color: Color(0xFF000000),
    );
    TextStyle normalTextStyle10px = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 11.5,
      color: Color(0xFF000000),
    );

    Widget scheduleButton = Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          dayButtonBuilder(
              "Mon", MediaQuery.of(context).size.width - 40, buttonTextStyle),
          dayButtonBuilder(
              "Tue", MediaQuery.of(context).size.width - 40, buttonTextStyle),
          dayButtonBuilder(
              "Wed", MediaQuery.of(context).size.width - 40, buttonTextStyle),
          dayButtonBuilder(
              "Thu", MediaQuery.of(context).size.width - 40, buttonTextStyle),
          dayButtonBuilder(
              "Fri", MediaQuery.of(context).size.width - 40, buttonTextStyle),
          dayButtonBuilder(
              "Sat", MediaQuery.of(context).size.width - 40, buttonTextStyle),
          dayButtonBuilder(
              "Sun", MediaQuery.of(context).size.width - 40, buttonTextStyle),
        ],
      ),
    );

    List<Widget> activePickUpScheduleChildrenWidget = [];
    activePickUpScheduleChildrenWidget.add(Text(
      "Pick Up Schedule",
      style: sectionHeaderTextStyle,
    ));
    activePickUpScheduleChildrenWidget.add(Row(
      children: <Widget>[
        Checkbox(
          value: isCreatePickUpSchedule,
          activeColor: Color(0xFF007E24),
          onChanged: (bool newValue) {
            setState(() {
              isCreatePickUpSchedule = newValue;
              if (!newValue) {
                mitraSettings.schedules.monday = true;
                mitraSettings.schedules.tuesday = true;
                mitraSettings.schedules.wednesday = true;
                mitraSettings.schedules.thursday = true;
                mitraSettings.schedules.friday = true;
                mitraSettings.schedules.saturday = true;
                mitraSettings.schedules.sunday = true;
              }
            });
          },
        ),
        Text(
          "Create Pick Up Schedule",
          style: normalTextStyle12px,
        ),
      ],
    ));
    if (isCreatePickUpSchedule) {
      activePickUpScheduleChildrenWidget.add(scheduleButton);
      activePickUpScheduleChildrenWidget.add(Text(
        _getScheduleText(),
        style: normalTextStyle12px,
      ));
    }

    Widget pickUpScheduleWidget = Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: activePickUpScheduleChildrenWidget,
      ),
    );

    List<Widget> garbageTypesChildrenWidget = [];
    garbageTypesChildrenWidget.add(Text(
      "Garbage Types",
      style: sectionHeaderTextStyle,
    ));
    for (int i = 0; i < mitraSettings.garbageSettings.length; i++) {
      var element = mitraSettings.garbageSettings[i];
      garbageTypesChildrenWidget.add(_garbageSettingBuilder(
          i, element, normalTextStyle12px, normalTextStyle10px));
    }
    Widget garbageTypesWidget = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: garbageTypesChildrenWidget,
      ),
    );

    Widget pickUpDistanceWidget = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Pickup Distance",
            style: sectionHeaderTextStyle,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 50),
                      child: Text(
                        "Maksimal",
                        style: normalTextStyle12px,
                      ),
                    ),
                    Flexible(
                      child: Container(
                        width: 30,
                        child: TextFormField(
                          controller: distanceController,
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ),
                    Text(
                      "Km",
                      style: normalTextStyle12px,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Text(
                  mitraSettings.getError(),
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    fontSize: 9,
                    color: Color(0xFFDC1714),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );

    Widget saveButtonWidget = Container(
        margin: EdgeInsets.only(top: 50, bottom: 30),
        child: Center(
            child: RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          onPressed: _saveSettings,
          child: Text(
            "Save",
            style: buttonTextStyle,
          ),
          color: Color(0xFF007E24),
        )));

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Center(
          child: Text(
            'Settings',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w900,
              fontSize: 20,
              color: Color(0xFF1D1D27),
            ),
          ),
        ),
      ),
      body: ListView(children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              pickUpScheduleWidget,
              Divider(),
              garbageTypesWidget,
              Divider(),
              pickUpDistanceWidget,
              saveButtonWidget,
            ],
          ),
        ),
      ]),
    );
  }

  void _saveSettings() async {
    setState(() {
      mitraSettings.maximumPickUpDistance = int.parse(distanceController.text);
      for (int i = 0; i < mitraSettings.garbageSettings.length; i++) {
        mitraSettings.garbageSettings[i].minimumQuantity =
            int.parse(minimumPickUpController[i].text);
        mitraSettings.garbageSettings[i].price =
            int.parse(priceController[i].text);
      }
    });

    List<String> errors = [];
    if (mitraSettings.getError() != "") {
      errors.add(mitraSettings.getError());
    }
    mitraSettings.garbageSettings.forEach((garbageSetting) {
      if (garbageSetting.getError() != "") {
        errors.add(garbageSetting.getError());
      }
    });

    if (errors.length == 0) {
      var _prefs = await SharedPreferences.getInstance();
      var token = _prefs.getString('token_mitra');
      var header = {
        'Authorization': 'Bearer $token',
        'Content-Type': 'application/json',
      };

      http
          .put(mitraSettingsUrl,
              headers: header, body: json.encode(mitraSettings.toJson()))
          .then((result) {
        var response = json.decode(result.body);
        if (response['error'] != null) {
          var errorMessage = response['error']['message'];
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Text(errorMessage),
                );
              });
        } else {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text("Save success!")));
        }
      });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text("Selesaikan masalah terlebih dahulu"),
            );
          });
    }
  }

  Widget dayButtonBuilder(
          String day, double parentWidth, TextStyle textStyle) =>
      Container(
        width: parentWidth / 8,
        child: RaisedButton(
          padding: EdgeInsets.all(0),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          onPressed: () {
            setState(() {
              if (day == "Mon") {
                mitraSettings.schedules.monday =
                    !mitraSettings.schedules.monday;
              } else if (day == "Tue") {
                mitraSettings.schedules.tuesday =
                    !mitraSettings.schedules.tuesday;
              } else if (day == "Wed") {
                mitraSettings.schedules.wednesday =
                    !mitraSettings.schedules.wednesday;
              } else if (day == "Thu") {
                mitraSettings.schedules.thursday =
                    !mitraSettings.schedules.thursday;
              } else if (day == "Fri") {
                mitraSettings.schedules.friday =
                    !mitraSettings.schedules.friday;
              } else if (day == "Sat") {
                mitraSettings.schedules.saturday =
                    !mitraSettings.schedules.saturday;
              } else if (day == "Sun") {
                mitraSettings.schedules.sunday =
                    !mitraSettings.schedules.sunday;
              }
            });
          },
          child: Text(day, style: textStyle),
          color: _isDaySelected(day) ? Color(0xFF007E24) : Color(0xFF909090),
        ),
      );

  Widget _garbageSettingBuilder(int index, GarbageSetting garbageSetting,
      TextStyle textStyleTitle, TextStyle textStyleSubTitle) {
    List<Widget> garbageSettingChildren = [];
    garbageSettingChildren.add(Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Checkbox(
                activeColor: Color(0xFF007E24),
                value: garbageSetting.selected,
                onChanged: (bool newValue) {
                  setState(() {
                    garbageSetting.selected = newValue;
                    if (!newValue) {
                      garbageSetting.price = 0;
                      garbageSetting.minimumQuantity = 0;
                    }
                  });
                },
              ),
              Text(
                garbageSetting.title,
                style: textStyleTitle,
              ),
            ],
          ),
        ),
        Expanded(
          child: Text(
            garbageSetting.getError(),
            style: TextStyle(
              fontFamily: "Roboto",
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal,
              fontSize: 9,
              color: Color(0xFFDC1714),
            ),
          ),
        ),
      ],
    ));
    if (garbageSetting.selected) {
      garbageSettingChildren.add(Container(
        margin: EdgeInsets.only(left: 48, bottom: 0, top: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Minimum Pickup",
              style: textStyleSubTitle,
            ),
            Flexible(
              child: Container(
                width: 30,
                child: TextFormField(
                  controller: minimumPickUpController[index],
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                ),
              ),
            ),
            Text(
              "${garbageSetting.quantityType}",
              style: textStyleSubTitle,
            ),
            Text(
              "Rp",
              style: textStyleSubTitle,
            ),
            Flexible(
              child: Container(
                width: 50,
                child: TextFormField(
                  controller: priceController[index],
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                ),
              ),
            ),
            Text(
              "/${garbageSetting.quantityType}",
              style: textStyleSubTitle,
            ),
          ],
        ),
      ));
    }

    return Container(
      child: Column(
        children: garbageSettingChildren,
      ),
    );
  }

  Future<MitraSettings> _getData() async {
    var _prefs = await SharedPreferences.getInstance();
    var token = _prefs.getString('token_mitra');
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    final response = await http.get(mitraSettingsUrl, headers: header);
    return MitraSettings.fromJson(json.decode(response.body));
  }

  bool _isDaySelected(String day) {
    if (day == "Mon") {
      return mitraSettings.schedules.monday;
    } else if (day == "Tue") {
      return mitraSettings.schedules.tuesday;
    } else if (day == "Wed") {
      return mitraSettings.schedules.wednesday;
    } else if (day == "Thu") {
      return mitraSettings.schedules.thursday;
    } else if (day == "Fri") {
      return mitraSettings.schedules.friday;
    } else if (day == "Sat") {
      return mitraSettings.schedules.saturday;
    } else if (day == "Sun") {
      return mitraSettings.schedules.sunday;
    }
    return false;
  }

  String _getScheduleText() {
    List<String> activeSchedule = [];
    if (mitraSettings.schedules.monday) {
      activeSchedule.add("Monday");
    }
    if (mitraSettings.schedules.tuesday) {
      activeSchedule.add("Tuesday");
    }
    if (mitraSettings.schedules.wednesday) {
      activeSchedule.add("Wednesday");
    }
    if (mitraSettings.schedules.thursday) {
      activeSchedule.add("Thursday");
    }
    if (mitraSettings.schedules.friday) {
      activeSchedule.add("Friday");
    }
    if (mitraSettings.schedules.saturday) {
      activeSchedule.add("Saturday");
    }
    if (mitraSettings.schedules.sunday) {
      activeSchedule.add("Sunday");
    }

    String result = "";
    if (activeSchedule.length == 0) {
      result = "No pickup";
    } else if (activeSchedule.length == 1) {
      result = "Pickup every " + activeSchedule[0];
    } else if (activeSchedule.length == 2) {
      result =
          "Pickup every " + activeSchedule[0] + " and " + activeSchedule[1];
    } else {
      result = "Pickup every " + activeSchedule[0];
      for (int i = 1; i < activeSchedule.length - 1; i++) {
        result += ", " + activeSchedule[i];
      }
      result += ", and " + activeSchedule[activeSchedule.length - 1];
    }

    return result;
  }
}
