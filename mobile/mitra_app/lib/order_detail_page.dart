import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'models/order_detail.dart';
import 'adjust_order_page.dart';
import 'login_provider.dart';
import 'const.dart' show BASE_URL, ORDER_STATUS_SCHEDULED, ORDER_STATUS_PICKED_UP, 
ORDER_STATUS_COMPLETED, ORDER_STATUS_CANCELLED_BY_MITRA, ORDER_STATUS_CANCELLED_BY_CUSTOMER;

const orderDetailPrefixUrl = '$BASE_URL/order/';
const pickUpOrderPrefixUrl = '$BASE_URL/order/pickup/';
const completeOrderPrefixUrl = '$BASE_URL/order/complete/';
const cancelOrderPrefixUrl = '$BASE_URL/order/cancel/';

class OrderDetailPage extends StatefulWidget {
  final int id;

  OrderDetailPage(this.id);

  @override
  State<StatefulWidget> createState() => OrderDetailPageState(id);
}

class OrderDetailPageState extends State<OrderDetailPage> {
  final int id;
  OrderDetail orderDetail;
  Completer<GoogleMapController> _controller;
  Future<OrderDetail> orderDetailFuture;

  OrderDetailPageState(this.id);

  @override
  void initState() {
    _controller = Completer();
    orderDetailFuture = _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<OrderDetail>(
      future: orderDetailFuture,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          orderDetail = snapshot.data;
          return buildAfterFetch(context);
        }
        return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  Widget buildAfterFetch(BuildContext context) {
    TextStyle titleTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w900,
      fontSize: 20,
      color: Color(0xFF1D1D27),
    );
    TextStyle detailTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 13,
      color: Color(0xFF000000),
    );
    TextStyle pickUpDetailTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 11,
      color: Color(0xFF1D1D1D),
    );

    String status = orderDetail.status;
    Widget headerCardWidget = Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 0),
      child: Text(
        'Status : ${status}',
        style: TextStyle(
          fontFamily: 'Roboto',
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.w500,
          fontSize: 14,
          color: Color(0xFF000000),
        ),
      ),
    );
    Widget adjustButton = Container();
    Widget mapWidget = Container();
    Widget cancelButton = RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      onPressed: _changeTo("Cancel"),
      child: Text(
        "Cancel", 
        style: TextStyle(
          fontFamily: "Roboto",
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.w900,
          fontSize: 15,
          color: Color(0xFFFFFFFF),
        ),
      ),
      color: Color(0xFFDC1714),
    );
    Widget buttonWidget = Container();
    Widget notesWidget = Container();
    Widget phoneWidget = Center(
      child: Container(
        margin: EdgeInsets.only(top: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 120,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.horizontal(left: Radius.circular(10)),
                ),
                onPressed: () => launch("tel:${orderDetail.phone}"),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.call,
                      color: Colors.white,
                    ),
                    Text(
                      "Call", 
                      style: TextStyle(
                        fontFamily: "Roboto",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ],
                ),
                color: Color(0xFF007E24),
              ),
            ),
            VerticalDivider(
              width: 1,
              color: Color(0xFFF2F2F2),
            ),
            Container(
              width: 120,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.horizontal(right: Radius.circular(10)),
                ),
                onPressed: () => launch("sms:${orderDetail.phone}"),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.mail,
                      color: Colors.white,
                    ),
                    Text(
                      "Message", 
                      style: TextStyle(
                        fontFamily: "Roboto",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ],
                ),
                color: Color(0xFF007E24),
              ),
            ),
          ],
        ),
      ),
    );
    if (status == ORDER_STATUS_SCHEDULED) {
      buttonWidget = activeOrderButtonBuilder("Pick Up", cancelButton);
      if (orderDetail.notes != "") {  
        notesWidget = Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Divider(),
              Container(
                margin: EdgeInsets.only(left: 20, top: 10),
                child: Text("Notes:", style: detailTextStyle,)
              ),
              Container(
                margin: EdgeInsets.only(left: 40, top: 10),
                child: Text("\"${orderDetail.notes}\"", style: detailTextStyle,),
              ),
            ],
          ),
        );
      }
    } else if (status == ORDER_STATUS_PICKED_UP) {
      buttonWidget = activeOrderButtonBuilder("Complete", cancelButton);
      adjustButton = Center(
        child: Container(
          width: 100,
          margin: EdgeInsets.only(left: 20, right: 20, top: 40, bottom: 10),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(),
            ),
            onPressed: _adjust,
            child: Row(
              children: <Widget>[
                Icon(Icons.settings),
                Text(
                  "Adjust", 
                  style: TextStyle(
                    fontFamily: "Roboto",
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                    color: Color(0XFF000000),
                  ),
                ),
              ],
            ),
            color: Colors.white,
          ),
        ),
      );
      mapWidget = GoogleMap(
        onMapCreated: (controller) {
          _controller.complete(controller);
        },
        initialCameraPosition: CameraPosition(
          target: LatLng(orderDetail.latitude, orderDetail.longitude),
          zoom: 15.0,
        ),
        markers: Set.from(
          [Marker(
            markerId: MarkerId("Customer Location"),
            position: LatLng(orderDetail.latitude, orderDetail.longitude),
            infoWindow: InfoWindow(
              title: "Pick Up Location",
              snippet: "Pick Up in This Location",
            ),
            icon: BitmapDescriptor.defaultMarker,
          ),]
        ),
      );
    } else if (status == ORDER_STATUS_COMPLETED) {
      headerCardWidget = Container(
        margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              'Completed',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: Color(0xFF007E24),
              ),
            ),
            Text(
              '${orderDetail.time}',
              style: detailTextStyle
            ),
          ],
        )
      );
      phoneWidget = Container();
    } else if (status == ORDER_STATUS_CANCELLED_BY_MITRA || status ==ORDER_STATUS_CANCELLED_BY_CUSTOMER) {
      headerCardWidget = Container(
        margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Cancelled',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Color(0xFFDC1714),
                  ),
                ),
                Text(
                  '${orderDetail.time}',
                  style: detailTextStyle
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "${orderDetail.reason}",
                style: detailTextStyle
              ),
            ),
          ],
        )
      );
      phoneWidget = Container();
    }

    List<Widget> garbageDetails = [];
    for (GarbageDetail garbageDetail in orderDetail.garbageDetail) {
      garbageDetails.add(Container(
        margin: EdgeInsets.only(top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('${garbageDetail.title}', style: detailTextStyle,),
            Text('${garbageDetail.quantity} ${garbageDetail.quantityType}', style: detailTextStyle,),
          ],
        ),
      ));
    }

    Widget cardWidget = Container(
      margin: EdgeInsets.only(top: 20, left: 20, right: 20),
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            headerCardWidget,
            Divider(),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(child: Text('${orderDetail.name}', style: detailTextStyle)),
                      Expanded(child: Text('${orderDetail.distance} Km', style: detailTextStyle, textAlign: TextAlign.right,)),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text("${orderDetail.address}", style: detailTextStyle,)
                  ),
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Garbage", style: detailTextStyle,),
                  Column(
                    children: garbageDetails,
                  ),
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Price to Pay", style: detailTextStyle,),
                  Text("Rp ${orderDetail.price}", style: detailTextStyle,),
                ],
              ),
            ),
            notesWidget,
            adjustButton,
          ],
        ),
      ),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Color(0x5F000000),
            offset: Offset(0.0, 5.0),
            blurRadius: 5.0,
          ),
        ]
      ),
    );

    if (orderDetail.status == ORDER_STATUS_PICKED_UP) {
      garbageDetails = [];
      for (GarbageDetail garbageDetail in orderDetail.garbageDetail) {
        garbageDetails.add(Container(
          margin: EdgeInsets.only(left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('${garbageDetail.title}', style: pickUpDetailTextStyle,),
              Text('${garbageDetail.quantity} ${garbageDetail.quantityType}', style: pickUpDetailTextStyle,),
            ],
          ),
        ));
      }
      adjustButton = Container(
        width: 65,
        height: 30,
        margin: EdgeInsets.only(top: 15, bottom: 15),
        child: RaisedButton(
          padding: EdgeInsets.symmetric(horizontal: 2),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(),
          ),
          onPressed: _adjust,
          child: Center(
            child: Row(
              children: <Widget>[
                Icon(Icons.settings),
                Text(
                  "Adjust", 
                  style: TextStyle(
                    fontFamily: "Roboto",
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    fontSize: 11,
                    color: Color(0XFF000000),
                  ),
                ),
              ],
            ),
          ),
          color: Colors.white,
        ),
      );
      if (orderDetail.notes != "") {  
        notesWidget = Container(
          margin: EdgeInsets.only(left: 20, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Notes:", style: detailTextStyle,),
              Container(
                margin: EdgeInsets.only(left: 20),
                child: Text("\"${orderDetail.notes}\"", style: pickUpDetailTextStyle,),
              ),
            ],
          ),
        );
      }

      Widget cardWidget = Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        width: MediaQuery.of(context).size.width,
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "${orderDetail.name}",
                      style: TextStyle(
                        fontFamily: "Roboto",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                        color: Color(0xFF000000),
                      ),
                    ),
                    adjustButton,
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(child: Text("${orderDetail.address}", style: pickUpDetailTextStyle,)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width: 45,
                          child: RaisedButton(
                            shape: CircleBorder(),
                            onPressed: () => launch("tel:${orderDetail.phone}"),
                            child: Center(
                              child: Icon(
                                Icons.call,
                                size: 15,
                                color: Colors.white,
                              ),
                            ),
                            color: Color(0xFF007E24),
                          ),
                        ),
                        Container(
                          width: 45,
                          child: RaisedButton(
                            shape: CircleBorder(),
                            onPressed: () => launch("sms:${orderDetail.phone}"),
                            child: Center(
                              child: Icon(
                                Icons.mail,
                                size: 15,
                                color: Colors.white,
                              ),
                            ),
                            color: Color(0xFF007E24),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Garbage", style: detailTextStyle,),
                    Column(
                      children: garbageDetails,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Price to Pay", style: detailTextStyle,),
                    Text("Rp ${orderDetail.price}", style: detailTextStyle,),
                  ],
                ),
              ),
              notesWidget,
              buttonWidget
            ],
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      );
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('Order Detail', style: titleTextStyle,),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                Navigator.of(context).pop();
              },
          ),
        ),
        body: Container(
          height: double.maxFinite,
          child: Stack(
            children: <Widget>[
              Positioned(child: mapWidget),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  cardWidget,
                ],
              ),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('Order Detail', style: titleTextStyle,),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                Navigator.of(context).pop();
              },
          ),
        ),
        body: ListView(
          children: <Widget>[
            cardWidget,
            phoneWidget,
            buttonWidget,
          ],
        ),
      );
    }

  }

  Widget activeOrderButtonBuilder(String status, Widget cancelButton) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          cancelButton,
          RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            onPressed: _changeTo("$status"),
            child: Text(
              "$status", 
              style: TextStyle(
                fontFamily: "Roboto",
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w900,
                fontSize: 15,
                color: Color(0xFFFFFFFF),
              ),
            ),
            color: Color(0xFF007E24),
          ),
        ],
      ),
    );
  }

  void _adjust() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => AdjustPage(orderDetail.id)))
                         .then((onValue) {
                           setState(() { 
                            orderDetailFuture = _getData();
                          });
                         });
  }

  Function _changeTo(String status) {
    var token = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    if (status == "Pick Up") {
      return () {
        http.post(pickUpOrderPrefixUrl + id.toString(), headers: header).then((response) {
          setState(() { 
            orderDetailFuture = _getData();
          });
        });
      };
    } else if (status == "Complete") {
      return () {
        http.post(completeOrderPrefixUrl + id.toString(), headers: header).then((response) {
          setState(() { 
            orderDetailFuture = _getData();
          });
        });
      };
    } else if (status == "Cancel") {
      TextEditingController textEditingController = TextEditingController();
      return () {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
              title: Text("Cancel Order?"),
              content: Column(
                children: <Widget>[
                  Text(
                    "Tekan 'Ya' jika anda yakin ingin membatalkan order"
                  ),
                  TextField(
                    controller: textEditingController,
                    decoration: InputDecoration(hintText: "Cancellation Reason"),
                  ),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text("Tidak"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text("Ya"),
                  onPressed: () {
                    http.post(cancelOrderPrefixUrl + id.toString(), headers: header, body: json.encode({
                      'reason': textEditingController.text,
                    })).then((response) {
                      setState(() { 
                        orderDetailFuture = _getData();
                      });
                    });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
        );
      };
    }
  }

  Future<OrderDetail> _getData() async {
    var token = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    final response = await http.get(orderDetailPrefixUrl + id.toString(), headers: header);
    return OrderDetail.fromJson(json.decode(response.body));
  }
}
