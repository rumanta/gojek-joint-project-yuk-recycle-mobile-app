import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'const.dart' show BASE_URL, BASE_API;

var updateFCMTokenUrl = "$BASE_API/api/v1/notification/token";
class LoginProvider {

  LoginProvider();

  static SharedPreferences _prefs;
  static Client client;
  static FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  Future init() async {
    _prefs = await SharedPreferences.getInstance();
    client = new Client();
  }

  static sendFCMTokenToBackend(String tokenFcm) {
    var token = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    client.put(updateFCMTokenUrl, headers: header, body: json.encode({
      'token': tokenFcm,
    }));
  }

  static setAsOnlineOrOfflineState(bool online) {
    if (online) {
      _firebaseMessaging.getToken().then((token) {
        sendFCMTokenToBackend(token);
      });
    } else {
      sendFCMTokenToBackend("a");
    }
  }

  static login(String phone, String password) async {

    final loginUrl = BASE_URL + '/login';
    Map body = {
      "phone": phone,
      "password": password,
    };
  
    await client.post(loginUrl, body: json.encode(body)).then((response) {
      Map<String, dynamic> responseBody = json.decode(response.body);

      if (response.statusCode == HttpStatus.ok) {
        _prefs.setString('token_mitra', responseBody['token']['value']);
        _prefs.setBool('isLoggedIn', responseBody['status'] == 'logged_in');
        _firebaseMessaging.getToken().then((token) {
          sendFCMTokenToBackend(token);
        });
      } else {
        print(responseBody['error']['message']);
      }
    });

    return _prefs.get('isLoggedIn');
  }

  static checkIsLoggedIn() {

    if (_prefs.getBool('isLoggedIn') == null) {
      _prefs.setBool('isLoggedIn', false);
    }

    return _prefs.getBool('isLoggedIn');
  }

  static getTokenMitra() {

    return _prefs.getString('token_mitra');
  }

  static logout() {
    sendFCMTokenToBackend("a");
    _prefs.setBool('isLoggedIn', false);
    _prefs.remove('mitra_token');
  }

}




