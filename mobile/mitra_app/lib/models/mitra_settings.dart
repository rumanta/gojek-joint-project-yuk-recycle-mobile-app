import '../util.dart';

class MitraSettings {
  final Schedules schedules;
  final List<GarbageSetting> garbageSettings;
  int maximumPickUpDistance;

  MitraSettings({this.schedules, this.garbageSettings, this.maximumPickUpDistance});

  factory MitraSettings.fromJson(Map<String, dynamic> data) {
    var garbageSettings = data['garbage_settings'] as List;
    return MitraSettings(
      schedules: Schedules.fromJson(data['schedules']),
      garbageSettings: garbageSettings.map((setting) => GarbageSetting.fromJson(setting)).toList(),
      maximumPickUpDistance: getOrDefaultValue(data['maximum_pick_up_distance'], 0),
    );
  }

  Map<String, dynamic> toJson() => {
    'schedules': schedules.toJson(),
    'garbage_settings': garbageSettings.map((setting) => setting.toJson()).toList(),
    'maximum_pick_up_distance': maximumPickUpDistance,
  };

  String getError() {
    if (maximumPickUpDistance == 0) {
      return "Jarak harus di atas 0 km";
    } else if (maximumPickUpDistance >= 30) {
      return "Jarak harus di bawah 30 km";
    }
    return "";
  }
}

class Schedules {
  bool sunday;
  bool monday;
  bool tuesday;
  bool wednesday;
  bool thursday;
  bool friday;
  bool saturday;

  Schedules({this.sunday, this.monday, this.tuesday, this.wednesday, this.thursday, this.friday, this.saturday});

  factory Schedules.fromJson(Map<String, dynamic> data) {
    return Schedules(
      sunday: getOrDefaultValue(data['sunday'], false),
      monday: getOrDefaultValue(data['monday'], false),
      tuesday: getOrDefaultValue(data['tuesday'], false),
      wednesday: getOrDefaultValue(data['wednesday'], false),
      thursday: getOrDefaultValue(data['thursday'], false),
      friday: getOrDefaultValue(data['friday'], false),
      saturday: getOrDefaultValue(data['saturday'], false),
    );
  }

  Map<String, dynamic> toJson() => {
    'sunday': sunday,
    'monday': monday,
    'tuesday': tuesday,
    'wednesday': wednesday,
    'thursday': thursday,
    'friday': friday,
    'saturday': saturday,
  };

  bool isAllSelected() => sunday && monday && tuesday && wednesday && thursday && friday && saturday;
}

class GarbageSetting {
  bool selected;
  String title;
  int minimumQuantity;
  int price;
  String quantityType;

  GarbageSetting({this.selected, this.title, this.minimumQuantity, this.price, this.quantityType});

  factory GarbageSetting.fromJson(Map<String, dynamic> data) {
    return GarbageSetting(
      selected: getOrDefaultValue(data['selected'], false),
      title: getOrDefaultValue(data['title'], ""),
      minimumQuantity: getOrDefaultValue(data['minimum_quantity'], 0),
      price: getOrDefaultValue(data['price'], 0),
      quantityType: getOrDefaultValue(data['quantity_type'], ""),
    );
  }

  Map<String, dynamic> toJson() => {
    'selected': selected,
    'title': title,
    'minimum_quantity': minimumQuantity,
    'price': price,
  };

  String getError() {
    if (selected && price < 500) {
      return "Harga tidak boleh di bawah 500";
    }
    return "";
  }
}
