import '../util.dart';

class OrderList {
  final List<Order> orders;

  OrderList({this.orders});

  factory OrderList.fromJson(Map<String, dynamic> data) {
    var orderList = getOrDefaultValue(data['data'], []) as List;
    return OrderList(
      orders: orderList.map((order) => Order.fromJson(order)).toList(),
    );
  }
}

class Order {
  int id;
  String name;
  String time;
  String status;
  String scheduledTime;
  String reason;

  Order({this.id, this.name, this.time, this.status, this.scheduledTime, this.reason});

  factory Order.fromJson(Map<String, dynamic> data) {
    return Order(
      id :getOrDefaultValue(data['id'], 0),
      name: getOrDefaultValue(data['name'], ''),
      time: getOrDefaultValue(data['time'], ''),
      status: getOrDefaultValue(data['status'], ''),
      scheduledTime: getOrDefaultValue(data['scheduled_time'], ''),
      reason: getOrDefaultValue(data['reason'], ''),
    );
  }
}
