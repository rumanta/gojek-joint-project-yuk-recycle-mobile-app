import 'package:http/http.dart' as http;
import 'package:http/http.dart' show Client;
import 'package:mitra_app/adjust_order_page.dart';
import 'dart:convert';

class ApiProvider {
  Client client = Client();

  Future<String> adjustOrderHttpRequest(token, id, List orderItem) async {
    Map<String,String> headers = {
      'Content-Type' : 'application/json',
      'Authorization': 'Bearer $token',
    };
    
    List orderDetail = [];

    orderItem.forEach((garbageDetail) {
      print(garbageDetail.id);
      orderDetail.add({
        "garbage_type_id": int.parse(garbageDetail.id),
        "quantity": garbageDetail.quantity,
      });
    });
    var body = {
      "order_detail": orderDetail
    };
    var json = jsonEncode(body);
    print(body.toString());
    final response = await client.put(
      "http://152.118.201.222:21414/api/v1/mitra/order/adjust/" + id.toString(),
      headers: headers,
      body: json,
    );

    if(response.statusCode == 200) {
      var result = jsonDecode(response.body);
      return "Success";
    } else {
      print("Adjust Order Failed");
      return "Failed";
    }
  }

  Future<dynamic> orderDetailHttpRequest(token, id) async {
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization': 'Bearer $token',
    };
    final response = await client.get(
      "http://152.118.201.222:21414/api/v1/mitra/order/" + id.toString(),
      headers: headers,
    );

    if(response.statusCode == 200) {
      var result = jsonDecode(response.body);
      var garbageDetail = result['garbage_detail'];
      return garbageDetail;
    }
  }
  Future<OrderItems> garbageTypeHttpRequest(token) async {
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization': 'Bearer $token',
    };
    print(token);
    final response = await client.get(
      "http://152.118.201.222:21414/api/v1/mitra/garbage-types",
      headers: headers,
    );

    if (response.statusCode == 200) {
      var result = jsonDecode(response.body);
      var garbageList = result['garbage_types'];
      orderItem = OrderItems.fromJson(garbageList);
      return orderItem;

    } else {
      print("Get Garbage Detail Fails, Cannot Return Garbage List");
    }
  }

}