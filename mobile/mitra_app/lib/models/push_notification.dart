import 'dart:convert';
import '../util.dart';

class PushNotification {
  final DataMessage dataMessage;

  PushNotification({this.dataMessage});

  factory PushNotification.fromJson(Map<String, dynamic> data) {
    return PushNotification(
      dataMessage: DataMessage.fromJson(data['data']),
    );
  }
}

class DataMessage {
  String type;
  int token;
  int orderId;
  String name;
  String address;
  String distance;
  List<GarbageDetail> garbageDetail;
  int price;
  String notes;

  DataMessage({this.type, this.token, this.orderId, this.name, this.address, this.distance, this.garbageDetail, this.price, this.notes});

  factory DataMessage.fromJson(dynamic data) {
    var garbageDetails = [];
    if (data['garbage_detail'] != null) {
      garbageDetails = json.decode(data['garbage_detail']) as List;
    }
    return DataMessage(
      type: getOrDefaultValue(data['type'], ""),
      token: int.parse(getOrDefaultValue(data['token'], "0")),
      orderId: int.parse(getOrDefaultValue(data['order_id'], "0")),
      name: getOrDefaultValue(data['name'], ""),
      address: getOrDefaultValue(data['address'], ""),
      distance: double.parse(getOrDefaultValue(data['distance'], "0.0")).toStringAsFixed(2),
      garbageDetail: garbageDetails.map((garbageDetail) => GarbageDetail.fromJson(garbageDetail)).toList(),
      price: int.parse(getOrDefaultValue(data['price'], "0")),
      notes: getOrDefaultValue(data['notes'], ""),
    );
  }
}

class GarbageDetail {
  String title;
  int quantity;
  String quantityType;

  GarbageDetail({this.title, this.quantity, this.quantityType});

  factory GarbageDetail.fromJson(dynamic data) {
    return GarbageDetail(
      title: data['title'],
      quantity: data['quantity'],
      quantityType: data['quantity_type'],
    );
  }
}
