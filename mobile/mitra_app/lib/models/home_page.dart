import '../util.dart';

class HomepageData {
  bool online;
  String mitraUsername;
  int totalOrder;
  int activeOrder;
  int pickedOrder;
  int completedOrder;
  int spending;

  HomepageData({this.online, this.mitraUsername, this.totalOrder, this.activeOrder, this.pickedOrder, this.completedOrder, this.spending});

  factory HomepageData.fromJson(Map<String, dynamic> data) {
    Map<String, dynamic> mitraHomeData = data['mitra_home'];
    Map<String, dynamic> statistic = mitraHomeData['weekly_statistic'];
    return HomepageData(
      online: getOrDefaultValue(mitraHomeData['is_online'], false),
      mitraUsername: getOrDefaultValue(mitraHomeData['mitra_name'], ""),
      totalOrder: getOrDefaultValue(statistic['order_total'], 0),
      activeOrder: getOrDefaultValue(statistic['active_order'], 0),
      pickedOrder: getOrDefaultValue(statistic['picked_order'], 0),
      completedOrder: getOrDefaultValue(statistic['completed_order'], 0),
      spending: getOrDefaultValue(statistic['spending'], 0),
    );
  }

  String getOnlineStatusText() {
    return online ? 'Ready to Pick Up!' : 'I\'m not Available';
  }
}
