import 'package:flutter/material.dart';
import 'package:mitra_app/login_provider.dart';
import 'navigation_bar.dart';

class PhoneNumberFieldValidator {
  static String validate(String value) {
    final RegExp numberRegExp = RegExp(r'^[0-9]*$');

    if (value.isEmpty) {
      return 'Silahkan isi nomor ponsel Anda';
    } else if (!numberRegExp.hasMatch(value)) {
      return 'Nomor ponsel tidak boleh mengandung huruf';
    }

    return null;
  }
}

class PasswordFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Silahkan isi password Anda' : null;
  }
}

final colorBlue = Color(0xFF293542);

class LoginPageMitra extends StatefulWidget {
  @override
  State createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPageMitra> {

  final _formKey = GlobalKey<FormState>();
  static final _phoneValue = new TextEditingController();
  static final _passwordValue = new TextEditingController();
  Future<bool> _loginState;

  static var _loginBtnState = 0;

  Future<bool> getLoginState() async {
    bool loginState = LoginProvider.checkIsLoggedIn();

    return loginState;
  }

  @override
  void initState() {
    _loginState = getLoginState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    void _resetLoginForm() {
      _phoneValue.clear();
      _passwordValue.clear();

      setState(() {
        _loginBtnState = 0;
      });
    }

    void _showErrorDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('Login Failed'),
            content: new Text('Username and Password does not match!'),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                }
              ),
            ],
          );
        },
      );
    }

    void _handlePressLoginBtn() {
      setState(() {
        _loginBtnState = 1;
      });

      LoginProvider.login(_phoneValue.text, _passwordValue.text).then((loggedIn) {
        if (loggedIn == true) {
          _resetLoginForm();
          Navigator.of(context).pushReplacementNamed('/home');
        } else {
          setState(() {
            _loginBtnState = 0;
          });
          _showErrorDialog();
        }
      });
    }

    Widget _buildLogo = new Container(
      padding: const EdgeInsets.only(bottom: 10),
      child: new Image.asset(
        'assets/yuk_recycle_logo.png', scale: 3.0, width: 200.0, height: 100.0
      ),
    );

    var _buildFormPhoneNumber = Row (
      children: [
        Container(
          padding: const EdgeInsets.only(right: 10),
          child: new Image.asset(
            'assets/phone_icon.png', scale: 3.0, width: 25.0, height: 25.0
          ),
        ),
        Expanded(
          child: new TextFormField(
            decoration: new InputDecoration(
              hintText: 'Nomor Ponsel',
            ),
            controller: _phoneValue,
            validator: PhoneNumberFieldValidator.validate,
            keyboardType: TextInputType.number,
          ),
        )
      ],
    );

    var _buildFormPassword = Row (
      children: [
        Container(
          padding: const EdgeInsets.only(right: 10),
          child: new Image.asset(
            'assets/lock_icon.png', scale: 3.0, width: 25.0, height: 25.0
          ),
        ),
        Expanded(
          child: new TextFormField(
            decoration: new InputDecoration(
              hintText: 'Password',
            ),
            obscureText: true,
            keyboardType: TextInputType.text,
            controller: _passwordValue,
            validator: PasswordFieldValidator.validate,
          ),
        ),
      ],
    );

    var _buildFormButton = (context) {
      if (_loginBtnState == 0) {
        return Container(
          padding: EdgeInsets.only(top: 38.0),          
          child: ButtonTheme(
            buttonColor: Color(0xFFF2F2F2),
            minWidth: 400.0,
            height: 42,
            child: RaisedButton(
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _handlePressLoginBtn();
                }
              },
              child: Text('Login',
                style: TextStyle(color: Color(0xFF293542), fontSize: 20.0),
              ),
            ),
          ),
        );
      } else {
        return Container(
          padding: EdgeInsets.only(top: 30.0),
          child: CircularProgressIndicator()
        );
      }
    };

    Form loginForm(BuildContext context, snapshot) => new Form(
      key: _formKey,
      child: new Theme(
        data: ThemeData(
          brightness:Brightness.dark,
          inputDecorationTheme: new InputDecorationTheme(
            hintStyle: TextStyle(
              color: Colors.grey, fontSize: 14,
            )
          )
        ),
        child: Container(
          padding: const EdgeInsets.only(top: 60.0, left: 40, right: 40),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _buildFormPhoneNumber,
              _buildFormPassword,
              _buildFormButton(context),
            ]
          ),
        ),
      )
    );

    return new FutureBuilder<bool> (
      future: _loginState,
      builder: (context,snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            return new Container();
          case ConnectionState.waiting:
            return CircularProgressIndicator();
          case ConnectionState.done:
            if (snapshot.hasData) {
              var loginState = snapshot.data;
              var loginPage = new Scaffold(
                  backgroundColor: colorBlue,
                  body: new Stack (
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _buildLogo,
                          loginForm(context, snapshot),
                        ],
                      )
                    ]
                    )
                );
              if (loginState) {
                // Navigator.of(context).pushReplacementNamed("/home");
                return MitraAppNavBar();
              } else {
                return loginPage;
              }
            } else {
              return Container(child: Text('Error..'));
            }
          break;
          default:
            return Container();
        }
      },
    );
  }
}
