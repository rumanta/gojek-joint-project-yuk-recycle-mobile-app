const BASE_API = 'http://152.118.201.222:21414';
const BASE_URL = BASE_API + '/api/v1/mitra';

const ORDER_STATUS_SCHEDULED = "Scheduled";
const ORDER_STATUS_PICKED_UP = "Picked up";
const ORDER_STATUS_COMPLETED = "Completed";
const ORDER_STATUS_CANCELLED_BY_MITRA = "Cancelled by Mitra";
const ORDER_STATUS_CANCELLED_BY_CUSTOMER = "Cancelled by Customer";
