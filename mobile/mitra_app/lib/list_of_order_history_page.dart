import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'models/order_list.dart';
import 'order_detail_page.dart';
import 'login_provider.dart';
import 'const.dart' show BASE_URL, ORDER_STATUS_SCHEDULED, ORDER_STATUS_PICKED_UP, 
ORDER_STATUS_COMPLETED, ORDER_STATUS_CANCELLED_BY_MITRA, ORDER_STATUS_CANCELLED_BY_CUSTOMER;

const orderHistoryUrl = '$BASE_URL/order/history';

class OrderHistoryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => OrderHistoryPageState();  
}

class OrderHistoryPageState extends State<OrderHistoryPage> {

  OrderList orderList;
  Future<OrderList> orderListFuture;

  @override
  void initState() {
    orderListFuture = _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<OrderList>(
      future: orderListFuture,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          orderList = snapshot.data;
          return buildAfterFetch(context);
        }
        return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  Widget buildAfterFetch(BuildContext context) {
    TextStyle nameTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 13,
      color: Color(0xFF1D1D27),
    );
    TextStyle statusCompletedTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 11,
      color: Color(0xFF007E24),
    );
    TextStyle statusCancelledTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 11,
      color: Color(0xFFDC1714),
    );
    TextStyle timeTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.italic,
      fontWeight: FontWeight.w500,
      fontSize: 9,
      color: Color(0xFF1D1D27),
    );

    List<Widget> orderHistories = [];
    for (int i = 0; i < orderList.orders.length; i++) {
      var order = orderList.orders[i];

      Widget statusWidget;
      Widget orderLogoWidget;
      if (order.status == ORDER_STATUS_COMPLETED) {
        statusWidget = Text("${order.status}", style: statusCompletedTextStyle,);
        orderLogoWidget = Container(
          margin: EdgeInsets.only(right: 20, bottom: 10, left: 10, top: 5),
          child: const Icon(Icons.check, color: Color(0xFF007E24),),
        );
      } else {
        String reason = order.reason == "" ? "" : " - ${order.reason}";
        statusWidget = Text("${order.status}$reason", style: statusCancelledTextStyle,);
        orderLogoWidget = Container(
          margin: EdgeInsets.only(right: 20, bottom: 10, left: 10, top: 5),
          child: const Icon(Icons.cancel, color: Color(0xFFDC1714),),
        );
      }

      orderHistories.add(ListTile(
        title: Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  orderLogoWidget,
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 5),
                          child: Text(order.name, style: nameTextStyle,),
                        ),
                        statusWidget,
                        Container(
                          margin: EdgeInsets.only(top: 3),
                          child: Text(order.time, style: timeTextStyle,)
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Divider(),
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => OrderDetailPage(order.id)))
                      .then((onValue) {
                        setState(() { 
                          orderListFuture = _getData();
                        });
                      });
        },
      ));
    }

    if (orderHistories.length == 0) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/no_order_history.png"),
          ],
        ),
      );
    }

    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 15),
          child: Column(
            children: orderHistories,
          ),
        ),
      ],
    );
  }

  Future<OrderList> _getData() async {
    var token = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    final response = await http.get(orderHistoryUrl, headers: header);
    return OrderList.fromJson(json.decode(response.body));
  }
}
