import 'package:flutter/material.dart';
import 'dart:convert';
import 'login_provider.dart';
import 'models/home_page.dart';
import 'package:http/http.dart' as http;
import 'const.dart' show BASE_URL;

const homePageUrl = '$BASE_URL/home';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _HomePageHelper(),
    );
  }
}
class _HomePageHelper extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageHelperState();
}

class _HomePageHelperState extends State<_HomePageHelper> {
  Future<HomepageData> homePageDatafuture;
  HomepageData homepageData;
  bool alreadyFetch = false;

  @override
  void initState() {
    homePageDatafuture = _getData();
    super.initState();
  }

  Future<HomepageData> _getData() async {
    var token = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    final response = await http.get(homePageUrl, headers: header);
    return HomepageData.fromJson(json.decode(response.body));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<HomepageData>(
      future: homePageDatafuture,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (!alreadyFetch) {
            alreadyFetch = true;
            homepageData = snapshot.data;
          }
          return buildAfterFetch(context);
        }
        return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  Widget buildAfterFetch(BuildContext context) {
    TextStyle textStyleStatisticDetailHeader = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 13,
      color: Color(0xFF1D1D1D),
    );
    TextStyle textStyleStatisticDetailName = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 12,
      color: Color(0xFF1D1D1D),
    );
    TextStyle textStyleStatisticDetailNominal = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      fontSize: 12,
      color: Color(0xFF000000),
    );
    TextStyle textStyleStatisticFooter = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 13,
      color: Color(0xFF000000),
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 5),
              child: ImageIcon(
                AssetImage('assets/home.png'),
                color: Color(0xFF1D1D1D),
              ),
            ),
            Text("Home", style: TextStyle(
              fontFamily: 'Roboto',
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w900,
              fontSize: 20,
              color: Color(0xFF1D1D27),
            ),),
          ],
        ),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 20, bottom: 10),
              child: Text(
                'Halo, ${homepageData.mitraUsername}!',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 20,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w900,
                  color: Color(0xFF1D1D27),
                ),
              ),
            ),
            Divider(height: 5,),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    homepageData.getOnlineStatusText(),
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.normal,
                      fontSize: 14,
                      color: Color(0xFF000000),
                    ),
                  ),
                  Switch(
                    value: homepageData.online,
                    activeColor: Color(0xFF009688),
                    onChanged: (bool newValue) {
                      setState(() {
                        homepageData.online = newValue;
                        LoginProvider.setAsOnlineOrOfflineState(homepageData.online);
                      });
                    },
                  )
                ],
              ),
            ),
            Divider(height: 5,),
            Container(
              margin: EdgeInsets.only(top: 50),
              width: MediaQuery.of(context).size.width,
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 0),
                      child: Text(
                        'Weekly\'s Statistic',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          color: Color(0xFF000000),
                        ),
                      ),
                    ),
                    Divider(),
                    Container(
                      margin: EdgeInsets.only(left: 40, right: 40, top: 10, bottom: 50),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Order Report', style: textStyleStatisticDetailHeader),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Scheduled', style: textStyleStatisticDetailName,),
                                Text('${homepageData.activeOrder}', style: textStyleStatisticDetailNominal,),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Picked Up', style: textStyleStatisticDetailName,),
                                Text('${homepageData.pickedOrder}', style: textStyleStatisticDetailNominal,),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Completed', style: textStyleStatisticDetailName,),
                                Text('${homepageData.completedOrder}', style: textStyleStatisticDetailNominal,),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Spendings', style: textStyleStatisticFooter,),
                          Text('Rp ${homepageData.spending}', style: textStyleStatisticFooter,),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color(0x5F000000),
                    offset: Offset(0.0, 5.0),
                    blurRadius: 5.0,
                  ),
                ]
              ),
            ),
          ],
        )
      ),
    );
  }
}
