import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'models/order_list.dart';
import 'order_detail_page.dart';
import 'login_provider.dart';
import 'const.dart' show BASE_URL, ORDER_STATUS_SCHEDULED, ORDER_STATUS_PICKED_UP, 
ORDER_STATUS_COMPLETED, ORDER_STATUS_CANCELLED_BY_MITRA, ORDER_STATUS_CANCELLED_BY_CUSTOMER;

const activeOrderUrl = '$BASE_URL/order/active';

class ActiveOrderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ActiveOrderPageState();
}

class ActiveOrderPageState extends State<ActiveOrderPage> {

  OrderList orderList;
  Future<OrderList> orderListFuture;

  @override
  void initState() {
    orderListFuture = _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<OrderList>(
      future: orderListFuture,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          orderList = snapshot.data;
          return buildAfterFetch(context);
        }
        return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  Widget buildAfterFetch(BuildContext context) {
    TextStyle nameTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 13,
      color: Color(0xFF1D1D27),
    );
    TextStyle statusTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 11,
      color: Color(0xFF293542),
    );
    TextStyle statusTextStyleColor = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w500,
      fontSize: 11,
      color: Color(0xFF0645AD),
    );
    TextStyle timeTextStyle = TextStyle(
      fontFamily: "Roboto",
      fontStyle: FontStyle.italic,
      fontWeight: FontWeight.w500,
      fontSize: 9,
      color: Color(0xFF1D1D1D),
    );

    List<Widget> activeOrders = [];
    for (int i = 0; i < orderList.orders.length; i++) {
      var order = orderList.orders[i];

      Widget statusWidget;
      Widget orderLogoWidget;
      if (order.status == ORDER_STATUS_SCHEDULED) {
        statusWidget = Row(
          children: <Widget>[
            Text("Status: ", style: statusTextStyle,),
            Text(order.status, style: statusTextStyleColor,),
            Text(" on ${order.scheduledTime}", style: statusTextStyle,),
          ],
        );
        orderLogoWidget = Container(
          margin: EdgeInsets.only(right: 20, bottom: 10, left: 10, top: 5),
          child: const ImageIcon(AssetImage("assets/calendar.png"), color: Color(0xFF007E24),),
        );
      } else {
        statusWidget = Row(
          children: <Widget>[
            Text("Status: ", style: statusTextStyle,),
            Text(order.status, style: statusTextStyleColor,),
          ],
        );
        orderLogoWidget = Container(
          margin: EdgeInsets.only(right: 20, bottom: 10, left: 10, top: 5),
          child: order.status == ORDER_STATUS_PICKED_UP ? 
          const ImageIcon(AssetImage("assets/scooter.png"), color: Color(0xFF007E24),) : 
          const ImageIcon(AssetImage("assets/magnifying-glass.png"), color: Color(0xFF007E24),),
        );
      }

      activeOrders.add(ListTile(
        title: Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  orderLogoWidget,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        child: Text(order.name, style: nameTextStyle,),
                      ),
                      statusWidget,
                      Container(
                        margin: EdgeInsets.only(top: 3),
                        child: Text(order.time, style: timeTextStyle,)
                      ),
                    ],
                  ),
                ],
              ),
              Divider(),
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => OrderDetailPage(order.id)))
                               .then((onValue) {
                                 setState(() { 
                                    orderListFuture = _getData();
                                 });
                               });
        },
      ));
    }

    if (activeOrders.length == 0) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/no_active_order.png"),
          ],
        ),
      );
    }

    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 15),
          child: Column(
            children: activeOrders,
          ),
        ),
      ],
    );
  }

  Future<OrderList> _getData() async {
    var token = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };

    final response = await http.get(activeOrderUrl, headers: header);
    return OrderList.fromJson(json.decode(response.body));
  }
}
