import 'package:flutter/material.dart';
import 'list_of_active_order_page.dart';
import 'list_of_order_history_page.dart';

const activeOrderState = "Active";
const orderHistoryState = "History";

class OrderPage extends StatefulWidget {
  String type;

  OrderPage({this.type=activeOrderState});

  @override
  State<StatefulWidget> createState() => OrderPageState(type);
}

class OrderPageState extends State<OrderPage> {
  String type;

  OrderPageState(this.type);

  @override
  Widget build(BuildContext context) {
    TextStyle titleTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w900,
      fontSize: 20,
      color: Color(0xFF1D1D27),
    );

    if (type == activeOrderState) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('Ongoing Orders', style: titleTextStyle,),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.history),
              color: Colors.black,
              onPressed: () {
                setState(() {
                  type = orderHistoryState;
                });
              },
            ),
          ],
        ),
        body: ActiveOrderPage(),
      );
    } else if (type == orderHistoryState) {
      return WillPopScope(
        onWillPop: () async {
          setState(() {
            type = activeOrderState;
          });
          return false;
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text('Order Histories', style: titleTextStyle,),
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
                color: Colors.black,
                onPressed: () {
                  setState(() {
                    type = activeOrderState;
                  });
                },
            ),
          ),
          body: OrderHistoryPage(),
        ),
      );
    }
  }
}
