import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'login_page_mitra.dart';
import 'navigation_bar.dart';
import 'login_provider.dart';
import 'edit_account.dart';
import 'app_info_page.dart';
import 'account_page.dart';
import 'package:flutter/services.dart';

void main() async {
  await LoginProvider().init();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]).then((_) { 
    runApp(MitraApp());
  });
}

class MitraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yuk Recycle - Mitra',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          textTheme: TextTheme(
              body1: TextStyle(fontSize: 30.0, fontFamily: 'Roboto_Medium'))),
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/': (_) => LoginPageMitra(),
        '/home': (_) => MitraAppNavBar(),
        '/home/account': (_) => AccountEditPage(),
        '/home/account/edit_account': (_) => AccountEditPage(),
        '/home/account/privacy_policy': (_) => AppInfo(
              title: 'Privacy Policy',
              fetchData: AccountProvider.fetchPrivacyPolicy,
            ),
        '/home/account/terms_condition': (_) => AppInfo(
              title: 'Terms and Condition',
              fetchData: AccountProvider.fetchTermsAndAgreement,
            ),
      },
    );
  }
}
