import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:io';
import 'package:http/http.dart' show Client;
import 'login_provider.dart';
import 'models/api_provider.dart';
import 'const.dart';

OrderItems orderItem;
ApiProvider apiProvider = ApiProvider();
var garbageDetail;

class Sampah {
  String id;
  String namaSampah;
  String satuan;
  String pic;
  String desc;
  int quantity;

  Sampah({this.namaSampah, this.id, this.quantity, this.satuan, this.pic, this.desc});
  factory Sampah.fromJson(Map<String, dynamic> json, String id) {
    return Sampah(
      namaSampah: json['title'],
      desc: json['description'],
      satuan: json['quantity_type'],
      pic: json['image'],
      quantity: 0,
      id: id,
    );
  }
}

class OrderItems {
  Map<String, Sampah> itemSampah = {};

  OrderItems({this.itemSampah});

  factory OrderItems.fromJson(List<dynamic> json) {
    Map<String, Sampah> listSampah = Map<String, Sampah>();
    for (int i = 0; i < json.length; i++){
      String id = (i+1).toString();
      String namaSampah = json[i]['title'];
      listSampah[namaSampah] = Sampah.fromJson(json[i], id);
    }
    
    return OrderItems(
      itemSampah: listSampah
    );
  }

  List<Sampah> getNonZeroQuantityItem() {
    List<Sampah> listItem = [];
    itemSampah.forEach((namaSampah, objSampah) {
      if (objSampah.quantity != 0) {
        listItem.add(objSampah);
      }
    });
    return listItem;
  }

  List<Sampah> getOnlyListedGarbageOnOrderDetail(garbageDetail) {
    garbageDetail.forEach((detailSampah) {
      String namaSampah = detailSampah['title'];
      itemSampah[namaSampah].quantity = detailSampah['quantity'];
    });
    return itemSampah.values.toList();
  }
}

class AdjustPage extends StatelessWidget {
  int id;

  AdjustPage(this.id);
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: initOrderItem(),
      builder: (context,snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            return new Container();
          case ConnectionState.waiting:
            return Center(
              child: CircularProgressIndicator()
            );
          case ConnectionState.done:
            return AdjustOrderPage(id: id,);
          default:
            return Container();
        }
      }
    );
  }

  Future<OrderItems> initOrderItem() async {
    var token = LoginProvider.getTokenMitra();
    garbageDetail = await apiProvider.orderDetailHttpRequest(token, id);
    return await apiProvider.garbageTypeHttpRequest(token);
  }
}

class AdjustOrderPage extends StatelessWidget {
  int id;

  AdjustOrderPage({this.id});

  final myController = TextEditingController();
  
  TextStyle titleTextStyle = TextStyle(
    fontFamily: 'Roboto',
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w900,
    fontSize: 20,
    color: Color(0xFF1D1D27),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: Key('AdjustOrder'),    
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('Adjust Order', style: titleTextStyle,),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                Navigator.of(context).pop();
              },
          ),
        ),
      body: ListView(
        children: [
          Column(
            children: orderItem.getOnlyListedGarbageOnOrderDetail(garbageDetail)
                .map((element) => new JenisSampah(
              id: element.id,
              jenisSampah: element.namaSampah,
              satuan: element.satuan,
              pic: element.pic,
              quantity: element.quantity,
              desc: element.desc,
            )).toList(),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 20),
            child: RaisedButton(
              key: Key('orderButton'),
              color: Color(0xff007E24),
              onPressed: () {
                var token = LoginProvider.getTokenMitra();
                apiProvider.adjustOrderHttpRequest(token, id, orderItem.itemSampah.values.toList()).then((status) {
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
                        title: Text("Succeeded"),
                        content: Text(
                          'Your order has been adjusted'
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text("Back"),
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    }
                  );
                });
              },
              child: Text('Simpan', style: TextStyle(color: Colors.white)),
            ),
          ),
        ],
        padding: const EdgeInsets.all(5.0),
      ),
    );
  }
}

class JenisSampah extends StatefulWidget {
  String jenisSampah;
  String id;
  String pic;
  String satuan;
  String desc;
  int quantity;
  JenisSampah({this.jenisSampah, this.pic, Key key, this.id, this.satuan, this.quantity, this.desc}) : super(key: key);
  _JenisSampahState createState() => new _JenisSampahState();
}

class _JenisSampahState extends State<JenisSampah> {
  int itemCount;

  @override
  Widget build(BuildContext context) {
    if (widget.satuan == 'Botol') {
      widget.satuan = 'Btl';
    }
    itemCount =  widget.quantity;
    double heightPerCard = (MediaQuery.of(context).size.height - 220)/5;
    return Card(
      child: Container(
        padding: EdgeInsets.only(left: 15, top: (heightPerCard-64)/2, bottom: (heightPerCard-64)/2),
        decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.all(
                      const Radius.circular(10.0),
                  ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SvgPicture.memory(
              base64Decode(widget.pic),
              width: 64,
              height: 64,
            ),
            Container(
              width: MediaQuery.of(context).size.width-110,
              child: Row(
                // crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(widget.jenisSampah,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              )),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 5),
                          child: Text(widget.desc,
                              style: TextStyle(fontSize: 12.0)),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      children: [
                        Row(
                          children: <Widget>[
                            new IconButton(
                                key: Key('quantMinus' + widget.id),
                                icon: new Icon(Icons.remove, size: 20),
                                onPressed: ()
                                    {
                                      setState(() {
                                        itemCount > 0 ? itemCount-- : itemCount;
                                        widget.quantity = itemCount;
                                        orderItem.itemSampah[widget.jenisSampah].quantity = itemCount;
                                      });
                                    }),
                            new Text(
                              itemCount.toString() + " " + widget.satuan,
                              key: Key('quantItem' + widget.id,),
                              style: TextStyle(
                                fontSize: 16.0
                              ),
                            ),
                            new IconButton(
                                key: Key('quantPlus' + widget.id),
                                icon: new Icon(Icons.add, size: 20),
                                onPressed: ()
                                {
                                  setState(() {
                                    itemCount++;
                                    widget.quantity = itemCount;
                                    orderItem.itemSampah[widget.jenisSampah].quantity = itemCount;
                                  });
                                }),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


