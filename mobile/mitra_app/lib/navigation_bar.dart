import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'account_page.dart';
import 'help_page.dart';
import 'home_page.dart';
import 'order_page.dart';
import 'settings_page.dart';
import 'models/push_notification.dart';
import 'login_provider.dart';

import 'package:http/http.dart' as http;
import 'const.dart' show BASE_URL;
import 'dart:convert';
import 'adjust_order_page.dart';

var baseOrderAttemptUrl = "$BASE_URL/order/attempt";
var approveOrderAttemptUrl = "$baseOrderAttemptUrl/approve";
var rejectOrderAttemptUrl = "$baseOrderAttemptUrl/reject";

typedef void Callback();
typedef Future<bool> GetLoginState();

class MitraAppNavBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MitraAppNavBarState();
}

class _MitraAppNavBarState extends State<MitraAppNavBar> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomePage(),
    OrderPage(),
    HelpPage(),
    SettingsPage(),
    AccountPage(),
  ];

  @override
  void initState() {
    super.initState();
    configureFirebaseCloudMessaging();
  }

  void configureFirebaseCloudMessaging() {
    var callback = (Map<String, dynamic> message) => messagingCallback(message);
    _firebaseMessaging.configure(
      onMessage: callback,
      onResume: callback,
      onLaunch: callback,
    );
  }

  Future<dynamic> messagingCallback(Map<String, dynamic> message) async {
    PushNotification pushNotification = await PushNotification.fromJson(message);
    DataMessage dataMessage = pushNotification.dataMessage;
    String type = dataMessage.type;
    if (type == 'RECEIVE_ORDER') {
      showPickUpMessage(dataMessage.name, dataMessage.address, dataMessage.distance, dataMessage.garbageDetail, dataMessage.price, dataMessage.token, dataMessage.orderId, dataMessage.notes);
    } else if (type == 'CANCEL_ORDER') {
      _showDialogCancelledOrder();
    }
  }

  void approveOrder(int token, int orderId) async {
    var mitraToken = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $mitraToken',
      'Content-Type': 'application/json',
    };

    http.post(approveOrderAttemptUrl, headers: header, body: json.encode({
      'token': token,
      'order_id': orderId,
    }));
  }

  void rejectOrder(int token, int orderId) async {
    var mitraToken = LoginProvider.getTokenMitra();
    var header = {
      'Authorization': 'Bearer $mitraToken',
      'Content-Type': 'application/json',
    };

    http.post(rejectOrderAttemptUrl, headers: header, body: json.encode({
      'token': token,
      'order_id': orderId,
    }));
  }

  void _showDialogCancelledOrder() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("We are Sorry!"),
          content: new Text("Your order has been cancelled by our Customer"),
          actions: <Widget>[
            new ButtonTheme(
              minWidth: 80,
              child: FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                    'Back',
                    style: TextStyle(color: Color(0xFF1D1D1D), fontSize: 16.0)
                ),
              )
            ),
          ],
        );
      }
    );
  }

  void showPickUpMessage(String name, String location, String distance, List<GarbageDetail> garbageDetails, int price, int token, int orderId, String notes) {
    TextStyle headerTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
      fontSize: 15,
      color: Color(0xFF000000),
    );
    TextStyle sectionHeaderTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
      fontSize: 12,
      color: Color(0xFF909090),
    );
    TextStyle detailTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 15,
      color: Color(0xFF1D1D1D),
    );
    TextStyle garbageDetailTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.normal,
      fontSize: 12,
      color: Color(0xFF1D1D1D),
    );
    TextStyle priceTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
      fontSize: 15,
      color: Color(0xFF1D1D1D),
    );
    TextStyle buttonTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w900,
      fontSize: 15,
      color: Color(0xFFFFFFFF),
    );

    Widget cancelButtonWidget = Container(
      margin: EdgeInsets.only(top: 50, bottom: 30),
      child: Center(
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5)
          ),
          onPressed: () {
            rejectOrder(token, orderId);
            Navigator.of(context).pop();
          },
          child: Text("Cancel", style: TextStyle(
            fontFamily: 'Roboto',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w900,
            fontSize: 15,
            color: Color(0xFF000000),
          ),),
          color: Colors.white,
        )
      )
    );
    Widget pickUpButtonWidget = Container(
      margin: EdgeInsets.only(top: 50, bottom: 30),
      child: Center(
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5)
          ),
          onPressed: () {
            approveOrder(token, orderId);
            Navigator.of(context).pop();
          },
          child: Text("Pick Up", style: buttonTextStyle,),
          color: Color(0XFF293542),
        )
      )
    );
    List<Widget> garbageDetailChildren = [];
    for (GarbageDetail garbageDetail in garbageDetails) {
      garbageDetailChildren.add(Container(
        padding: EdgeInsets.fromLTRB(10, 5, 0, 3),
        
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('${garbageDetail.title}', style: garbageDetailTextStyle,),
            Text('${garbageDetail.quantity} ${garbageDetail.quantityType}', style: garbageDetailTextStyle,),
          ],
        ),
      ),);
    }
    Widget garbageDetailWidget = Container(
      child: Column(
        children: garbageDetailChildren,
      ),
    );

    Widget notesWidget = Container();
    if (notes != "") {
      Container(
        margin: EdgeInsets.only(top: 30),
        child: notesWidget = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Divider(),
            Container(child: Text('Pesan Untuk Mitra', style: sectionHeaderTextStyle,), margin: EdgeInsets.only(top: 10),),
            Container(child: Text("\"$notes\"", style: detailTextStyle,), padding: EdgeInsets.fromLTRB(10, 10, 0, 0),),
          ],
        ),
      );
    }

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SimpleDialog(
          shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
          title: Column(
            children: <Widget>[
              Center(child: Text('Order Masuk', style: headerTextStyle,)),
              Divider(),
            ],
          ),
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Nama', style: sectionHeaderTextStyle,),
                  Container(child: Text('$name', style: detailTextStyle,), padding: EdgeInsets.fromLTRB(10, 10, 0, 0), ),
                  Divider(),
                  Text('Alamat', style: sectionHeaderTextStyle,),
                  Container(child: Text('$location', style: detailTextStyle,), padding: EdgeInsets.fromLTRB(10, 10, 3, 0),),
                  Divider(),
                  Text('Jarak', style: sectionHeaderTextStyle,),
                  Container(child: Text('$distance Km dari anda', style: detailTextStyle,), padding: EdgeInsets.fromLTRB(10, 10, 0, 0), ),
                  Divider(),
                  Text('Detail', style: sectionHeaderTextStyle,),
                  garbageDetailWidget,
                  Divider(),
                  Container(
                    padding: EdgeInsets.only(top: 3),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Total Harga', style: priceTextStyle,),
                        Text('Rp. $price', style: priceTextStyle,),
                      ],
                    ),
                  ),
                  notesWidget,
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        cancelButtonWidget,
                        pickUpButtonWidget,
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key('MitraNavBarScaffold'),
      body: _children[_currentIndex],
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
          canvasColor: const Color(0xFFF2F2F2),
        ),
        child: BottomNavigationBar(
          key: Key('MitraNavBar'),
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/house.png'), 
                color: _currentIndex == 0 ? const Color(0xFF007E24) : const Color(0xFF293542),
              ),
              title: Text(
                'Home',
                key: Key('HomePageButton'),
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 10,
                  fontWeight: _currentIndex == 0 ? FontWeight.bold : FontWeight.normal,
                  color: _currentIndex == 0 ? const Color(0xFF007E24) : const Color(0xFF293542),
                ),
              ),
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/list.png'), 
                color: _currentIndex == 1 ? const Color(0xFF007E24) : const Color(0xFF293542),
              ),
              title: Text(
                'Order',
                key: Key('OrderPageButton'),
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 10,
                  fontWeight: _currentIndex == 1 ? FontWeight.bold : FontWeight.normal,
                  color: _currentIndex == 1 ? const Color(0xFF007E24) : const Color(0xFF293542),
                ),
              ),
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/info.png'), 
                color: _currentIndex == 2 ? const Color(0xFF007E24) : const Color(0xFF293542),
              ),
              title: Text(
                'Help',
                key: Key('HelpPageButton'),
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 10,
                  fontWeight: _currentIndex == 2 ? FontWeight.bold : FontWeight.normal,
                  color: _currentIndex == 2 ? const Color(0xFF007E24) : const Color(0xFF293542),
                ),
              ),
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/settings.png'), 
                color: _currentIndex == 3 ? const Color(0xFF007E24) : const Color(0xFF293542),
              ),
              title: Text(
                'Settings',
                key: Key('SettingsPageButton'),
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 10,
                  fontWeight: _currentIndex == 3 ? FontWeight.bold : FontWeight.normal,
                  color: _currentIndex == 3 ? const Color(0xFF007E24) : const Color(0xFF293542),
                ),
              ),
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/account.png'),),
              activeIcon: ImageIcon(AssetImage('assets/account-active.png'), color: const Color(0xFF007E24),),
              title: Text(
                'Account',
                key: Key('AccountPageButton'),
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 10,
                  fontWeight: _currentIndex == 4 ? FontWeight.bold : FontWeight.normal,
                  color: _currentIndex == 4 ? const Color(0xFF007E24) : const Color(0xFF293542),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
