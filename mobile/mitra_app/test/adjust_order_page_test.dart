import 'package:mitra_app/models/api_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:http/http.dart';
import 'package:http/http.dart' show Client;
import 'package:http/testing.dart';
import 'package:mitra_app/adjust_order_page.dart';
import 'dart:convert';

void main() {
  test('Get Garbage Type Detail from Server', () async {
    final apiProvider = ApiProvider();

    // Mock HTTP
    apiProvider.client = MockClient((request) async {
      final mapJson = {
        'garbage_types' : [
          {
          'title': 'Sampah Plastik',
          'description': 'Pile of Used Plastik',
          'quantity_type': 'Kg',
          'image': 'qwertyuiop'
          }
        ]
      };
      return Response(json.encode(mapJson), 200);
    });

    // API Call
    String token = 'initoken';
    OrderItems orderItem = await apiProvider.garbageTypeHttpRequest(token);
    Map<String, Sampah> itemSampah = orderItem.itemSampah;

    // Value Test    
    expect(itemSampah.isNotEmpty, true);
    
    // Check Sampah Attribute
    expect(itemSampah['Sampah Plastik'] is Sampah, true);
    expect(itemSampah['Sampah Plastik'].namaSampah, 'Sampah Plastik');
    expect(itemSampah['Sampah Plastik'].desc, 'Pile of Used Plastik');
    expect(itemSampah['Sampah Plastik'].satuan, 'Kg');
    expect(itemSampah['Sampah Plastik'].pic, 'qwertyuiop');
  }); 
}
