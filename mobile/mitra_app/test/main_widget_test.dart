import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mitra_app/navigation_bar.dart';
import 'package:mitra_app/home_page.dart';
import 'package:mitra_app/order_page.dart';
import 'package:mitra_app/help_page.dart';
import 'package:mitra_app/settings_page.dart';
import 'package:mitra_app/account_page.dart';

void main() {
  testWidgets('MitraAppNavBar can navigate to other page', (WidgetTester tester) async {
    void testNavBar(Widget widget, int index) {
      final Finder navBarScaffoldFinder = find.byKey(Key('MitraNavBarScaffold'));
      final Scaffold scaffold = navBarScaffoldFinder.evaluate().first.widget;
      expect(scaffold.body.runtimeType, equals(widget.runtimeType));

      Finder mitraNavBarFinder = find.byKey(Key('MitraNavBar'));
      BottomNavigationBar bottomNavigationBar = mitraNavBarFinder.evaluate().first.widget;
      expect(bottomNavigationBar.currentIndex, equals(index));
    }

    await tester.pumpWidget(MaterialApp(home: MitraAppNavBar()));
    testNavBar(HomePage(), 0);

    await tester.tap(find.byKey(Key('HelpPageButton')));
    await tester.pump();
    testNavBar(HelpPage(), 2);
    
    await tester.tap(find.byKey(Key('AccountPageButton')));
    await tester.pump();
    testNavBar(AccountPage(), 4);
  }, skip: true);


  testWidgets('HomePage can toggle online offline button', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: Scaffold(body: HomePage(),)));

    expect(find.text('Ready to Pick Up!'), findsOneWidget);
    expect(find.text('I\'m Not Available'), findsNothing);

    await tester.tap(find.byType(Switch));
    await tester.pump();

    expect(find.text('Ready to Pick Up!'), findsNothing);
    expect(find.text('I\'m Not Available'), findsOneWidget);
  }, skip: true);
}
