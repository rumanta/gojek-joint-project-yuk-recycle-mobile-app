import 'package:flutter_test/flutter_test.dart';
import 'package:mitra_app/login_page_mitra.dart';

void main() {

  test('Empty phone number returns error string', () {
    var result = PhoneNumberFieldValidator.validate('');
    expect(result, 'Silahkan isi nomor ponsel Anda');
  });

  test('Non-numeric phone number returns error string', () {
    var result = PhoneNumberFieldValidator.validate('abc081212');
    expect(result, 'Nomor ponsel tidak boleh mengandung huruf');
  });

  test('Numeric phone number returns null', () {
    var result = PhoneNumberFieldValidator.validate('0812345678');
    expect(result, null);
  });

  test('Empty password returns error string', () {
    var result = PasswordFieldValidator.validate('');
    expect(result, 'Silahkan isi password Anda');
  });

  test('Non-empty password returns null', () {
    var result = PasswordFieldValidator.validate('asdasd');
    expect(result, null);
  });
}