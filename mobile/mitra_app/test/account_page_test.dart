import 'dart:convert';
import 'dart:io' show HttpStatus;
import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:mitra_app/account_page.dart';
import 'package:mockito/mockito.dart';

MockClient mockClient(Map<String, dynamic> response, status) {
  return MockClient((request) async {
    return Response(json.encode(response), status);
  });
}

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  testWidgets('Test load Account Page', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: AccountPage()),
      navigatorObservers: [mockObserver],
      // routes: <String, WidgetBuilder>{
      //   '/home/account/privacy_policy': (_) => Text('privacy'),
      // },
    ));
  });

  group(
    'testing api call handler',
    () {
      test(
        'Test fetch account data success',
        () async {
          final provider = AccountProvider();
          AccountProvider.client = mockClient({
            'fullName': 'Full Name',
            'email': 'anon@mail.com',
            'phone': '081234567890',
            'verified': 'true',
          }, HttpStatus.ok);

          AccountData response = await AccountProvider.fetchAccountData();

          expect(response.fullName, 'Full Name');
          expect(response.email, 'anon@mail.com');
          expect(response.phone, '081234567890');
          expect(response.verified, true);
        },
        skip: 'not implemented',
      );

      test(
        'Test update account data success',
        () async {
          final provider = AccountProvider();
          AccountProvider.client = mockClient({
            'status': 'updated',
            'data': {
              'fullName': 'Full Name',
              'email': 'anon@mail.com',
              'phone': '081234567890',
              'verified': 'true',
            }
          }, HttpStatus.ok);

          Map response = await AccountProvider.updateAccountData(null);

          expect(response['status'], 'updated');
          expect(response['data']['fullName'], 'Full Name');
          expect(response['data']['email'], 'anon@mail.com');
          expect(response['data']['phone'], '081234567890');
          expect(response['data']['verified'], 'true');
        },
        skip: 'not implemented',
      );
    },
  );

  group('widget testing', () {
    //   testWidgets('account status widget containing correct data', (WidgetTester tester) async {
    //     await tester.pumpWidget(MaterialApp(home: AccountEdit()));
    //
    //     expect(find.byType(Text), findsNWidgets(4));
    //   });
  });
}
