import 'package:flutter_driver/driver_extension.dart';

import 'package:mitra_app/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}
