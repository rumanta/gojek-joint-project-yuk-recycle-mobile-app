package models

import (
	"github.com/jinzhu/gorm"
)

/*
	order_id - FK ke tabel ORDER
	status_created_at
	status - int FK ke tabel STATUS
	isinya cuma salah satu dari 1,2,3,4
*/

type OrderStatusHistory struct {
	gorm.Model         // <-- created at
	OrderID       uint `gorm:"type:integer REFERENCES Orders(id)"`
	OrderStatusID uint `gorm:"type:integer REFERENCES Order_Statuses(id)"`
}
