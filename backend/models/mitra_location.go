package models

import (
	"github.com/jinzhu/gorm"
)

type MitraLocation struct {
	gorm.Model
	MitraID uint    `gorm:"type:integer REFERENCES Mitras(id)"`
	Mitra   Mitra   `gorm:"foreignkey:MitraID"`
	Lat     float64 `gorm:"type:decimal(11,8)"`
	Lon     float64 `gorm:"type:decimal(11,8)"`
	Address string
}
