package models

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

var db *gorm.DB

func init() {

	godotenv.Load(os.Getenv("GOPATH") + "/src/PPLA4/backend/.env")

	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")

	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password)
	fmt.Println(dbURI)
	conn, err := gorm.Open("postgres", dbURI)

	if err != nil {
		fmt.Println(err) // logging
	}

	db = conn
	// db.Debug().AutoMigrate(
	// 	&Mitra{},
	// 	&Customer{},
	// 	&GarbageType{},
	// 	&MitraGarbageSetting{},
	// 	&MitraPickUpDistance{},
	// 	&MitraPickUpSchedule{},
	// 	&MitraLocation{},
	// 	&OrderStatus{},
	// 	&Order{},
	// 	&OrderDetail{},
	// 	&OrderStatusHistory{},
	// 	&OrderAttemptStatus{},
	// 	&OrderAttempt{},
	// )
}

func GetDB() *gorm.DB {
	return db
}

func SetDB(new_db *gorm.DB) {
	db = new_db
}
