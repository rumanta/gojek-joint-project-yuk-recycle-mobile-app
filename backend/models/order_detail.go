package models

import (
	"github.com/jinzhu/gorm"
)

/*
	order_id - FK ke tabel ORDER
	garbage_type_id - FK ke tabel GARBAGE_TYPE
	quantity - int
*/

type OrderDetail struct {
	gorm.Model         // <-- created at
	OrderID       uint `gorm:"type:integer REFERENCES Orders(id)"`
	GarbageTypeID uint `gorm:"type:integer REFERENCES Garbage_Types(id)"`
	TotalAmount   int  // quantity * agreement
	Quantity      int
}
