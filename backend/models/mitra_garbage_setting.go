package models

import (
	"github.com/jinzhu/gorm"
)

type MitraGarbageSetting struct {
	gorm.Model
	MitraID         uint `gorm:"type:integer REFERENCES Mitras(id)"`
	GarbageTypeID   uint `gorm:"type:integer REFERENCES Garbage_Types(id)"`
	MinimumQuantity uint
	Price           uint
}
