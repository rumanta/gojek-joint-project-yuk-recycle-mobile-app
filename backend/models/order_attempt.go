package models

import (
	"github.com/jinzhu/gorm"
)

type OrderAttempt struct {
	gorm.Model
	OrderID              uint `gorm:"type:integer REFERENCES Orders(id)"`
	MitraID              uint `gorm:"type:integer REFERENCES Mitras(id)"`
	OrderAttemptStatusID uint `gorm:"type:integer REFERENCES Order_Attempt_Statuses(id)"`
	Token                uint
}
