package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

/*
	tabel ORDER:
	id -
	created_at -
	customer_id -
	mitra_id -
	customer latitude -
	customer longitude -
	latest_status - int (1, 2, 3, 4) FK ke tabel STATUS
*/

type Order struct {
	gorm.Model
	MitraID            uint
	CustomerID         uint `gorm:"type:integer REFERENCES Customers(id)"`
	CustomerLat        float64
	CustomerLon        float64
	LatestStatus       int
	ScheduledAt        time.Time
	Notes              string
	Address            string
	CancellationReason string
}
