package models

import (
	"github.com/jinzhu/gorm"
)

/*
	status_id (int), description (string)
	1 : init order, finding mitra
	2 : accepted by a mitra (to be picked up)
	3 : completed
	4 : cancelled (dari 1 ke 4 atau 2 ke 4)
*/

type OrderStatus struct {
	gorm.Model
	Description string
}
