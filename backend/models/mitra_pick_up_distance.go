package models

import (
	"github.com/jinzhu/gorm"
)

type MitraPickUpDistance struct {
	gorm.Model
	MitraID         uint  `gorm:"type:integer REFERENCES Mitras(id)"`
	Mitra           Mitra `gorm:"foreignkey:MitraID"`
	MaximumDistance uint
}
