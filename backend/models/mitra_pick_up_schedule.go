package models

import (
	"github.com/jinzhu/gorm"
)

type MitraPickUpSchedule struct {
	gorm.Model
	MitraID   uint  `gorm:"type:integer REFERENCES Mitras(id)"`
	Mitra     Mitra `gorm:"foreignkey:MitraID"`
	Sunday    bool  `gorm:"default:true"`
	Monday    bool  `gorm:"default:true"`
	Tuesday   bool  `gorm:"default:true"`
	Wednesday bool  `gorm:"default:true"`
	Thursday  bool  `gorm:"default:true"`
	Friday    bool  `gorm:"default:true"`
	Saturday  bool  `gorm:"default:true"`
}
