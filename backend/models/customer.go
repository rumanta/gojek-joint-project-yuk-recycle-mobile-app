package models

import (
	"github.com/jinzhu/gorm"
)

type Customer struct {
	gorm.Model
	Phone             string `gorm:"type:varchar(50);unique; default:null"`
	Password          string
	Name              string `gorm:"type:varchar(50)"`
	Email             string `gorm:"type:varchar(50);not null;unique"`
	LoginType         int
	NotificationToken string
}
