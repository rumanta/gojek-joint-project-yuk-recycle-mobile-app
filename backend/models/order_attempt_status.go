package models

import (
	"github.com/jinzhu/gorm"
)

/*
	id (int), description (string)
	1 : not yet decided by a mitra
	2 : accepted by a mitra (to be picked up)
	3 : rejected by a mitra
*/

type OrderAttemptStatus struct {
	gorm.Model
	Description string
}
