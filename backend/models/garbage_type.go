package models

import (
	"github.com/jinzhu/gorm"
)

type GarbageType struct {
	gorm.Model
	Title        string
	Description  string
	QuantityType string
	ImageBase64  string

	MitraGarbageSettings []MitraGarbageSetting `gorm:"foreignkey:GarbageTypeID"`
}
