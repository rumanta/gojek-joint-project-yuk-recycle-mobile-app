package models

import (
	"github.com/jinzhu/gorm"
)

type Mitra struct {
	gorm.Model
	Phone    string `gorm:"type:varchar(50);not null;unique default:null"`
	Password string

	GarbageSettings   []MitraGarbageSetting `gorm:"foreignkey:MitraID"`
	Name              string                `gorm:"type:varchar(50)"`
	Email             string                `gorm:"type:varchar(50);not null;unique"`
	NotificationToken string
}
