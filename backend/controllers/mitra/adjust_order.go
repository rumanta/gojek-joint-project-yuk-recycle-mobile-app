package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gopkg.in/go-playground/validator.v9"
)

type AdjustOrderController struct {
	AdjustOrderService services.IAdjustOrderService
}

type AdjustOrderResponse struct {
	Status string     `json:"status,omitempty"`
	Error  *JSONError `json:"error,omitempty"`
}

func (a AdjustOrderController) AdjustOrderHandler(w http.ResponseWriter, r *http.Request) {
	orderID, err := strconv.Atoi(mux.Vars(r)["orderID"])

	if err != nil {
		e := &JSONError{Code: "Invalid_order_id", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &AdjustOrderResponse{Error: e})
		return
	}

	req, err := decodeAdjustOrderRequest(r)

	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &AdjustOrderResponse{Error: e})
		return
	}

	err = a.AdjustOrderService.AdjustOrder(uint(orderID), r.Context().Value("UserID").(uint), req)

	if err != nil {
		e := &JSONError{Code: "make_order_failed", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &AdjustOrderResponse{Error: e})
		return
	}

	writeEncoded(w, &AdjustOrderResponse{
		Status: "OK",
	})
}

func decodeAdjustOrderRequest(r *http.Request) (*services.AdjustOrderRequest, error) {
	req := new(services.AdjustOrderRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}

	validate := validator.New()
	err := validate.Struct(req)

	if err != nil {
		return nil, err
	}

	return req, nil
}
