package mitracontrollers

import (
	mockServices "PPLA4/backend/mocks/services/mitra"
	services "PPLA4/backend/services/mitra"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHomepage_GetMitraHomepage(t *testing.T) {
	tests := []struct {
		description                 string
		inputMitraID                uint
		returnHomepageDataMock      *services.HomepageData
		returnErrorHomepageDataMock error
		expectedHomepageResponse    *HomepageResponse
		expectedHTTPCode            int
		expectedJsonError           *JSONError
	}{
		{
			description:                 "Failed get homepage data",
			inputMitraID:                uint(1),
			returnHomepageDataMock:      nil,
			returnErrorHomepageDataMock: errors.New("Failed get homepage data"),
			expectedHTTPCode:            http.StatusBadRequest,
			expectedJsonError:           &JSONError{Code: "invalid_request", Message: "Failed get homepage data"},
		}, {
			description:  "Success get Homepage mitra",
			inputMitraID: uint(1),
			returnHomepageDataMock: &services.HomepageData{
				WeeklyOrderStatistic: &services.OrderStatistic{
					OrderTotal:    uint(2),
					ActiveOrder:   uint(1),
					PickedUpOrder: uint(1),
					SpendingTotal: uint(12000),
				},
				MitraName: "william",
				IsOnline:  true,
			},
			returnErrorHomepageDataMock: nil,
			expectedHomepageResponse: &HomepageResponse{
				HomepageData: &services.HomepageData{
					WeeklyOrderStatistic: &services.OrderStatistic{
						OrderTotal:    uint(2),
						ActiveOrder:   uint(1),
						PickedUpOrder: uint(1),
						SpendingTotal: uint(12000),
					},
					MitraName: "william",
					IsOnline:  true,
				},
			},
			expectedHTTPCode:  http.StatusOK,
			expectedJsonError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/home", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedHomepageService := new(mockServices.IHomepageService)
		mockedHomepageService.On("GetHomepageData", tc.inputMitraID).Return(
			tc.returnHomepageDataMock,
			tc.returnErrorHomepageDataMock,
		)
		homepageController := Homepage{HomepageService: mockedHomepageService}

		handler := http.HandlerFunc(homepageController.GetMitraHomepage)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		if tc.expectedHTTPCode == http.StatusOK {
			var result HomepageResponse
			json.Unmarshal([]byte(returnedBody), &result)
			assert.Equal(tc.expectedHomepageResponse, &result, tc.description)
		} else {
			var result ErrorResponse
			json.Unmarshal([]byte(returnedBody), &result)
			assert.Equal(tc.expectedJsonError, result.Error, tc.description)
		}

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}
