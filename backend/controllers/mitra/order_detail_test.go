package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestActiveOrder_GetOrderDetail(t *testing.T) {
	tests := []struct {
		description           string
		inputMitraID          uint
		inputOrderID          string
		inputOrderIDUint      uint
		returnOrderDetailMock *services.OrderDetailResponse
		returnErrorMock       error
		expectedHTTPCode      int
		expectedJsonError     *JSONError
	}{
		{
			description:           "Order id not uint",
			inputMitraID:          uint(1),
			inputOrderID:          "a",
			inputOrderIDUint:      0,
			returnOrderDetailMock: nil,
			returnErrorMock:       nil,
			expectedHTTPCode:      http.StatusBadRequest,
			expectedJsonError:     &JSONError{Code: "invalid_request", Message: "order id is not valid"},
		}, {
			description:           "Get data not success",
			inputMitraID:          uint(1),
			inputOrderID:          "2",
			inputOrderIDUint:      2,
			returnOrderDetailMock: nil,
			returnErrorMock:       errors.New("Error"),
			expectedHTTPCode:      http.StatusBadRequest,
			expectedJsonError:     &JSONError{Code: "invalid_request", Message: "Error"},
		}, {
			description:      "Get data success",
			inputMitraID:     uint(1),
			inputOrderID:     "2",
			inputOrderIDUint: 2,
			returnOrderDetailMock: &services.OrderDetailResponse{
				ID:   1,
				Name: "Endrawan",
			},
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusOK,
			expectedJsonError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/order/1", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		req = mux.SetURLVars(req, map[string]string{"orderID": tc.inputOrderID})
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedOrderDetailService := new(mockServices.IOrderDetailService)
		mockedOrderDetailService.On("GetOrderDetailData", tc.inputOrderIDUint, tc.inputMitraID).Return(
			tc.returnOrderDetailMock,
			tc.returnErrorMock,
		)
		orderDetailController := OrderDetail{OrderDetailService: mockedOrderDetailService}

		handler := http.HandlerFunc(orderDetailController.GetOrderDetail)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		if tc.expectedHTTPCode == http.StatusOK {
			var result services.OrderDetailResponse
			json.Unmarshal([]byte(returnedBody), &result)
			assert.Equal(tc.returnOrderDetailMock.ID, result.ID, tc.description)
		} else {
			var result ErrorResponse
			json.Unmarshal([]byte(returnedBody), &result)
			assert.Equal(tc.expectedJsonError, result.Error, tc.description)
		}

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}
