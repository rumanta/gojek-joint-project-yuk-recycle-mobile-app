package mitracontrollers

import (
	middleware "PPLA4/backend/middleware"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestAuthenticate(t *testing.T) {
	tests := []struct {
		description       string
		JSONRequest       string
		inputPhoneMock    string
		inputPasswordMock string
		returnTokenMock   *middleware.Token
		returnErrorMock   error
		expectedHTTPCode  int
		expectedErrorCode string
		expectedStatus    string
		// expectedBody      string
	}{
		{
			description:       "Given unknown field return invalid_request",
			JSONRequest:       `{}`,
			inputPhoneMock:    "",
			inputPasswordMock: "",
			returnTokenMock:   nil,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "Given wrong password return unauthorized",
			JSONRequest:       `{"phone":"08123456789", "password":"passuworudo"}`,
			inputPhoneMock:    "08123456789",
			inputPasswordMock: "passuworudo",
			returnTokenMock:   nil,
			returnErrorMock:   errors.New("username and password doesn't match"),
			expectedHTTPCode:  http.StatusUnauthorized,
			expectedErrorCode: "unauthorized",
			expectedStatus:    "",
		}, {
			description:       "Authenticate Success",
			JSONRequest:       `{"phone":"08123456789", "password":"passuworudo"}`,
			inputPhoneMock:    "08123456789",
			inputPasswordMock: "passuworudo",
			returnTokenMock:   &middleware.Token{Value: "qzwsxedcrfv", ExpiresAt: "12345678"},
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusOK,
			expectedErrorCode: "",
			expectedStatus:    "logged_in",
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/login", strings.NewReader(tc.JSONRequest))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedAuthService := new(mockServices.IAuthService)
		mockedAuthService.On("Login", tc.inputPhoneMock, tc.inputPasswordMock).Return(tc.returnTokenMock, tc.returnErrorMock)
		authController := Authentication{AuthService: mockedAuthService}

		handler := http.HandlerFunc(authController.Authenticate)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result AuthenticateResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
		assert.Equal(tc.expectedStatus, result.Status, tc.description)
	}
}

func TestDecodeAuthenticateRequest(t *testing.T) {
	tests := []struct {
		description     string
		JSONRequest     string
		expectedRequest *AuthenticateRequest
		expectedError   error
	}{
		{
			description:     "Unknown field",
			JSONRequest:     `{"password":"abcdefghi", "location":"jakarta"}`,
			expectedRequest: nil,
			expectedError:   errors.New(`json: unknown field "location"`),
		}, {
			description:     "Phone is missing",
			JSONRequest:     `{"password":"abcdefghi"}`,
			expectedRequest: nil,
			expectedError:   errors.New("phone number is required"),
		}, {
			description:     "Password is missing",
			JSONRequest:     `{"phone":"0812345678"}`,
			expectedRequest: nil,
			expectedError:   errors.New("password is required"),
		}, {
			description:     "Success decoding",
			JSONRequest:     `{"phone":"081804886455", "password":"abcdefghi"}`,
			expectedRequest: &AuthenticateRequest{Phone: &[]string{"081804886455"}[0], Password: &[]string{"abcdefghi"}[0]},
			expectedError:   nil,
		}, {
			description:     "Success decoding +62 to 0",
			JSONRequest:     `{"phone":"+6281804886455", "password":"abcdefghi"}`,
			expectedRequest: &AuthenticateRequest{Phone: &[]string{"081804886455"}[0], Password: &[]string{"abcdefghi"}[0]},
			expectedError:   nil,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/login", strings.NewReader(tc.JSONRequest))
		ret, err := decodeAuthenticateRequest(req)
		assert.Equal(tc.expectedRequest, ret, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
