package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"net/http"

	"gopkg.in/go-playground/validator.v9"

	"encoding/json"
)

type ManageAccount struct {
	ManageMitraAccountService services.IManageAccountService
}

type ManageAccountMitraRequestOrResponse struct {
	Mitra  *services.MitraData `json:"mitra" validate:"required"`
	Error  *JSONError          `json:"error,omitempty"`
	Status string              `json:"status,omitempty"`
}

func (m ManageAccount) GetMitraData(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value("UserID").(uint)

	mitraData, err := m.ManageMitraAccountService.GetMitraAccountData(userID)

	if err != nil {
		writeError(w, err)
		return
	}

	data := &ManageAccountMitraRequestOrResponse{
		Mitra:  mitraData,
		Status: "Ok",
	}
	writeEncoded(w, data)

}

func (m ManageAccount) UpdateMitraData(w http.ResponseWriter, r *http.Request) {
	mitraId := r.Context().Value("UserID").(uint)

	data := new(ManageAccountMitraRequestOrResponse)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	if err := decoder.Decode(data); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeError(w, err)
		return
	}

	validate := validator.New()
	err := validate.Struct(data)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeError(w, err)
		return
	}

	err = m.ManageMitraAccountService.UpdateMitraAccountData(mitraId, *data.Mitra)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeError(w, err)
		return
	}

	writeEncoded(w, &ManageAccountMitraRequestOrResponse{Status: "Ok"})
}
