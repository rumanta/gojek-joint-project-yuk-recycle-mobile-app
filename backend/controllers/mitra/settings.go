package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"encoding/json"
	"net/http"

	"gopkg.in/go-playground/validator.v9"
)

type Settings struct {
	SettingsService services.ISettingsService
}

type SettingsRequestOrResponse struct {
	Schedules             *services.Schedules       `json:"schedules,omitempty" validate:"required"`
	GarbageSettings       []services.GarbageSetting `json:"garbage_settings,omitempty" validate:"required"`
	MaximumPickUpDistance uint                      `json:"maximum_pick_up_distance,omitempty" validate:"required,numeric"`
	Error                 *JSONError                `json:"error,omitempty"`
	Status                string                    `json:"status,omitempty"`
}

func (s Settings) GetSettings(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("UserID").(uint)
	schedules, garbageSettings, maximumPickUpDistance, _ := s.SettingsService.GetSettingsData(userId)
	data := &SettingsRequestOrResponse{
		Schedules:             schedules,
		GarbageSettings:       garbageSettings,
		MaximumPickUpDistance: maximumPickUpDistance,
	}
	writeEncoded(w, data)
}

func (s Settings) UpdateSettings(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("UserID").(uint)
	data, err := decodeSettingsRequest(r)

	if err != nil {
		writeError(w, err)
		return
	}

	err = s.SettingsService.UpdateSettingsData(
		userId,
		data.Schedules,
		data.GarbageSettings,
		data.MaximumPickUpDistance,
	)

	if err != nil {
		writeError(w, err)
		return
	}

	data = &SettingsRequestOrResponse{Status: "Ok"}
	writeEncoded(w, data)
}

func decodeSettingsRequest(r *http.Request) (*SettingsRequestOrResponse, error) {
	request := new(SettingsRequestOrResponse)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	if err := decoder.Decode(request); err != nil {
		return nil, err
	}

	validate := validator.New()
	err := validate.Struct(request)

	if err != nil {
		return nil, err
	}

	return request, nil
}
