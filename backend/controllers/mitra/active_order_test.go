package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestActiveOrder_GetActiveOrders(t *testing.T) {
	tests := []struct {
		description            string
		inputMitraID           uint
		returnActiveOrdersMock []services.ActiveOrder
		returnErrorMock        error
		expectedHTTPCode       int
	}{
		{
			description:  "Get data success",
			inputMitraID: uint(1),
			returnActiveOrdersMock: []services.ActiveOrder{
				services.ActiveOrder{
					ID:            1,
					Name:          "Endrawan Andika",
					Time:          "Monday, 15 April 2019, 15:51",
					Status:        "Searching",
					ScheduledTime: "Monday, 01 January 0001",
				}, services.ActiveOrder{
					ID:            2,
					Name:          "Endrawan Andika",
					Time:          "Monday, 15 April 2019, 15:51",
					Status:        "Scheduled",
					ScheduledTime: "Wednesday, 17 April 2019",
				}, services.ActiveOrder{
					ID:            3,
					Name:          "Endrawan Andika",
					Time:          "Monday, 15 April 2019, 15:51",
					Status:        "Pick Up",
					ScheduledTime: "Monday, 01 January 0001",
				},
			},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		}, {
			description:            "Get data not success",
			inputMitraID:           uint(1),
			returnActiveOrdersMock: nil,
			returnErrorMock:        errors.New("Error"),
			expectedHTTPCode:       http.StatusBadRequest,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/order/active", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedActiveOrderService := new(mockServices.IActiveOrderService)
		mockedActiveOrderService.On("GetActiveOrdersData", tc.inputMitraID).Return(
			tc.returnActiveOrdersMock,
			tc.returnErrorMock,
		)
		activeOrderController := ActiveOrder{ActiveOrderService: mockedActiveOrderService}

		handler := http.HandlerFunc(activeOrderController.GetActiveOrders)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result ActiveOrderResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnActiveOrdersMock, result.Data, tc.description)
	}
}
