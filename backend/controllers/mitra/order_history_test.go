package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestOrderHistory_GetOrderHistories(t *testing.T) {
	tests := []struct {
		description              string
		inputMitraID             uint
		returnOrderHistoriesMock []services.OrderHistory
		returnErrorMock          error
		expectedHTTPCode         int
	}{
		{
			description:  "Get data success",
			inputMitraID: uint(1),
			returnOrderHistoriesMock: []services.OrderHistory{
				services.OrderHistory{
					ID:     1,
					Name:   "Endrawan Andika",
					Time:   "Monday, 15 April 2019, 15:51",
					Status: "Completed",
				}, services.OrderHistory{
					ID:     2,
					Name:   "Endrawan Andika",
					Time:   "Monday, 15 April 2019, 15:51",
					Status: "Cancelled",
					Reason: "Gpp cancel aja",
				},
			},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		}, {
			description:              "Get data not success",
			inputMitraID:             uint(1),
			returnOrderHistoriesMock: nil,
			returnErrorMock:          errors.New("Error"),
			expectedHTTPCode:         http.StatusBadRequest,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/order/history", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedOrderHistoryService := new(mockServices.IOrderHistoryService)
		mockedOrderHistoryService.On("GetOrderHistoriesData", tc.inputMitraID).Return(
			tc.returnOrderHistoriesMock,
			tc.returnErrorMock,
		)
		orderHistoryController := OrderHistory{OrderHistoryService: mockedOrderHistoryService}

		handler := http.HandlerFunc(orderHistoryController.GetOrderHistories)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result OrderHistoryResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnOrderHistoriesMock, result.Data, tc.description)
	}
}
