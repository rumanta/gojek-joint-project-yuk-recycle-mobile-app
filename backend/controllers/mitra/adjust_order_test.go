package mitracontrollers

import (
	mocks "PPLA4/backend/mocks/services/mitra"
	services "PPLA4/backend/services/mitra"
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestAdjustOrderHandler(t *testing.T) {
	tests := []struct {
		description        string
		orderID            string
		mitraID            uint
		JSONRequest        string
		AdjustOrderRequest *services.AdjustOrderRequest
		ReturnErrorMock    error
		expectedHTTPCode   int
	}{
		{
			description:      "Order id isn't uint",
			orderID:          "a",
			JSONRequest:      `{"location":"jakarta"}`,
			mitraID:          uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Invalid request body",
			orderID:          "3",
			JSONRequest:      `{"location":"jakarta"}`,
			mitraID:          uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Invalid request body on required",
			orderID:          "3",
			JSONRequest:      `{}`,
			mitraID:          uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Adjust isn't success",
			orderID:          "3",
			JSONRequest:      `{"order_detail": [ { "garbage_type_id": 1, "quantity": 10 }, { "garbage_type_id": 2, "quantity": 30 } ]}`,
			mitraID:          uint(1),
			ReturnErrorMock:  errors.New("got some errors"),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description: "Adjust success",
			orderID:     "3",
			JSONRequest: `{"order_detail": [ { "garbage_type_id": 1, "quantity": 10 }, { "garbage_type_id": 2, "quantity": 30 } ]}`,
			AdjustOrderRequest: &services.AdjustOrderRequest{
				OrderDetail: []services.OrderDetailRequest{
					services.OrderDetailRequest{
						GarbageTypeID: 1,
						Quantity:      10,
					},
					services.OrderDetailRequest{
						GarbageTypeID: 2,
						Quantity:      30,
					},
				},
			},
			mitraID:          uint(1),
			ReturnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/adjust/order/3", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.mitraID))
		req = mux.SetURLVars(req, map[string]string{"orderID": tc.orderID})
		if err != nil {
			t.Fatal(err)
		}

		mockedAdjustOrderService := new(mocks.IAdjustOrderService)
		mockedAdjustOrderService.On("AdjustOrder", uint(3), uint(1), mock.Anything).Return(tc.ReturnErrorMock)

		adjustOrderController := AdjustOrderController{AdjustOrderService: mockedAdjustOrderService}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(adjustOrderController.AdjustOrderHandler)
		handler.ServeHTTP(rr, req)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}
