package mitracontrollers

import (
	mocks "PPLA4/backend/mocks/services/mitra"
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestCancelOrder(t *testing.T) {
	tests := []struct {
		description      string
		orderID          string
		mitraID          uint
		JSONRequest      string
		ReturnErrorMock  error
		expectedHTTPCode int
	}{
		{
			description:      "Order id isn't uint",
			orderID:          "a",
			JSONRequest:      `{"location":"jakarta"}`,
			mitraID:          uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Invalid request body",
			orderID:          "3",
			JSONRequest:      `{"location":"jakarta"}`,
			mitraID:          uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Cancel isn't success",
			orderID:          "3",
			JSONRequest:      `{"reason":"endrawan pengen cancel ordernya pokoknya"}`,
			mitraID:          uint(1),
			ReturnErrorMock:  errors.New("got some errors"),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Cancel success",
			orderID:          "3",
			JSONRequest:      `{"reason":"endrawan pengen cancel ordernya pokoknya"}`,
			mitraID:          uint(1),
			ReturnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/order/3", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.mitraID))
		req = mux.SetURLVars(req, map[string]string{"orderID": tc.orderID})
		if err != nil {
			t.Fatal(err)
		}

		mockedCancelOrderService := new(mocks.ICancelOrderService)
		mockedCancelOrderService.On("CancelOrder", uint(3), uint(1), "endrawan pengen cancel ordernya pokoknya").Return(tc.ReturnErrorMock)

		cancelOrderController := CancelOrderController{CancelOrderService: mockedCancelOrderService}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(cancelOrderController.CancelOrderHandler)
		handler.ServeHTTP(rr, req)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}
