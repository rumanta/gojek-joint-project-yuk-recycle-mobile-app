package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"context"
	"encoding/json"
	"errors"
	"strings"

	"net/http"
	"net/http/httptest"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestManageAccount_GetMitraData(t *testing.T) {
	tests := []struct {
		description         string
		inputMitraID        uint
		returnMitraDataMock *services.MitraData
		returnErrorMock     error
		expectedHTTPCode    int
		expectedBody        string
	}{
		{
			description:  "Get data success",
			inputMitraID: uint(1),
			returnMitraDataMock: &services.MitraData{
				Phone:    "+6212345678",
				Name:     "This is dummy",
				Email:    "test@gmail.com",
				Verified: true,
			},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
		{
			description:         "Get data failed",
			inputMitraID:        uint(1),
			returnMitraDataMock: nil,
			returnErrorMock:     errors.New("Error"),
			expectedHTTPCode:    http.StatusBadRequest,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/account", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))

		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedManageAccountService := new(mockServices.IManageAccountService)
		mockedManageAccountService.On("GetMitraAccountData", tc.inputMitraID).Return(
			tc.returnMitraDataMock,
			tc.returnErrorMock,
		)

		manageAccountController := ManageAccount{ManageMitraAccountService: mockedManageAccountService}

		handler := http.HandlerFunc(manageAccountController.GetMitraData)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result ManageAccountMitraRequestOrResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnMitraDataMock, result.Mitra, tc.description)
	}
}

func TestManageAccount_UpdateMitraData(t *testing.T) {
	tests := []struct {
		description           string
		JSONRequest           string
		inputMitraID          uint
		returnUpdateErrorMock error
		expectedHTTPCode      int
	}{
		{
			description:      "Test invalid json body",
			JSONRequest:      `{"location":"jakarta"}`,
			inputMitraID:     uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Test invalid json requirement",
			JSONRequest:      `{"mitra": {"name": "dummy"}}`,
			inputMitraID:     uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description: "Test fail to update data",
			JSONRequest: `{
				"mitra": {
					"name":"Test hahah mau m,kan",
					"phone":"081239999",
					"email":"kucing@terbang.com",
					"location":{
						"lat":-6.364564,
						"lon":106.828694,
						"address":"depok"
					}
				}
			}`,
			inputMitraID:          uint(1),
			returnUpdateErrorMock: errors.New("got error"),
			expectedHTTPCode:      http.StatusBadRequest,
		}, {
			description: "Test fail to update data",
			JSONRequest: `{
				"mitra": {
					"name":"Test hahah mau m,kan",
					"phone":"081239999",
					"email":"kucing@terbang.com",
					"location":{
						"lat":-6.364564,
						"lon":106.828694,
						"address":"depok"
					}
				}
			}`,
			inputMitraID:     uint(1),
			expectedHTTPCode: http.StatusOK,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/mitra/account", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))

		if err != nil {
			t.Fatal(err)
		}

		mockedManageAccountService := new(mockServices.IManageAccountService)
		mockedManageAccountService.On("UpdateMitraAccountData", mock.Anything, mock.Anything).Return(tc.returnUpdateErrorMock)

		manageAccount := ManageAccount{ManageMitraAccountService: mockedManageAccountService}

		rr := httptest.NewRecorder()

		handler := http.HandlerFunc(manageAccount.UpdateMitraData)
		handler.ServeHTTP(rr, req)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}
