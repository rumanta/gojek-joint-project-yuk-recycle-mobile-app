package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"net/http"
)

type Homepage struct {
	HomepageService services.IHomepageService
}

type HomepageResponse struct {
	HomepageData *services.HomepageData `json:"mitra_home"`
}

func (h Homepage) GetMitraHomepage(w http.ResponseWriter, r *http.Request) {
	mitraID := r.Context().Value("UserID").(uint)

	homepageData, err := h.HomepageService.GetHomepageData(mitraID)

	if err != nil {
		writeHomepageError(w, err)
		return
	}

	homepageResponse := &HomepageResponse{
		HomepageData: homepageData,
	}

	writeEncoded(w, homepageResponse)
}

func writeHomepageError(w http.ResponseWriter, err error) {
	e := &JSONError{Code: "invalid_request", Message: err.Error()}
	w.WriteHeader(http.StatusBadRequest)
	writeEncoded(w, &ErrorResponse{Error: e})
}
