package mitracontrollers

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestOrderAttempt_ApproveOrderAttempt(t *testing.T) {
	tests := []struct {
		description       string
		inputMitraID      uint
		inputToken        uint
		inputOrderID      uint
		JSONRequest       string
		returnErrorMock   error
		expectedHTTPCode  int
		expectedErrorCode string
		expectedStatus    string
	}{
		{
			description:       "Malformed JSON",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"ini_apa_ya": true}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "JSON request not valid",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id": "huruf"}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "JSON token not exist",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id":3}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "Can not reject",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id":3, "token":2}`,
			returnErrorMock:   errors.New("Error"),
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "order_attempt_failed",
			expectedStatus:    "",
		}, {
			description:       "Success reject",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id":3, "token":2}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusOK,
			expectedErrorCode: "",
			expectedStatus:    "OK",
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/mitra/order/attempt/approve", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedOrderAttemptService := new(mockServices.IOrderAttemptService)
		mockedOrderAttemptService.On("ApproveOrderAttempt", tc.inputOrderID, tc.inputMitraID, tc.inputToken).Return(tc.returnErrorMock)
		mockedOrderController := OrderAttempt{OrderAttemptService: mockedOrderAttemptService}

		handler := http.HandlerFunc(mockedOrderController.ApproveOrderAttempt)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result OrderAttemptResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
		assert.Equal(tc.expectedStatus, result.Status, tc.description)
	}
}

func TestOrderAttempt_RejectOrderAttempt(t *testing.T) {
	tests := []struct {
		description       string
		inputMitraID      uint
		inputToken        uint
		inputOrderID      uint
		JSONRequest       string
		returnErrorMock   error
		expectedHTTPCode  int
		expectedErrorCode string
		expectedStatus    string
	}{
		{
			description:       "Malformed JSON",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"ini_apa_ya": true}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "JSON request not valid",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id": "huruf"}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "JSON token not exist",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id":3}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "Can not reject",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id":3, "token":2}`,
			returnErrorMock:   errors.New("Error"),
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "order_attempt_failed",
			expectedStatus:    "",
		}, {
			description:       "Success reject",
			inputMitraID:      uint(1),
			inputToken:        2,
			inputOrderID:      3,
			JSONRequest:       `{"order_id":3, "token":2}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusOK,
			expectedErrorCode: "",
			expectedStatus:    "OK",
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/mitra/order/attempt/reject", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedOrderAttemptService := new(mockServices.IOrderAttemptService)
		mockedOrderAttemptService.On("RejectOrderAttempt", tc.inputOrderID, tc.inputMitraID, tc.inputToken).Return(tc.returnErrorMock)
		mockedOrderController := OrderAttempt{OrderAttemptService: mockedOrderAttemptService}

		handler := http.HandlerFunc(mockedOrderController.RejectOrderAttempt)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result OrderAttemptResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
		assert.Equal(tc.expectedStatus, result.Status, tc.description)
	}
}
