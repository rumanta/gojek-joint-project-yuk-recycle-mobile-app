package mitracontrollers

type JSONError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}
