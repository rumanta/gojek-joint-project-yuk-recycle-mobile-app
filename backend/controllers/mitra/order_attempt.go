package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"encoding/json"
	"net/http"

	"gopkg.in/go-playground/validator.v9"
)

type OrderAttempt struct {
	OrderAttemptService services.IOrderAttemptService
}

type OrderAttemptRequest struct {
	OrderID uint `json:"order_id,omitempty" validate:"required"`
	Token   uint `json:"token,omitempty" validate:"required"`
}

type OrderAttemptResponse struct {
	Error  *JSONError `json:"error,omitempty"`
	Status string     `json:"status,omitempty"`
}

func (o OrderAttempt) ApproveOrderAttempt(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("UserID").(uint)
	data, err := decodeOrderAttemptRequest(r)

	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &OrderAttemptResponse{Error: e})
		return
	}

	err = o.OrderAttemptService.ApproveOrderAttempt(data.OrderID, userId, data.Token)

	if err != nil {
		e := &JSONError{Code: "order_attempt_failed", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &OrderAttemptResponse{Error: e})
		return
	}

	writeEncoded(w, &OrderAttemptResponse{Status: "OK"})
}

func (o OrderAttempt) RejectOrderAttempt(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("UserID").(uint)
	data, err := decodeOrderAttemptRequest(r)

	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &OrderAttemptResponse{Error: e})
		return
	}

	err = o.OrderAttemptService.RejectOrderAttempt(data.OrderID, userId, data.Token)

	if err != nil {
		e := &JSONError{Code: "order_attempt_failed", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &OrderAttemptResponse{Error: e})
		return
	}

	writeEncoded(w, &OrderAttemptResponse{Status: "OK"})
}

func decodeOrderAttemptRequest(r *http.Request) (*OrderAttemptRequest, error) {
	request := new(OrderAttemptRequest)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	if err := decoder.Decode(request); err != nil {
		return nil, err
	}

	validate := validator.New()
	err := validate.Struct(request)

	if err != nil {
		return nil, err
	}

	return request, nil
}
