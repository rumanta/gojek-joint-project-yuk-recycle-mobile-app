package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestSettings_GetSettings(t *testing.T) {
	tests := []struct {
		description                     string
		inputMitraID                    uint
		returnSchedulesMock             *services.Schedules
		returnGarbageSettingsMock       []services.GarbageSetting
		returnMaximumPickUpDistanceMock uint
		returnErrorMock                 error
		expectedHTTPCode                int
		expectedBody                    string
	}{
		{
			description:  "Get data success",
			inputMitraID: uint(1),
			returnSchedulesMock: &services.Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			returnGarbageSettingsMock: []services.GarbageSetting{
				services.GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 1, Price: 500},
				services.GarbageSetting{Selected: true, Title: "Sampah 2", MinimumQuantity: 2, Price: 600},
			},
			returnMaximumPickUpDistanceMock: 10,
			returnErrorMock:                 nil,
			expectedHTTPCode:                http.StatusOK,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/settings", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedSettingsService := new(mockServices.ISettingsService)
		mockedSettingsService.On("GetSettingsData", tc.inputMitraID).Return(
			tc.returnSchedulesMock,
			tc.returnGarbageSettingsMock,
			tc.returnMaximumPickUpDistanceMock,
			tc.returnErrorMock,
		)
		settingsController := Settings{SettingsService: mockedSettingsService}

		handler := http.HandlerFunc(settingsController.GetSettings)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result SettingsRequestOrResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnSchedulesMock, result.Schedules, tc.description)
		assert.Equal(tc.returnGarbageSettingsMock, result.GarbageSettings, tc.description)
		assert.Equal(tc.returnMaximumPickUpDistanceMock, result.MaximumPickUpDistance, tc.description)
	}
}

func TestSettings_UpdateSettings(t *testing.T) {
	tests := []struct {
		description                string
		inputMitraID               uint
		inputSchedules             *services.Schedules
		inputGarbageSettings       []services.GarbageSetting
		inputMaximumPickUpDistance uint
		JSONRequest                string
		returnErrorMock            error
		expectedHTTPCode           int
		expectedErrorCode          string
		expectedStatus             string
	}{
		{
			description:                "Malformed JSON",
			inputMitraID:               uint(1),
			inputSchedules:             nil,
			inputGarbageSettings:       nil,
			inputMaximumPickUpDistance: 0,
			JSONRequest:                `{"ini_apa_ya": true}`,
			returnErrorMock:            nil,
			expectedHTTPCode:           http.StatusBadRequest,
			expectedErrorCode:          "invalid_request",
			expectedStatus:             "",
		}, {
			description:                "JSON request not valid",
			inputMitraID:               uint(1),
			inputSchedules:             nil,
			inputGarbageSettings:       nil,
			inputMaximumPickUpDistance: 0,
			JSONRequest:                `{"maximum_pick_up_distance": "huruf"}`,
			returnErrorMock:            nil,
			expectedHTTPCode:           http.StatusBadRequest,
			expectedErrorCode:          "invalid_request",
			expectedStatus:             "",
		}, {
			description:                "JSON schedules not exist",
			inputMitraID:               uint(1),
			inputSchedules:             nil,
			inputGarbageSettings:       nil,
			inputMaximumPickUpDistance: 0,
			JSONRequest:                `{"garbage_settings": [{"selected": true,"title": "sampah","minimum_quantity": 1,"price": 400},{"selected": true,"title": "sampah2","minimum_quantity": 2,"price": 600}],"maximum_pick_up_distance": 5}`,
			returnErrorMock:            nil,
			expectedHTTPCode:           http.StatusBadRequest,
			expectedErrorCode:          "invalid_request",
			expectedStatus:             "",
		}, {
			description:  "Invalid data",
			inputMitraID: uint(1),
			inputSchedules: &services.Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			inputGarbageSettings: []services.GarbageSetting{
				services.GarbageSetting{Selected: true, Title: "sampah", MinimumQuantity: 1, Price: 400},
				services.GarbageSetting{Selected: true, Title: "sampah2", MinimumQuantity: 2, Price: 600},
			},
			inputMaximumPickUpDistance: 10,
			JSONRequest:                `{"schedules":{"sunday": true,"monday": true,"tuesday": true,"wednesday": true},"garbage_settings": [{"selected": true,"title": "sampah","minimum_quantity": 1,"price": 400},{"selected": true,"title": "sampah2","minimum_quantity": 2,"price": 600}],"maximum_pick_up_distance": 10}`,
			returnErrorMock:            errors.New("Error"),
			expectedHTTPCode:           http.StatusBadRequest,
			expectedErrorCode:          "invalid_request",
			expectedStatus:             "",
		}, {
			description:  "Success update",
			inputMitraID: uint(1),
			inputSchedules: &services.Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			inputGarbageSettings: []services.GarbageSetting{
				services.GarbageSetting{Selected: true, Title: "sampah", MinimumQuantity: 1, Price: 500},
				services.GarbageSetting{Selected: true, Title: "sampah2", MinimumQuantity: 2, Price: 600},
			},
			inputMaximumPickUpDistance: 10,
			JSONRequest:                `{"schedules":{"sunday": true,"monday": true,"tuesday": true,"wednesday": true},"garbage_settings": [{"selected": true,"title": "sampah","minimum_quantity": 1,"price": 500},{"selected": true,"title": "sampah2","minimum_quantity": 2,"price": 600}],"maximum_pick_up_distance": 10}`,
			returnErrorMock:            nil,
			expectedHTTPCode:           http.StatusOK,
			expectedErrorCode:          "",
			expectedStatus:             "Ok",
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/mitra/settings", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputMitraID))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedSettingsService := new(mockServices.ISettingsService)
		mockedSettingsService.On("UpdateSettingsData", tc.inputMitraID, tc.inputSchedules, tc.inputGarbageSettings, tc.inputMaximumPickUpDistance).Return(tc.returnErrorMock)
		settingsController := Settings{SettingsService: mockedSettingsService}

		handler := http.HandlerFunc(settingsController.UpdateSettings)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result SettingsRequestOrResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
		assert.Equal(tc.expectedStatus, result.Status, tc.description)
	}
}
