package mitracontrollers

import (
	"PPLA4/backend/middleware"
	services "PPLA4/backend/services/mitra"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"gopkg.in/go-playground/validator.v9"
)

type Registration struct {
	RegistrationService services.IRegistrationService
}

type RegisterResponse struct {
	Username *string           `json:"username,omitempty"`
	Token    *middleware.Token `json:"token,omitempty"`
	Error    *JSONError        `json:"error,omitempty"`
	Status   string            `json:"status,omitempty"`
}

type RegisterRequest struct {
	Phone    string `json:"phone" validate:"required,numeric,min=5"`
	Password string `json:"password" validate:"required,min=6,max=30"`
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
}

func (reg Registration) Register(w http.ResponseWriter, r *http.Request) {
	req, err := decodeRegisterRequest(r)
	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &RegisterResponse{Error: e})
		return
	}

	ret, err := reg.RegistrationService.Register(req.Phone, req.Password, req.Name, req.Email)

	if err != nil {
		e := &JSONError{Code: "unauthorized", Message: err.Error()}
		w.WriteHeader(http.StatusUnauthorized)
		writeEncoded(w, &RegisterResponse{Error: e})
		return
	}

	writeEncoded(w, &RegisterResponse{Token: ret, Status: "ok"})
}

func decodeRegisterRequest(r *http.Request) (*RegisterRequest, error) {
	req := new(RegisterRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}

	validate := validator.New()
	err := validate.Struct(req)

	if err != nil {
		return nil, err
	}

	// up to now, number is still valid in pascal real number format.
	// convert +6281234567 to 081234567
	if strings.Contains(req.Phone, "+62") {
		req.Phone = strings.Replace(req.Phone, "+62", "0", 1)
	}

	// check if it still contains '+', or '-'
	if strings.Contains(req.Phone, "+") || strings.Contains(req.Phone, "-") {
		return nil, errors.New("Phone number shouldn't contain + or -")
	}

	// check if prefix is 08. TODO: check with operator
	if !strings.HasPrefix(req.Phone, "08") {
		return nil, errors.New("Phone number doesn't start with +628 or 08")
	}

	return req, nil
}
