package mitracontrollers

import (
	"encoding/json"
	"strings"

	"net/http"
	"net/http/httptest"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"
	services "PPLA4/backend/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestGetTermsAndConditionsData(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/mitra/homepage", strings.NewReader(""))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	mockedTermsAndConditionsService := new(mockServices.ITermsAndConditionsService)
	mockedTermsAndConditionsService.On("GetTermsAndConditionsDetail").Return(
		[]services.TermsAndConditionsInfoDetail{
			services.TermsAndConditionsInfoDetail{Title: "TermsAndConditions 1", Body: "Body 1"},
		},
	)
	homepageController := TermsAndConditions{TermsAndConditionsService: mockedTermsAndConditionsService}

	handler := http.HandlerFunc(homepageController.GetTermsAndConditionsData)
	handler.ServeHTTP(rr, req)

	returnedBody := rr.Body.String()
	var result TermsAndConditionsInfoResponse
	json.Unmarshal([]byte(returnedBody), &result)

	assert.True(t, len(result.Data) > 0)
}
