package mitracontrollers

import (
	"PPLA4/backend/middleware"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	mockServices "PPLA4/backend/mocks/services/mitra"

	"github.com/stretchr/testify/assert"
)

func TestDecodeRegisterRequest(t *testing.T) {
	tests := []struct {
		description         string
		JSONRequest         string
		expectedRequest     *RegisterRequest
		expectedExistsError bool
	}{
		{
			description:         "Unknown field",
			JSONRequest:         `{"password":"abcdefghi", "location":"jakarta"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is missing",
			JSONRequest:         `{"password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Password is missing",
			JSONRequest:         `{"phone":"0812345678", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Name is missing",
			JSONRequest:         `{"phone":"0812345678", "password":"abcdefghi", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Email is invalid",
			JSONRequest:         `{"phone":"0812345678", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholehgmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Email is invalid",
			JSONRequest:         `{"phone":"0812345678", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmailcom"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is empty",
			JSONRequest:         `{"phone":"", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Password is less than 6 characters",
			JSONRequest:         `{"phone":"0812345678", "password":"abc", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is not numeric",
			JSONRequest:         `{"phone":"abcdef", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Success decoding",
			JSONRequest:         `{"phone":"+62812345678", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     &RegisterRequest{Phone: "0812345678", Password: "abcdefghi", Name: "yusuf sholeh", Email: "yusufsholeh@gmail.com"},
			expectedExistsError: false,
		}, {
			description:         "Phone doesn't start with 08",
			JSONRequest:         `{"phone":"+6212345678", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone doesn't start with 08",
			JSONRequest:         `{"phone":"812345678", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is pascal numeric",
			JSONRequest:         `{"phone":"-62812345678", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is less than 5 digits",
			JSONRequest:         `{"phone":"0813", "password":"abcdefghi", "name":"yusuf sholeh", "email":"yusufsholeh@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/register", strings.NewReader(tc.JSONRequest))
		ret, err := decodeRegisterRequest(req)
		assert.Equal(tc.expectedRequest, ret, tc.description)
		assert.Equal(tc.expectedExistsError, (err != nil), tc.description)
	}
}

func TestRegister(t *testing.T) {
	tests := []struct {
		description       string
		JSONRequest       string
		inputPhoneMock    string
		inputPasswordMock string
		inputNameMock     string
		inputEmailMock    string
		returnTokenMock   *middleware.Token
		returnErrorMock   error
		expectedHTTPCode  int
		expectedErrorCode string
		expectedStatus    string
		// expectedBody      string
	}{
		{
			description:       "Given unknown field return invalid_request",
			JSONRequest:       `{}`,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
			expectedStatus:    "",
		}, {
			description:       "Given username already exists return unauthorized",
			JSONRequest:       `{"phone":"08123456789", "password":"passuworudo", "name":"yusuf sholeh", "email": "yusuf_sholeh@gmail.com"}`,
			inputPhoneMock:    "08123456789",
			inputPasswordMock: "passuworudo",
			inputNameMock:     "yusuf sholeh",
			inputEmailMock:    "yusuf_sholeh@gmail.com",
			returnTokenMock:   nil,
			returnErrorMock:   errors.New("username already exists"),
			expectedHTTPCode:  http.StatusUnauthorized,
			expectedErrorCode: "unauthorized",
			expectedStatus:    "",
		}, {
			description:       "Username created",
			JSONRequest:       `{"phone": "08123456789", "password": "passuworudo", "name": "yusuf sholeh", "email": "yusuf_sholeh@gmail.com"}`,
			inputPhoneMock:    "08123456789",
			inputPasswordMock: "passuworudo",
			inputNameMock:     "yusuf sholeh",
			inputEmailMock:    "yusuf_sholeh@gmail.com",
			returnTokenMock:   &middleware.Token{Value: "qzwsxedcrfv", ExpiresAt: "12345678"},
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusOK,
			expectedErrorCode: "",
			expectedStatus:    "ok",
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/register", strings.NewReader(tc.JSONRequest))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedRegistrationServices := new(mockServices.IRegistrationService)
		mockedRegistrationServices.On("Register", tc.inputPhoneMock, tc.inputPasswordMock, tc.inputNameMock, tc.inputEmailMock).Return(tc.returnTokenMock, tc.returnErrorMock)
		registration := Registration{RegistrationService: mockedRegistrationServices}

		handler := http.HandlerFunc(registration.Register)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()

		var result AuthenticateResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
		assert.Equal(tc.expectedStatus, result.Status, tc.description)
	}
}
