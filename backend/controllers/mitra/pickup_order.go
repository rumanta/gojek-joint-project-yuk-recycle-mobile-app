package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type PickUpOrderController struct {
	PickUpOrderService services.IPickUpOrderService
}

type PickUpOrderResponse struct {
	Error  *JSONError `json:"error,omitempty"`
	Status string     `json:"status,omitempty"`
}

func (p PickUpOrderController) PickUpOrderHandler(w http.ResponseWriter, r *http.Request) {
	orderID, err := strconv.Atoi(mux.Vars(r)["orderID"])

	if err != nil {
		e := &JSONError{Code: "Invalid_order_id", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &PickUpOrderResponse{Error: e})
		return
	}

	userID := r.Context().Value("UserID").(uint)

	err = p.PickUpOrderService.PickUpOrder(uint(orderID), uint(userID))

	if err != nil {
		e := &JSONError{Code: "service_error", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &PickUpOrderResponse{Error: e})
		return
	}

	writeEncoded(w, &PickUpOrderResponse{Status: "OK"})
}
