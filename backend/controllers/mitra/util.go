package mitracontrollers

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func writeEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
	fmt.Println(data)
}

func writeError(w http.ResponseWriter, err error) {
	e := &JSONError{Code: "invalid_request", Message: err.Error()}
	w.WriteHeader(http.StatusBadRequest)
	writeEncoded(w, &SettingsRequestOrResponse{Error: e})
}
