package mitracontrollers

import (
	"encoding/json"
	"net/http"
)

// Dummy end point for checking token
var Dummy = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var ret map[string]interface{}
	if r != nil && r.Context().Value("UserID") != nil && r.Context().Value("UserType") != nil && r.Context().Value("LoginType") != nil {
		ret = map[string]interface{}{
			"message":    "Success calling, token is valid",
			"user_id":    r.Context().Value("UserID").(uint),
			"user_type":  r.Context().Value("UserType").(string),
			"login_type": r.Context().Value("LoginType").(int),
		}
	}
	json.NewEncoder(w).Encode(ret)
}
