package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type CompleteOrderController struct {
	CompleteOrderService services.ICompleteOrderService
}

type CompleteOrderResponse struct {
	Error  *JSONError `json:"error,omitempty"`
	Status string     `json:"status,omitempty"`
}

func (p CompleteOrderController) CompleteOrderHandler(w http.ResponseWriter, r *http.Request) {
	orderID, err := strconv.Atoi(mux.Vars(r)["orderID"])

	if err != nil {
		e := &JSONError{Code: "Invalid_order_id", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &CompleteOrderResponse{Error: e})
		return
	}

	userID := r.Context().Value("UserID").(uint)

	err = p.CompleteOrderService.CompleteOrder(uint(orderID), uint(userID))

	if err != nil {
		e := &JSONError{Code: "service_error", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &CompleteOrderResponse{Error: e})
		return
	}

	writeEncoded(w, &CompleteOrderResponse{Status: "OK"})
}
