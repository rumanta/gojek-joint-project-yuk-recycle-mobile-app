package mitracontrollers

import (
	services "PPLA4/backend/services/mitra"
	"net/http"
)

type OrderHistory struct {
	OrderHistoryService services.IOrderHistoryService
}

type OrderHistoryResponse struct {
	Data  []services.OrderHistory `json:"data,omitempty"`
	Error *JSONError              `json:"error,omitempty"`
}

func (a OrderHistory) GetOrderHistories(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("UserID").(uint)
	orderHistories, err := a.OrderHistoryService.GetOrderHistoriesData(userId)

	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &OrderHistoryResponse{Error: e})
		return
	}

	data := &OrderHistoryResponse{
		Data: orderHistories,
	}
	writeEncoded(w, data)
}
