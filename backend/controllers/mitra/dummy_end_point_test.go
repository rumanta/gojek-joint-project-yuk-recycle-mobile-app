package mitracontrollers

import (
	"context"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDummyFail(t *testing.T) {
	req, _ := http.NewRequest("GET", "/login", strings.NewReader(``))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Dummy)
	handler.ServeHTTP(rr, req)

	returnedBody := rr.Body.String()
	assert.Equal(t, "null\n", returnedBody, "Value should be null")
}

func TestDummySuccess(t *testing.T) {
	req, _ := http.NewRequest("GET", "/login", strings.NewReader(``))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Dummy)
	ctx := req.Context()
	ctx = context.WithValue(ctx, "UserID", uint(15))
	req = req.WithContext(ctx)
	ctx = context.WithValue(ctx, "UserType", "somevalue")
	req = req.WithContext(ctx)
	ctx = context.WithValue(ctx, "LoginType", 1)
	req = req.WithContext(ctx)
	handler.ServeHTTP(rr, req)

	returnedBody := rr.Body.String()
	assert.Equal(t, returnedBody != "null\n", true, "Calling dummy when token is valid")
}
