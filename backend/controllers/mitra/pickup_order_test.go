package mitracontrollers

import (
	mocks "PPLA4/backend/mocks/services/mitra"
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestPickUpOrder(t *testing.T) {
	tests := []struct {
		description      string
		orderID          string
		mitraID          uint
		ReturnErrorMock  error
		expectedHTTPCode int
	}{
		{
			description:      "Order id isn't uint",
			orderID:          "a",
			mitraID:          uint(1),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "pick up isn't success",
			orderID:          "3",
			mitraID:          uint(1),
			ReturnErrorMock:  errors.New("got some errors"),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "pick up success",
			orderID:          "3",
			mitraID:          uint(1),
			ReturnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/mitra/order/3", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.mitraID))
		req = mux.SetURLVars(req, map[string]string{"orderID": tc.orderID})
		if err != nil {
			t.Fatal(err)
		}

		mockedPickUpOrderService := new(mocks.IPickUpOrderService)
		mockedPickUpOrderService.On("PickUpOrder", uint(3), uint(1)).Return(tc.ReturnErrorMock)
		pickUpOrderController := PickUpOrderController{PickUpOrderService: mockedPickUpOrderService}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(pickUpOrderController.PickUpOrderHandler)
		handler.ServeHTTP(rr, req)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}
