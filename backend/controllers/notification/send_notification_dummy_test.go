package notification

import (
	mockServices "PPLA4/backend/mocks/services/notification"
	services "PPLA4/backend/services/notification"

	"github.com/stretchr/testify/mock"

	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/NaySoftware/go-fcm"
	"github.com/stretchr/testify/assert"
)

func TestNotify(t *testing.T) {
	tests := []struct {
		description      string
		JSONRequest      string
		ClientTokens     []string
		Payload          *fcm.NotificationPayload
		Data             *services.NotificationService
		ReturnStatusMock *fcm.FcmResponseStatus
		ReturnErrorMock  error
		expectedHTTPCode int
	}{
		{
			description:      "Error decoding with Unknown Field",
			JSONRequest:      `{"client_tokens":["a","b"], "location":"jakarta"}`,
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:  "Error send notification",
			JSONRequest:  `{"client_tokens":["a","b"]}`,
			ClientTokens: []string{"a", "b"},
			Payload: &fcm.NotificationPayload{
				Title: "FCM Message",
				Body:  "This is an FCM Message",
			},
			Data:             &services.NotificationService{},
			ReturnErrorMock:  errors.New("Error on sending notification"),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:  "Success send notification",
			JSONRequest:  `{"client_tokens":["a","b"]}`,
			ClientTokens: []string{"a", "b"},
			Payload: &fcm.NotificationPayload{
				Title: "FCM Message",
				Body:  "This is an FCM Message",
			},
			Data:             &services.NotificationService{},
			ReturnStatusMock: new(fcm.FcmResponseStatus),
			ReturnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/notification/dummy", strings.NewReader(tc.JSONRequest))
		if err != nil {
			t.Fatal(err)
		}

		mockedNotificationService := new(mockServices.INotificationService)
		mockedNotificationService.On("SendNotification",
			tc.ClientTokens,
			mock.Anything,
			mock.Anything,
		).Return(tc.ReturnStatusMock, tc.ReturnErrorMock)
		dummyNotificationController := DummyNotificationController{NotificationService: mockedNotificationService}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(dummyNotificationController.NotifyHandler)
		handler.ServeHTTP(rr, req)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}

func TestDecodeNotifyRequest(t *testing.T) {
	tests := []struct {
		description          string
		JSONRequest          string
		expectedClientTokens []string
		expectedError        error
	}{
		{
			description:          "Unknown Field",
			JSONRequest:          `{"client_tokens": ["a","b","c"], "location":"jakarta"}`,
			expectedClientTokens: []string{},
			expectedError:        errors.New(`json: unknown field "location"`),
		}, {
			description:          "Success Decoding",
			JSONRequest:          `{"client_tokens": ["a","b","c"]}`,
			expectedClientTokens: []string{"a", "b", "c"},
			expectedError:        nil,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/login", strings.NewReader(tc.JSONRequest))
		clientTokens, err := decodeDummyNotification(req)
		assert.Equal(tc.expectedClientTokens, clientTokens, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
