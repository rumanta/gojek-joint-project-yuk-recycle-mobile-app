package notification

import (
	services "PPLA4/backend/services/notification"
	"encoding/json"
	"fmt"
	"net/http"
)

type UpdateTokenController struct {
	UpdateTokenService services.IUpdateTokenService
}

type UpdateTokenRequest struct {
	Token string `json:"token"`
}

type UpdateTokenResponse struct {
	Error  *JSONError `json:"error,omitempty"`
	Status string     `json:"status,omitempty"`
}

func (u UpdateTokenController) UpdateTokenHandler(w http.ResponseWriter, r *http.Request) {
	token, err := decodeUpdateTokenRequest(r)
	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &UpdateTokenResponse{Error: e})
		fmt.Println(err.Error())
		return
	}

	userId := r.Context().Value("UserID").(uint)
	userType := r.Context().Value("UserType").(string)
	err = u.UpdateTokenService.UpdateToken(userId, userType, token)

	if err != nil {
		e := &JSONError{Code: "data_update_error", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &UpdateTokenResponse{Error: e})
		fmt.Println(err.Error())
		return
	}

	writeEncoded(w, &UpdateTokenResponse{Status: "success update notification token"})
}

func decodeUpdateTokenRequest(r *http.Request) (string, error) {
	req := new(UpdateTokenRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return "", err
	}
	return req.Token, nil
}
