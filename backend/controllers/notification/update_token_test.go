package notification

import (
	"PPLA4/backend/middleware"

	mockServices "PPLA4/backend/mocks/services/notification"

	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUpdateToken(t *testing.T) {
	tests := []struct {
		description      string
		JSONRequest      string
		token            string
		ReturnErrorMock  error
		expectedHTTPCode int
	}{
		{
			description:      "Error decoding with Unknown Field",
			JSONRequest:      `{"token":"abcdef", "location":"jakarta"}`,
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Error database update token",
			JSONRequest:      `{"token":"abcdef"}`,
			token:            "abcdef",
			ReturnErrorMock:  errors.New("Cannot update token"),
			expectedHTTPCode: http.StatusBadRequest,
		}, {
			description:      "Success Update Token on Database",
			JSONRequest:      `{"token":"abcdef"}`,
			token:            "abcdef",
			ReturnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}
	userID := uint(100)
	userType := middleware.MitraUserType
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/notification/token", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", userID))
		req = req.WithContext(context.WithValue(req.Context(), "UserType", userType))
		if err != nil {
			t.Fatal(err)
		}

		mockedUpdateTokenServices := new(mockServices.IUpdateTokenService)
		mockedUpdateTokenServices.On("UpdateToken", userID, userType, tc.token).Return(tc.ReturnErrorMock)
		updateTokenController := UpdateTokenController{UpdateTokenService: mockedUpdateTokenServices}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(updateTokenController.UpdateTokenHandler)
		handler.ServeHTTP(rr, req)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
	}
}

func TestDecodeUpdateTokenRequest(t *testing.T) {
	tests := []struct {
		description   string
		JSONRequest   string
		expectedToken string
		expectedError error
	}{
		{
			description:   "Unknown Field",
			JSONRequest:   `{"token":"abcdef", "location":"jakarta"}`,
			expectedToken: "",
			expectedError: errors.New(`json: unknown field "location"`),
		}, {
			description:   "Success Decoding",
			JSONRequest:   `{"token":"abcdef"}`,
			expectedToken: "abcdef",
			expectedError: nil,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/login", strings.NewReader(tc.JSONRequest))
		token, err := decodeUpdateTokenRequest(req)
		assert.Equal(tc.expectedToken, token, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
