package notification

import (
	services "PPLA4/backend/services/notification"
	"encoding/json"
	"net/http"

	"github.com/NaySoftware/go-fcm"
)

type DummyNotificationController struct {
	NotificationService services.INotificationService
}

type NotifyRequest struct {
	ClientTokens []string `json:"client_tokens,omitempty"`
}

type NotifyResponse struct {
	Error    *JSONError             `json:"error,omitempty"`
	Status   string                 `json:"status,omitempty"`
	Response *fcm.FcmResponseStatus `json:"response,omitempty"`
}

func (d DummyNotificationController) NotifyHandler(w http.ResponseWriter, r *http.Request) {
	clientTokens, err := decodeDummyNotification(r)
	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &NotifyResponse{Error: e})
		return
	}

	status, err := d.NotificationService.SendNotification(clientTokens,
		&fcm.NotificationPayload{
			Title: "FCM Message",
			Body:  "This is an FCM Message",
		},
		&services.NotificationData{},
	)

	if err != nil {
		e := &JSONError{Code: "notify_error", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &NotifyResponse{Error: e})
		return
	}

	writeEncoded(w, &NotifyResponse{Status: "notification sent", Response: status})
}

func decodeDummyNotification(r *http.Request) ([]string, error) {
	request := new(NotifyRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(request); err != nil {
		return []string{}, err
	}
	return request.ClientTokens, nil
}
