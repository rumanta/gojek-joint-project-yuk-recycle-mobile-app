package notification

import (
	"encoding/json"
	"net/http"
)

func writeEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
