package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"net/http"
)

type GarbageType struct {
	GarbageTypeService services.IGarbageTypeService
}

type GarbageTypeListResponse struct {
	GarbageTypeList []services.GarbageTypeData `json:"garbage_types,omitempty"`
	Error           *JSONError                 `json:"error,omitempty"`
	Status          string                     `json:"status,omitempty"`
}

func (g GarbageType) GetGarbageTypeList(w http.ResponseWriter, r *http.Request) {
	garbageTypeList, err := g.GarbageTypeService.GetGarbageTypeList()

	if err != nil {
		writeErrorGarbageType(w, err)
		return
	}

	data := GarbageTypeListResponse{
		GarbageTypeList: garbageTypeList,
	}
	writeEncoded(w, data)
}

func writeErrorGarbageType(w http.ResponseWriter, err error) {
	e := &JSONError{Code: "invalid_request", Message: err.Error()}
	w.WriteHeader(http.StatusBadRequest)
	writeEncoded(w, &GarbageTypeListResponse{Error: e})
}
