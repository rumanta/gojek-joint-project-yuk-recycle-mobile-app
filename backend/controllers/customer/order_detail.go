package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type OrderDetail struct {
	OrderDetailService services.IOrderDetailService
}

type ErrorResponse struct {
	Error *JSONError `json:"error,omitempty"`
}

func (o OrderDetail) GetOrderDetail(w http.ResponseWriter, r *http.Request) {
	orderID, err := strconv.Atoi(mux.Vars(r)["orderID"])

	if err != nil {
		writeOrderDetailError(w, errors.New("order id is not valid"))
		return
	}

	userID := r.Context().Value("UserID").(uint)
	response, err := o.OrderDetailService.GetOrderDetailData(uint(orderID), userID)

	if err != nil {
		writeOrderDetailError(w, err)
		return
	}

	writeEncoded(w, response)
}

func writeOrderDetailError(w http.ResponseWriter, err error) {
	e := &JSONError{Code: "invalid_request", Message: err.Error()}
	w.WriteHeader(http.StatusBadRequest)
	writeEncoded(w, &ErrorResponse{Error: e})
}
