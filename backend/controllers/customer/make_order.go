package customercontrollers

import (
	"PPLA4/backend/middleware"
	services "PPLA4/backend/services/customer"
	worker "PPLA4/backend/services/worker"
	"encoding/json"
	"fmt"
	"net/http"

	"gopkg.in/go-playground/validator.v9"
)

type MakeOrderController struct {
	MakeOrderWorker worker.IMakeOrderWorker
}

type MakeOrderResponse struct {
	MitraID uint       `json:"mitra_id",omitempty"`
	Error   *JSONError `json:"error,omitempty"`
	Status  string     `json:"status,omitempty"`
}

func (m MakeOrderController) MakeOrderHandler(w http.ResponseWriter, r *http.Request) {
	userType := r.Context().Value("UserType").(string)
	if userType != middleware.CustomerUserType {
		e := &JSONError{Code: "malformed_token", Message: "malformed_token"}
		w.WriteHeader(http.StatusForbidden)
		writeEncoded(w, &MakeOrderResponse{Error: e})
		fmt.Println("malformed_token make order")
		return
	}

	req, err := decodeMakeOrderRequest(r)
	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &MakeOrderResponse{Error: e})
		fmt.Println(err.Error())
		return
	}

	req.CustomerID = r.Context().Value("UserID").(uint)
	_, err = m.MakeOrderWorker.Work(req)

	if err != nil {
		e := &JSONError{Code: "make_order_failed", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &MakeOrderResponse{Error: e})
		fmt.Println(err.Error())
		return
	}

	writeEncoded(w, &MakeOrderResponse{
		Status: "order has been processed",
	})
}

func decodeMakeOrderRequest(r *http.Request) (*services.MakeOrderRequest, error) {
	req := new(services.MakeOrderRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}

	validate := validator.New()
	err := validate.Struct(req)

	if err != nil {
		return nil, err
	}

	return req, nil
}
