package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"net/http"
)

type ActiveOrder struct {
	ActiveOrderService services.IActiveOrderService
}

type ActiveOrderResponse struct {
	Data  []services.ActiveOrder `json:"data,omitempty"`
	Error *JSONError             `json:"error,omitempty"`
}

func (a ActiveOrder) GetActiveOrders(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("UserID").(uint)
	activeOrders, err := a.ActiveOrderService.GetActiveOrdersData(userId)

	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &ActiveOrderResponse{Error: e})
		return
	}

	data := &ActiveOrderResponse{
		Data: activeOrders,
	}
	writeEncoded(w, data)
}
