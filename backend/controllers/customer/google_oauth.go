package customercontrollers

import (
	"encoding/json"
	"net/http"

	"PPLA4/backend/middleware"
	services "PPLA4/backend/services/customer"
)

type OauthHandlerRequest struct {
	Code string `json:"code"`
}

type OauthHandlerResponse struct {
	Token  *middleware.Token `json:"token,omitempty"`
	Error  *JSONError        `json:"error,omitempty"`
	Status string            `json:"status,omitempty"`
}

type GoogleOAuth struct {
	GoogleOAuthService services.IGoogleOAuthService
}

func (g GoogleOAuth) GoogleOAuthHandler(w http.ResponseWriter, r *http.Request) {
	oauthRequest := &OauthHandlerRequest{}
	err := json.NewDecoder(r.Body).Decode(&oauthRequest)

	if err != nil {
		e := &JSONError{Code: "json_request_error", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &OauthHandlerResponse{Error: e})
		return
	}

	token, err := g.GoogleOAuthService.OAuthLogin(oauthRequest.Code)

	if err != nil {
		e := &JSONError{Code: "authorization_error", Message: err.Error()}
		w.WriteHeader(http.StatusUnauthorized)
		writeEncoded(w, &OauthHandlerResponse{Error: e})
		return
	}

	writeEncoded(w, &OauthHandlerResponse{Token: token, Status: "logged_in"})
}
