package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"gopkg.in/go-playground/validator.v9"
)

type ManageAccount struct {
	ManageAccountService services.IManageAccountService
}

type PrivacyPolicyResponse struct {
	Data []services.PrivacyPolicyDetail `json:"data,omitempty"`
}

type CustomerRequest struct {
	Phone       string `json:"phone,omitempty" validate:"numeric,min=5"`
	Name        string `json:"name,omitempty"`
	Email       string `json:"email,omitempty"  validate:"email"`
	OldPassword string `json:"old_password,omitempty"`
	NewPassword string `json:"new_password,omitempty"`
}

type CustomerResponse struct {
	CustomerData *services.CustomerData `json:"customer_data,omitempty"`
	Error        *JSONError             `json:"error,omitempty"`
	Status       string                 `json:"status,omitempty"`
}

func (m ManageAccount) GetPrivacyPolicyDetail(w http.ResponseWriter, r *http.Request) {
	listDetail := m.ManageAccountService.GetPrivacyPolicyDetail()
	data := &PrivacyPolicyResponse{
		Data: listDetail,
	}
	writeEncoded(w, data)
}

func (m ManageAccount) GetCustomerData(w http.ResponseWriter, r *http.Request) {
	customerId := r.Context().Value("UserID").(uint)
	customerData, _ := m.ManageAccountService.GetCustomerData(customerId)
	data := &CustomerResponse{
		CustomerData: customerData,
	}
	writeEncoded(w, data)
}

func (m ManageAccount) UpdateCustomerData(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("UserID").(uint)
	data, err := decodeCustomerDataRequest(r)

	if err != nil {
		writeError(w, err)
		return
	}

	customerData := services.CustomerData{
		Phone:       data.Phone,
		Name:        data.Name,
		Email:       data.Email,
		OldPassword: data.OldPassword,
		NewPassword: data.NewPassword,
	}
	err = m.ManageAccountService.UpdateCustomerData(userId, &customerData)

	if err != nil {
		writeError(w, err)
		return
	}

	response := &CustomerResponse{
		CustomerData: &customerData,
		Status:       "Ok",
	}
	writeEncoded(w, response)
}

func decodeCustomerDataRequest(r *http.Request) (*CustomerRequest, error) {
	request := new(CustomerRequest)
	decoder := json.NewDecoder(r.Body)

	decoder.DisallowUnknownFields()

	if err := decoder.Decode(request); err != nil {
		return nil, err
	}

	validate := validator.New()
	err := validate.Struct(request)

	if err != nil {
		return nil, err
	}

	// up to now, number is still valid in pascal real number format.
	// convert +6281234567 to 081234567
	if strings.Contains(request.Phone, "+62") {
		request.Phone = strings.Replace(request.Phone, "+62", "0", 1)
	}

	// check if it still contains '+', or '-'
	if strings.Contains(request.Phone, "+") || strings.Contains(request.Phone, "-") {
		return nil, errors.New("Phone number shouldn't contain + or -")
	}

	// check if prefix is 08. TODO: check with operator
	if !strings.HasPrefix(request.Phone, "08") {
		return nil, errors.New("Phone number doesn't start with +628 or 08")
	}

	return request, nil
}

func writeError(w http.ResponseWriter, err error) {
	e := &JSONError{Code: "invalid_request", Message: err.Error()}
	w.WriteHeader(http.StatusBadRequest)
	writeEncoded(w, &CustomerResponse{Error: e})
}
