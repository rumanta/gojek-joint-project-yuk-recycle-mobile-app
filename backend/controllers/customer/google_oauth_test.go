package customercontrollers

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"PPLA4/backend/middleware"
	mockService "PPLA4/backend/mocks/services/customer"

	"github.com/stretchr/testify/assert"
)

func TestGoogleOAuthHandlerWhenErrorDecoding(t *testing.T) {
	req, err := http.NewRequest("GET", "/register", strings.NewReader(`invalid_json_format{invalid}`))
	if err != nil {
		t.Errorf("there was an unexpected error")
		return
	}
	rr := httptest.NewRecorder()
	googleOAuth := GoogleOAuth{}
	handler := http.HandlerFunc(googleOAuth.GoogleOAuthHandler)
	handler.ServeHTTP(rr, req)

	returnedBody := rr.Body.String()

	var result OauthHandlerResponse
	json.Unmarshal([]byte(returnedBody), &result)

	assert.Equal(t, result.Error.Code, "json_request_error", "JSON request should be error")
}

func TestGoogleOAuthHandlerWhenErrorLoggingIn(t *testing.T) {
	req, err := http.NewRequest("GET", "/register", strings.NewReader(`{"code": "abcdefghi"}`))
	if err != nil {
		t.Errorf("there was an unexpected error")
		return
	}
	rr := httptest.NewRecorder()
	mockedGoogleOAuthService := new(mockService.IGoogleOAuthService)
	mockedGoogleOAuthService.On("OAuthLogin", "abcdefghi").Return(nil, errors.New("an error has occured"))
	googleOAuth := GoogleOAuth{GoogleOAuthService: mockedGoogleOAuthService}
	handler := http.HandlerFunc(googleOAuth.GoogleOAuthHandler)
	handler.ServeHTTP(rr, req)

	returnedBody := rr.Body.String()

	var result OauthHandlerResponse
	json.Unmarshal([]byte(returnedBody), &result)

	assert.Equal(t, result.Error.Code, "authorization_error", "Error occured when logging in")
}

func TestGoogleOAuthHandlerSuccess(t *testing.T) {
	req, err := http.NewRequest("GET", "/register", strings.NewReader(`{"code": "abcdefghi"}`))
	if err != nil {
		t.Errorf("there was an unexpected error")
		return
	}
	rr := httptest.NewRecorder()
	mockedGoogleOAuthService := new(mockService.IGoogleOAuthService)
	mockedGoogleOAuthService.On("OAuthLogin", "abcdefghi").Return(&middleware.Token{}, nil)
	googleOAuth := GoogleOAuth{GoogleOAuthService: mockedGoogleOAuthService}
	handler := http.HandlerFunc(googleOAuth.GoogleOAuthHandler)
	handler.ServeHTTP(rr, req)

	returnedBody := rr.Body.String()

	var result OauthHandlerResponse
	json.Unmarshal([]byte(returnedBody), &result)

	assert.Equal(t, result.Status, "logged_in", "Status should be logged in")
}
