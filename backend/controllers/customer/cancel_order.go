package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type CancelOrderController struct {
	CancelOrderService services.ICancelOrderService
}

type CancelOrderRequest struct {
	CancellationReason string `json:"reason"`
}

type CancelOrderResponse struct {
	Error  *JSONError `json:"error,omitempty"`
	Status string     `json:"status,omitempty"`
}

func (p CancelOrderController) CancelOrderHandler(w http.ResponseWriter, r *http.Request) {
	orderID, err := strconv.Atoi(mux.Vars(r)["orderID"])

	if err != nil {
		e := &JSONError{Code: "Invalid_order_id", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &CancelOrderResponse{Error: e})
		return
	}

	reason, err := decodeCancelOrderRequest(r)

	if err != nil {
		e := &JSONError{Code: "decoding_body_failed", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &CancelOrderResponse{Error: e})
		return
	}

	userID := r.Context().Value("UserID").(uint)

	err = p.CancelOrderService.CancelOrder(uint(orderID), uint(userID), reason)

	if err != nil {
		e := &JSONError{Code: "service_error", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &CancelOrderResponse{Error: e})
		return
	}

	writeEncoded(w, &CancelOrderResponse{Status: "OK"})
}

func decodeCancelOrderRequest(r *http.Request) (string, error) {
	req := new(CancelOrderRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return "", err
	}
	return req.CancellationReason, nil
}
