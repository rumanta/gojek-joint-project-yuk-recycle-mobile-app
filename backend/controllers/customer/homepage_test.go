package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"encoding/json"
	"strings"

	"net/http"
	"net/http/httptest"
	"testing"

	mockServices "PPLA4/backend/mocks/services/customer"

	"github.com/stretchr/testify/assert"
)

func TestHomepage_GetHomepageData(t *testing.T) {
	tests := []struct {
		description                  string
		returnHomepageInfoDetailMock []services.HomepageInfoDetail
		returnErrorMock              error
		expectedHTTPCode             int
		expectedBody                 string
	}{
		{
			description: "Get data success",
			returnHomepageInfoDetailMock: []services.HomepageInfoDetail{
				services.HomepageInfoDetail{Title: "What Is Recycle", Body: "Yuk-recycle adalah aplikasi yang membantu Indonesia untuk Go-Green"},
				services.HomepageInfoDetail{Title: "Why It Matters", Body: "Kebersihan lingkungan perlu kita jaga, Yuk-Recycle menghadirkan aplikasi bisnis untuk membantu mitra mendapatkan sampah-sampah dari rumah warga"},
				services.HomepageInfoDetail{Title: "Pengguna aplikasi atau Customer dapat membantu Indonesia Go-Green sekaligus mendapatkan profit dari sampah yang sudah tidak digunakan lagi"},
			},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/customer/homepage", strings.NewReader(""))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedHomepageService := new(mockServices.IHomepageService)
		mockedHomepageService.On("GetHomepageInfoDetail").Return(
			tc.returnHomepageInfoDetailMock,
		)
		homepageController := Homepage{HomepageService: mockedHomepageService}

		handler := http.HandlerFunc(homepageController.GetHomepageData)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result HomepageInfoResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnHomepageInfoDetailMock, result.Data, tc.description)
	}
}
