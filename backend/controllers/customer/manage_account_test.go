package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"context"
	"encoding/json"
	"errors"
	"strings"

	"net/http"
	"net/http/httptest"
	"testing"

	mockServices "PPLA4/backend/mocks/services/customer"

	"github.com/stretchr/testify/assert"
)

func TestManageAccount_GetPrivacyPolicyDetail(t *testing.T) {
	tests := []struct {
		description                   string
		returnPrivacyPolicyDetailMock []services.PrivacyPolicyDetail
		returnErrorMock               error
		expectedHTTPCode              int
		expectedBody                  string
	}{
		{
			description: "Get data success",
			returnPrivacyPolicyDetailMock: []services.PrivacyPolicyDetail{
				services.PrivacyPolicyDetail{Title: "Personal Information Which We Collect", Body: "Kita mengumpulkan informasi yang bisa mengidentifikasi atau bisa diidentifikasi, kontak, atau mencari lokasi pengguna dan device yang memiliki informasi pengguna tersebut. Informasi personal meliputi, tapi tidak terbatas pada, nama, alamat, tanggal lahir, nomor telepon, alamat e-mail, akun bank dan jenis kelamin."},
				services.PrivacyPolicyDetail{Title: "The Use of Personal Information", Body: "Kita mungkin menggunakan informasi personal anda untuk tujuan kegunaan aplikasi ini dan sesuai dengan peraturan yang berlaku. Kita menggunakan informasi anda untuk mengidentifikasi dan mendaftarkan pengguna sebagai user, dan memverisikasi dan deaktivasi."},
				services.PrivacyPolicyDetail{Title: "Access and Correction of Personal Information", Body: "Sesuai dengan hukum yang berlaku, pengguna bisa mengajukan permintaan kepada kami untuk akses pembenaran informasi personal yang pegang dengan mengkontak kami sesuai detail di bawah"},
				services.PrivacyPolicyDetail{Title: "Security of Your Personal Information", Body: "Kemamanan informasi data anda adalah kepentingan utama yang kami jaga. Kami akan menggunakan seluruh cara untuk mengamankan informasi personal pengguna."},
			},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/customer/privacy-policy", strings.NewReader(""))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedManageAccount := new(mockServices.IManageAccountService)
		mockedManageAccount.On("GetPrivacyPolicyDetail").Return(
			tc.returnPrivacyPolicyDetailMock,
		)
		manageAccountController := ManageAccount{ManageAccountService: mockedManageAccount}

		handler := http.HandlerFunc(manageAccountController.GetPrivacyPolicyDetail)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result PrivacyPolicyResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnPrivacyPolicyDetailMock, result.Data, tc.description)
	}
}

func TestManageAccount_GetCustomerData(t *testing.T) {
	tests := []struct {
		description            string
		inputCustomerID        uint
		returnCustomerDataMock *services.CustomerData
		returnErrorMock        error
		expectedHTTPCode       int
		expectedBody           string
	}{
		{
			description:     "Get data success",
			inputCustomerID: uint(1),
			returnCustomerDataMock: &services.CustomerData{
				Phone: "+6212345678",
				Name:  "This is dummy",
				Email: "test@gmail.com",
			},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/customer/account", strings.NewReader(""))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputCustomerID))

		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedManageAccountService := new(mockServices.IManageAccountService)
		mockedManageAccountService.On("GetCustomerData", tc.inputCustomerID).Return(
			tc.returnCustomerDataMock,
			tc.returnErrorMock,
		)

		manageAccountController := ManageAccount{ManageAccountService: mockedManageAccountService}

		handler := http.HandlerFunc(manageAccountController.GetCustomerData)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result CustomerResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnCustomerDataMock, result.CustomerData, tc.description)
	}
}

func TestDecodeCustomerRequest(t *testing.T) {
	tests := []struct {
		description         string
		JSONRequest         string
		expectedRequest     *CustomerRequest
		expectedExistsError bool
	}{
		{
			description:         "Unknown field",
			JSONRequest:         `{"old_password":"abcdefghi", "location":"jakarta"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is missing",
			JSONRequest:         `{"old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Email is invalid",
			JSONRequest:         `{"phone":"0812345678", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"testgmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Email is invalid",
			JSONRequest:         `{"phone":"0812345678", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmailcom"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is empty",
			JSONRequest:         `{"phone":"", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is not numeric",
			JSONRequest:         `{"phone":"abcdef", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Success decoding",
			JSONRequest:         `{"phone":"+62812345678", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     &CustomerRequest{Phone: "0812345678", OldPassword: "abcdefghi", NewPassword: "abcdefghi", Name: "this is dummy", Email: "test@gmail.com"},
			expectedExistsError: false,
		}, {
			description:         "Phone doesn't start with 08",
			JSONRequest:         `{"phone":"+6212345678", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone doesn't start with 08",
			JSONRequest:         `{"phone":"812345678", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is pascal numeric",
			JSONRequest:         `{"phone":"-62812345678", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "Phone is less than 5 digits",
			JSONRequest:         `{"phone":"0813", "old_password":"abcdefghi", "new_password":"abcdefghi", "name":"this is dummy", "email":"test@gmail.com"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/customer/account", strings.NewReader(tc.JSONRequest))
		ret, err := decodeCustomerDataRequest(req)
		assert.Equal(tc.expectedRequest, ret, tc.description)
		assert.Equal(tc.expectedExistsError, (err != nil), tc.description)
	}
}

func TestManageAccount_UpdateCustomerData(t *testing.T) {
	tests := []struct {
		description        string
		inputCustomerID    uint
		returnCustomerMock *services.CustomerData
		inputCustomerData  *services.CustomerData
		JSONRequest        string
		returnErrorMock    error
		expectedHTTPCode   int
		expectedStatus     string
		expectedErrorCode  string
	}{
		{
			description:     "Update data success",
			inputCustomerID: uint(1),
			returnCustomerMock: &services.CustomerData{
				Phone: "012345678",
				Name:  "This is dummy",
				Email: "test@gmail.com",
			},
			inputCustomerData: &services.CustomerData{
				Phone:       "0831123",
				Name:        "asdasd123",
				Email:       "asd@email.com",
				OldPassword: "",
				NewPassword: "",
			},
			JSONRequest:       `{"phone": "+62831123","name": "asdasd123","email": "asd@email.com","old_password": "","new_password": ""}`,
			returnErrorMock:   nil,
			expectedHTTPCode:  http.StatusOK,
			expectedStatus:    "Ok",
			expectedErrorCode: "",
		},
		{
			description:     "Bad Request JSON",
			inputCustomerID: uint(1),
			returnCustomerMock: &services.CustomerData{
				Phone: "012345678",
				Name:  "This is dummy",
				Email: "test@gmail.com",
			},
			inputCustomerData: &services.CustomerData{
				Phone:       "0831123",
				Name:        "asdasd123",
				Email:       "asd@email.com",
				OldPassword: "",
				NewPassword: "",
			},
			JSONRequest:       `{"no_hp": "+62831123"}`,
			returnErrorMock:   errors.New("Error"),
			expectedHTTPCode:  http.StatusBadRequest,
			expectedStatus:    "",
			expectedErrorCode: "invalid_request",
		},
		{
			description:     "Failed to update data",
			inputCustomerID: uint(1),
			returnCustomerMock: &services.CustomerData{
				Phone: "012345678",
				Name:  "This is dummy",
				Email: "test@gmail.com",
			},
			inputCustomerData: &services.CustomerData{
				Phone:       "0831123",
				Name:        "asdasd123",
				Email:       "asd@email.com",
				OldPassword: "",
				NewPassword: "",
			},
			JSONRequest:       `{"phone": "+62831123","name": "asdasd123","email": "asd@email.com","old_password": "","new_password": ""}`,
			returnErrorMock:   errors.New("Error"),
			expectedHTTPCode:  http.StatusBadRequest,
			expectedStatus:    "",
			expectedErrorCode: "invalid_request",
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("POST", "/api/v1/customer/account", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", tc.inputCustomerID))

		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedManageAccountService := new(mockServices.IManageAccountService)
		mockedManageAccountService.On("GetCustomerData", tc.inputCustomerID).Return(
			tc.returnCustomerMock,
			tc.returnErrorMock,
		)
		mockedManageAccountService.On("UpdateCustomerData", tc.inputCustomerID, tc.inputCustomerData).Return(
			tc.returnErrorMock,
		)

		manageAccountController := ManageAccount{ManageAccountService: mockedManageAccountService}

		handler := http.HandlerFunc(manageAccountController.UpdateCustomerData)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result CustomerResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
		assert.Equal(tc.expectedStatus, result.Status, tc.description)
	}
}
