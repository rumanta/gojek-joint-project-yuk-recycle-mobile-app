package customercontrollers

import (
	"PPLA4/backend/middleware"
	services "PPLA4/backend/services/customer"
	"encoding/json"
	"errors"
	"net/http"
)

type Authentication struct {
	AuthService services.IAuthService
}

type AuthenticateResponse struct {
	Username *string           `json:"username,omitempty"`
	Token    *middleware.Token `json:"token,omitempty"`
	Error    *JSONError        `json:"error,omitempty"`
	Status   string            `json:"status,omitempty"`
}

type AuthenticateRequest struct {
	Email    *string `json:"email"`
	Password *string `json:"password"`
}

func (a Authentication) Authenticate(w http.ResponseWriter, r *http.Request) {
	req, err := decodeAuthenticateRequest(r)
	if err != nil {
		e := &JSONError{Code: "invalid_request", Message: err.Error()}
		w.WriteHeader(http.StatusBadRequest)
		writeEncoded(w, &AuthenticateResponse{Error: e})
		return
	}

	ret, err := a.AuthService.Login(*req.Email, *req.Password)

	if err != nil {
		e := &JSONError{Code: "unauthorized", Message: err.Error()}
		w.WriteHeader(http.StatusUnauthorized)
		writeEncoded(w, &AuthenticateResponse{Error: e})
		return
	}

	writeEncoded(w, &AuthenticateResponse{Token: ret, Status: "logged_in"})
}

func decodeAuthenticateRequest(r *http.Request) (*AuthenticateRequest, error) {
	req := new(AuthenticateRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}

	if req.Email == nil {
		return nil, errors.New("email is required")
	}

	if req.Password == nil {
		return nil, errors.New("password is required")
	}

	return req, nil
}
