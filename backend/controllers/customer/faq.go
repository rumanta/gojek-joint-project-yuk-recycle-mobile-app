package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"net/http"
)

type FAQ struct {
	FAQService services.IFAQService
}

type FAQInfoResponse struct {
	Data []services.FAQInfoDetail `json:"data,omitempty"`
}

func (f FAQ) GetFAQData(w http.ResponseWriter, r *http.Request) {
	listDetail := f.FAQService.GetFAQDetail()
	data := &FAQInfoResponse{
		Data: listDetail,
	}
	writeEncoded(w, data)
}
