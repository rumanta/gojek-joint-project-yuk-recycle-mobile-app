package customercontrollers

import (
	"encoding/json"
	"strings"

	"net/http"
	"net/http/httptest"
	"testing"

	mockServices "PPLA4/backend/mocks/services/customer"
	services "PPLA4/backend/services/customer"

	"github.com/stretchr/testify/assert"
)

func TestGetFAQData(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/customer/homepage", strings.NewReader(""))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	mockedFAQService := new(mockServices.IFAQService)
	mockedFAQService.On("GetFAQDetail").Return(
		[]services.FAQInfoDetail{
			services.FAQInfoDetail{Title: "FAQ 1", Body: "Body 1"},
		},
	)
	homepageController := FAQ{FAQService: mockedFAQService}

	handler := http.HandlerFunc(homepageController.GetFAQData)
	handler.ServeHTTP(rr, req)

	returnedBody := rr.Body.String()
	var result FAQInfoResponse
	json.Unmarshal([]byte(returnedBody), &result)

	assert.True(t, len(result.Data) > 0)
}
