package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"net/http"
)

type Homepage struct {
	HomepageService services.IHomepageService
}

type HomepageInfoResponse struct {
	Data []services.HomepageInfoDetail `json:"data,omitempty"`
}

func (h Homepage) GetHomepageData(w http.ResponseWriter, r *http.Request) {
	listDetail := h.HomepageService.GetHomepageInfoDetail()
	data := &HomepageInfoResponse{
		Data: listDetail,
	}
	writeEncoded(w, data)
}
