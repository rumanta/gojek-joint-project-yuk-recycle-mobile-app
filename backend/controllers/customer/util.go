package customercontrollers

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func writeEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
	fmt.Println(data)
}
