package customercontrollers

import (
	"PPLA4/backend/middleware"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	mockWorker "PPLA4/backend/mocks/services/worker"
	"PPLA4/backend/models"
	services "PPLA4/backend/services/customer"

	"github.com/stretchr/testify/assert"
)

func TestDecodeMakeOrderRequest(t *testing.T) {
	tests := []struct {
		description         string
		JSONRequest         string
		expectedRequest     *services.MakeOrderRequest
		expectedExistsError bool
	}{
		{
			description:         "Unknown field",
			JSONRequest:         `{"password":"abcdefghi", "location":"jakarta"}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		}, {
			description:         "CustomerLon is missing",
			JSONRequest:         `{"customer_lat": 3.7}`,
			expectedRequest:     nil,
			expectedExistsError: true,
		},
		{
			description: "Success Decoding",
			JSONRequest: `{"customer_lat": 3.7, "customer_lon": 3.8, "order_detail": [ { "garbage_type_id": 1, "quantity": 10 }, { "garbage_type_id": 2, "quantity": 30 } ] }`,
			expectedRequest: &services.MakeOrderRequest{
				CustomerLat: float64(3.7),
				CustomerLon: float64(3.8),
				OrderDetailRequest: []services.MakeOrderDetailRequest{
					services.MakeOrderDetailRequest{
						GarbageTypeID: uint(1),
						Quantity:      uint(10),
					},
					services.MakeOrderDetailRequest{
						GarbageTypeID: uint(2),
						Quantity:      uint(30),
					},
				},
			},
			expectedExistsError: false,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/register", strings.NewReader(tc.JSONRequest))
		ret, err := decodeMakeOrderRequest(req)
		assert.Equal(tc.expectedRequest, ret, tc.description)
		assert.Equal(tc.expectedExistsError, (err != nil), tc.description)
	}
}

func TestMakeOrderHandler(t *testing.T) {
	tests := []struct {
		description        string
		JSONRequest        string
		inputMakeOrderMock *services.MakeOrderRequest
		returnMitraMock    *models.Mitra
		returnErrorMock    error
		expectedHTTPCode   int
		expectedErrorCode  string
	}{
		{
			description:       "Given unknown field return invalid_request",
			JSONRequest:       `{}`,
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "invalid_request",
		}, {
			description: "Given valid make order request return make order failed",
			JSONRequest: `{"customer_lat": 3.7, "customer_lon": 3.8, "order_detail": [ { "garbage_type_id": 1, "quantity": 10 }, { "garbage_type_id": 2, "quantity": 30 } ] }`,
			inputMakeOrderMock: &services.MakeOrderRequest{
				CustomerID:  uint(10),
				CustomerLat: float64(3.7),
				CustomerLon: float64(3.8),
				OrderDetailRequest: []services.MakeOrderDetailRequest{
					services.MakeOrderDetailRequest{
						GarbageTypeID: uint(1),
						Quantity:      uint(10),
					},
					services.MakeOrderDetailRequest{
						GarbageTypeID: uint(2),
						Quantity:      uint(30),
					},
				},
			},
			returnMitraMock:   nil,
			returnErrorMock:   errors.New("make order failed"),
			expectedHTTPCode:  http.StatusBadRequest,
			expectedErrorCode: "make_order_failed",
		}, {
			description: "Make order conducted",
			JSONRequest: `{"customer_lat": 3.7, "customer_lon": 3.8, "order_detail": [ { "garbage_type_id": 1, "quantity": 10 }, { "garbage_type_id": 2, "quantity": 30 } ] }`,
			inputMakeOrderMock: &services.MakeOrderRequest{
				CustomerID:  uint(10),
				CustomerLat: float64(3.7),
				CustomerLon: float64(3.8),
				OrderDetailRequest: []services.MakeOrderDetailRequest{
					services.MakeOrderDetailRequest{
						GarbageTypeID: uint(1),
						Quantity:      uint(10),
					},
					services.MakeOrderDetailRequest{
						GarbageTypeID: uint(2),
						Quantity:      uint(30),
					},
				},
			},
			returnMitraMock:  &models.Mitra{},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		},
	}
	userID := uint(10)
	userType := middleware.CustomerUserType
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/register", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", userID))
		req = req.WithContext(context.WithValue(req.Context(), "UserType", userType))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedMakeOrderWorker := new(mockWorker.IMakeOrderWorker)
		mockedMakeOrderWorker.On("Work", tc.inputMakeOrderMock).Return(tc.returnMitraMock, tc.returnErrorMock)
		makeOrderController := MakeOrderController{MakeOrderWorker: mockedMakeOrderWorker}

		handler := http.HandlerFunc(makeOrderController.MakeOrderHandler)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()

		var result AuthenticateResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
	}
}

func TestMakeOrderHandlerNotAuthorizedUser(t *testing.T) {
	tests := []struct {
		description        string
		JSONRequest        string
		inputMakeOrderMock *services.MakeOrderRequest
		returnMitraMock    *models.Mitra
		returnErrorMock    error
		expectedHTTPCode   int
		expectedErrorCode  string
	}{
		{
			description:       "Mitra make request to customer",
			JSONRequest:       `{"customer_lat": 3.7, "customer_lon": 3.8, "order_detail": [ { "garbage_type_id": 1, "quantity": 10 }, { "garbage_type_id": 2, "quantity": 30 } ] }`,
			expectedHTTPCode:  http.StatusForbidden,
			expectedErrorCode: "malformed_token",
		},
	}
	userID := uint(10)
	userType := middleware.MitraUserType
	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/register", strings.NewReader(tc.JSONRequest))
		req = req.WithContext(context.WithValue(req.Context(), "UserID", userID))
		req = req.WithContext(context.WithValue(req.Context(), "UserType", userType))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedMakeOrderWorker := new(mockWorker.IMakeOrderWorker)
		mockedMakeOrderWorker.On("Work", tc.inputMakeOrderMock).Return(tc.returnMitraMock, tc.returnErrorMock)
		makeOrderController := MakeOrderController{MakeOrderWorker: mockedMakeOrderWorker}

		handler := http.HandlerFunc(makeOrderController.MakeOrderHandler)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()

		var result AuthenticateResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		if result.Error != nil {
			assert.Equal(tc.expectedErrorCode, result.Error.Code, tc.description)
		}
	}
}
