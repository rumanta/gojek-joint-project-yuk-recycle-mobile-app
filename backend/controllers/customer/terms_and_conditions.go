package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"net/http"
)

type TermsAndConditions struct {
	TermsAndConditionsService services.ITermsAndConditionsService
}

type TermsAndConditionsInfoResponse struct {
	Data []services.TermsAndConditionsInfoDetail `json:"data,omitempty"`
}

func (f TermsAndConditions) GetTermsAndConditionsData(w http.ResponseWriter, r *http.Request) {
	listDetail := f.TermsAndConditionsService.GetTermsAndConditionsDetail()
	data := &TermsAndConditionsInfoResponse{
		Data: listDetail,
	}
	writeEncoded(w, data)
}
