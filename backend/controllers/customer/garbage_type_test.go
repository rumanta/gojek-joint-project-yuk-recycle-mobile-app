package customercontrollers

import (
	services "PPLA4/backend/services/customer"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	mockServices "PPLA4/backend/mocks/services/customer"
)

func TestGarbageType_GetGarbageTypeList(t *testing.T) {
	tests := []struct {
		description           string
		returnGarbageTypeMock []services.GarbageTypeData
		returnErrorMock       error
		expectedHTTPCode      int
	}{
		{
			description: "Fetch garbage type list success",
			returnGarbageTypeMock: []services.GarbageTypeData{
				services.GarbageTypeData{Title: "Sampah Plastik", Description: "Plastik", QuantityType: "KG", Image: "image_url"},
				services.GarbageTypeData{Title: "Sampah Organik", Description: "Organik", QuantityType: "KG", Image: "image_url"},
			},
			returnErrorMock:  nil,
			expectedHTTPCode: http.StatusOK,
		}, {
			description:           "Fetch garbage type list failed",
			returnGarbageTypeMock: nil,
			returnErrorMock:       errors.New("Failed to fetch garbage types"),
			expectedHTTPCode:      http.StatusBadRequest,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		req, err := http.NewRequest("GET", "/api/v1/customer/garbage-types", strings.NewReader(""))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		mockedGarbageTypeService := new(mockServices.IGarbageTypeService)
		mockedGarbageTypeService.On("GetGarbageTypeList").Return(
			tc.returnGarbageTypeMock,
			tc.returnErrorMock,
		)
		garbageTypeController := GarbageType{GarbageTypeService: mockedGarbageTypeService}

		handler := http.HandlerFunc(garbageTypeController.GetGarbageTypeList)
		handler.ServeHTTP(rr, req)

		returnedBody := rr.Body.String()
		var result GarbageTypeListResponse
		json.Unmarshal([]byte(returnedBody), &result)

		assert.Equal(tc.expectedHTTPCode, rr.Code, tc.description)
		assert.Equal(tc.returnGarbageTypeMock, result.GarbageTypeList, tc.description)
	}
}
