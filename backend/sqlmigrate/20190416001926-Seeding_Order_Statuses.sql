
-- +migrate Up
INSERT INTO order_statuses(id, created_at, updated_at, description) VALUES
	('1', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', 'init order, finding mitra'),
	('2', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', 'accepted by a mitra'),
	('3', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', 'completed'),
	('4', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', 'cancelled');
SELECT SETVAL('order_statuses_id_seq', 4);

-- +migrate Down
DELETE FROM order_statuses WHERE id BETWEEN 1 AND 4;
SELECT SETVAL('order_statuses_id_seq', 1, false);