
-- +migrate Up

-- SEQUENCE: public.mitras_id_seq
CREATE SEQUENCE public.mitras_id_seq;

-- Table: mitras
CREATE TABLE mitras
(
    id integer NOT NULL DEFAULT nextval('mitras_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    phone character varying(50) COLLATE pg_catalog."default" NOT NULL,
    password text COLLATE pg_catalog."default",
    name character varying(50) COLLATE pg_catalog."default",
    email character varying(50) COLLATE pg_catalog."default" NOT NULL,
    notification_token text COLLATE pg_catalog."default",
    CONSTRAINT mitras_pkey PRIMARY KEY (id),
    CONSTRAINT mitras_email_key UNIQUE (email),
    CONSTRAINT mitras_phone_key UNIQUE (phone)
);

-- Index: idx_mitras_deleted_at
CREATE INDEX idx_mitras_deleted_at
    ON mitras USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_mitras_deleted_at;
DROP TABLE mitras;
DROP SEQUENCE mitras_id_seq;
