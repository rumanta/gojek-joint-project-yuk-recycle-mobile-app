
-- +migrate Up
ALTER TABLE order_attempts ALTER COLUMN token TYPE BIGINT;

-- +migrate Down
DELETE FROM order_attempts;
ALTER TABLE order_attempts ALTER COLUMN token TYPE INT;