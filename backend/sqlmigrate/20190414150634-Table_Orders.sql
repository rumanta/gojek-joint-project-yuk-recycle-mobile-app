
-- +migrate Up

-- SEQUENCE: orders_id_seq
CREATE SEQUENCE orders_id_seq;

-- Table: orders
CREATE TABLE orders
(
    id integer NOT NULL DEFAULT nextval('orders_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    mitra_id integer,
    customer_id integer,
    customer_lat numeric,
    customer_lon numeric,
    latest_status integer,
    scheduled_at timestamp with time zone,
    notes text COLLATE pg_catalog."default",
    address text COLLATE pg_catalog."default",
    CONSTRAINT orders_pkey PRIMARY KEY (id),
    CONSTRAINT orders_customer_id_fkey FOREIGN KEY (customer_id)
        REFERENCES customers (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_orders_deleted_at
CREATE INDEX idx_orders_deleted_at
    ON orders USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_orders_deleted_at;
DROP TABLE orders;
DROP SEQUENCE orders_id_seq;