
-- +migrate Up
ALTER TABLE orders ADD COLUMN cancellation_reason text;

-- +migrate Down
ALTER TABLE orders DROP COLUMN cancellation_reason;
