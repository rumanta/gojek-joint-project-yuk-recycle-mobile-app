
-- +migrate Up
INSERT INTO customers(id, created_at, updated_at, phone, password, name, email, login_type) VALUES 
	('1', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567890', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Muhammad Yusuf Sholeh', 'yusuf@ppl-a4.com', '1'),
	('2', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567891', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Donny Samuel', 'donny@ppl-a4.com', '1'),
	('3', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567892', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Misael Jonathan', 'misael@ppl-a4.com', '1'),
	('4', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567893', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'William Rumanta', 'william@ppl-a4.com', '1'),
	('5', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567894', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'R. Azka Ali Fazagani', 'azka@ppl-a4.com', '1'),
	('6', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567895', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Endrawan Andika Wicaksana', 'endrawan@ppl-a4.com', '1'),
	('7', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567896', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Fayya Nadhira Anyatasia', 'fayya@ppl-a4.com', '1'),
	('8', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567897', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Ginanjar Cahya Komara', 'ginanjar@ppl-a4.com', '1'),
	('9', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567898', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Rizki Oceano', 'rizki@ppl-a4.com', '1'),
	('10', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', '081234567899', '$2a$10$c20zMBYePO8Lh7xzEbJvcOSNdRYlFOWe6NVfgvJ.HXzTkzhXTeuZq', 'Maya Retno Ayu', 'maya@ppl-a4.com', '1');
SELECT SETVAL('customers_id_seq', 10);

-- +migrate Down
DELETE FROM customers WHERE id BETWEEN 1 AND 10;
SELECT SETVAL('customers_id_seq', 1, false);