
-- +migrate Up

-- SEQUENCE: orders_attempt_statuses_id_seq
CREATE SEQUENCE order_attempt_statuses_id_seq;

-- Table: order_attempt_statuses
CREATE TABLE order_attempt_statuses
(
    id integer NOT NULL DEFAULT nextval('order_attempt_statuses_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    description text COLLATE pg_catalog."default",
    CONSTRAINT order_attempt_statuses_pkey PRIMARY KEY (id)
);

-- Index: idx_order_attempt_statuses_deleted_at
CREATE INDEX idx_order_attempt_statuses_deleted_at
    ON order_attempt_statuses USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_order_attempt_statuses_deleted_at;
DROP TABLE order_attempt_statuses;
DROP SEQUENCE order_attempt_statuses_id_seq;





