
-- +migrate Up

-- SEQUENCE: orders_attempts_id_seq
CREATE SEQUENCE order_attempts_id_seq;

-- Table: order_attempts
CREATE TABLE order_attempts
(
    id integer NOT NULL DEFAULT nextval('order_attempts_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    order_id integer,
    mitra_id integer,
    order_attempt_status_id integer,
    token integer,
    CONSTRAINT order_attempts_pkey PRIMARY KEY (id),
    CONSTRAINT order_attempts_mitra_id_fkey FOREIGN KEY (mitra_id)
        REFERENCES mitras (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT order_attempts_order_attempt_status_id_fkey FOREIGN KEY (order_attempt_status_id)
        REFERENCES order_attempt_statuses (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT order_attempts_order_id_fkey FOREIGN KEY (order_id)
        REFERENCES orders (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_order_attempts_deleted_at
CREATE INDEX idx_order_attempts_deleted_at
    ON order_attempts USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_order_attempts_deleted_at;
DROP TABLE order_attempts;
DROP SEQUENCE order_attempts_id_seq;

