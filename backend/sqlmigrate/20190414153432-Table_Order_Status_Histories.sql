
-- +migrate Up

-- SEQUENCE: orders_status_histories_id_seq
CREATE SEQUENCE order_status_histories_id_seq;

-- Table: order_status_histories
CREATE TABLE order_status_histories
(
    id integer NOT NULL DEFAULT nextval('order_status_histories_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    order_id integer,
    order_status_id integer,
    CONSTRAINT order_status_histories_pkey PRIMARY KEY (id),
    CONSTRAINT order_status_histories_order_id_fkey FOREIGN KEY (order_id)
        REFERENCES orders (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT order_status_histories_order_status_id_fkey FOREIGN KEY (order_status_id)
        REFERENCES order_statuses (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_order_status_histories_deleted_at
CREATE INDEX idx_order_status_histories_deleted_at
    ON order_status_histories USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_order_status_histories_deleted_at;
DROP TABLE order_status_histories;
DROP SEQUENCE order_status_histories_id_seq;






