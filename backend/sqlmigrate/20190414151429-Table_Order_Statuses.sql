
-- +migrate Up

-- SEQUENCE: order_statuses_id_seq
CREATE SEQUENCE order_statuses_id_seq;

-- Table: order_statuses
CREATE TABLE order_statuses
(
    id integer NOT NULL DEFAULT nextval('order_statuses_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    description text COLLATE pg_catalog."default",
    CONSTRAINT order_statuses_pkey PRIMARY KEY (id)
);

-- Index: idx_order_statuses_deleted_at
CREATE INDEX idx_order_statuses_deleted_at
    ON order_statuses USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_order_statuses_deleted_at;
DROP TABLE order_statuses;
DROP SEQUENCE order_statuses_id_seq;




