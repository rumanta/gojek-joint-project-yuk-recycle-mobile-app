
-- +migrate Up

-- SEQUENCE: mitra_locations_id_seq
CREATE SEQUENCE mitra_locations_id_seq;

-- Table: mitra_locations
CREATE TABLE mitra_locations
(
    id integer NOT NULL DEFAULT nextval('mitra_locations_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    mitra_id integer,
    lat numeric(11,8),
    lon numeric(11,8),
    address text COLLATE pg_catalog."default",
    CONSTRAINT mitra_locations_pkey PRIMARY KEY (id),
    CONSTRAINT mitra_locations_mitra_id_fkey FOREIGN KEY (mitra_id)
        REFERENCES mitras (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_mitra_locations_deleted_at
CREATE INDEX idx_mitra_locations_deleted_at
    ON mitra_locations USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_mitra_locations_deleted_at;
DROP TABLE mitra_locations;
DROP SEQUENCE mitra_locations_id_seq;