
-- +migrate Up
CREATE OR REPLACE FUNCTION status_update() 
    RETURNS trigger AS 
' BEGIN UPDATE orders SET latest_status = NEW.order_status_id WHERE id = NEW.order_id; RETURN NEW;  END; '
LANGUAGE 'plpgsql';

CREATE TRIGGER orders_latest_status_update
  AFTER INSERT OR UPDATE
  ON order_status_histories
  FOR EACH ROW
  EXECUTE PROCEDURE status_update();

-- +migrate Down
DROP TRIGGER orders_latest_status_update ON order_status_histories;
DROP FUNCTION status_update();