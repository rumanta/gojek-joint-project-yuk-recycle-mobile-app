
-- +migrate Up
INSERT INTO order_statuses(id, created_at, updated_at, description) VALUES
	('6', '2019-04-25 11:51:39.38991+00', '2019-04-25 11:51:39.38991+00', 'No service available'),
	('7', '2019-04-25 11:51:39.38991+00', '2019-04-25 11:51:39.38991+00', 'No mitra take the order');
SELECT SETVAL('order_statuses_id_seq', 7);

-- +migrate Down
DELETE FROM order_statuses WHERE id BETWEEN 6 AND 7;
SELECT SETVAL('order_statuses_id_seq', 5, false);