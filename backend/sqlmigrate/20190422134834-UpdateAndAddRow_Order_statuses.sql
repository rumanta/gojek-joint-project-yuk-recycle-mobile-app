
-- +migrate Up
INSERT INTO order_statuses(id, created_at, updated_at, description) VALUES
    ('5', '2019-04-22 13:52:40.38991+00', '2019-04-22 13:52:40.38991+00', 'picked up');
UPDATE order_statuses SET description = 'Searching driver';
UPDATE order_statuses SET description = 'Scheduled' WHERE id = 2;
UPDATE order_statuses SET description = 'Picked up' WHERE id = 3;
UPDATE order_statuses SET description = 'Completed' WHERE id = 4;
UPDATE order_statuses SET description = 'Cancelled' WHERE id = 5;
SELECT SETVAL('order_statuses_id_seq', 5);

-- +migrate Down
UPDATE order_statuses SET description = 'init order, finding mitra' WHERE id = 1;
UPDATE order_statuses SET description = 'accepted by a mitra' WHERE id = 2;
UPDATE order_statuses SET description = 'completed' WHERE id = 3;
UPDATE order_statuses SET description = 'cancelled' WHERE id = 4;
DELETE FROM order_statuses WHERE id = 5;
SELECT SETVAL('order_statuses_id_seq', 4);
