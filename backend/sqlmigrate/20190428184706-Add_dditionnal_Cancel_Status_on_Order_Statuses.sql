
-- +migrate Up
INSERT INTO order_statuses(id, created_at, updated_at, description) VALUES
	('8', '2019-04-28 11:51:39.38991+00', '2019-04-28 11:51:39.38991+00', 'Cancelled by Customer');
SELECT SETVAL('order_statuses_id_seq', 8);
UPDATE order_statuses SET description = 'Cancelled by Mitra' WHERE id = 5;
UPDATE order_statuses SET description = 'Cancelled by Customer' WHERE id = 6;
UPDATE order_statuses SET description = 'No service available' WHERE id = 7;
UPDATE order_statuses SET description = 'No mitra take the order' WHERE id = 8;

-- +migrate Down
UPDATE order_statuses SET description = 'No mitra take the order' WHERE id = 7;
UPDATE order_statuses SET description = 'No service available' WHERE id = 6;
UPDATE order_statuses SET description = 'Cancelled' WHERE id = 5;
DELETE FROM order_statuses WHERE id = 8;
SELECT SETVAL('order_statuses_id_seq', 7, false);
