
-- +migrate Up
INSERT INTO order_attempt_statuses(id, created_at, updated_at, description) VALUES
	('1', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', 'not yet decided by a mitra'),
	('2', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', 'accepted by a mitra'),
	('3', '2019-04-15 15:51:39.38991+00', '2019-04-15 15:51:39.38991+00', 'rejected by a mitra');
SELECT SETVAL('order_attempt_statuses_id_seq', 3);

-- +migrate Down
DELETE FROM order_attempt_statuses WHERE id BETWEEN 1 AND 3;
SELECT SETVAL('order_attempt_statuses_id_seq', 1, false);