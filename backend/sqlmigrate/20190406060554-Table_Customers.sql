
-- +migrate Up

-- SEQUENCE: public.customers_id_seq
CREATE SEQUENCE customers_id_seq;

-- Table: public.customers
CREATE TABLE customers
(
    id integer NOT NULL DEFAULT nextval('customers_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    phone character varying(50) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    password text COLLATE pg_catalog."default",
    name character varying(50) COLLATE pg_catalog."default",
    email character varying(50) COLLATE pg_catalog."default" NOT NULL,
    login_type integer,
    notification_token text COLLATE pg_catalog."default",
    CONSTRAINT customers_pkey PRIMARY KEY (id),
    CONSTRAINT customers_email_key UNIQUE (email),
    CONSTRAINT customers_phone_key UNIQUE (phone)
);

-- Index: idx_customers_deleted_at
CREATE INDEX idx_customers_deleted_at
    ON customers USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_customers_deleted_at;
DROP TABLE customers;
DROP SEQUENCE customers_id_seq;