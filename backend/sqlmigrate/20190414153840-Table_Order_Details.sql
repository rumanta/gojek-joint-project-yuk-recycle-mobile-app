
-- +migrate Up

-- SEQUENCE: orders_details_id_seq
CREATE SEQUENCE order_details_id_seq;

-- Table: order_details
CREATE TABLE order_details
(
    id integer NOT NULL DEFAULT nextval('order_details_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    order_id integer,
    garbage_type_id integer,
    total_amount integer,
    quantity integer,
    CONSTRAINT order_details_pkey PRIMARY KEY (id),
    CONSTRAINT order_details_garbage_type_id_fkey FOREIGN KEY (garbage_type_id)
        REFERENCES garbage_types (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT order_details_order_id_fkey FOREIGN KEY (order_id)
        REFERENCES orders (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_order_details_deleted_at
CREATE INDEX idx_order_details_deleted_at
    ON order_details USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_order_details_deleted_at;
DROP TABLE order_details;
DROP SEQUENCE order_details_id_seq;











