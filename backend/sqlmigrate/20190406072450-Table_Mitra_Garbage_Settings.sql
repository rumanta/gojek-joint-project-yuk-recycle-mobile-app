
-- +migrate Up

-- SEQUENCE: mitra_garbage_settings_id_seq
CREATE SEQUENCE mitra_garbage_settings_id_seq;

-- Table: mitra_garbage_settings
CREATE TABLE mitra_garbage_settings
(
    id integer NOT NULL DEFAULT nextval('mitra_garbage_settings_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    mitra_id integer,
    garbage_type_id integer,
    minimum_quantity integer,
    price integer,
    CONSTRAINT mitra_garbage_settings_pkey PRIMARY KEY (id),
    CONSTRAINT mitra_garbage_settings_garbage_type_id_fkey FOREIGN KEY (garbage_type_id)
        REFERENCES garbage_types (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT mitra_garbage_settings_mitra_id_fkey FOREIGN KEY (mitra_id)
        REFERENCES mitras (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_mitra_garbage_settings_deleted_at
CREATE INDEX idx_mitra_garbage_settings_deleted_at
    ON mitra_garbage_settings USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_mitra_garbage_settings_deleted_at;
DROP TABLE mitra_garbage_settings;
DROP SEQUENCE mitra_garbage_settings_id_seq;