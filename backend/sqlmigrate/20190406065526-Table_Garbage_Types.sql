
-- +migrate Up

-- SEQUENCE: garbage_types_id_seq
CREATE SEQUENCE garbage_types_id_seq;

-- Table: garbage_types
CREATE TABLE garbage_types
(
    id integer NOT NULL DEFAULT nextval('garbage_types_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    quantity_type text COLLATE pg_catalog."default",
    image_base64 text COLLATE pg_catalog."default",
    CONSTRAINT garbage_types_pkey PRIMARY KEY (id)
);

-- Index: idx_garbage_types_deleted_at
CREATE INDEX idx_garbage_types_deleted_at
    ON garbage_types USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_garbage_types_deleted_at;
DROP TABLE garbage_types;
DROP SEQUENCE garbage_types_id_seq;