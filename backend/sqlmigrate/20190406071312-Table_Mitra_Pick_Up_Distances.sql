
-- +migrate Up

-- SEQUENCE: mitra_pick_up_distances_id_seq
CREATE SEQUENCE mitra_pick_up_distances_id_seq;

-- Table: mitra_pick_up_distances
CREATE TABLE mitra_pick_up_distances
(
    id integer NOT NULL DEFAULT nextval('mitra_pick_up_distances_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    mitra_id integer,
    maximum_distance integer,
    CONSTRAINT mitra_pick_up_distances_pkey PRIMARY KEY (id),
    CONSTRAINT mitra_pick_up_distances_mitra_id_fkey FOREIGN KEY (mitra_id)
        REFERENCES mitras (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_mitra_pick_up_distances_deleted_at
CREATE INDEX idx_mitra_pick_up_distances_deleted_at
    ON mitra_pick_up_distances USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_mitra_pick_up_distances_deleted_at;
DROP TABLE mitra_pick_up_distances;
DROP SEQUENCE mitra_pick_up_distances_id_seq;
