
-- +migrate Up

-- SEQUENCE: mitra_pick_up_schedules_id_seq
CREATE SEQUENCE mitra_pick_up_schedules_id_seq;

-- Table: mitra_pick_up_schedules
CREATE TABLE mitra_pick_up_schedules
(
    id integer NOT NULL DEFAULT nextval('mitra_pick_up_schedules_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    mitra_id integer,
    sunday boolean DEFAULT true,
    monday boolean DEFAULT true,
    tuesday boolean DEFAULT true,
    wednesday boolean DEFAULT true,
    thursday boolean DEFAULT true,
    friday boolean DEFAULT true,
    saturday boolean DEFAULT true,
    CONSTRAINT mitra_pick_up_schedules_pkey PRIMARY KEY (id),
    CONSTRAINT mitra_pick_up_schedules_mitra_id_fkey FOREIGN KEY (mitra_id)
        REFERENCES mitras (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

-- Index: idx_mitra_pick_up_schedules_deleted_at
CREATE INDEX idx_mitra_pick_up_schedules_deleted_at
    ON mitra_pick_up_schedules USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down
DROP INDEX idx_mitra_pick_up_schedules_deleted_at;
DROP TABLE mitra_pick_up_schedules;
DROP SEQUENCE mitra_pick_up_schedules_id_seq;