#!/bin/bash
psql -U postgres -d database_name -c "INSERT INTO garbage_types (title) VALUES ('Botol Plastik');"
psql -U postgres -d database_name -c "INSERT INTO garbage_types (title) VALUES ('Botol Kaca');"
psql -U postgres -d database_name -c "INSERT INTO garbage_types (title) VALUES ('Kertas/Kardus');"
psql -U postgres -d database_name -c "INSERT INTO garbage_types (title) VALUES ('Sampah Kertas');"
psql -U postgres -d database_name -c "INSERT INTO garbage_types (title) VALUES ('Sampah Plastik');"

export API_HOST=localhost
export API_PORT=8000

curl --location --request POST "$API_HOST:$API_PORT/api/v1/mitra/new" --header "Content-Type: application/json" --data "{\"phone\":\"+628123\",\"password\":\"12345678\",\"name\":\"wiliam\",\"email\":\"wili@m.com\"}"
curl --location --request POST "$API_HOST:$API_PORT/api/v1/mitra/new" --header "Content-Type: application/json" --data "{\"phone\":\"+6281234\",\"password\":\"12345678\",\"name\":\"endra\",\"email\":\"endr@wan.com\"}"
curl --location --request POST "$API_HOST:$API_PORT/api/v1/mitra/new" --header "Content-Type: application/json" --data "{\"phone\":\"+62812345\",\"password\":\"12345678\",\"name\":\"ucup\",\"email\":\"yusuf@sholeh.com\"}"
curl --location --request POST "$API_HOST:$API_PORT/api/v1/mitra/new" --header "Content-Type: application/json" --data "{\"phone\":\"+628123456\",\"password\":\"12345678\",\"name\":\"donny\",\"email\":\"donnys@muel.com\"}"
curl --location --request POST "$API_HOST:$API_PORT/api/v1/mitra/new" --header "Content-Type: application/json" --data "{\"phone\":\"+6281234567\",\"password\":\"12345678\",\"name\":\"misael\",\"email\":\"mis@el.com\"}"
curl --location --request POST "$API_HOST:$API_PORT/api/v1/mitra/new" --header "Content-Type: application/json" --data "{\"phone\":\"+62812345678\",\"password\":\"12345678\",\"name\":\"azka\",\"email\":\"azk@li.com\"}"
