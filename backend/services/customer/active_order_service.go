package customerservices

import (
	"PPLA4/backend/repositories"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"
	"time"
)

const (
	CanNotAccessDatabase = "Oops, try again later"
)

type ActiveOrderService struct {
	OrderRepository       repositories.IOrderRepository
	MitraRepository       repositories.IMitraRepository
	OrderStatusRepository repositories.IOrderStatusRepository
}

type IActiveOrderService interface {
	GetActiveOrdersData(customerID uint) ([]ActiveOrder, error)
}

type ActiveOrder struct {
	ID            uint   `json:"id,omitempty"`
	Name          string `json:"name,omitempty"`
	Time          string `json:"time,omitempty"`
	Status        string `json:"status,omitempty"`
	ScheduledTime string `json:"scheduled_time,omitempty"`
}

func (a ActiveOrderService) GetActiveOrdersData(customerID uint) ([]ActiveOrder, error) {
	orders, err := a.OrderRepository.GetLatest20ActiveOrdersByCustomerID(customerID)

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	activeOrders := make([]ActiveOrder, len(orders))

	for i, order := range orders {
		if order.LatestStatus == orderConstant.InitOrder {
			status, _ := a.OrderStatusRepository.GetByID(uint(order.LatestStatus))
			activeOrders[i] = ActiveOrder{
				ID:            order.ID,
				Time:          order.CreatedAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006, 15:04"),
				Status:        status.Description,
				ScheduledTime: order.ScheduledAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006"),
			}
			continue
		}

		mitra, err := a.MitraRepository.GetByID(order.MitraID)

		if err != nil {
			return nil, errors.New(CanNotAccessDatabase)
		}

		status, _ := a.OrderStatusRepository.GetByID(uint(order.LatestStatus))

		activeOrders[i] = ActiveOrder{
			ID:            order.ID,
			Name:          mitra.Name,
			Time:          order.CreatedAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006, 15:04"),
			Status:        status.Description,
			ScheduledTime: order.ScheduledAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006"),
		}
	}

	return activeOrders, nil
}
