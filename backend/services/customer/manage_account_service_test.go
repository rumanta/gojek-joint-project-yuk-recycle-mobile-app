package customerservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/bcrypt"

	mockRepositories "PPLA4/backend/mocks/repositories"
)

const (
	expectedPersonalInfoWhichWeCollect      = "Kita mengumpulkan informasi yang bisa mengidentifikasi atau bisa diidentifikasi, kontak, atau mencari lokasi pengguna dan device yang memiliki informasi pengguna tersebut. Informasi personal meliputi, tapi tidak terbatas pada, nama, alamat, tanggal lahir, nomor telepon, alamat e-mail, akun bank dan jenis kelamin."
	expectedTheUseOfPersonalInfo            = "Kita mungkin menggunakan informasi personal anda untuk tujuan kegunaan aplikasi ini dan sesuai dengan peraturan yang berlaku. Kita menggunakan informasi anda untuk mengidentifikasi dan mendaftarkan pengguna sebagai user, dan memverisikasi dan deaktivasi."
	expectedAccessAndCorrectionPersonalInfo = "Sesuai dengan hukum yang berlaku, pengguna bisa mengajukan permintaan kepada kami untuk akses pembenaran informasi personal yang pegang dengan mengkontak kami sesuai detail di bawah."
	expectedSecurityOfYourPersonalInfo      = "Kemamanan informasi data anda adalah kepentingan utama yang kami jaga. Kami akan menggunakan seluruh cara untuk mengamankan informasi personal pengguna."
)

func TestManageAccountService_GetPrivacyPolicyDetail(t *testing.T) {
	manageAccountService := ManageAccountService{}
	privacyPolicyDetails := manageAccountService.GetPrivacyPolicyDetail()

	assert := assert.New(t)

	for _, detail := range privacyPolicyDetails {

		actualTitle := detail.Title
		actualBody := detail.Body

		if actualTitle == "Personal Information Which We Collect" {
			assert.Equal(expectedPersonalInfoWhichWeCollect, actualBody)
		} else if actualTitle == "The Use of Personal Information" {
			assert.Equal(expectedTheUseOfPersonalInfo, actualBody)
		} else if actualTitle == "Access and Correction of Personal Information" {
			assert.Equal(expectedAccessAndCorrectionPersonalInfo, actualBody)
		} else if actualTitle == "Security of Your Personal Information" {
			assert.Equal(expectedSecurityOfYourPersonalInfo, actualBody)
		}
	}
}

func TestManageAccountService_GetCustomerData(t *testing.T) {
	tests := []struct {
		description          string
		customerId           uint
		returnCustomerMock   *models.Customer
		returnError          error
		expectedCustomerData *CustomerData
		expectedError        error
	}{
		{
			description: "Unable to fetch customer data",
			customerId:  uint(2),
			returnCustomerMock: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			returnError:          errors.New("Error"),
			expectedCustomerData: nil,
			expectedError:        errors.New("Unable to fetch customer data"),
		}, {
			description: "Get data success",
			customerId:  uint(1),
			returnCustomerMock: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			returnError: nil,
			expectedCustomerData: &CustomerData{
				Phone:     "+6212345678",
				Name:      "This is dummy",
				Email:     "test@gmail.com",
				LoginType: 1,
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		mockedCustomerRepository.On("GetByID", tc.customerId).Return(tc.returnCustomerMock, tc.returnError)

		manageAccountService := ManageAccountService{CustomerRepository: mockedCustomerRepository}

		customerData, err := manageAccountService.GetCustomerData(tc.customerId)

		assert.Equal(tc.expectedCustomerData, customerData, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}

func TestManageAccountService_UpdateCustomerData(t *testing.T) {
	oldPassword := "123password"
	hashedCustomerOldPassword, _ := bcrypt.GenerateFromPassword([]byte(oldPassword), bcrypt.DefaultCost)
	customerOldPassword := string(hashedCustomerOldPassword)

	tests := []struct {
		description          string
		customerId           uint
		returnCustomerMock   *models.Customer
		returnCustomerUpdate *models.Customer
		oldPassword          string
		returnError          error
		expectedError        error
	}{
		{
			description: "Unable to fetch customer data",
			customerId:  uint(2),
			returnCustomerMock: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			returnCustomerUpdate: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+62123123",
				Password:          "123password",
				Name:              "Another dummy",
				Email:             "ujian@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			oldPassword:   oldPassword,
			returnError:   errors.New("Error"),
			expectedError: errors.New("Unable to fetch customer data"),
		},
		{
			description: "Update data success",
			customerId:  uint(1),
			returnCustomerMock: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          customerOldPassword,
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			returnCustomerUpdate: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+62123123",
				Password:          "newpassword",
				Name:              "Another dummy",
				Email:             "ujian@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			oldPassword:   oldPassword,
			returnError:   nil,
			expectedError: nil,
		},
		{
			description: "Google Account Unable Update Password",
			customerId:  uint(1),
			returnCustomerMock: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          customerOldPassword,
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				LoginType:         2,
				NotificationToken: "",
			},
			returnCustomerUpdate: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+62123123",
				Password:          "newpassword",
				Name:              "Another dummy",
				Email:             "ujian@gmail.com",
				LoginType:         2,
				NotificationToken: "",
			},
			oldPassword:   oldPassword,
			returnError:   nil,
			expectedError: errors.New("Unable to change password for Google sign-in account"),
		},
		{
			description: "Old password is incorrect",
			customerId:  uint(1),
			returnCustomerMock: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "oldpassword",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			returnCustomerUpdate: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+62123123",
				Password:          "newpassword",
				Name:              "Another dummy",
				Email:             "ujian@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			oldPassword:   oldPassword,
			returnError:   nil,
			expectedError: errors.New("Old password is incorrect"),
		},
	}

	specialTest := []struct {
		description          string
		customerId           uint
		returnCustomerMock   *models.Customer
		returnCustomerMock2  *models.Customer
		returnCustomerUpdate *models.Customer
		oldPassword          string
		returnErrorMock      error
		returnErrorUpdate    error
		expectedError        error
	}{
		{
			description: "Failed to update customer data",
			customerId:  uint(2),
			returnCustomerMock: &models.Customer{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          customerOldPassword,
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			returnCustomerMock2: &models.Customer{
				Model:             gorm.Model{ID: 2},
				Phone:             "+62123123",
				Password:          customerOldPassword,
				Name:              "Another dummy",
				Email:             "ujian@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			returnCustomerUpdate: &models.Customer{
				Model:             gorm.Model{ID: 2},
				Phone:             "+6212345678",
				Password:          oldPassword,
				Name:              "Another dummy",
				Email:             "ujian@gmail.com",
				LoginType:         1,
				NotificationToken: "",
			},
			oldPassword:       oldPassword,
			returnErrorMock:   nil,
			returnErrorUpdate: errors.New("Error"),
			expectedError:     errors.New("Failed to update customer data"),
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		mockedCustomerRepository.On("GetByID", tc.customerId).Return(tc.returnCustomerMock, tc.returnError)
		mockedCustomerRepository.On("Update", tc.returnCustomerUpdate).Return(tc.returnCustomerUpdate, tc.returnError)
		manageAccountService := ManageAccountService{CustomerRepository: mockedCustomerRepository}

		updatedCustomer := &CustomerData{
			Phone:       tc.returnCustomerUpdate.Phone,
			OldPassword: tc.oldPassword,
			NewPassword: tc.returnCustomerUpdate.Password,
			Name:        tc.returnCustomerUpdate.Name,
			Email:       tc.returnCustomerUpdate.Email,
		}

		err := manageAccountService.UpdateCustomerData(tc.customerId, updatedCustomer)

		assert.Equal(tc.expectedError, err, tc.description)
	}

	for _, tc := range specialTest {
		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)

		mockedCustomerRepository.On("GetByID", tc.customerId).Return(tc.returnCustomerMock2, tc.returnErrorMock)
		mockedCustomerRepository.On("Update", tc.returnCustomerUpdate).Return(tc.returnCustomerUpdate, tc.returnErrorUpdate)

		manageAccountService := ManageAccountService{CustomerRepository: mockedCustomerRepository}

		updatedCustomer := &CustomerData{
			Phone:       tc.returnCustomerUpdate.Phone,
			OldPassword: tc.oldPassword,
			NewPassword: tc.returnCustomerUpdate.Password,
			Name:        tc.returnCustomerUpdate.Name,
			Email:       tc.returnCustomerUpdate.Email,
		}

		err := manageAccountService.UpdateCustomerData(tc.customerId, updatedCustomer)

		assert.Equal(tc.expectedError, err, tc.description)
	}

}
