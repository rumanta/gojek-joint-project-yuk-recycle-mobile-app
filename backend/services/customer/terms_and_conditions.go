package customerservices

type TermsAndConditionsService struct {
}

type ITermsAndConditionsService interface {
	GetTermsAndConditionsDetail() []TermsAndConditionsInfoDetail
}

type TermsAndConditionsInfoDetail struct {
	Title string `json:"title,omitempty"`
	Body  string `json:"body,omitempty"`
}

func (f TermsAndConditionsService) GetTermsAndConditionsDetail() []TermsAndConditionsInfoDetail {

	detailList := []TermsAndConditionsInfoDetail{
		TermsAndConditionsInfoDetail{
			Title: "Information's completeness",
			Body:  "Customers are required to provide complete information regarding the type and specifications of the goods to be delivered.",
		},
		TermsAndConditionsInfoDetail{
			Title: "Item's Packaging",
			Body:  "Yuk-Recycle does not provide specific box for shipping. Customers are responsible for properly packing the goods to be delivered. For fragile items made of glass, ceramic and includes cakes, ice cream, it is suggested that the items is specially packaged. Yuk-Recycle is not responsible for damage or deformation that occurs upon delivery of such goods.",
		},
		TermsAndConditionsInfoDetail{
			Title: "Customer's Safety",
			Body:  "Yuk-Recycle mitra has been briefed to drive his vehicle in a safe manner. However, customers who use Yuk-Recycle service are responsible for their own safety.",
		},
	}

	return detailList
}
