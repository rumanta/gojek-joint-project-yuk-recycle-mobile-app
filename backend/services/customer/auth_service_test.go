package customerservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"errors"
	"testing"

	mockMiddleware "PPLA4/backend/mocks/middleware"
	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"

	"github.com/stretchr/testify/assert"
)

func TestLogin(t *testing.T) {
	tests := []struct {
		description        string
		email              string
		password           string
		inputEmailMock     string
		returnCustomerMock *models.Customer
		returnErrorMock    error
		inputUserIDMock    uint
		inputUserTypeMock  string
		expectedToken      *middleware.Token
		expectedError      error
	}{
		{
			description:        "Login when no Connection",
			email:              "yusuf_sholeh@gmail.com",
			password:           "abcdefghi",
			inputEmailMock:     "yusuf_sholeh@gmail.com",
			returnCustomerMock: nil,
			returnErrorMock:    errors.New("connection error"),
			expectedToken:      nil,
			expectedError:      errors.New(UsernameAndPasswordDoesntMatch),
		}, {
			description:        "Login when username not found",
			email:              "yusuf_sholeh@gmail.com",
			password:           "abcdefghi",
			inputEmailMock:     "yusuf_sholeh@gmail.com",
			returnCustomerMock: nil,
			returnErrorMock:    gorm.ErrRecordNotFound,
			expectedToken:      nil,
			expectedError:      errors.New(UsernameAndPasswordDoesntMatch),
		}, {
			description:        "Login given wrong password",
			email:              "yusuf_sholeh@gmail.com",
			password:           "abcdefghi",
			inputEmailMock:     "yusuf_sholeh@gmail.com",
			returnCustomerMock: &models.Customer{Email: "yusuf_sholeh@gmail.com", Password: "pqrstuvwxyz"},
			returnErrorMock:    nil,
			expectedToken:      nil,
			expectedError:      errors.New(UsernameAndPasswordDoesntMatch),
		}, {
			description:        "Login given correct username and password",
			email:              "yusuf_sholeh@gmail.com",
			password:           "abcdefghi",
			inputEmailMock:     "yusuf_sholeh@gmail.com",
			returnCustomerMock: &models.Customer{Email: "yusuf_sholeh@gmail.com", Password: "abcdefghi"},
			returnErrorMock:    nil,
			inputUserIDMock:    0,
			inputUserTypeMock:  "customer",
			expectedToken:      &middleware.Token{Value: "qazwsxedcrfv", ExpiresAt: "12345678"},
			expectedError:      nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		if tc.returnCustomerMock != nil {
			hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(tc.returnCustomerMock.Password), bcrypt.DefaultCost)
			tc.returnCustomerMock.Password = string(hashedPassword)
		}
		mockedCustomerRepository.On("GetByEmail", tc.inputEmailMock).Return(tc.returnCustomerMock, tc.returnErrorMock)

		mockedTokenCreation := new(mockMiddleware.ITokenCreation)
		mockedTokenCreation.On("CreateToken", tc.inputUserIDMock, tc.inputUserTypeMock, middleware.NormalLoginType).Return(tc.expectedToken)

		authService := AuthService{CustomerRepository: mockedCustomerRepository, TokenCreation: mockedTokenCreation}
		token, err := authService.Login(tc.email, tc.password)

		assert.Equal(token, tc.expectedToken, tc.description)
		assert.Equal(err, tc.expectedError, tc.description)
	}
}
