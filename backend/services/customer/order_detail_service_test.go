package customerservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"
	"time"

	"github.com/jinzhu/gorm"

	"github.com/stretchr/testify/mock"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
)

func TestActiveOrderService_GetOrderDetailData(t *testing.T) {
	tests := []struct {
		description                       string
		orderID                           uint
		customerID                        uint
		returnOrderMock                   *models.Order
		returnMitraMock                   *models.Mitra
		returnMitraLocationMock           *models.MitraLocation
		returnGarbageTypesMock            []models.GarbageType
		returnOrderDetailsMock            []models.OrderDetail
		returnOrderStatusHistoryMock      *models.OrderStatusHistory
		returnErrorOrderMock              error
		returnErrorMitraMock              error
		returnErrorMitraLocationMock      error
		returnErrorGarbageTypesMock       error
		returnErrorOrderDetailsMock       error
		returnErrorOrderStatusHistoryMock error
		expectedOrderDetailResponse       *OrderDetailResponse
		expectedError                     error
	}{
		{
			description:                       "Orders error",
			orderID:                           1,
			customerID:                        2,
			returnOrderMock:                   nil,
			returnMitraMock:                   nil,
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              errors.New("Error"),
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(CanNotAccessDatabase),
		}, {
			description:                       "There are customer access other customer order",
			orderID:                           1,
			customerID:                        2,
			returnOrderMock:                   &models.Order{CustomerID: 3},
			returnMitraMock:                   nil,
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(CanNotAccessOrder),
		}, {
			description: "Order detail searching",
			orderID:     1,
			customerID:  2,
			returnOrderMock: &models.Order{
				Model:        gorm.Model{CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
				CustomerID:   2,
				MitraID:      3,
				LatestStatus: 1,
				Address:      "UI Depok",
			},
			returnMitraMock:         nil,
			returnMitraLocationMock: nil,
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse: &OrderDetailResponse{
				ID:      1,
				Address: "UI Depok",
				GarbageDetail: []GarbageDetail{
					GarbageDetail{Title: "Sampah 1", Quantity: 6, QuantityType: "Kg"},
					GarbageDetail{Title: "Sampah 2", Quantity: 6, QuantityType: "Kg"},
				},
				Status: "Searching",
				Time:   "Monday, 22 April 2019, 20:14",
			},
			expectedError: nil,
		}, {
			description:                       "Customers error",
			orderID:                           1,
			customerID:                        2,
			returnOrderMock:                   &models.Order{CustomerID: 2, MitraID: 3, LatestStatus: 2},
			returnMitraMock:                   nil,
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              errors.New("Error"),
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(CanNotAccessDatabase),
		}, {
			description:                       "Mitra locations error",
			orderID:                           1,
			customerID:                        2,
			returnOrderMock:                   &models.Order{CustomerID: 2, MitraID: 3, LatestStatus: 2},
			returnMitraMock:                   &models.Mitra{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      errors.New("Error"),
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(CanNotAccessDatabase),
		}, {
			description:                       "Garbage types error",
			orderID:                           1,
			customerID:                        2,
			returnOrderMock:                   &models.Order{CustomerID: 2, MitraID: 3, LatestStatus: 2},
			returnMitraMock:                   &models.Mitra{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock:           &models.MitraLocation{MitraID: 3},
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       errors.New("Error"),
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(CanNotAccessDatabase),
		}, {
			description:             "Order details error",
			orderID:                 1,
			customerID:              2,
			returnOrderMock:         &models.Order{CustomerID: 2, MitraID: 3, LatestStatus: 2},
			returnMitraMock:         &models.Mitra{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 3},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       errors.New("Error"),
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(CanNotAccessDatabase),
		}, {
			description:             "Order status histories error",
			orderID:                 1,
			customerID:              2,
			returnOrderMock:         &models.Order{CustomerID: 2, MitraID: 3, LatestStatus: 2},
			returnMitraMock:         &models.Mitra{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 3},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: errors.New("Error"),
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(CanNotAccessDatabase),
		}, {
			description: "Get detail success",
			orderID:     1,
			customerID:  2,
			returnOrderMock: &models.Order{
				CustomerID:         2,
				MitraID:            3,
				LatestStatus:       5,
				Address:            "UI Depok",
				CancellationReason: "Mau cancel aja",
				CustomerLat:        -6.30786100,
				CustomerLon:        106.83883300,
			},
			returnMitraMock:         &models.Mitra{Model: gorm.Model{ID: 3}, Name: "Endrawan", Phone: "081234567895"},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 3},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock: &models.OrderStatusHistory{
				Model:         gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
				OrderID:       1,
				OrderStatusID: 5,
			},
			returnErrorOrderMock:              nil,
			returnErrorMitraMock:              nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse: &OrderDetailResponse{
				ID:       1,
				Name:     "Endrawan",
				Address:  "UI Depok",
				Distance: 11868.265705783926,
				Price:    6000,
				GarbageDetail: []GarbageDetail{
					GarbageDetail{Title: "Sampah 1", Quantity: 6, QuantityType: "Kg"},
					GarbageDetail{Title: "Sampah 2", Quantity: 6, QuantityType: "Kg"},
				},
				Status: "Cancelled",
				Time:   "Monday, 22 April 2019, 20:14",
				Reason: "Mau cancel aja",
				Phone:  "081234567895",
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetByID", mock.Anything).Return(tc.returnOrderMock, tc.returnErrorOrderMock)

		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByID", mock.Anything).Return(tc.returnMitraMock, tc.returnErrorMitraMock)

		mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
		mockedMitraLocationRepository.On("GetByMitraId", mock.Anything).Return(tc.returnMitraLocationMock, tc.returnErrorMitraLocationMock)

		mockedGarbageTypeRepository := new(mockRepositories.IGarbageTypeRepository)
		mockedGarbageTypeRepository.On("GetAll", mock.Anything).Return(tc.returnGarbageTypesMock, tc.returnErrorGarbageTypesMock)

		mockedOrderDetailRepository := new(mockRepositories.IOrderDetailRepository)
		mockedOrderDetailRepository.On("GetByOrderID", mock.Anything).Return(tc.returnOrderDetailsMock, tc.returnErrorOrderDetailsMock)

		mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
		mockedOrderStatusHistoryRepository.On("GetByOrderIDAndOrderStatusID", mock.Anything, mock.Anything).Return(tc.returnOrderStatusHistoryMock, tc.returnErrorOrderStatusHistoryMock)

		mockedOrderStatusRepository := new(mockRepositories.IOrderStatusRepository)
		mockedOrderStatusRepository.On("GetByID", uint(1)).Return(&models.OrderStatus{Description: "Searching"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(2)).Return(&models.OrderStatus{Description: "Scheduled"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(3)).Return(&models.OrderStatus{Description: "Pick Up"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(4)).Return(&models.OrderStatus{Description: "Completed"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(5)).Return(&models.OrderStatus{Description: "Cancelled"}, nil)

		orderDetailService := OrderDetailService{
			OrderRepository:              mockedOrderRepository,
			MitraRepository:              mockedMitraRepository,
			MitraLocationRepository:      mockedMitraLocationRepository,
			OrderDetailRepository:        mockedOrderDetailRepository,
			GarbageTypeRepository:        mockedGarbageTypeRepository,
			OrderStatusHistoryRepository: mockedOrderStatusHistoryRepository,
			OrderStatusRepository:        mockedOrderStatusRepository,
		}

		orderDetailResponse, err := orderDetailService.GetOrderDetailData(tc.orderID, tc.customerID)

		assert.Equal(tc.expectedOrderDetailResponse, orderDetailResponse, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
