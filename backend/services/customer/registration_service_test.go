package customerservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"errors"
	"reflect"
	"testing"

	"github.com/jinzhu/gorm"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestRegister(t *testing.T) {
	tests := []struct {
		description        string
		phone              string
		password           string
		name               string
		email              string
		returnErrorOnEmail error
		returnErrorOnPhone error
		returnCustomerMock *models.Customer
		returnErrorMock    error
		inputUserIDMock    uint
		inputUserTypeMock  string
		expectedCustomer   *models.Customer
		expectedError      error
	}{
		{
			description:        "Register error: email already exists",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnEmail: errors.New(emailAlreadyExists),
			returnErrorOnPhone: gorm.ErrRecordNotFound,
			expectedCustomer:   nil,
			expectedError:      errors.New(emailAlreadyExists),
		},
		{
			description:        "Register error: phone already exists",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnPhone: errors.New(phoneAlreadyExists),
			returnErrorOnEmail: gorm.ErrRecordNotFound,
			expectedCustomer:   nil,
			expectedError:      errors.New(phoneAlreadyExists),
		},
		{
			description:        "Register still error after checking email and phone",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnPhone: gorm.ErrRecordNotFound,
			returnErrorOnEmail: gorm.ErrRecordNotFound,
			returnCustomerMock: nil,
			returnErrorMock:    errors.New("Cannot create customer record"),
			inputUserIDMock:    0,
			inputUserTypeMock:  "customer",
			expectedCustomer:   nil,
			expectedError:      errors.New("Cannot create customer record"),
		},
		{
			description:        "Register success",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnPhone: gorm.ErrRecordNotFound,
			returnErrorOnEmail: gorm.ErrRecordNotFound,
			returnCustomerMock: &models.Customer{Phone: "08123456789", Password: "abcdefghi", Email: "yusuf_sholeh@gmail.com"},
			returnErrorMock:    nil,
			inputUserIDMock:    0,
			inputUserTypeMock:  "customer",
			expectedCustomer:   &models.Customer{Phone: "08123456789", Password: "", Email: "yusuf_sholeh@gmail.com"},
			expectedError:      nil,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		mockedCustomerRepository.On("GetByPhoneNumber", tc.phone).Return(&models.Customer{}, tc.returnErrorOnPhone)
		mockedCustomerRepository.On("GetByEmail", tc.email).Return(&models.Customer{}, tc.returnErrorOnEmail)
		mockedCustomerRepository.On("Add", mock.MatchedBy(func(m *models.Customer) bool {
			return reflect.DeepEqual(m, &models.Customer{Phone: tc.phone, Password: tc.password, Name: tc.name, Email: tc.email, LoginType: middleware.NormalLoginType})
		})).Return(tc.returnCustomerMock, tc.returnErrorMock)
		registrationService := RegistrationService{CustomerRepository: mockedCustomerRepository}
		customer, err := registrationService.Register(tc.phone, tc.password, tc.name, tc.email)
		assert.Equal(tc.expectedCustomer, customer, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
