package customerservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"errors"

	"golang.org/x/crypto/bcrypt"
)

type ManageAccountService struct {
	CustomerRepository repositories.ICustomerRepository
}

type IManageAccountService interface {
	GetPrivacyPolicyDetail() []PrivacyPolicyDetail
	GetCustomerData(customerId uint) (*CustomerData, error)
	UpdateCustomerData(customerId uint, requestCustomerData *CustomerData) error
}

type PrivacyPolicyDetail struct {
	Title string `json:"title,omitempty"`
	Body  string `json:"body,omitempty"`
}

type CustomerData struct {
	Phone       string `json:"phone,omitempty"`
	Name        string `json:"name,omitempty"`
	Email       string `json:"email,omitempty"`
	LoginType   int    `json:"login_type,omitempty"`
	OldPassword string `json:"old_password,omitempty"`
	NewPassword string `json:"new_password,omitempty"`
}

const (
	personalInfoWhichWeCollect       = "Kita mengumpulkan informasi yang bisa mengidentifikasi atau bisa diidentifikasi, kontak, atau mencari lokasi pengguna dan device yang memiliki informasi pengguna tersebut. Informasi personal meliputi, tapi tidak terbatas pada, nama, alamat, tanggal lahir, nomor telepon, alamat e-mail, akun bank dan jenis kelamin."
	theUseOfPersonalInfo             = "Kita mungkin menggunakan informasi personal anda untuk tujuan kegunaan aplikasi ini dan sesuai dengan peraturan yang berlaku. Kita menggunakan informasi anda untuk mengidentifikasi dan mendaftarkan pengguna sebagai user, dan memverisikasi dan deaktivasi."
	accessAndCorrectionPersonalInfo  = "Sesuai dengan hukum yang berlaku, pengguna bisa mengajukan permintaan kepada kami untuk akses pembenaran informasi personal yang pegang dengan mengkontak kami sesuai detail di bawah."
	securityOfYourPersonalInfo       = "Kemamanan informasi data anda adalah kepentingan utama yang kami jaga. Kami akan menggunakan seluruh cara untuk mengamankan informasi personal pengguna."
	updateNotAllowedError            = "Unable to fetch customer data"
	updateError                      = "Failed to update customer data"
	passwordMismatch                 = "Old password is incorrect"
	googleAccountUpdatePasswordError = "Unable to change password for Google sign-in account"
)

func (s ManageAccountService) GetPrivacyPolicyDetail() []PrivacyPolicyDetail {

	detailList := []PrivacyPolicyDetail{
		PrivacyPolicyDetail{
			Title: "Personal Information Which We Collect",
			Body:  personalInfoWhichWeCollect,
		},
		PrivacyPolicyDetail{
			Title: "The Use of Personal Information",
			Body:  theUseOfPersonalInfo,
		},
		PrivacyPolicyDetail{
			Title: "Access and Correction of Personal Information",
			Body:  accessAndCorrectionPersonalInfo,
		},
		PrivacyPolicyDetail{
			Title: "Security of Your Personal Information",
			Body:  securityOfYourPersonalInfo,
		},
	}

	return detailList
}

func (s ManageAccountService) GetCustomerData(customerId uint) (*CustomerData, error) {
	customerObj, err := s.CustomerRepository.GetByID(customerId)

	if err != nil {
		return nil, errors.New(updateNotAllowedError)
	}

	customerData := &CustomerData{
		Phone:       customerObj.Phone,
		Name:        customerObj.Name,
		Email:       customerObj.Email,
		LoginType:   customerObj.LoginType,
		OldPassword: "",
		NewPassword: "",
	}

	return customerData, nil
}

func (s ManageAccountService) UpdateCustomerData(
	customerId uint,
	requestCustomerData *CustomerData,
) error {
	customer, err := s.CustomerRepository.GetByID(customerId)

	if err != nil {
		return errors.New(updateNotAllowedError)
	}

	if requestCustomerData.NewPassword != "" && customer.LoginType == 2 {
		return errors.New(googleAccountUpdatePasswordError)
	}

	if requestCustomerData.NewPassword != "" {
		error := bcrypt.CompareHashAndPassword([]byte(customer.Password), []byte(requestCustomerData.OldPassword))

		if error != nil {
			return errors.New(passwordMismatch)
		}

	}

	newCustomer := models.Customer{
		Model:             customer.Model,
		Phone:             requestCustomerData.Phone,
		Name:              requestCustomerData.Name,
		Email:             requestCustomerData.Email,
		Password:          requestCustomerData.NewPassword,
		LoginType:         customer.LoginType,
		NotificationToken: customer.NotificationToken,
	}

	_, updatedErr := s.CustomerRepository.Update(&newCustomer)

	if updatedErr != nil {
		return errors.New(updateError)
	}

	return nil
}
