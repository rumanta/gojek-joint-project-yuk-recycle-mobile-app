package oauthconfiguration

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/oauth2"
)

func TestExchangeToken(t *testing.T) {
	googleConfig := new(GoogleOAuthConfiguration)
	_, err := googleConfig.ExchangeToken("abc", oauth2.Endpoint{})
	assert.Error(t, err, "There should be an error")
}

func TestGetUserProfile(t *testing.T) {
	googleConfig := new(GoogleOAuthConfiguration)
	_, err := googleConfig.GetUserProfile("invalid_link", "invalid_access_token")
	assert.Error(t, err, "There should be an error")
}
