package oauthconfiguration

import (
	"net/http"
	"os"

	"golang.org/x/oauth2"
)

const (
	profileScope = "https://www.googleapis.com/auth/userinfo.profile"
)

// UserProfile returned from google
type UserProfile struct {
	Email string `json:"email"`
	Name  string `json:"name"`
}

// GoogleOAuthConfiguration implements IGoogleOAuthConfig. Private service configuration for communicating to google
type GoogleOAuthConfiguration struct {
}

// IGoogleOAuthConfiguration private service for communication to google
type IGoogleOAuthConfiguration interface {
	ExchangeToken(authCode string, endpoint oauth2.Endpoint) (*oauth2.Token, error)
	GetUserProfile(url, accessToken string) (*http.Response, error)
}

// ExchangeToken for getting access token
func (g GoogleOAuthConfiguration) ExchangeToken(authorizationCode string, endpoint oauth2.Endpoint) (*oauth2.Token, error) {
	googleOauthConfig := &oauth2.Config{
		ClientID:     os.Getenv("oauth_client_id"),
		ClientSecret: os.Getenv("oauth_client_secret"),
		RedirectURL:  os.Getenv("redirect_oauth_google"),
		Scopes: []string{
			profileScope,
		},
		Endpoint: endpoint,
	}
	token, err := googleOauthConfig.Exchange(oauth2.NoContext, authorizationCode)
	return token, err
}

// GetUserProfile given access token -> then http.get to google -> return user profile
func (g GoogleOAuthConfiguration) GetUserProfile(url, accessToken string) (*http.Response, error) {
	response, err := http.Get(url + accessToken)
	return response, err
}
