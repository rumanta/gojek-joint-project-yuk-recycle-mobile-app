package customerservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	makeOrderCheckInterval "PPLA4/backend/services/customer/make_order_database_checking_interval"
	notificationServices "PPLA4/backend/services/notification"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/NaySoftware/go-fcm"
	"github.com/jinzhu/gorm"
	geo "github.com/kellydunn/golang-geo"
)

type MakeOrderServices struct {
	OrderRepository               repositories.IOrderRepository
	MitraRepository               repositories.IMitraRepository
	MitraLocationRepository       repositories.IMitraLocationRepository
	MitraPickUpDistanceRepository repositories.IMitraPickUpDistanceRepository
	MitraGarbageSettingRepository repositories.IMitraGarbageSettingRepository
	NotificationServices          notificationServices.INotificationService
	MakeOrderCheckInterval        makeOrderCheckInterval.ICheckingInterval
	OrderAttemptRepository        repositories.IOrderAttemptRepository
	CustomerRepository            repositories.ICustomerRepository
	GarbageTypeRepository         repositories.IGarbageTypeRepository
	OrderDetailRepository         repositories.IOrderDetailRepository
	OrderStatusHistoryRepository  repositories.IOrderStatusHistoryRepository
	MitraPickUpScheduleRepository repositories.IMitraPickUpScheduleRepository
}

type IMakeOrderServices interface {
	MakeOrder(makeOrderRequest *MakeOrderRequest) (*models.Mitra, *models.Order, *models.Customer, error)
}

type MakeOrderDetailRequest struct {
	GarbageTypeID   uint   `json:"garbage_type_id" validate:"required"`
	GarbageTypeName string `json:"garbage_type_name"`
	Quantity        uint   `json:"quantity" validate:"required"`
	QuantityType    string `json:"quantity_type"`
}

type MakeOrderRequest struct {
	CustomerID         uint                     `json:"customer_id,omitempty"`
	CustomerLat        float64                  `json:"customer_lat" validate:"required,numeric"`
	CustomerLon        float64                  `json:"customer_lon" validate:"required,numeric"`
	Notes              string                   `json:"notes,omitempty"`
	Address            string                   `json:"address,omitempty"`
	OrderDetailRequest []MakeOrderDetailRequest `json:"order_detail" validate:"required"`
}

const (
	NoServicesAvailable = "This services is not available in your area"
	NoMitraTakeTheOrder = "No mitra take the order"
	MinimumPrice        = 500
)

func (o MakeOrderServices) MakeOrder(makeOrderRequest *MakeOrderRequest) (*models.Mitra, *models.Order, *models.Customer, error) {
	customer, _ := o.CustomerRepository.GetByID(makeOrderRequest.CustomerID)
	customerName := customer.Name

	order, err := o.OrderRepository.Save(
		&models.Order{
			CustomerID:   makeOrderRequest.CustomerID,
			CustomerLat:  makeOrderRequest.CustomerLat,
			CustomerLon:  makeOrderRequest.CustomerLon,
			Notes:        makeOrderRequest.Notes,
			Address:      makeOrderRequest.Address,
			LatestStatus: orderConstant.InitOrder,
		},
	)

	if err != nil {
		return nil, nil, customer, err
	}

	o.OrderStatusHistoryRepository.Save( // update latest status on table Order by trigger
		&models.OrderStatusHistory{
			OrderID:       order.ID,
			OrderStatusID: orderConstant.InitOrder,
		},
	)

	mitraLocations, err := o.MitraLocationRepository.GetAllByLocationAndDistance(
		makeOrderRequest.CustomerLat,
		makeOrderRequest.CustomerLon,
		1e9,
	)

	if err == gorm.ErrRecordNotFound {
		o.OrderStatusHistoryRepository.Save( // update latest status on table Order by trigger
			&models.OrderStatusHistory{
				OrderID:       order.ID,
				OrderStatusID: orderConstant.NoServiceAvailable,
			},
		)
		return nil, order, customer, errors.New(NoServicesAvailable)
	}

	if err != nil {
		o.OrderStatusHistoryRepository.Save( // update latest status on table Order by trigger
			&models.OrderStatusHistory{
				OrderID:       order.ID,
				OrderStatusID: orderConstant.NoServiceAvailable,
			},
		)
		return nil, order, customer, err
	}

	if len(mitraLocations) == 0 {
		o.OrderStatusHistoryRepository.Save( // update latest status on table Order by trigger
			&models.OrderStatusHistory{
				OrderID:       order.ID,
				OrderStatusID: orderConstant.NoServiceAvailable,
			},
		)
		return nil, order, customer, errors.New(NoServicesAvailable)
	}

	mitraFoundInCoveredDistance := false

	customerPosition := geo.NewPoint(makeOrderRequest.CustomerLat, makeOrderRequest.CustomerLon)
	for _, mitraLocation := range mitraLocations {
		mitraPosition := geo.NewPoint(mitraLocation.Lat, mitraLocation.Lon)
		distanceToMitra := customerPosition.GreatCircleDistance(mitraPosition)
		mitraDistance, err := o.MitraPickUpDistanceRepository.GetByMitraId(mitraLocation.MitraID)
		if err != nil {
			return nil, nil, customer, err
		}
		if distanceToMitra <= float64(mitraDistance.MaximumDistance) {
			mitraFoundInCoveredDistance = true
			mitraSettings, err := o.MitraGarbageSettingRepository.GetByMitraId(mitraLocation.MitraID)
			if err != nil {
				return nil, nil, customer, err
			}
			garbageSettingsByGarbageTypeId := make(map[uint]models.MitraGarbageSetting)
			for _, mitraSetting := range mitraSettings {
				garbageSettingsByGarbageTypeId[mitraSetting.GarbageTypeID] = mitraSetting
			}
			sumPrice := uint(0)
			isValidMitra := true
			for _, orderDetail := range makeOrderRequest.OrderDetailRequest {
				item := garbageSettingsByGarbageTypeId[orderDetail.GarbageTypeID]
				sumPrice += item.Price * orderDetail.Quantity
				if orderDetail.Quantity < item.MinimumQuantity || item.Price < MinimumPrice {
					isValidMitra = false
					fmt.Println("Quantity:")
					fmt.Println(orderDetail.Quantity, item.MinimumQuantity)
					fmt.Println("Minimum price:")
					fmt.Println(item.Price, MinimumPrice)
					break
				}
			}
			if isValidMitra {
				mitra, err := o.MitraRepository.GetByID(mitraLocation.MitraID)
				if err != nil {
					return nil, nil, customer, err
				}
				token := uint(rand.Int31())  // TODO: use more secure prng
				fmt.Println(token, order.ID) // logging
				_, err = o.OrderAttemptRepository.Save(
					&models.OrderAttempt{
						OrderID:              order.ID,
						MitraID:              mitra.ID,
						OrderAttemptStatusID: 1,
						Token:                token,
					},
				)
				if err != nil {
					return nil, nil, customer, err
				}
				garbageDetailData := []*notificationServices.GarbageDetail{}
				for _, orderDetail := range makeOrderRequest.OrderDetailRequest {
					garbageDetailData = append(garbageDetailData, &notificationServices.GarbageDetail{
						Title:        orderDetail.GarbageTypeName,
						Quantity:     orderDetail.Quantity,
						QuantityType: orderDetail.QuantityType,
					})
				}
				status, err := o.NotificationServices.SendNotification(
					[]string{mitra.NotificationToken},
					&fcm.NotificationPayload{
						Title: "Incoming Order",
						Body:  "Click here to accept or reject the order",
					},
					&notificationServices.NotificationData{
						ClickAction:   "FLUTTER_NOTIFICATION_CLICK",
						Token:         token,
						TypeData:      "RECEIVE_ORDER",
						OrderID:       order.ID,
						Name:          customerName,
						Address:       makeOrderRequest.Address,
						Distance:      distanceToMitra,
						GarbageDetail: garbageDetailData,
						Price:         sumPrice,
						Notes:         order.Notes,
					},
				)
				if err != nil || status.Fail > 0 {
					// failed sent notification to mitra, skip this mitra
					fmt.Println("an error has occured when sending notification to ", mitra.ID, mitra.Phone) // logging
				} else {
					// success
					fmt.Println("Notification has been sent to ", mitra.ID, mitra.Phone) // logging
					status, _ := o.MakeOrderCheckInterval.CheckDatabase(order.ID, mitra.ID, 30)
					if status == makeOrderCheckInterval.Accepted {
						for _, orderDetail := range makeOrderRequest.OrderDetailRequest {
							item := garbageSettingsByGarbageTypeId[orderDetail.GarbageTypeID]
							o.OrderDetailRepository.Save(
								&models.OrderDetail{
									OrderID:       order.ID,
									GarbageTypeID: orderDetail.GarbageTypeID,
									TotalAmount:   int(item.Price * orderDetail.Quantity),
									Quantity:      int(orderDetail.Quantity),
								},
							)
						}
						pickUpSchedule, _ := o.MitraPickUpScheduleRepository.GetByMitraId(mitra.ID)
						requiredDay := getClosestDay(int(time.Now().Weekday()), pickUpSchedule)
						order.ScheduledAt = time.Now().AddDate(0, 0, requiredDay)
						order.MitraID = mitra.ID
						o.OrderRepository.Save(order)
						o.OrderStatusHistoryRepository.Save( // update latest status on table Order by trigger
							&models.OrderStatusHistory{
								OrderID:       order.ID,
								OrderStatusID: orderConstant.AcceptedAndScheduled,
							},
						)

						return mitra, order, customer, nil
					}
				}
			}
		}
	}

	if !mitraFoundInCoveredDistance {
		o.OrderStatusHistoryRepository.Save( // update latest status on table Order by trigger
			&models.OrderStatusHistory{
				OrderID:       order.ID,
				OrderStatusID: orderConstant.NoServiceAvailable,
			},
		)
		return nil, order, customer, errors.New(NoServicesAvailable)
	}

	o.OrderStatusHistoryRepository.Save( // update latest status on table Order by trigger
		&models.OrderStatusHistory{
			OrderID:       order.ID,
			OrderStatusID: orderConstant.NoMitraTakeTheOrder,
		},
	)
	return nil, order, customer, errors.New(NoMitraTakeTheOrder)
}

func getClosestDay(currentDay int, pickUpSchedule *models.MitraPickUpSchedule) int {
	i := currentDay
	requiredDay := 0
	dayOfWeekScheduled := getArrayOfPickUpSchedule(pickUpSchedule)
	for {
		if dayOfWeekScheduled[i] {
			return requiredDay
		}
		i = (i + 1) % 7
		requiredDay++
		if i == currentDay {
			break
		}
	}
	return -1
}

// return bit array of length 7
func getArrayOfPickUpSchedule(pickUpSchedule *models.MitraPickUpSchedule) []bool {
	result := make([]bool, 7)
	result[0] = pickUpSchedule.Sunday
	result[1] = pickUpSchedule.Monday
	result[2] = pickUpSchedule.Tuesday
	result[3] = pickUpSchedule.Wednesday
	result[4] = pickUpSchedule.Thursday
	result[5] = pickUpSchedule.Friday
	result[6] = pickUpSchedule.Saturday
	return result
}
