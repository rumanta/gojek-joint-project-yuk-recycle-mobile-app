package customerservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"
	"time"

	geo "github.com/kellydunn/golang-geo"
)

const (
	CanNotAccessOrder = "Oops, try again later"
)

type OrderDetailService struct {
	OrderRepository              repositories.IOrderRepository
	MitraRepository              repositories.IMitraRepository
	MitraLocationRepository      repositories.IMitraLocationRepository
	OrderDetailRepository        repositories.IOrderDetailRepository
	GarbageTypeRepository        repositories.IGarbageTypeRepository
	OrderStatusHistoryRepository repositories.IOrderStatusHistoryRepository
	OrderStatusRepository        repositories.IOrderStatusRepository
}

type IOrderDetailService interface {
	GetOrderDetailData(orderID, customerID uint) (*OrderDetailResponse, error)
}

type OrderDetailResponse struct {
	ID            uint            `json:"id"`
	Name          string          `json:"name"`
	Address       string          `json:"address"`
	Distance      float64         `json:"distance,omitempty"`
	Price         uint            `json:"price"`
	GarbageDetail []GarbageDetail `json:"garbage_detail"`
	Status        string          `json:"status"`
	Time          string          `json:"time"`
	Reason        string          `json:"reason"`
	Phone         string          `json:"phone"`
	Notes         string          `json:"notes"`
}

type GarbageDetail struct {
	Title        string `json:"title,omitempty"`
	Quantity     uint   `json:"quantity,omitempty"`
	QuantityType string `json:"quantity_type,omitempty"`
}

func (o OrderDetailService) GetOrderDetailData(orderID, customerID uint) (*OrderDetailResponse, error) {
	order, err := o.OrderRepository.GetByID(orderID)

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	if order.CustomerID != customerID {
		return nil, errors.New(CanNotAccessOrder)
	}

	if order.LatestStatus == orderConstant.InitOrder {
		garbageTypes, _ := o.GarbageTypeRepository.GetAll()
		orderDetails, _ := o.OrderDetailRepository.GetByOrderID(orderID)

		garbageTypeByGarbageTypeID := make(map[uint]models.GarbageType)
		for _, garbageType := range garbageTypes {
			garbageTypeByGarbageTypeID[garbageType.ID] = garbageType
		}

		sumPrice := uint(0)
		garbageDetails := make([]GarbageDetail, len(orderDetails))
		for i, orderDetail := range orderDetails {
			sumPrice += uint(orderDetail.TotalAmount)
			garbageType := garbageTypeByGarbageTypeID[orderDetail.GarbageTypeID]
			garbageDetails[i] = GarbageDetail{
				Title:        garbageType.Title,
				Quantity:     uint(orderDetail.Quantity),
				QuantityType: garbageType.QuantityType,
			}
		}

		status, _ := o.OrderStatusRepository.GetByID(uint(order.LatestStatus))

		return &OrderDetailResponse{
			ID:            orderID,
			Address:       order.Address,
			GarbageDetail: garbageDetails,
			Status:        status.Description,
			Time:          order.CreatedAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006, 15:04"),
			Notes:         order.Notes,
		}, nil
	}

	mitra, err := o.MitraRepository.GetByID(order.MitraID)

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	mitraLocation, err := o.MitraLocationRepository.GetByMitraId(mitra.ID)

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	garbageTypes, err := o.GarbageTypeRepository.GetAll()

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	orderDetails, err := o.OrderDetailRepository.GetByOrderID(orderID)

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	orderStatusHistory, err := o.OrderStatusHistoryRepository.GetByOrderIDAndOrderStatusID(orderID, uint(order.LatestStatus))

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	customerPosition := geo.NewPoint(order.CustomerLat, order.CustomerLon)
	mitraPosition := geo.NewPoint(mitraLocation.Lat, mitraLocation.Lon)
	distanceToMitra := customerPosition.GreatCircleDistance(mitraPosition)

	garbageTypeByGarbageTypeID := make(map[uint]models.GarbageType)
	for _, garbageType := range garbageTypes {
		garbageTypeByGarbageTypeID[garbageType.ID] = garbageType
	}

	sumPrice := uint(0)
	garbageDetails := make([]GarbageDetail, len(orderDetails))
	for i, orderDetail := range orderDetails {
		sumPrice += uint(orderDetail.TotalAmount)
		garbageType := garbageTypeByGarbageTypeID[orderDetail.GarbageTypeID]
		garbageDetails[i] = GarbageDetail{
			Title:        garbageType.Title,
			Quantity:     uint(orderDetail.Quantity),
			QuantityType: garbageType.QuantityType,
		}
	}

	status, _ := o.OrderStatusRepository.GetByID(uint(order.LatestStatus))

	response := &OrderDetailResponse{
		ID:            orderID,
		Name:          mitra.Name,
		Address:       order.Address,
		Distance:      distanceToMitra,
		Price:         sumPrice,
		GarbageDetail: garbageDetails,
		Status:        status.Description,
		Time:          orderStatusHistory.CreatedAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006, 15:04"),
		Reason:        order.CancellationReason,
		Phone:         mitra.Phone,
		Notes:         order.Notes,
	}

	return response, nil
}
