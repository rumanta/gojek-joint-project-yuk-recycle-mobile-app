package customerservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"
	"time"

	"github.com/jinzhu/gorm"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
)

func TestActiveOrderService_GetActiveOrdersData(t *testing.T) {
	tests := []struct {
		description           string
		customerID            uint
		returnOrdersMock      []models.Order
		returnMitrasMock      []*models.Mitra
		returnErrorOrdersMock error
		returnErrorMitrasMock error
		expectedActiveOrders  []ActiveOrder
		expectedError         error
	}{
		{
			description:           "Orders error",
			customerID:            uint(1),
			returnOrdersMock:      nil,
			returnMitrasMock:      nil,
			returnErrorOrdersMock: errors.New("Error"),
			returnErrorMitrasMock: nil,
			expectedActiveOrders:  nil,
			expectedError:         errors.New(CanNotAccessDatabase),
		}, {
			description: "Mitras error",
			customerID:  uint(1),
			returnOrdersMock: []models.Order{
				models.Order{
					Model:        gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					MitraID:      1,
					LatestStatus: 2,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				},
			},
			returnMitrasMock: []*models.Mitra{
				&models.Mitra{
					Model: gorm.Model{ID: 1},
					Name:  "Endrawan",
				},
			},
			returnErrorOrdersMock: nil,
			returnErrorMitrasMock: errors.New("Error"),
			expectedActiveOrders:  nil,
			expectedError:         errors.New(CanNotAccessDatabase),
		}, {
			description: "Get data success",
			customerID:  uint(1),
			returnOrdersMock: []models.Order{
				models.Order{
					Model:        gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					MitraID:      1,
					LatestStatus: 1,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				}, models.Order{
					Model:        gorm.Model{ID: 2, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					MitraID:      2,
					LatestStatus: 2,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				}, models.Order{
					Model:        gorm.Model{ID: 3, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					MitraID:      3,
					LatestStatus: 3,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				},
			},
			returnMitrasMock: []*models.Mitra{
				&models.Mitra{
					Model: gorm.Model{ID: 1},
					Name:  "Endrawan",
				}, &models.Mitra{
					Model: gorm.Model{ID: 2},
					Name:  "Endrawan Andika",
				}, &models.Mitra{
					Model: gorm.Model{ID: 3},
					Name:  "Endrawan Andika Wicaksana",
				},
			},
			returnErrorOrdersMock: nil,
			returnErrorMitrasMock: nil,
			expectedActiveOrders: []ActiveOrder{
				ActiveOrder{
					ID:            1,
					Time:          "Monday, 22 April 2019, 20:14",
					Status:        "Searching",
					ScheduledTime: "Thursday, 25 April 2019",
				}, ActiveOrder{
					ID:            2,
					Name:          "Endrawan Andika",
					Time:          "Monday, 22 April 2019, 20:14",
					Status:        "Scheduled",
					ScheduledTime: "Thursday, 25 April 2019",
				}, ActiveOrder{
					ID:            3,
					Name:          "Endrawan Andika Wicaksana",
					Time:          "Monday, 22 April 2019, 20:14",
					Status:        "Pick Up",
					ScheduledTime: "Thursday, 25 April 2019",
				},
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetLatest20ActiveOrdersByCustomerID", tc.customerID).Return(tc.returnOrdersMock, tc.returnErrorOrdersMock)

		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		for _, customer := range tc.returnMitrasMock {
			mockedMitraRepository.On("GetByID", customer.ID).Return(customer, tc.returnErrorMitrasMock)
		}

		mockedOrderStatusRepository := new(mockRepositories.IOrderStatusRepository)
		mockedOrderStatusRepository.On("GetByID", uint(1)).Return(&models.OrderStatus{Description: "Searching"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(2)).Return(&models.OrderStatus{Description: "Scheduled"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(3)).Return(&models.OrderStatus{Description: "Pick Up"}, nil)

		activeOrderService := ActiveOrderService{
			OrderRepository:       mockedOrderRepository,
			MitraRepository:       mockedMitraRepository,
			OrderStatusRepository: mockedOrderStatusRepository,
		}

		activeOrders, err := activeOrderService.GetActiveOrdersData(tc.customerID)

		assert.Equal(tc.expectedActiveOrders, activeOrders, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
