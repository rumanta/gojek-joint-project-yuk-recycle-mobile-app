package customerservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"
	"time"

	"github.com/jinzhu/gorm"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
)

func TestOrderHistoryService_GetOrderHistoriessData(t *testing.T) {
	tests := []struct {
		description            string
		customerID             uint
		returnOrdersMock       []models.Order
		returnMitrasMock       []*models.Mitra
		returnErrorOrdersMock  error
		returnErrorMitrasMock  error
		expectedOrderHistories []OrderHistory
		expectedError          error
	}{
		{
			description:            "Orders error",
			customerID:             uint(1),
			returnOrdersMock:       nil,
			returnMitrasMock:       nil,
			returnErrorOrdersMock:  errors.New("Error"),
			returnErrorMitrasMock:  nil,
			expectedOrderHistories: nil,
			expectedError:          errors.New(CanNotAccessDatabase),
		}, {
			description: "Mitras error",
			customerID:  uint(1),
			returnOrdersMock: []models.Order{
				models.Order{
					Model:        gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					MitraID:      1,
					LatestStatus: 1,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				},
			},
			returnMitrasMock: []*models.Mitra{
				&models.Mitra{
					Model: gorm.Model{ID: 1},
					Name:  "Endrawan",
				},
			},
			returnErrorOrdersMock:  nil,
			returnErrorMitrasMock:  errors.New("Error"),
			expectedOrderHistories: nil,
			expectedError:          errors.New(CanNotAccessDatabase),
		}, {
			description: "Get data success",
			customerID:  uint(1),
			returnOrdersMock: []models.Order{
				models.Order{
					Model:        gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					MitraID:      1,
					LatestStatus: 4,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				}, models.Order{
					Model:              gorm.Model{ID: 2, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					MitraID:            2,
					LatestStatus:       5,
					ScheduledAt:        time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
					CancellationReason: "Gpp cancel aja",
				},
			},
			returnMitrasMock: []*models.Mitra{
				&models.Mitra{
					Model: gorm.Model{ID: 1},
					Name:  "Endrawan",
				}, &models.Mitra{
					Model: gorm.Model{ID: 2},
					Name:  "Endrawan Andika",
				}, &models.Mitra{
					Model: gorm.Model{ID: 3},
					Name:  "Endrawan Andika Wicaksana",
				},
			},
			returnErrorOrdersMock: nil,
			returnErrorMitrasMock: nil,
			expectedOrderHistories: []OrderHistory{
				OrderHistory{
					ID:     1,
					Name:   "Endrawan",
					Time:   "Monday, 22 April 2019, 20:14",
					Status: "Completed",
				}, OrderHistory{
					ID:     2,
					Name:   "Endrawan Andika",
					Time:   "Monday, 22 April 2019, 20:14",
					Status: "Cancelled",
					Reason: "Gpp cancel aja",
				},
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetLatest20OrderHistoriesByCustomerID", tc.customerID).Return(tc.returnOrdersMock, tc.returnErrorOrdersMock)

		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		for _, mitra := range tc.returnMitrasMock {
			mockedMitraRepository.On("GetByID", mitra.ID).Return(mitra, tc.returnErrorMitrasMock)
		}

		mockedOrderStatusRepository := new(mockRepositories.IOrderStatusRepository)
		mockedOrderStatusRepository.On("GetByID", uint(4)).Return(&models.OrderStatus{Description: "Completed"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(5)).Return(&models.OrderStatus{Description: "Cancelled"}, nil)

		orderHistoryService := OrderHistoryService{
			OrderRepository:       mockedOrderRepository,
			MitraRepository:       mockedMitraRepository,
			OrderStatusRepository: mockedOrderStatusRepository,
		}

		activeOrders, err := orderHistoryService.GetOrderHistoriesData(tc.customerID)

		assert.Equal(tc.expectedOrderHistories, activeOrders, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
