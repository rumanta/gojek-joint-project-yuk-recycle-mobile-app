package customerservices

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	expectedWhatIsRecycleBody    = "Yuk-recycle adalah aplikasi yang membantu Indonesia untuk Go-Green"
	expectedWhyItMattersBody     = "Kebersihan lingkungan perlu kita jaga, Yuk-Recycle menghadirkan aplikasi bisnis untuk membantu mitra mendapatkan sampah-sampah dari rumah warga"
	expectedBenefitCustomersBody = "Pengguna aplikasi atau Customer dapat membantu Indonesia Go-Green sekaligus mendapatkan profit dari sampah yang sudah tidak digunakan lagi"
)

func TestHomepageService_GetHomepageInfoDetail(t *testing.T) {
	homepageService := HomepageService{}
	homepageInfoDetails := homepageService.GetHomepageInfoDetail()

	assert := assert.New(t)

	for _, infoDetail := range homepageInfoDetails {

		actualTitle := infoDetail.Title
		actualBody := infoDetail.Body

		if actualTitle == "What Is Recycle" {
			assert.Equal(expectedWhatIsRecycleBody, actualBody)
		} else if actualTitle == "Why It Matters" {
			assert.Equal(expectedWhyItMattersBody, actualBody)
		} else if actualTitle == "What Benefits For Customers" {
			assert.Equal(expectedBenefitCustomersBody, actualBody)
		}
	}
}
