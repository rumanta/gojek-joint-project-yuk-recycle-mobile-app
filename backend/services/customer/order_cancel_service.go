package customerservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"PPLA4/backend/services/notification"
	notificationServices "PPLA4/backend/services/notification"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"

	"github.com/NaySoftware/go-fcm"
)

type CancelOrderService struct {
	OrderRepository     repositories.IOrderRepository
	OrderStatusHistory  repositories.IOrderStatusHistoryRepository
	NotificationService notification.INotificationService
	CustomerRepository  repositories.ICustomerRepository
	MitraRepository     repositories.IMitraRepository
}

type ICancelOrderService interface {
	CancelOrder(orderID, customerID uint, reason string) error
}

func (p CancelOrderService) CancelOrder(orderID, customerID uint, reason string) error {
	order, err := p.OrderRepository.GetByID(orderID)

	if err != nil {
		return err
	}

	if customerID != order.CustomerID {
		return errors.New(orderConstant.InvalidCorrespondingCustomer)
	}

	if !orderConstant.IsPrecededStatus[orderConstant.CancelledByCustomer][order.LatestStatus] {
		return errors.New(orderConstant.InvalidOrderStatusTransition)
	}

	mitra, _ := p.MitraRepository.GetByID(order.MitraID)

	order.CancellationReason = reason
	p.OrderRepository.Save(order)
	p.OrderStatusHistory.Save(
		&models.OrderStatusHistory{
			OrderID:       orderID,
			OrderStatusID: orderConstant.CancelledByCustomer,
		},
	)

	p.NotificationService.SendNotification(
		[]string{mitra.NotificationToken},
		&fcm.NotificationPayload{
			Title: "We're sorry!",
			Body:  "Your order has been cancelled",
		},
		&notificationServices.NotificationData{
			ClickAction: "FLUTTER_NOTIFICATION_CLICK",
			TypeData:    "CANCEL_ORDER",
			OrderID:     order.ID,
			Response:    "ORDER CANCELLED",
		},
	)

	return nil
}
