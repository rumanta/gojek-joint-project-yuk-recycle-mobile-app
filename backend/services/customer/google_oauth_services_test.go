package customerservices

import (
	"PPLA4/backend/middleware"
	mockToken "PPLA4/backend/mocks/middleware"
	mockRepository "PPLA4/backend/mocks/repositories"
	mockConfiguration "PPLA4/backend/mocks/services/customer/oauth_configuration"
	"PPLA4/backend/models"
	"errors"
	"net/http/httptest"
	"testing"

	"golang.org/x/oauth2/google"

	"github.com/jinzhu/gorm"

	"github.com/stretchr/testify/mock"

	"golang.org/x/oauth2"

	"github.com/stretchr/testify/assert"
)

func TestOAuthLoginWhenErrorOnExchangeToken(t *testing.T) {
	authorizationToken := "some codes"

	mockedConfiguration := new(mockConfiguration.IGoogleOAuthConfiguration)
	mockedConfiguration.On("ExchangeToken", authorizationToken, google.Endpoint).Return(nil, errors.New("exchange error"))
	googleOAuthService := GoogleOAuthService{Config: mockedConfiguration}
	_, err := googleOAuthService.OAuthLogin(authorizationToken)
	assert.Error(t, err, "some error")
}

func TestOAuthLoginWhenErrorOnGetUserProfile(t *testing.T) {
	authorizationToken := "some codes"
	returnOAuthToken := &oauth2.Token{AccessToken: "access_token"}

	mockedConfiguration := new(mockConfiguration.IGoogleOAuthConfiguration)
	mockedConfiguration.On("ExchangeToken", authorizationToken, google.Endpoint).Return(returnOAuthToken, nil)
	mockedConfiguration.On("GetUserProfile", getAccessTokenURL, returnOAuthToken.AccessToken).Return(nil, errors.New("get user profile error"))
	googleOAuthService := GoogleOAuthService{Config: mockedConfiguration}
	_, err := googleOAuthService.OAuthLogin(authorizationToken)
	assert.Error(t, err, "some error")
}

func TestOAuthLoginWhenErrorOnDecodingUserProfile(t *testing.T) {
	authorizationToken := "some codes"
	returnOAuthToken := &oauth2.Token{AccessToken: "access_token"}

	mockedConfiguration := new(mockConfiguration.IGoogleOAuthConfiguration)
	mockedConfiguration.On("ExchangeToken", authorizationToken, google.Endpoint).Return(returnOAuthToken, nil)
	rr := httptest.NewRecorder()
	rr.Write([]byte("unformat_json{x}x"))
	response := rr.Result()
	mockedConfiguration.On("GetUserProfile", getAccessTokenURL, returnOAuthToken.AccessToken).Return(response, nil)
	googleOAuthService := GoogleOAuthService{Config: mockedConfiguration}
	_, err := googleOAuthService.OAuthLogin(authorizationToken)
	assert.Error(t, err, "some error")
}

func TestOAuthLoginWhenUserNotExistsGivenErrorOnAddNewUser(t *testing.T) {
	authorizationToken := "some codes"
	returnOAuthToken := &oauth2.Token{AccessToken: "access_token"}
	name := "yusuf sholeh"
	email := "yusuf_sholeh@gmail.com"

	mockedConfiguration := new(mockConfiguration.IGoogleOAuthConfiguration)
	mockedConfiguration.On("ExchangeToken", authorizationToken, google.Endpoint).Return(returnOAuthToken, nil)
	rr := httptest.NewRecorder()
	rr.Write([]byte(`{"email":"` + email + `",` + `"name":"` + name + `"}`))
	response := rr.Result()
	mockedConfiguration.On("GetUserProfile", getAccessTokenURL, returnOAuthToken.AccessToken).Return(response, nil)
	mockedCustomerRepository := new(mockRepository.ICustomerRepository)
	mockedCustomerRepository.On("GetByEmail", email).Return(nil, gorm.ErrRecordNotFound)
	mockedCustomerRepository.On("Add", &models.Customer{Email: email, Name: name, LoginType: middleware.OAuthLoginType}).Return(nil, errors.New("error adding record"))
	googleOAuthService := GoogleOAuthService{Config: mockedConfiguration, CustomerRepository: mockedCustomerRepository}
	_, err := googleOAuthService.OAuthLogin(authorizationToken)
	assert.Error(t, err, "some error")
}

func TestOAuthLoginWhenUserNotExistsAndNoErrorOnAddReturnSuccess(t *testing.T) {
	authorizationToken := "some codes"
	returnOAuthToken := &oauth2.Token{AccessToken: "access_token"}
	name := "yusuf sholeh"
	email := "yusuf_sholeh@gmail.com"
	customerID := uint(1)

	mockedConfiguration := new(mockConfiguration.IGoogleOAuthConfiguration)
	mockedConfiguration.On("ExchangeToken", authorizationToken, google.Endpoint).Return(returnOAuthToken, nil)
	rr := httptest.NewRecorder()
	rr.Write([]byte(`{"email":"` + email + `",` + `"name":"` + name + `"}`))
	response := rr.Result()
	mockedConfiguration.On("GetUserProfile", getAccessTokenURL, returnOAuthToken.AccessToken).Return(response, nil)
	mockedCustomerRepository := new(mockRepository.ICustomerRepository)
	mockedCustomerRepository.On("GetByEmail", email).Return(nil, gorm.ErrRecordNotFound)
	mockedCustomerRepository.On("Add", &models.Customer{Email: email, Name: name, LoginType: middleware.OAuthLoginType}).Return(&models.Customer{Model: gorm.Model{ID: customerID}}, nil)
	mockedTokenCreation := new(mockToken.ITokenCreation)
	mockedTokenCreation.On("CreateToken", customerID, middleware.CustomerUserType, middleware.OAuthLoginType).Return(nil)
	googleOAuthService := GoogleOAuthService{Config: mockedConfiguration, CustomerRepository: mockedCustomerRepository, TokenCreation: mockedTokenCreation}
	_, err := googleOAuthService.OAuthLogin(authorizationToken)
	assert.NoError(t, err, "there should be no error")

}

func TestOAuthLoginWhenErrorOnQueryGetByEmail(t *testing.T) {
	authorizationToken := "some codes"
	returnOAuthToken := &oauth2.Token{AccessToken: "access_token"}
	name := "yusuf sholeh"
	email := "yusuf_sholeh@gmail.com"

	mockedConfiguration := new(mockConfiguration.IGoogleOAuthConfiguration)
	mockedConfiguration.On("ExchangeToken", authorizationToken, google.Endpoint).Return(returnOAuthToken, nil)
	rr := httptest.NewRecorder()
	rr.Write([]byte(`{"email":"` + email + `",` + `"name":"` + name + `"}`))
	response := rr.Result()
	mockedConfiguration.On("GetUserProfile", getAccessTokenURL, returnOAuthToken.AccessToken).Return(response, nil)
	mockedCustomerRepository := new(mockRepository.ICustomerRepository)
	mockedCustomerRepository.On("GetByEmail", "yusuf_sholeh@gmail.com").Return(nil, errors.New("there was some error"))
	googleOAuthService := GoogleOAuthService{Config: mockedConfiguration, CustomerRepository: mockedCustomerRepository}
	_, err := googleOAuthService.OAuthLogin(authorizationToken)
	assert.Error(t, err, "there should be no error")
}

func TestOAuthLoginWhenUserAlreadyExistsWhenNoErrorOnQueryReturnSuccess(t *testing.T) {
	authorizationToken := "some codes"
	returnOAuthToken := &oauth2.Token{AccessToken: "access_token"}
	name := "yusuf sholeh"
	email := "yusuf_sholeh@gmail.com"
	customerID := uint(1)

	mockedConfiguration := new(mockConfiguration.IGoogleOAuthConfiguration)
	mockedConfiguration.On("ExchangeToken", mock.Anything, mock.Anything).Return(returnOAuthToken, nil)
	rr := httptest.NewRecorder()
	rr.Write([]byte(`{"email":"` + email + `",` + `"name":"` + name + `"}`))
	response := rr.Result()
	mockedConfiguration.On("GetUserProfile", getAccessTokenURL, returnOAuthToken.AccessToken).Return(response, nil)
	mockedCustomerRepository := new(mockRepository.ICustomerRepository)
	mockedCustomerRepository.On("GetByEmail", "yusuf_sholeh@gmail.com").Return(&models.Customer{Model: gorm.Model{ID: customerID}, LoginType: middleware.NormalLoginType}, nil)
	mockedTokenCreation := new(mockToken.ITokenCreation)
	mockedTokenCreation.On("CreateToken", customerID, middleware.CustomerUserType, middleware.NormalLoginType).Return(nil)
	googleOAuthService := GoogleOAuthService{Config: mockedConfiguration, CustomerRepository: mockedCustomerRepository, TokenCreation: mockedTokenCreation}
	_, err := googleOAuthService.OAuthLogin(authorizationToken)
	assert.NoError(t, err, "there should be no error")
}
