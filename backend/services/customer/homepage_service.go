package customerservices

type HomepageService struct {
}

type IHomepageService interface {
	GetHomepageInfoDetail() []HomepageInfoDetail
}

type HomepageInfoDetail struct {
	Title string `json:"title,omitempty"`
	Body  string `json:"body,omitempty"`
}

const (
	whatIsRecycle    = "Yuk-recycle adalah aplikasi yang membantu Indonesia untuk Go-Green"
	whyItMatters     = "Kebersihan lingkungan perlu kita jaga, Yuk-Recycle menghadirkan aplikasi bisnis untuk membantu mitra mendapatkan sampah-sampah dari rumah warga"
	benefitCustomers = "Pengguna aplikasi atau Customer dapat membantu Indonesia Go-Green sekaligus mendapatkan profit dari sampah yang sudah tidak digunakan lagi"
)

func (r HomepageService) GetHomepageInfoDetail() []HomepageInfoDetail {

	detailList := []HomepageInfoDetail{
		HomepageInfoDetail{
			Title: "What Is Recycle",
			Body:  whatIsRecycle,
		},
		HomepageInfoDetail{
			Title: "Why It Matters",
			Body:  whyItMatters,
		},
		HomepageInfoDetail{
			Title: "What Benefits For Customers",
			Body:  benefitCustomers,
		},
	}

	return detailList
}
