package customerservices

import (
	mockRepositories "PPLA4/backend/mocks/repositories"
	mockMakeOrderCheckInterval "PPLA4/backend/mocks/services/customer/make_order_database_checking_interval"
	mockNotification "PPLA4/backend/mocks/services/notification"
	"PPLA4/backend/models"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"
	"testing"
	"time"

	"github.com/NaySoftware/go-fcm"

	"github.com/stretchr/testify/mock"

	"github.com/jinzhu/gorm"

	"github.com/stretchr/testify/assert"
)

func TestMakeOrderWhenOrderSaveErrorReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(nil, errors.New("got some errors"))

	makeOrderService := MakeOrderServices{
		OrderRepository:    mockedOrderRepository,
		CustomerRepository: mockedCustomerRepository,
	}

	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New("got some errors"), err)
}
func TestMakeOrderWhenGetAllByLocationErrorReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(1.3)
	customerLon := float64(50.9)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(nil, errors.New("got some errors"))
	makeOrderService := MakeOrderServices{
		MitraLocationRepository:      mockedMitraLocationRepository,
		CustomerRepository:           mockedCustomerRepository,
		OrderRepository:              mockedOrderRepository,
		OrderStatusHistoryRepository: mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New("got some errors"), err)
}

func TestMakeOrderWhenGetAllByLocationErrorNoRecordFoundReturnErrorServiceNotAvailable(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(1.3)
	customerLon := float64(50.9)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(nil, gorm.ErrRecordNotFound)
	makeOrderService := MakeOrderServices{
		MitraLocationRepository:      mockedMitraLocationRepository,
		CustomerRepository:           mockedCustomerRepository,
		OrderStatusHistoryRepository: mockedOrderStatusHistoryRepository,
		OrderRepository:              mockedOrderRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New(NoServicesAvailable), err)
}

func TestMakeOrderWhenGetAllByLocationNoMitraReturnedReturnErrorServiceNotAvailable(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(1.3)
	customerLon := float64(50.9)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return([]models.MitraLocation{}, nil)
	makeOrderService := MakeOrderServices{
		MitraLocationRepository:      mockedMitraLocationRepository,
		CustomerRepository:           mockedCustomerRepository,
		OrderRepository:              mockedOrderRepository,
		OrderStatusHistoryRepository: mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New(NoServicesAvailable), err)
}

func TestMakeOrderWhenGetMitraOnMitraPickUpDistanceErrorReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraID2 := uint(40)
	mitraLat2 := float64(4)
	mitraLon2 := float64(4)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
		models.MitraLocation{
			Model:   gorm.Model{ID: 2},
			MitraID: mitraID2,
			Lat:     mitraLat2,
			Lon:     mitraLon2,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(nil, errors.New("got some errors"))

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
	}

	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New("got some errors"), err)
}

func TestMakeOrderWhenGetMitraOnMitraGarbageSettingsRepositoryErrorReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(6000000)
	mitraID2 := uint(40)
	mitraLat2 := float64(4)
	mitraLon2 := float64(4)
	// mitraMaximumDistance2 := uint(4000000)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
		models.MitraLocation{
			Model:   gorm.Model{ID: 2},
			MitraID: mitraID2,
			Lat:     mitraLat2,
			Lon:     mitraLon2,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(nil, errors.New("got some errors"))

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New("got some errors"), err)
}

func TestMakeOrderWhenGetMitraOnMitraRepositoryErrorReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(6000000)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	returnedMitraGarbageSettings := []models.MitraGarbageSetting{
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID1,
			MinimumQuantity: quantity1 - 1,
			Price:           MinimumPrice + 1,
		},
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID2,
			MinimumQuantity: quantity2 - 1,
			Price:           MinimumPrice + 1,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(returnedMitraGarbageSettings, nil)

	mockedMitraRepository := new(mockRepositories.IMitraRepository)
	mockedMitraRepository.On("GetByID", uint(60)).Return(nil, errors.New("got some errors"))

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		MitraRepository:               mockedMitraRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New("got some errors"), err)
}

func TestMakeOrderWhenNoMitraFoundInCoveredDistanceReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(0)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	returnedMitraGarbageSettings := []models.MitraGarbageSetting{
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID1,
			MinimumQuantity: quantity1 - 1,
			Price:           MinimumPrice + 1,
		},
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID2,
			MinimumQuantity: quantity2 - 1,
			Price:           MinimumPrice + 1,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(returnedMitraGarbageSettings, nil)

	mockedMitraRepository := new(mockRepositories.IMitraRepository)
	mockedMitraRepository.On("GetByID", uint(60)).Return(nil, errors.New(NoMitraTakeTheOrder))

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		MitraRepository:               mockedMitraRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New(NoServicesAvailable), err)
}

func TestMakeOrderWhenNoMitraTakeTheOrderReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(6000000)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	returnedMitraGarbageSettings := []models.MitraGarbageSetting{
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID1,
			MinimumQuantity: quantity1 + 1,
		},
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID2,
			MinimumQuantity: quantity2 + 1,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(returnedMitraGarbageSettings, nil)

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New(NoMitraTakeTheOrder), err)
}

func TestMakeOrderWhenSaveOrderAttemptError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(6000000)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	returnedMitraGarbageSettings := []models.MitraGarbageSetting{
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID1,
			MinimumQuantity: quantity1 - 1,
			Price:           MinimumPrice + 1,
		},
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID2,
			MinimumQuantity: quantity2 - 1,
			Price:           MinimumPrice + 1,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(returnedMitraGarbageSettings, nil)

	mockedMitraRepository := new(mockRepositories.IMitraRepository)
	mockedMitraRepository.On("GetByID", uint(60)).Return(&models.Mitra{Model: gorm.Model{ID: mitraID1}}, nil)

	mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
	mockedOrderAttemptRepository.On("Save", mock.Anything).Return(nil, errors.New("got some errors"))

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		MitraRepository:               mockedMitraRepository,
		OrderAttemptRepository:        mockedOrderAttemptRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New("got some errors"), err)
}

func TestMakeOrderWhenSendNotificationErrorReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(6000000)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	returnedMitraGarbageSettings := []models.MitraGarbageSetting{
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID1,
			MinimumQuantity: quantity1 - 1,
			Price:           MinimumPrice + 1,
		},
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID2,
			MinimumQuantity: quantity2 - 1,
			Price:           MinimumPrice + 1,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", &models.Order{
		CustomerID:   customerID,
		CustomerLat:  customerLat,
		CustomerLon:  customerLon,
		LatestStatus: orderConstant.InitOrder,
	}).Return(&models.Order{CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(returnedMitraGarbageSettings, nil)

	mockedMitraRepository := new(mockRepositories.IMitraRepository)
	mockedMitraRepository.On("GetByID", uint(60)).Return(&models.Mitra{Model: gorm.Model{ID: mitraID1}}, nil)

	mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
	mockedOrderAttemptRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedNotificationServices := new(mockNotification.INotificationService)
	mockedNotificationServices.On("SendNotification", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("got some errors"))

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		MitraRepository:               mockedMitraRepository,
		OrderAttemptRepository:        mockedOrderAttemptRepository,
		NotificationServices:          mockedNotificationServices,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
	}
	mitra, _, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Nil(mitra)
	assert.Equal(errors.New(NoMitraTakeTheOrder), err)
}

func TestMakeOrderWhenSendNotificationSuccessAndOrderScheduledWithDate(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(6000000)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	returnedMitraGarbageSettings := []models.MitraGarbageSetting{
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID1,
			MinimumQuantity: quantity1 - 1,
			Price:           MinimumPrice + 1,
		},
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID2,
			MinimumQuantity: quantity2 - 1,
			Price:           MinimumPrice + 1,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", mock.Anything).Return(&models.Order{Model: gorm.Model{ID: uint(8888)}, CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)
	mockedOrderRepository.On("Save", mock.Anything).Return(&models.Order{Model: gorm.Model{ID: uint(8888)}, CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(returnedMitraGarbageSettings, nil)

	mockedMitraRepository := new(mockRepositories.IMitraRepository)
	mockedMitraRepository.On("GetByID", uint(60)).Return(&models.Mitra{Model: gorm.Model{ID: mitraID1}}, nil)

	mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
	mockedOrderAttemptRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedNotificationServices := new(mockNotification.INotificationService)
	mockedNotificationServices.On("SendNotification", mock.Anything, mock.Anything, mock.Anything).Return(&fcm.FcmResponseStatus{Fail: 0}, nil)

	mockedMakeOrderCheckInterval := new(mockMakeOrderCheckInterval.ICheckingInterval)
	mockedMakeOrderCheckInterval.On("CheckDatabase", uint(8888), mitraID1, 30).Return(uint(2), nil)

	mockedOrderDetailRepository := new(mockRepositories.IOrderDetailRepository)
	mockedOrderDetailRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedMitraPickUpScheduledRepository := new(mockRepositories.IMitraPickUpScheduleRepository)
	mockedMitraPickUpScheduledRepository.On("GetByMitraId", mitraID1).Return(
		&models.MitraPickUpSchedule{
			Model:     gorm.Model{ID: mitraID1},
			MitraID:   mitraID1,
			Sunday:    false,
			Monday:    true,
			Tuesday:   false,
			Wednesday: false,
			Thursday:  false,
			Friday:    false,
			Saturday:  true,
		},
		nil,
	)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		MitraRepository:               mockedMitraRepository,
		OrderAttemptRepository:        mockedOrderAttemptRepository,
		NotificationServices:          mockedNotificationServices,
		MakeOrderCheckInterval:        mockedMakeOrderCheckInterval,
		OrderDetailRepository:         mockedOrderDetailRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
		MitraPickUpScheduleRepository: mockedMitraPickUpScheduledRepository,
	}

	timer := time.Now()
	mitra, order, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Equal(mitraID1, mitra.ID)
	assert.Equal(nil, err)
	assert.Equal(true, timer.Before(order.ScheduledAt))
}

func TestMakeOrderWhenSendNotificationSuccessAndErrorOnScheduled(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	customerPreferenceDistance := float64(1e9)

	makeOrderRequest := MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []MakeOrderDetailRequest{
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mitraID1 := uint(60)
	mitraLat1 := float64(6)
	mitraLon1 := float64(6)
	mitraMaximumDistance1 := uint(6000000)

	returnedMitraLocations := []models.MitraLocation{
		models.MitraLocation{
			Model:   gorm.Model{ID: 1},
			MitraID: mitraID1,
			Lat:     mitraLat1,
			Lon:     mitraLon1,
		},
	}

	returnedMitraDistance := &models.MitraPickUpDistance{
		MitraID:         mitraID1,
		MaximumDistance: mitraMaximumDistance1,
	}

	returnedMitraGarbageSettings := []models.MitraGarbageSetting{
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID1,
			MinimumQuantity: quantity1 - 1,
			Price:           MinimumPrice + 1,
		},
		models.MitraGarbageSetting{
			MitraID:         uint(mitraID1),
			GarbageTypeID:   garbageTypeID2,
			MinimumQuantity: quantity2 - 1,
			Price:           MinimumPrice + 1,
		},
	}

	mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
	mockedMitraLocationRepository.On("GetAllByLocationAndDistance", customerLat, customerLon, customerPreferenceDistance).Return(returnedMitraLocations, nil)

	mockedOrderRepository := new(mockRepositories.IOrderRepository)
	mockedOrderRepository.On("Save", mock.Anything).Return(&models.Order{Model: gorm.Model{ID: uint(8888)}, CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)
	mockedOrderRepository.On("Save", mock.Anything).Return(&models.Order{Model: gorm.Model{ID: uint(8888)}, CustomerID: customerID, CustomerLat: customerLat, CustomerLon: customerLon, LatestStatus: 1},
		nil,
	)

	mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
	mockedMitraPickUpDistanceRepository.On("GetByMitraId", uint(60)).Return(returnedMitraDistance, nil)

	mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
	mockedCustomerRepository.On("GetByID", customerID).Return(
		&models.Customer{
			Model: gorm.Model{ID: customerID},
			Name:  "muhammad yusuf sholeh",
		},
		nil,
	)

	mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
	mockedMitraGarbageSettingRepository.On("GetByMitraId", uint(60)).Return(returnedMitraGarbageSettings, nil)

	mockedMitraRepository := new(mockRepositories.IMitraRepository)
	mockedMitraRepository.On("GetByID", uint(60)).Return(&models.Mitra{Model: gorm.Model{ID: mitraID1}}, nil)

	mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
	mockedOrderAttemptRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedNotificationServices := new(mockNotification.INotificationService)
	mockedNotificationServices.On("SendNotification", mock.Anything, mock.Anything, mock.Anything).Return(&fcm.FcmResponseStatus{Fail: 0}, nil)

	mockedMakeOrderCheckInterval := new(mockMakeOrderCheckInterval.ICheckingInterval)
	mockedMakeOrderCheckInterval.On("CheckDatabase", uint(8888), mitraID1, 30).Return(uint(2), nil)

	mockedOrderDetailRepository := new(mockRepositories.IOrderDetailRepository)
	mockedOrderDetailRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
	mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

	mockedMitraPickUpScheduledRepository := new(mockRepositories.IMitraPickUpScheduleRepository)
	mockedMitraPickUpScheduledRepository.On("GetByMitraId", mitraID1).Return(
		&models.MitraPickUpSchedule{
			Model:     gorm.Model{ID: mitraID1},
			MitraID:   mitraID1,
			Sunday:    false,
			Monday:    false,
			Tuesday:   false,
			Wednesday: false,
			Thursday:  false,
			Friday:    false,
			Saturday:  false,
		},
		nil,
	)

	makeOrderService := MakeOrderServices{
		MitraLocationRepository:       mockedMitraLocationRepository,
		OrderRepository:               mockedOrderRepository,
		MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
		CustomerRepository:            mockedCustomerRepository,
		MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		MitraRepository:               mockedMitraRepository,
		OrderAttemptRepository:        mockedOrderAttemptRepository,
		NotificationServices:          mockedNotificationServices,
		MakeOrderCheckInterval:        mockedMakeOrderCheckInterval,
		OrderDetailRepository:         mockedOrderDetailRepository,
		OrderStatusHistoryRepository:  mockedOrderStatusHistoryRepository,
		MitraPickUpScheduleRepository: mockedMitraPickUpScheduledRepository,
	}

	timer := time.Now()
	mitra, order, _, err := makeOrderService.MakeOrder(&makeOrderRequest)
	assert := assert.New(t)
	assert.Equal(mitraID1, mitra.ID)
	assert.Equal(nil, err)
	assert.Equal(false, timer.Before(order.ScheduledAt))
}
