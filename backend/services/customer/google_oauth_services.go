package customerservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"encoding/json"
	"fmt"
	"io/ioutil"

	configuration "PPLA4/backend/services/customer/oauth_configuration"

	"github.com/jinzhu/gorm"
	"golang.org/x/oauth2/google"
)

const (
	getAccessTokenURL = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="
)

// GoogleOAuthService is a service configuration for endpoint
type GoogleOAuthService struct {
	CustomerRepository repositories.ICustomerRepository
	Config             configuration.IGoogleOAuthConfiguration
	TokenCreation      middleware.ITokenCreation
}

// IGoogleOAuthService configuration dependencies
type IGoogleOAuthService interface {
	OAuthLogin(authorizationCode string) (*middleware.Token, error)
}

func (g GoogleOAuthService) OAuthLogin(authorizationCode string) (*middleware.Token, error) {
	token, err := g.Config.ExchangeToken(authorizationCode, google.Endpoint)

	if err != nil {
		return nil, err
	}

	response, err := g.Config.GetUserProfile(getAccessTokenURL, token.AccessToken)

	if err != nil {
		return nil, err
	}

	contents, _ := ioutil.ReadAll(response.Body)
	fmt.Printf(string(contents))

	userProfile := new(configuration.UserProfile)
	errDecoding := json.Unmarshal([]byte(contents), userProfile)

	if errDecoding != nil {
		return nil, errDecoding
	}

	// Email doesnt exists, create user
	customer, err := g.CustomerRepository.GetByEmail(userProfile.Email)

	if err == gorm.ErrRecordNotFound {
		customerNew, err := g.CustomerRepository.Add(&models.Customer{Email: userProfile.Email, Name: userProfile.Name, LoginType: middleware.OAuthLoginType})
		if err != nil {
			return nil, err
		}
		return g.TokenCreation.CreateToken(customerNew.ID, middleware.CustomerUserType, middleware.OAuthLoginType), nil
	}

	if err != nil {
		return nil, err
	}

	return g.TokenCreation.CreateToken(customer.ID, middleware.CustomerUserType, customer.LoginType), nil
}
