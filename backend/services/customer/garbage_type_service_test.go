package customerservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"

	mockRepositories "PPLA4/backend/mocks/repositories"
)

func TestGarbageTypeService_GetGarbageTypeList(t *testing.T) {
	tests := []struct {
		description             string
		returnGarbageTypesMock  []models.GarbageType
		returnError             error
		expectedGarbageTypeList []GarbageTypeData
		expectedError           error
	}{
		{
			description: "Fetch garbage type list success",
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah Plastik", Description: "Plastik", QuantityType: "KG", ImageBase64: "image_url"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah Organik", Description: "Organik", QuantityType: "KG", ImageBase64: "image_url"},
			},
			returnError: nil,
			expectedGarbageTypeList: []GarbageTypeData{
				GarbageTypeData{Title: "Sampah Plastik", Description: "Plastik", QuantityType: "KG", Image: "image_url"},
				GarbageTypeData{Title: "Sampah Organik", Description: "Organik", QuantityType: "KG", Image: "image_url"},
			},
			expectedError: nil,
		}, {
			description: "Failed to fetch garbage type list",
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah Plastik", Description: "Plastik", QuantityType: "KG", ImageBase64: "image_url"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah Organik", Description: "Organik", QuantityType: "KG", ImageBase64: "image_url"},
			},
			returnError:             errors.New("Failed to fetch garbage types"),
			expectedGarbageTypeList: nil,
			expectedError:           errors.New("Failed to fetch garbage types"),
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedGarbageTypeRepository := new(mockRepositories.IGarbageTypeRepository)
		mockedGarbageTypeRepository.On("GetAll").Return(tc.returnGarbageTypesMock, tc.returnError)

		garbageTypeService := GarbageTypeService{GarbageTypeRepository: mockedGarbageTypeRepository}

		garbageTypeList, err := garbageTypeService.GetGarbageTypeList()

		assert.Equal(tc.expectedGarbageTypeList, garbageTypeList, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
