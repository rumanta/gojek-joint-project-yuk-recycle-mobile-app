package customerservices

import (
	"PPLA4/backend/repositories"
	"errors"
)

type GarbageTypeService struct {
	GarbageTypeRepository repositories.IGarbageTypeRepository
}

type IGarbageTypeService interface {
	GetGarbageTypeList() ([]GarbageTypeData, error)
}

type GarbageTypeData struct {
	Title        string `json:"title,omitempty"`
	Description  string `json:"description,omitempty"`
	QuantityType string `json:"quantity_type,omitempty"`
	Image        string `json:"image,omitempty"`
}

const (
	fetchError = "Failed to fetch garbage types"
)

func (g GarbageTypeService) GetGarbageTypeList() ([]GarbageTypeData, error) {
	garbageTypes, err := g.GarbageTypeRepository.GetAll()

	if err != nil {
		return nil, errors.New(fetchError)
	}

	var garbageTypeList []GarbageTypeData

	for _, garbageType := range garbageTypes {
		garbageTypeData := GarbageTypeData{
			Title:        garbageType.Title,
			Description:  garbageType.Description,
			QuantityType: garbageType.QuantityType,
			Image:        garbageType.ImageBase64,
		}

		garbageTypeList = append(garbageTypeList, garbageTypeData)
	}

	return garbageTypeList, nil
}
