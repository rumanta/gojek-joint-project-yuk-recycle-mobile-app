package customerservices

type FAQService struct {
}

type IFAQService interface {
	GetFAQDetail() []FAQInfoDetail
}

type FAQInfoDetail struct {
	Title string `json:"title,omitempty"`
	Body  string `json:"body,omitempty"`
}

func (f FAQService) GetFAQDetail() []FAQInfoDetail {

	detailList := []FAQInfoDetail{
		FAQInfoDetail{
			Title: "Apa keuntungan yang saya dapatkan?",
			Body:  "Sampah yang ada di rumah akan berkurang, kamu juga mendapatkan pengetahuan mengenai sampah yang bisa di recycle. Ada insentifnya juga loh.",
		},
		FAQInfoDetail{
			Title: "Bagaimana cara untuk melakukan order?",
			Body:  "Kamu hanya tinggal menekan tombol recycle yang terdapat pada navigasi, lalu ikuti petunjuk yang ada di sana.",
		},
		FAQInfoDetail{
			Title: "Jika ingin membatalkan pesanan bagaimana caranya?",
			Body:  "Setiap order dapat dibatalkan. Lihat detail tiap order dan akan ada tombol cancel yang bisa ditekan dan order akan dibatalkan.",
		},
		FAQInfoDetail{
			Title: "Apakah bisa menambah lebih dari 5 jenis sampah yang tersedia?",
			Body:  "Untuk sekarang, jenis sampah yang tersedia hanya 5 jenis sampah dan belum bisa menambah jenis baru. Penambahan akan bisa dilakukan pada update berikutnya.",
		},
	}

	return detailList
}
