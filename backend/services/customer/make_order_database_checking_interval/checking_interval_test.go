package makeordercheckinginterval

import (
	mockRepositories "PPLA4/backend/mocks/repositories"
	"PPLA4/backend/models"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckDatabaseErrorQueryGetByOrderID(t *testing.T) {
	orderID := uint(30)
	mitraID := uint(30)
	upperBoundTimeInSecond := int(1)

	mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
	mockedOrderAttemptRepository.On("GetByOrderIDAndMitraID", orderID, mitraID).Return(nil, errors.New("got some errors"))

	checkingInterval := CheckingInterval{
		OrderAttemptRepository: mockedOrderAttemptRepository,
	}

	status, err := checkingInterval.CheckDatabase(orderID, mitraID, upperBoundTimeInSecond)

	assert.Equal(t, uint(0), status)
	assert.Error(t, err)
}

func TestCheckDatabaseOrderAccepted(t *testing.T) {
	orderID := uint(30)
	mitraID := uint(30)
	upperBoundTimeInSecond := int(1)

	mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
	mockedOrderAttemptRepository.On("GetByOrderIDAndMitraID", orderID, mitraID).Return(&models.OrderAttempt{
		OrderAttemptStatusID: 2,
	}, nil)

	checkingInterval := CheckingInterval{
		OrderAttemptRepository: mockedOrderAttemptRepository,
	}

	status, err := checkingInterval.CheckDatabase(orderID, mitraID, upperBoundTimeInSecond)

	assert.Equal(t, uint(2), status)
	assert.NoError(t, err)
}

func TestCheckDatabaseWhenNoMitraAcceptTheOrder(t *testing.T) {
	orderID := uint(30)
	mitraID := uint(30)
	upperBoundTimeInSecond := int(1)

	mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
	mockedOrderAttemptRepository.On("GetByOrderIDAndMitraID", orderID, mitraID).Return(&models.OrderAttempt{
		OrderAttemptStatusID: 8,
	}, nil)

	checkingInterval := CheckingInterval{
		OrderAttemptRepository: mockedOrderAttemptRepository,
	}

	status, err := checkingInterval.CheckDatabase(orderID, mitraID, upperBoundTimeInSecond)

	assert.Equal(t, uint(9), status)
	assert.NoError(t, err)
}
