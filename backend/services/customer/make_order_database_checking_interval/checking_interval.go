package makeordercheckinginterval

import (
	"PPLA4/backend/repositories"
	"time"
)

type CheckingInterval struct {
	OrderAttemptRepository repositories.IOrderAttemptRepository
}

type ICheckingInterval interface {
	CheckDatabase(orderID uint, mitraID uint, upperBoundTimeInSecond int) (uint, error)
}

/*
	0 error occured
	1 not yet decided
	2 accepted
	3 rejected
	9 time out
*/

const (
	ErrorOccured  = 0
	NotYetDecided = 1
	Accepted      = 2
	Rejected      = 3
	TimeOut       = 9
)

func (c CheckingInterval) CheckDatabase(orderID uint, mitraID uint, upperBoundTimeInSecond int) (uint, error) {
	now := time.Now()
	after := now.Add(time.Second * time.Duration(upperBoundTimeInSecond))
	for {
		time.Sleep(500 * time.Millisecond)

		res, err := c.OrderAttemptRepository.GetByOrderIDAndMitraID(orderID, mitraID)
		if err != nil {
			return ErrorOccured, err
		}

		if res.OrderAttemptStatusID == Accepted || res.OrderAttemptStatusID == Rejected {
			return res.OrderAttemptStatusID, nil
		}

		now = time.Now()
		if now.After(after) {
			break
		}
	}
	return TimeOut, nil
}
