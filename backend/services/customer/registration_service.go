package customerservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"errors"

	"github.com/jinzhu/gorm"
)

const (
	phoneAlreadyExists = "phone already exists"
	emailAlreadyExists = "email already exists"
)

type RegistrationService struct {
	CustomerRepository repositories.ICustomerRepository
}

type IRegistrationService interface {
	Register(phone, password, name, email string) (*models.Customer, error)
}

func (r RegistrationService) Register(phone, password, name, email string) (*models.Customer, error) {
	if _, err := r.CustomerRepository.GetByEmail(email); err != gorm.ErrRecordNotFound { // email exists
		return nil, errors.New(emailAlreadyExists)
	}

	if _, err := r.CustomerRepository.GetByPhoneNumber(phone); err != gorm.ErrRecordNotFound { // phone exists
		return nil, errors.New(phoneAlreadyExists)
	}

	customer, err := r.CustomerRepository.Add(&models.Customer{Phone: phone, Password: password, Name: name, Email: email, LoginType: middleware.NormalLoginType})

	if err != nil {
		return nil, err
	}

	customer.Password = ""
	return customer, nil
}
