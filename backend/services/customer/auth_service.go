package customerservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/repositories"
	"errors"

	"github.com/jinzhu/gorm"

	"golang.org/x/crypto/bcrypt"
)

const (
	UsernameAndPasswordDoesntMatch = "username and password doesn't match"
)

type AuthService struct {
	CustomerRepository repositories.ICustomerRepository
	TokenCreation      middleware.ITokenCreation
}

type IAuthService interface {
	Login(email, password string) (*middleware.Token, error)
}

func (a AuthService) Login(email, password string) (*middleware.Token, error) {
	customer, err := a.CustomerRepository.GetByEmail(email)

	if err != nil {
		if err == gorm.ErrRecordNotFound { // username not found
			return nil, errors.New(UsernameAndPasswordDoesntMatch)
		}
		return nil, errors.New(UsernameAndPasswordDoesntMatch) // connection error
	}

	errCmp := bcrypt.CompareHashAndPassword([]byte(customer.Password), []byte(password))

	if errCmp != nil { //Password does not match!, todo: specify error
		return nil, errors.New(UsernameAndPasswordDoesntMatch)
	}

	return a.TokenCreation.CreateToken(customer.ID, middleware.CustomerUserType, middleware.NormalLoginType), nil
}
