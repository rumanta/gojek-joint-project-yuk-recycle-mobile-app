package orderconstant

// order status
const (
	InitOrder            = 1
	AcceptedAndScheduled = 2
	PickedUp             = 3
	Completed            = 4
	CancelledByMitra     = 5
	CancelledByCustomer  = 6
	NoServiceAvailable   = 7
	NoMitraTakeTheOrder  = 8
)

// order status error message
const (
	InvalidOrderStatusTransition = "Invalid order status transition"
	InvalidCorrespondingMitra    = "Invalid corresponding mitra"
	InvalidCorrespondingCustomer = "Invalid corresponding customer"
	UnmatchedCurrentOrderStatus  = "Unmatched current order status"
)

// IsPrecededStatus every order status has preceded status
var IsPrecededStatus = map[int]map[int]bool{
	PickedUp: map[int]bool{
		InitOrder:            false,
		AcceptedAndScheduled: true,
		PickedUp:             false,
		Completed:            false,
		CancelledByMitra:     false,
		NoServiceAvailable:   false,
		NoMitraTakeTheOrder:  false,
		CancelledByCustomer:  false,
	},
	Completed: map[int]bool{
		InitOrder:            false,
		AcceptedAndScheduled: false,
		PickedUp:             true,
		Completed:            false,
		CancelledByMitra:     false,
		NoServiceAvailable:   false,
		NoMitraTakeTheOrder:  false,
		CancelledByCustomer:  false,
	},
	CancelledByMitra: map[int]bool{
		InitOrder:            false,
		AcceptedAndScheduled: true,
		PickedUp:             true,
		Completed:            false,
		CancelledByMitra:     false,
		NoServiceAvailable:   false,
		NoMitraTakeTheOrder:  false,
		CancelledByCustomer:  false,
	},
	CancelledByCustomer: map[int]bool{
		InitOrder:            false,
		AcceptedAndScheduled: true,
		PickedUp:             true,
		Completed:            false,
		CancelledByMitra:     false,
		NoServiceAvailable:   false,
		NoMitraTakeTheOrder:  false,
		CancelledByCustomer:  false,
	},
}
