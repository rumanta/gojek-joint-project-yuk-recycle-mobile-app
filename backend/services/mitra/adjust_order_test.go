package mitraservices

import (
	mockRepositories "PPLA4/backend/mocks/repositories"
	"PPLA4/backend/models"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"
	"testing"

	"github.com/jinzhu/gorm"

	"github.com/stretchr/testify/mock"

	"github.com/stretchr/testify/assert"
)

func TestAdjustOrder(t *testing.T) {
	orderIDSample := uint(30)
	mitraIDSample := uint(50)
	gotSomeErrorSample := errors.New("got some errors")
	tests := []struct {
		description               string
		orderID                   uint
		mitraID                   uint
		adjustOrderRequest        *AdjustOrderRequest
		returnOrderOnOrderGetByID *models.Order
		returnErrorOnOrderGetByID error
		returnOrderDetailOnGet    []models.OrderDetail
		returnErrorDetailOnGet    error
		returnMitraSettingsOnGet  []models.MitraGarbageSetting
		returnErrorMitraSettings  error
		expectedCallOnInsert      []*models.OrderDetail
		expectedCallOnDelete      []uint
		expectedCallOnUpdate      []*models.OrderDetail
		expectedError             error
	}{
		{
			description:               "record orderID not found",
			orderID:                   orderIDSample,
			mitraID:                   mitraIDSample,
			returnErrorOnOrderGetByID: gotSomeErrorSample,
			expectedError:             errors.New(BackendError),
		}, {
			description: "unmatched order id with mitra id",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			returnOrderOnOrderGetByID: &models.Order{
				MitraID: mitraIDSample - 1,
			},
			expectedError: errors.New(orderConstant.InvalidCorrespondingMitra),
		}, {
			description: "latest status isn't picked up",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			returnOrderOnOrderGetByID: &models.Order{
				MitraID:      mitraIDSample,
				LatestStatus: orderConstant.Completed,
			},
			expectedError: errors.New(orderConstant.UnmatchedCurrentOrderStatus),
		}, {
			description: "orderDetails get database connection error",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			returnOrderOnOrderGetByID: &models.Order{
				MitraID:      mitraIDSample,
				LatestStatus: orderConstant.PickedUp,
			},
			returnErrorDetailOnGet: gotSomeErrorSample,
			expectedError:          errors.New(BackendError),
		}, {
			description: "mitraSettings get database connection error",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			returnOrderOnOrderGetByID: &models.Order{
				MitraID:      mitraIDSample,
				LatestStatus: orderConstant.PickedUp,
			},
			returnOrderDetailOnGet:   []models.OrderDetail{},
			returnErrorMitraSettings: gotSomeErrorSample,
			expectedError:            errors.New(BackendError),
		}, {
			description: "all insert",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			adjustOrderRequest: &AdjustOrderRequest{
				OrderDetail: []OrderDetailRequest{
					OrderDetailRequest{
						GarbageTypeID: 4,
						Quantity:      4,
					},
					OrderDetailRequest{
						GarbageTypeID: 5,
						Quantity:      5,
					},
				},
			},
			returnOrderOnOrderGetByID: &models.Order{
				MitraID:      mitraIDSample,
				LatestStatus: orderConstant.PickedUp,
			},
			returnOrderDetailOnGet: []models.OrderDetail{
				models.OrderDetail{
					Model:         gorm.Model{ID: 101},
					OrderID:       orderIDSample,
					GarbageTypeID: 1,
					TotalAmount:   5000,
					Quantity:      10,
				},
				models.OrderDetail{
					Model:         gorm.Model{ID: 102},
					OrderID:       orderIDSample,
					GarbageTypeID: 2,
					TotalAmount:   10000,
					Quantity:      10,
				},
				models.OrderDetail{
					Model:         gorm.Model{ID: 103},
					OrderID:       orderIDSample,
					GarbageTypeID: 3,
					TotalAmount:   15000,
					Quantity:      10,
				},
			},
			returnMitraSettingsOnGet: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{
					GarbageTypeID: 1,
					Price:         500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 2,
					Price:         1000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 3,
					Price:         1500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 4,
					Price:         2000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 5,
					Price:         2500,
				},
			},
			expectedCallOnInsert: []*models.OrderDetail{
				&models.OrderDetail{
					Model:         gorm.Model{ID: 0},
					OrderID:       orderIDSample,
					GarbageTypeID: 4,
					TotalAmount:   8000,
					Quantity:      4,
				},
				&models.OrderDetail{
					Model:         gorm.Model{ID: 0},
					OrderID:       orderIDSample,
					GarbageTypeID: 5,
					TotalAmount:   12500,
					Quantity:      5,
				},
			},
		}, {
			description: "all remove",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			adjustOrderRequest: &AdjustOrderRequest{
				OrderDetail: []OrderDetailRequest{
					OrderDetailRequest{
						GarbageTypeID: 4,
						Quantity:      0,
					},
					OrderDetailRequest{
						GarbageTypeID: 5,
						Quantity:      0,
					},
				},
			},
			returnOrderOnOrderGetByID: &models.Order{
				MitraID:      mitraIDSample,
				LatestStatus: orderConstant.PickedUp,
			},
			returnOrderDetailOnGet: []models.OrderDetail{
				models.OrderDetail{
					Model:         gorm.Model{ID: 102},
					OrderID:       orderIDSample,
					GarbageTypeID: 4,
					TotalAmount:   20000,
					Quantity:      10,
				},
				models.OrderDetail{
					Model:         gorm.Model{ID: 103},
					OrderID:       orderIDSample,
					GarbageTypeID: 5,
					TotalAmount:   25000,
					Quantity:      10,
				},
			},
			returnMitraSettingsOnGet: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{
					GarbageTypeID: 1,
					Price:         500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 2,
					Price:         1000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 3,
					Price:         1500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 4,
					Price:         2000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 5,
					Price:         2500,
				},
			},
			expectedCallOnDelete: []uint{
				102, 103,
			},
		}, {
			description: "all update",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			adjustOrderRequest: &AdjustOrderRequest{
				OrderDetail: []OrderDetailRequest{
					OrderDetailRequest{
						GarbageTypeID: 1,
						Quantity:      5,
					},
					OrderDetailRequest{
						GarbageTypeID: 2,
						Quantity:      6,
					},
					OrderDetailRequest{
						GarbageTypeID: 3,
						Quantity:      4,
					},
				},
			},
			returnOrderOnOrderGetByID: &models.Order{
				MitraID:      mitraIDSample,
				LatestStatus: orderConstant.PickedUp,
			},
			returnOrderDetailOnGet: []models.OrderDetail{
				models.OrderDetail{
					Model:         gorm.Model{ID: 101},
					OrderID:       orderIDSample,
					GarbageTypeID: 1,
					TotalAmount:   5000,
					Quantity:      10,
				},
				models.OrderDetail{
					Model:         gorm.Model{ID: 102},
					OrderID:       orderIDSample,
					GarbageTypeID: 2,
					TotalAmount:   10000,
					Quantity:      10,
				},
				models.OrderDetail{
					Model:         gorm.Model{ID: 103},
					OrderID:       orderIDSample,
					GarbageTypeID: 3,
					TotalAmount:   15000,
					Quantity:      10,
				},
			},
			returnMitraSettingsOnGet: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{
					GarbageTypeID: 1,
					Price:         500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 2,
					Price:         1000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 3,
					Price:         1500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 4,
					Price:         2000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 5,
					Price:         2500,
				},
			},
			expectedCallOnUpdate: []*models.OrderDetail{
				&models.OrderDetail{
					Model:         gorm.Model{ID: 101},
					OrderID:       orderIDSample,
					GarbageTypeID: 1,
					TotalAmount:   2500,
					Quantity:      5,
				},
				&models.OrderDetail{
					Model:         gorm.Model{ID: 102},
					OrderID:       orderIDSample,
					GarbageTypeID: 2,
					TotalAmount:   6000,
					Quantity:      6,
				},
				&models.OrderDetail{
					Model:         gorm.Model{ID: 103},
					OrderID:       orderIDSample,
					GarbageTypeID: 3,
					TotalAmount:   6000,
					Quantity:      4,
				},
			},
		}, {
			description: "combination insert (1, 3), update(2), remove(4, 5)",
			orderID:     orderIDSample,
			mitraID:     mitraIDSample,
			adjustOrderRequest: &AdjustOrderRequest{
				OrderDetail: []OrderDetailRequest{
					OrderDetailRequest{
						GarbageTypeID: 1,
						Quantity:      5,
					},
					OrderDetailRequest{
						GarbageTypeID: 2,
						Quantity:      7,
					},
					OrderDetailRequest{
						GarbageTypeID: 3,
						Quantity:      9,
					},
					OrderDetailRequest{
						GarbageTypeID: 4,
						Quantity:      0,
					},
					OrderDetailRequest{
						GarbageTypeID: 5,
						Quantity:      0,
					},
				},
			},
			returnOrderOnOrderGetByID: &models.Order{
				MitraID:      mitraIDSample,
				LatestStatus: orderConstant.PickedUp,
			},
			returnOrderDetailOnGet: []models.OrderDetail{
				models.OrderDetail{
					Model:         gorm.Model{ID: 102},
					OrderID:       orderIDSample,
					GarbageTypeID: 2,
					TotalAmount:   10000,
					Quantity:      10,
				},
				models.OrderDetail{
					Model:         gorm.Model{ID: 104},
					OrderID:       orderIDSample,
					GarbageTypeID: 4,
					TotalAmount:   20000,
					Quantity:      10,
				},
				models.OrderDetail{
					Model:         gorm.Model{ID: 105},
					OrderID:       orderIDSample,
					GarbageTypeID: 5,
					TotalAmount:   25000,
					Quantity:      10,
				},
			},
			returnMitraSettingsOnGet: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{
					GarbageTypeID: 1,
					Price:         500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 2,
					Price:         1000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 3,
					Price:         1500,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 4,
					Price:         2000,
				},
				models.MitraGarbageSetting{
					GarbageTypeID: 5,
					Price:         2500,
				},
			},
			expectedCallOnInsert: []*models.OrderDetail{
				&models.OrderDetail{
					Model:         gorm.Model{ID: 0},
					OrderID:       orderIDSample,
					GarbageTypeID: 1,
					TotalAmount:   2500,
					Quantity:      5,
				},
				&models.OrderDetail{
					Model:         gorm.Model{ID: 0},
					OrderID:       orderIDSample,
					GarbageTypeID: 3,
					TotalAmount:   13500,
					Quantity:      9,
				},
			},
			expectedCallOnDelete: []uint{
				104, 105,
			},
			expectedCallOnUpdate: []*models.OrderDetail{
				&models.OrderDetail{
					Model:         gorm.Model{ID: 102},
					OrderID:       orderIDSample,
					GarbageTypeID: 2,
					TotalAmount:   7000,
					Quantity:      7,
				},
			},
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetByID", orderIDSample).Return(tc.returnOrderOnOrderGetByID, tc.returnErrorOnOrderGetByID)

		mockedOrderDetailRepository := new(mockRepositories.IOrderDetailRepository)
		mockedOrderDetailRepository.On("GetByOrderID", orderIDSample).Return(tc.returnOrderDetailOnGet, tc.returnErrorDetailOnGet)

		mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
		mockedMitraGarbageSettingRepository.On("GetByMitraId", mitraIDSample).Return(tc.returnMitraSettingsOnGet, tc.returnErrorMitraSettings)

		adjustOrderService := AdjustOrderService{
			OrderRepository:               mockedOrderRepository,
			OrderDetailRepository:         mockedOrderDetailRepository,
			MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
		}

		for _, expectedCallOnInsert := range tc.expectedCallOnInsert {
			copied := expectedCallOnInsert
			mockedOrderDetailRepository.On("Save", mock.MatchedBy(
				func(m *models.OrderDetail) bool {
					return m.Model.ID == copied.Model.ID &&
						m.OrderID == copied.OrderID &&
						m.GarbageTypeID == copied.GarbageTypeID &&
						m.TotalAmount == copied.TotalAmount &&
						m.Quantity == copied.Quantity
				},
			)).Return(nil, nil)
		}

		for _, expectedCallOnDelete := range tc.expectedCallOnDelete {
			mockedOrderDetailRepository.On("DeleteByID", expectedCallOnDelete).Return(nil)
		}

		for _, expectedCallOnUpdate := range tc.expectedCallOnUpdate {
			copied := expectedCallOnUpdate
			mockedOrderDetailRepository.On("Save", mock.MatchedBy(
				func(m *models.OrderDetail) bool {
					return m.Model.ID == copied.Model.ID &&
						m.OrderID == copied.OrderID &&
						m.GarbageTypeID == copied.GarbageTypeID &&
						m.TotalAmount == copied.TotalAmount &&
						m.Quantity == copied.Quantity
				},
			)).Return(nil, nil)
		}

		err := adjustOrderService.AdjustOrder(tc.orderID, tc.mitraID, tc.adjustOrderRequest)
		assert.Equal(tc.expectedError, err)
	}
}
