package mitraservices

type TermsAndConditionsService struct {
}

type ITermsAndConditionsService interface {
	GetTermsAndConditionsDetail() []TermsAndConditionsInfoDetail
}

type TermsAndConditionsInfoDetail struct {
	Title string `json:"title,omitempty"`
	Body  string `json:"body,omitempty"`
}

func (f TermsAndConditionsService) GetTermsAndConditionsDetail() []TermsAndConditionsInfoDetail {

	detailList := []TermsAndConditionsInfoDetail{
		TermsAndConditionsInfoDetail{
			Title: "Terms and Conditions",
			Body: `The colleges have adopted a basic set of safety rules which all drivers must agree to as part of the credentialing process. Drivers are expected to drive safely at all times. Other rules listed on the credentialing form include administrative rules.

Driving a College vehicle, rented vehicles, a personal vehicle or other vehicles on behalf of the College is a privilege, not a right. The safety of passengers, pedestrians and others is every driver's highest priority. The following rules are included by reference on the credentialing form and, by submitting credentials each driver agrees to abide by the following terms, conditions, and rules and regulations.

Drivers will:

1. Have and carry a valid driver's license while driving.

ADHERE TO THE FOLLOWING TERMS OF USE

2. Use College vehicles for authorized business only.

3. Not permit any unauthorized person to drive the vehicle. Unauthorized drivers may be personally liable for any accident
or loss.

4. Operate the College vehicle in accordance with College regulations, as may be provided in writing or verbally, and know
and observe all applicable traffic laws, ordinances and regulations.

5. Not transport unauthorized passengers such as hitchhikers. Not transport any alcohol (unless specifically permitted, e.g.,
dining or conference services), drugs, or other contraband in any College vehicle.

6. Not drive the vehicle "off road" unless it is appropriate and authorized for that use.`,
		},
	}

	return detailList
}
