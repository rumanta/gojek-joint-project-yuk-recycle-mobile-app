package mitraservices

type FAQService struct {
}

type IFAQService interface {
	GetFAQDetail() []FAQInfoDetail
}

type FAQInfoDetail struct {
	Title string `json:"title,omitempty"`
	Body  string `json:"body,omitempty"`
}

func (f FAQService) GetFAQDetail() []FAQInfoDetail {

	detailList := []FAQInfoDetail{
		FAQInfoDetail{
			Title: "Bagaimana cara menentukan tarif?",
			Body:  "Tarif ditentukan sendiri oleh mitra. Tapi kami menetapkan tarif minimum untuk setiap jenis sampah. Tarif minimum adalah Rp. 500,00.",
		},
		FAQInfoDetail{
			Title: "Apakah ada batas jarak yang bisa saya tempuh?",
			Body:  "Jarak juga ditentukan sendiri oleh mitra. Tapi kami menetapkan jarak maksimal yang dapat ditempuh oleh mitra. Jarak maksimal yang bisa ditempuh mitra adalah 30km.",
		},
		FAQInfoDetail{
			Title: "Pada menu settings apakah bisa saya lewatkan?",
			Body:  "Menu settings tidak harus diisi oleh mitra. Jika tidak diisi akan diisi default dari sistem berupa setiap hari dan semua jenis sampah pada tarif minimum dengan jarak tempuh terjauh.",
		},
		FAQInfoDetail{
			Title: "Apakah saya bisa mengganti lokasi warehouse saya?",
			Body:  "Lokasi warehouse mitra dapat diubah pada menu account. Akan terdapat tab map yang dapat mengubah lokasi dan dapat disimpan.",
		},
	}

	return detailList
}
