package mitraservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"errors"
	"fmt"
	"strings"

	"gopkg.in/go-playground/validator.v9"
)

type MitraLocationData struct {
	Lat     float64 `json:"lat" validate:"required"`
	Lon     float64 `json:"lon" validate:"required"`
	Address string  `json:"address" validate:"required"`
}

type MitraData struct {
	Name     string             `json:"name"`
	Phone    string             `json:"phone"`
	Email    string             `json:"email"`
	Verified bool               `json:"verified"`
	Location *MitraLocationData `json:"location" validate:"required"`
}

type ManageMitraAccountService struct {
	MitraRepository         repositories.IMitraRepository
	MitraLocationRepository repositories.IMitraLocationRepository
}

type IManageAccountService interface {
	GetMitraAccountData(mitraID uint) (*MitraData, error)
	UpdateMitraAccountData(
		mitraID uint,
		mitraData MitraData,
	) error
}

func (m ManageMitraAccountService) GetMitraAccountData(mitraID uint) (*MitraData, error) {
	mitra, err := m.MitraRepository.GetByID(mitraID)

	if err != nil {
		return nil, errors.New("Unable to fetch mitra data")
	}

	location, err := m.MitraLocationRepository.GetByMitraId(mitraID)

	mitraLocationData := &MitraLocationData{}

	if err != nil {
		mitraLocationData = nil
	} else {
		mitraLocationData = &MitraLocationData{
			Lat:     location.Lat,
			Lon:     location.Lon,
			Address: location.Address,
		}
	}

	verified := (mitra.Email != "") && (mitra.Phone != "") && (mitraLocationData != nil)

	mitraData := &MitraData{
		Name:     mitra.Name,
		Phone:    mitra.Phone,
		Email:    mitra.Email,
		Verified: verified,
		Location: mitraLocationData,
	}

	return mitraData, nil
}

func (m ManageMitraAccountService) UpdateMitraAccountData(mitraID uint, mitraDataNew MitraData) error {

	validate := validator.New()
	err := validate.Struct(mitraDataNew)

	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	locationUpdate := models.MitraLocation{
		MitraID: mitraID,
		Lat:     mitraDataNew.Location.Lat,
		Lon:     mitraDataNew.Location.Lon,
		Address: mitraDataNew.Location.Address,
	}

	_, err = m.MitraLocationRepository.Save(&locationUpdate)

	if err != nil {
		return errors.New("Failed to update or set Mitra Location")
	}

	mitra, err := m.MitraRepository.GetByID(mitraID)

	if err != nil {
		return errors.New("No such Mitra")
	}

	mitraUpdate := models.Mitra{
		Model: mitra.Model,
		Phone: mitraDataNew.Phone,
		Name:  mitraDataNew.Name,
		Email: mitraDataNew.Email,
	}

	_, err = m.MitraRepository.Update(&mitraUpdate)

	if err != nil {
		errString := err.Error()
		if strings.Contains(errString, "email") {
			return errors.New("unable to update, email already exists!")
		} else if strings.Contains(errString, "phone") {
			return errors.New("unable to update, phone already exists!")
		} else {
			return err
		}
	}

	return nil
}
