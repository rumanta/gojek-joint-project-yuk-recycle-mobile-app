package mitraservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestOrderAttemptService_CreateOrderAttempt(t *testing.T) {
	tests := []struct {
		description                 string
		orderID                     uint
		mitraID                     uint
		token                       uint
		returnOrderAttemptMock      *models.OrderAttempt
		returnErrorOrderAttemptMock error
		expectedError               error
	}{
		{
			description:                 "Order attempts error",
			orderID:                     uint(1),
			mitraID:                     uint(2),
			token:                       uint(3),
			returnOrderAttemptMock:      nil,
			returnErrorOrderAttemptMock: errors.New("Error"),
			expectedError:               errors.New(BackendError),
		}, {
			description:                 "Create order attempt success",
			orderID:                     uint(1),
			mitraID:                     uint(2),
			token:                       uint(3),
			returnOrderAttemptMock:      nil,
			returnErrorOrderAttemptMock: nil,
			expectedError:               nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
		mockedOrderAttemptRepository.On("Save", mock.Anything).Return(tc.returnOrderAttemptMock, tc.returnErrorOrderAttemptMock)

		orderAttemptService := OrderAttemptService{OrderAttemptRepository: mockedOrderAttemptRepository}
		err := orderAttemptService.CreateOrderAttempt(tc.orderID, tc.mitraID, tc.token)

		assert.Equal(tc.expectedError, err, tc.description)
	}
}

func TestOrderAttemptService_ApproveOrderAttempt(t *testing.T) {
	tests := []struct {
		description                     string
		orderID                         uint
		mitraID                         uint
		token                           uint
		returnOrderAttemptMock          *models.OrderAttempt
		returnOrderAttemptSaveMock      *models.OrderAttempt
		returnErrorOrderAttemptMock     error
		returnErrorOrderAttemptSaveMock error
		expectedSavedData               *models.OrderAttempt
		expectedError                   error
	}{
		{
			description:                     "Order attempts error",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          nil,
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     errors.New("Error"),
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               nil,
			expectedError:                   errors.New(BackendError),
		}, {
			description:                     "Error different token",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          &models.OrderAttempt{Token: 4},
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     nil,
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               nil,
			expectedError:                   errors.New(BackendError),
		}, {
			description:                     "Error already decide",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          &models.OrderAttempt{Token: 3, OrderAttemptStatusID: 2},
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     nil,
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               nil,
			expectedError:                   errors.New(AlreadyDecide),
		}, {
			description:                     "Approve order attempt success",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          &models.OrderAttempt{Token: 3, OrderAttemptStatusID: 1},
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     nil,
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               &models.OrderAttempt{Token: 3, OrderAttemptStatusID: 2},
			expectedError:                   nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
		mockedOrderAttemptRepository.On("GetByOrderIDAndMitraID", tc.orderID, tc.mitraID).Return(tc.returnOrderAttemptMock, tc.returnErrorOrderAttemptMock)
		mockedOrderAttemptRepository.On("Save", mock.Anything).Return(tc.returnOrderAttemptSaveMock, tc.returnErrorOrderAttemptSaveMock)

		orderAttemptService := OrderAttemptService{OrderAttemptRepository: mockedOrderAttemptRepository}
		err := orderAttemptService.ApproveOrderAttempt(tc.orderID, tc.mitraID, tc.token)

		assert.Equal(tc.expectedError, err, tc.description)

		if tc.expectedError == nil {
			mockedOrderAttemptRepository.AssertCalled(t, "Save", tc.expectedSavedData)
		}
	}
}

func TestOrderAttemptService_RejectOrderAttempt(t *testing.T) {
	tests := []struct {
		description                     string
		orderID                         uint
		mitraID                         uint
		token                           uint
		returnOrderAttemptMock          *models.OrderAttempt
		returnOrderAttemptSaveMock      *models.OrderAttempt
		returnErrorOrderAttemptMock     error
		returnErrorOrderAttemptSaveMock error
		expectedSavedData               *models.OrderAttempt
		expectedError                   error
	}{
		{
			description:                     "Order attempts error",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          nil,
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     errors.New("Error"),
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               nil,
			expectedError:                   errors.New(BackendError),
		}, {
			description:                     "Error different token",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          &models.OrderAttempt{Token: 4},
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     nil,
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               nil,
			expectedError:                   errors.New(BackendError),
		}, {
			description:                     "Error already decide",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          &models.OrderAttempt{Token: 3, OrderAttemptStatusID: 2},
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     nil,
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               nil,
			expectedError:                   errors.New(AlreadyDecide),
		}, {
			description:                     "Approve order attempt success",
			orderID:                         uint(1),
			mitraID:                         uint(2),
			token:                           uint(3),
			returnOrderAttemptMock:          &models.OrderAttempt{Token: 3, OrderAttemptStatusID: 1},
			returnOrderAttemptSaveMock:      nil,
			returnErrorOrderAttemptMock:     nil,
			returnErrorOrderAttemptSaveMock: nil,
			expectedSavedData:               &models.OrderAttempt{Token: 3, OrderAttemptStatusID: 3},
			expectedError:                   nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderAttemptRepository := new(mockRepositories.IOrderAttemptRepository)
		mockedOrderAttemptRepository.On("GetByOrderIDAndMitraID", tc.orderID, tc.mitraID).Return(tc.returnOrderAttemptMock, tc.returnErrorOrderAttemptMock)
		mockedOrderAttemptRepository.On("Save", mock.Anything).Return(tc.returnOrderAttemptSaveMock, tc.returnErrorOrderAttemptSaveMock)

		orderAttemptService := OrderAttemptService{OrderAttemptRepository: mockedOrderAttemptRepository}
		err := orderAttemptService.RejectOrderAttempt(tc.orderID, tc.mitraID, tc.token)

		assert.Equal(tc.expectedError, err, tc.description)

		if tc.expectedError == nil {
			mockedOrderAttemptRepository.AssertCalled(t, "Save", tc.expectedSavedData)
		}
	}
}
