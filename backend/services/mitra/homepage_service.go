package mitraservices

import (
	"PPLA4/backend/repositories"
	"errors"
	"time"

	orderconstant "PPLA4/backend/services/order_constant"
)

type HomepageService struct {
	MitraRepository       repositories.IMitraRepository
	OrderRepository       repositories.IOrderRepository
	OrderDetailRepository repositories.IOrderDetailRepository
}

type IHomepageService interface {
	GetWeeklyOrderStatistics(mitraID uint) (*OrderStatistic, error)
	GetOneWeekFromDate(t time.Time) (time.Time, time.Time)
	GetMitraName(mitraID uint) (string, error)
	GetOnlineMode(mitraID uint) (bool, error)
	GetHomepageData(mitraID uint) (*HomepageData, error)
}

type OrderStatistic struct {
	OrderTotal     uint `json:"order_total"`
	ActiveOrder    uint `json:"active_order"`
	PickedUpOrder  uint `json:"picked_order"`
	CompletedOrder uint `json:"completed_order"`
	SpendingTotal  uint `json:"spending"`
}

type HomepageData struct {
	WeeklyOrderStatistic *OrderStatistic `json:"weekly_statistic"`
	MitraName            string          `json:"mitra_name"`
	IsOnline             bool            `json:"is_online"`
}

const (
	getOrdersError         = "Failed to get orders"
	getOrderDetailError    = "Failed to get order details"
	getMitraError          = "Failed to get mitra"
	getOrderStatisticError = "Failed to get order statistic"
	getMitraNameError      = "Failed to get mitra name"
	getOnlineModeError     = "Failed to get online mode"
)

func (h HomepageService) GetHomepageData(mitraID uint) (*HomepageData, error) {
	orderStatistic, err := h.GetWeeklyOrderStatistics(mitraID)

	if err != nil {
		return nil, errors.New(getOrderStatisticError)
	}

	mitraName, err := h.GetMitraName(mitraID)

	if err != nil {
		return nil, errors.New(getMitraNameError)
	}

	isOnline, _ := h.GetOnlineMode(mitraID)

	homepageData := &HomepageData{
		WeeklyOrderStatistic: orderStatistic,
		MitraName:            mitraName,
		IsOnline:             isOnline,
	}

	return homepageData, nil
}

func (h HomepageService) GetMitraName(mitraID uint) (string, error) {
	mitra, err := h.MitraRepository.GetByID(mitraID)

	if err != nil {
		return "", errors.New(getMitraError)
	}

	return mitra.Name, nil
}

func (h HomepageService) GetOnlineMode(mitraID uint) (bool, error) {
	mitra, err := h.MitraRepository.GetByID(mitraID)
	isOnline := false

	if err != nil {
		return isOnline, errors.New(getMitraError)
	}

	if mitra.NotificationToken != "" && len(mitra.NotificationToken) > 0 && mitra.NotificationToken != "a" {
		isOnline = true
	}

	return isOnline, nil
}

func (h HomepageService) GetWeeklyOrderStatistics(mitraID uint) (*OrderStatistic, error) {
	timeNow := time.Now()
	firstDay, lastDay := h.GetOneWeekFromDate(timeNow)
	orders, err := h.OrderRepository.GetOrderByDateRange(mitraID, firstDay, lastDay)

	if err != nil {
		return nil, errors.New(getOrdersError)
	}
	activeOrder := uint(0)
	pickedUpOrder := uint(0)
	completedOrder := uint(0)
	spendingSum := uint(0)

	for _, order := range orders {
		if order.LatestStatus == orderconstant.AcceptedAndScheduled {
			activeOrder += 1
		} else if order.LatestStatus == orderconstant.PickedUp {
			pickedUpOrder += 1
		} else if order.LatestStatus == orderconstant.Completed {
			completedOrder += 1
		} else {
			continue
		}

		orderDetails, err := h.OrderDetailRepository.GetByOrderID(order.ID)
		if err != nil {
			return nil, errors.New(getOrderDetailError)
		}

		for _, orderDetail := range orderDetails {
			spendingSum += uint(orderDetail.TotalAmount)
		}
	}

	orderStatistic := &OrderStatistic{
		OrderTotal:     activeOrder + pickedUpOrder,
		ActiveOrder:    activeOrder,
		PickedUpOrder:  pickedUpOrder,
		CompletedOrder: completedOrder,
		SpendingTotal:  spendingSum,
	}

	return orderStatistic, nil
}

func (h HomepageService) GetOneWeekFromDate(t time.Time) (time.Time, time.Time) {
	diffDaysMap := map[time.Weekday]int{
		time.Monday:    0,
		time.Tuesday:   -1,
		time.Wednesday: -2,
		time.Thursday:  -3,
		time.Friday:    -4,
		time.Saturday:  -5,
		time.Sunday:    -6,
	}

	day := t.Weekday()
	diffDay := diffDaysMap[day]

	firstDay := t.AddDate(0, 0, diffDay)

	return firstDay, t
}
