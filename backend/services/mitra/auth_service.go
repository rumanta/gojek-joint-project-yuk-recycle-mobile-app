package mitraservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/repositories"
	"errors"

	"github.com/jinzhu/gorm"

	"golang.org/x/crypto/bcrypt"
)

const (
	UsernameAndPasswordDoesntMatch = "username and password doesn't match"
)

type AuthService struct {
	MitraRepository repositories.IMitraRepository
	TokenCreation   middleware.ITokenCreation
}

type IAuthService interface {
	Login(phone, password string) (*middleware.Token, error)
}

func (a AuthService) Login(phone, password string) (*middleware.Token, error) {
	mitra, err := a.MitraRepository.GetByPhoneNumber(phone)

	if err != nil {
		if err == gorm.ErrRecordNotFound { // username not found
			return nil, errors.New(UsernameAndPasswordDoesntMatch)
		}
		return nil, errors.New(UsernameAndPasswordDoesntMatch) // connection error
	}

	errCmp := bcrypt.CompareHashAndPassword([]byte(mitra.Password), []byte(password))

	if errCmp != nil { //Password does not match!, todo: specify error
		return nil, errors.New(UsernameAndPasswordDoesntMatch)
	}

	return a.TokenCreation.CreateToken(mitra.ID, middleware.MitraUserType, middleware.NormalLoginType), nil
}
