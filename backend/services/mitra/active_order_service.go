package mitraservices

import (
	"PPLA4/backend/repositories"
	"errors"
	"time"
)

const (
	CanNotAccessDatabase = "Oops, try again later"
)

type ActiveOrderService struct {
	OrderRepository       repositories.IOrderRepository
	CustomerRepository    repositories.ICustomerRepository
	OrderStatusRepository repositories.IOrderStatusRepository
}

type IActiveOrderService interface {
	GetActiveOrdersData(mitraId uint) ([]ActiveOrder, error)
}

type ActiveOrder struct {
	ID            uint   `json:"id,omitempty"`
	Name          string `json:"name,omitempty"`
	Time          string `json:"time,omitempty"`
	Status        string `json:"status,omitempty"`
	ScheduledTime string `json:"scheduled_time,omitempty"`
}

func (a ActiveOrderService) GetActiveOrdersData(mitraID uint) ([]ActiveOrder, error) {
	orders, err := a.OrderRepository.GetLatest20ActiveOrdersByMitraID(mitraID)

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	activeOrders := make([]ActiveOrder, len(orders))

	for i, order := range orders {
		customer, err := a.CustomerRepository.GetByID(order.CustomerID)

		if err != nil {
			return nil, errors.New(CanNotAccessDatabase)
		}

		status, _ := a.OrderStatusRepository.GetByID(uint(order.LatestStatus))

		activeOrders[i] = ActiveOrder{
			ID:            order.ID,
			Name:          customer.Name,
			Time:          order.CreatedAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006, 15:04"),
			Status:        status.Description,
			ScheduledTime: order.ScheduledAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006"),
		}
	}

	return activeOrders, nil
}
