package mitraservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"errors"
	"time"

	geo "github.com/kellydunn/golang-geo"
)

type OrderDetailService struct {
	OrderRepository              repositories.IOrderRepository
	CustomerRepository           repositories.ICustomerRepository
	MitraLocationRepository      repositories.IMitraLocationRepository
	OrderDetailRepository        repositories.IOrderDetailRepository
	GarbageTypeRepository        repositories.IGarbageTypeRepository
	OrderStatusHistoryRepository repositories.IOrderStatusHistoryRepository
	OrderStatusRepository        repositories.IOrderStatusRepository
}

type IOrderDetailService interface {
	GetOrderDetailData(orderID, mitraID uint) (*OrderDetailResponse, error)
}

type OrderDetailResponse struct {
	ID            uint            `json:"id"`
	Name          string          `json:"name"`
	Address       string          `json:"address"`
	Distance      float64         `json:"distance"`
	Price         uint            `json:"price"`
	GarbageDetail []GarbageDetail `json:"garbage_detail"`
	Status        string          `json:"status"`
	Time          string          `json:"time"`
	Reason        string          `json:"reason"`
	Phone         string          `json:"phone"`
	Latitude      float64         `json:"latitude"`
	Longitude     float64         `json:"longitude"`
	CanBePickedUp bool            `json:"can_be_picked_up"`
	Notes         string          `json:"notes"`
}

type GarbageDetail struct {
	Title        string `json:"title,omitempty"`
	Quantity     uint   `json:"quantity,omitempty"`
	QuantityType string `json:"quantity_type,omitempty"`
}

func (o OrderDetailService) GetOrderDetailData(orderID, mitraID uint) (*OrderDetailResponse, error) {
	order, err := o.OrderRepository.GetByID(orderID)

	if err != nil {
		return nil, errors.New(BackendError)
	}

	if order.MitraID != mitraID {
		return nil, errors.New(BackendError)
	}

	customer, err := o.CustomerRepository.GetByID(order.CustomerID)

	if err != nil {
		return nil, errors.New(BackendError)
	}

	mitraLocation, err := o.MitraLocationRepository.GetByMitraId(mitraID)

	if err != nil {
		return nil, errors.New(BackendError)
	}

	garbageTypes, err := o.GarbageTypeRepository.GetAll()

	if err != nil {
		return nil, errors.New(BackendError)
	}

	orderDetails, err := o.OrderDetailRepository.GetByOrderID(orderID)

	if err != nil {
		return nil, errors.New(BackendError)
	}

	orderStatusHistory, err := o.OrderStatusHistoryRepository.GetByOrderIDAndOrderStatusID(orderID, uint(order.LatestStatus))

	if err != nil {
		return nil, errors.New(BackendError)
	}

	customerPosition := geo.NewPoint(order.CustomerLat, order.CustomerLon)
	mitraPosition := geo.NewPoint(mitraLocation.Lat, mitraLocation.Lon)
	distanceToMitra := customerPosition.GreatCircleDistance(mitraPosition)

	garbageTypeByGarbageTypeID := make(map[uint]models.GarbageType)
	for _, garbageType := range garbageTypes {
		garbageTypeByGarbageTypeID[garbageType.ID] = garbageType
	}

	sumPrice := uint(0)
	garbageDetails := make([]GarbageDetail, len(orderDetails))
	for i, orderDetail := range orderDetails {
		sumPrice += uint(orderDetail.TotalAmount)
		garbageType := garbageTypeByGarbageTypeID[orderDetail.GarbageTypeID]
		garbageDetails[i] = GarbageDetail{
			Title:        garbageType.Title,
			Quantity:     uint(orderDetail.Quantity),
			QuantityType: garbageType.QuantityType,
		}
	}

	status, _ := o.OrderStatusRepository.GetByID(uint(order.LatestStatus))

	timeNow := time.Now()
	CanBePickedUp := order.ScheduledAt.Day() == timeNow.Day() &&
		order.ScheduledAt.Month() == timeNow.Month() &&
		order.ScheduledAt.Year() == timeNow.Year()

	response := &OrderDetailResponse{
		ID:            orderID,
		Name:          customer.Name,
		Address:       order.Address,
		Distance:      distanceToMitra,
		Price:         sumPrice,
		GarbageDetail: garbageDetails,
		Status:        status.Description,
		Time:          orderStatusHistory.CreatedAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006, 15:04"),
		Reason:        order.CancellationReason,
		Phone:         customer.Phone,
		Latitude:      order.CustomerLat,
		Longitude:     order.CustomerLon,
		CanBePickedUp: CanBePickedUp,
		Notes:         order.Notes,
	}

	return response, nil
}
