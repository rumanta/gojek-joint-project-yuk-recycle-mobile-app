package mitraservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"errors"
)

const (
	MinimumPrice                       = 500
	GarbageTypeNotExist                = "Garbage type not exist"
	PriceBelowMinimumPriceErrorMessage = "Price must greater or equal than 500"
	BackendError                       = "Oops, try again later"
)

type SettingsService struct {
	GarbageTypeRepository         repositories.IGarbageTypeRepository
	MitraGarbageSettingRepository repositories.IMitraGarbageSettingRepository
	MitraPickUpDistanceRepository repositories.IMitraPickUpDistanceRepository
	MitraPickUpScheduleRepository repositories.IMitraPickUpScheduleRepository
}

type ISettingsService interface {
	GetSettingsData(mitraId uint) (*Schedules, []GarbageSetting, uint, error)
	UpdateSettingsData(
		mitraId uint,
		requestSchedules *Schedules,
		requestGarbageSettings []GarbageSetting,
		requestMaximumPickUpDistance uint) error
}

type Schedules struct {
	Sunday    bool `json:"sunday,omitempty"`
	Monday    bool `json:"monday,omitempty"`
	Tuesday   bool `json:"tuesday,omitempty"`
	Wednesday bool `json:"wednesday,omitempty"`
	Thursday  bool `json:"thursday,omitempty"`
	Friday    bool `json:"friday,omitempty"`
	Saturday  bool `json:"saturday,omitempty"`
}

type GarbageSetting struct {
	Selected        bool   `json:"selected,omitempty"`
	Title           string `json:"title,omitempty"`
	MinimumQuantity uint   `json:"minimum_quantity,omitempty"`
	Price           uint   `json:"price,omitempty"`
	QuantityType    string `json:"quantity_type,omitempty"`
}

func (s SettingsService) GetSettingsData(mitraId uint) (*Schedules, []GarbageSetting, uint, error) {
	garbageTypes, err := s.GarbageTypeRepository.GetAll()

	if err != nil {
		return nil, nil, 0, errors.New(BackendError)
	}

	mitraGarbageSettings, err := s.MitraGarbageSettingRepository.GetByMitraId(mitraId)

	if err != nil {
		return nil, nil, 0, errors.New(BackendError)
	}

	pickUpDistance, err := s.MitraPickUpDistanceRepository.GetByMitraId(mitraId)

	if err != nil {
		return nil, nil, 0, errors.New(BackendError)
	}

	pickUpSchedule, err := s.MitraPickUpScheduleRepository.GetByMitraId(mitraId)

	if err != nil {
		return nil, nil, 0, errors.New(BackendError)
	}

	garbageSettingsByGarbageTypeId := make(map[uint]models.MitraGarbageSetting)
	for _, mitraGarbageSetting := range mitraGarbageSettings {
		garbageSettingsByGarbageTypeId[mitraGarbageSetting.GarbageTypeID] = mitraGarbageSetting
	}

	garbageSettings := make([]GarbageSetting, len(garbageTypes))
	for i, garbageType := range garbageTypes {
		mitraGarbageSetting := garbageSettingsByGarbageTypeId[garbageType.ID]
		garbageSettings[i].Title = garbageType.Title
		garbageSettings[i].QuantityType = garbageType.QuantityType
		if mitraGarbageSetting.Price >= MinimumPrice {
			garbageSettings[i].Selected = true
			garbageSettings[i].MinimumQuantity = mitraGarbageSetting.MinimumQuantity
			garbageSettings[i].Price = mitraGarbageSetting.Price
		}
	}
	schedules := &Schedules{
		Sunday:    pickUpSchedule.Sunday,
		Monday:    pickUpSchedule.Monday,
		Tuesday:   pickUpSchedule.Tuesday,
		Wednesday: pickUpSchedule.Wednesday,
		Thursday:  pickUpSchedule.Thursday,
		Friday:    pickUpSchedule.Friday,
		Saturday:  pickUpSchedule.Saturday,
	}
	maximumPickUpDistance := pickUpDistance.MaximumDistance

	return schedules, garbageSettings, maximumPickUpDistance, nil
}

func (s SettingsService) UpdateSettingsData(
	mitraId uint,
	requestSchedules *Schedules,
	requestGarbageSettings []GarbageSetting,
	requestMaximumPickUpDistance uint) error {

	garbageTypes, err := s.GarbageTypeRepository.GetAll()

	if err != nil {
		return errors.New(BackendError)
	}

	savedGarbageSettings, err := s.MitraGarbageSettingRepository.GetByMitraId(mitraId)

	if err != nil {
		return errors.New(BackendError)
	}

	savedPickUpDistance, err := s.MitraPickUpDistanceRepository.GetByMitraId(mitraId)

	if err != nil {
		return errors.New(BackendError)
	}

	savedPickUpSchedule, err := s.MitraPickUpScheduleRepository.GetByMitraId(mitraId)

	if err != nil {
		return errors.New(BackendError)
	}

	garbageTypesTitleById := make(map[uint]string)
	for _, garbageType := range garbageTypes {
		garbageTypesTitleById[garbageType.ID] = garbageType.Title
	}

	requestGarbageSettingsByTitle := make(map[string]GarbageSetting)
	for _, garbageSetting := range requestGarbageSettings {
		requestGarbageSettingsByTitle[garbageSetting.Title] = garbageSetting
	}

	for _, savedGarbageSetting := range savedGarbageSettings {
		garbageTypeTitle := garbageTypesTitleById[savedGarbageSetting.GarbageTypeID]
		requestGarbageSetting, exist := requestGarbageSettingsByTitle[garbageTypeTitle]
		if !exist {
			return errors.New(GarbageTypeNotExist)
		}
		if requestGarbageSetting.Selected && requestGarbageSetting.Price < MinimumPrice {
			return errors.New(PriceBelowMinimumPriceErrorMessage)
		}
	}

	for _, savedGarbageSetting := range savedGarbageSettings {
		garbageTypeTitle := garbageTypesTitleById[savedGarbageSetting.GarbageTypeID]
		requestGarbageSetting := requestGarbageSettingsByTitle[garbageTypeTitle]

		if requestGarbageSetting.Selected {
			savedGarbageSetting.MinimumQuantity = requestGarbageSetting.MinimumQuantity
			savedGarbageSetting.Price = requestGarbageSetting.Price
		} else {
			savedGarbageSetting.MinimumQuantity = 0
			savedGarbageSetting.Price = 0
		}
		s.MitraGarbageSettingRepository.Save(&savedGarbageSetting)
	}

	savedPickUpSchedule.Sunday = requestSchedules.Sunday
	savedPickUpSchedule.Monday = requestSchedules.Monday
	savedPickUpSchedule.Tuesday = requestSchedules.Tuesday
	savedPickUpSchedule.Wednesday = requestSchedules.Wednesday
	savedPickUpSchedule.Thursday = requestSchedules.Thursday
	savedPickUpSchedule.Friday = requestSchedules.Friday
	savedPickUpSchedule.Saturday = requestSchedules.Saturday
	s.MitraPickUpScheduleRepository.Save(savedPickUpSchedule)

	savedPickUpDistance.MaximumDistance = requestMaximumPickUpDistance
	s.MitraPickUpDistanceRepository.Save(savedPickUpDistance)

	return nil
}
