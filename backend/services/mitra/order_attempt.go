package mitraservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"errors"
)

const (
	NotYetDecided = 1
	Accepted      = 2
	Rejected      = 3
	AlreadyDecide = "You already decide the order status"
)

type OrderAttemptService struct {
	OrderAttemptRepository repositories.IOrderAttemptRepository
}

type IOrderAttemptService interface {
	CreateOrderAttempt(orderID uint, mitraID uint, token uint) error
	ApproveOrderAttempt(orderID uint, mitraID uint, token uint) error
	RejectOrderAttempt(orderID uint, mitraID uint, token uint) error
}

func (o OrderAttemptService) CreateOrderAttempt(orderID uint, mitraID uint, token uint) error {
	_, err := o.OrderAttemptRepository.Save(&models.OrderAttempt{
		OrderID:              orderID,
		MitraID:              mitraID,
		Token:                token,
		OrderAttemptStatusID: NotYetDecided,
	})

	if err != nil {
		return errors.New(BackendError)
	}

	return nil
}

func (o OrderAttemptService) ApproveOrderAttempt(orderID uint, mitraID uint, token uint) error {
	return approveOrRejectOrderAttempt(o.OrderAttemptRepository, orderID, mitraID, token, Accepted)
}

func (o OrderAttemptService) RejectOrderAttempt(orderID uint, mitraID uint, token uint) error {
	return approveOrRejectOrderAttempt(o.OrderAttemptRepository, orderID, mitraID, token, Rejected)
}

func approveOrRejectOrderAttempt(repository repositories.IOrderAttemptRepository, orderID uint, mitraID uint, token uint, status uint) error {
	orderAttempt, err := repository.GetByOrderIDAndMitraID(orderID, mitraID)

	if err != nil {
		return errors.New(BackendError)
	}

	if orderAttempt.Token != token {
		return errors.New(BackendError)
	}

	if orderAttempt.OrderAttemptStatusID != NotYetDecided {
		return errors.New(AlreadyDecide)
	}

	orderAttempt.OrderAttemptStatusID = status
	repository.Save(orderAttempt)

	return nil
}
