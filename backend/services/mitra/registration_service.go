package mitraservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"errors"

	"github.com/jinzhu/gorm"
)

const (
	phoneAlreadyExists = "phone already exists"
	emailAlreadyExists = "email already exists"
)

type RegistrationService struct {
	MitraRepository               repositories.IMitraRepository
	GarbageTypeRepository         repositories.IGarbageTypeRepository
	MitraGarbageSettingRepository repositories.IMitraGarbageSettingRepository
	MitraPickUpDistanceRepository repositories.IMitraPickUpDistanceRepository
	MitraPickUpScheduleRepository repositories.IMitraPickUpScheduleRepository
	TokenCreation                 middleware.ITokenCreation
}

type IRegistrationService interface {
	Register(phone, password, name, email string) (*middleware.Token, error)
}

func (r RegistrationService) Register(phone, password, name, email string) (*middleware.Token, error) {
	if _, err := r.MitraRepository.GetByEmail(email); err != gorm.ErrRecordNotFound { // email exists
		return nil, errors.New(emailAlreadyExists)
	}

	if _, err := r.MitraRepository.GetByPhoneNumber(phone); err != gorm.ErrRecordNotFound { // phone exists
		return nil, errors.New(phoneAlreadyExists)
	}

	mitra, err := r.MitraRepository.Add(&models.Mitra{Phone: phone, Password: password, Name: name, Email: email})

	if err != nil {
		return nil, err
	}

	garbageTypes, _ := r.GarbageTypeRepository.GetAll()
	for _, garbageType := range garbageTypes {
		r.MitraGarbageSettingRepository.Save(&models.MitraGarbageSetting{MitraID: mitra.ID, GarbageTypeID: garbageType.ID, MinimumQuantity: 0, Price: 0})
	}
	r.MitraPickUpDistanceRepository.Save(&models.MitraPickUpDistance{MitraID: mitra.ID, MaximumDistance: 0})
	r.MitraPickUpScheduleRepository.Save(&models.MitraPickUpSchedule{MitraID: mitra.ID})

	return r.TokenCreation.CreateToken(mitra.ID, middleware.MitraUserType, middleware.NormalLoginType), nil
}
