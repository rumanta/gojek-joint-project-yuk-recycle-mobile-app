package mitraservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	mockRepositories "PPLA4/backend/mocks/repositories"
)

func TestManageAccountService_GetMitraData(t *testing.T) {
	tests := []struct {
		description         string
		mitraId             uint
		returnMitraMock     *models.Mitra
		returnLocationMock  *models.MitraLocation
		returnMitraError    error
		returnLocationError error
		expectedMitraData   *MitraData
		expectedError       error
	}{
		{
			description: "Get data success",
			mitraId:     uint(1),
			returnMitraMock: &models.Mitra{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				NotificationToken: "",
				GarbageSettings:   []models.MitraGarbageSetting{},
			},
			returnLocationMock: &models.MitraLocation{
				Model:   gorm.Model{ID: 2},
				MitraID: 1,
				Lat:     123,
				Lon:     321,
				Address: "dummy address",
			},
			returnMitraError:    nil,
			returnLocationError: nil,
			expectedMitraData: &MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: &MitraLocationData{
					Lat:     123,
					Lon:     321,
					Address: "dummy address",
				},
			},
			expectedError: nil,
		}, {
			description:       "Unable to fetch mitra data",
			mitraId:           uint(2),
			returnMitraMock:   nil,
			returnMitraError:  errors.New("Error"),
			expectedMitraData: nil,
			expectedError:     errors.New("Unable to fetch mitra data"),
		}, {
			description: "Unable to fetch location data",
			mitraId:     uint(2),
			returnMitraMock: &models.Mitra{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				NotificationToken: "",
				GarbageSettings:   []models.MitraGarbageSetting{},
			},
			returnLocationMock:  nil,
			returnMitraError:    nil,
			returnLocationError: errors.New("Error"),
			expectedMitraData: &MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: false,
				Location: nil,
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByID", tc.mitraId).Return(tc.returnMitraMock, tc.returnMitraError)

		mockedLocationRepository := new(mockRepositories.IMitraLocationRepository)
		mockedLocationRepository.On("GetByMitraId", tc.mitraId).Return(tc.returnLocationMock, tc.returnLocationError)

		manageAccountService := ManageMitraAccountService{
			MitraRepository:         mockedMitraRepository,
			MitraLocationRepository: mockedLocationRepository,
		}

		mitraData, err := manageAccountService.GetMitraAccountData(tc.mitraId)

		assert.Equal(tc.expectedMitraData, mitraData, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}

func TestManageAccountService_UpdateMitraAccountData(t *testing.T) {
	tests := []struct {
		description             string
		mitraId                 uint
		returnMitraData         MitraData
		returnMitraMock         *models.Mitra
		returnLocationMock      *models.MitraLocation
		returnErrorMitraData    error
		returnErrorMitraUpdate  error
		returnErrorLocationSave error
		expectedErrorString     string
	}{
		{
			description: "No such mitra",
			mitraId:     1,
			returnMitraData: MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: &MitraLocationData{
					Lat:     123,
					Lon:     321,
					Address: "dummy address",
				},
			},
			returnMitraMock:         nil,
			returnLocationMock:      nil,
			returnErrorMitraData:    errors.New("Error"),
			returnErrorMitraUpdate:  nil,
			returnErrorLocationSave: nil,
			expectedErrorString:     "No such Mitra",
		}, {
			description: "Invalid argument",
			mitraId:     1,
			returnMitraData: MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: nil,
			},
			returnMitraMock:         nil,
			returnLocationMock:      nil,
			returnErrorMitraData:    nil,
			returnErrorMitraUpdate:  nil,
			returnErrorLocationSave: nil,
			expectedErrorString:     "Key: 'MitraData.Location' Error:Field validation for 'Location' failed on the 'required' tag",
		}, {
			description: "Failed to save location",
			mitraId:     1,
			returnMitraData: MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: &MitraLocationData{
					Lat:     123,
					Lon:     321,
					Address: "dummy address",
				},
			},
			returnMitraMock:         nil,
			returnLocationMock:      nil,
			returnErrorMitraData:    nil,
			returnErrorMitraUpdate:  nil,
			returnErrorLocationSave: errors.New("Failed to update or set Mitra Location"),
			expectedErrorString:     "Failed to update or set Mitra Location",
		}, {
			description: "Email already exists",
			mitraId:     1,
			returnMitraData: MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: &MitraLocationData{
					Lat:     123,
					Lon:     321,
					Address: "dummy address",
				},
			},
			returnMitraMock: &models.Mitra{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				NotificationToken: "",
				GarbageSettings:   []models.MitraGarbageSetting{},
			},
			returnLocationMock: &models.MitraLocation{
				Lat:     123,
				Lon:     321,
				Address: "dummy addresss",
			},
			returnErrorMitraData:    nil,
			returnErrorLocationSave: nil,
			returnErrorMitraUpdate:  errors.New(`violate constraint on column "email"`),
			expectedErrorString:     `unable to update, email already exists!`,
		}, {
			description: "Phone already exists error",
			mitraId:     1,
			returnMitraData: MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: &MitraLocationData{
					Lat:     123,
					Lon:     321,
					Address: "dummy address",
				},
			},
			returnMitraMock: &models.Mitra{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				NotificationToken: "",
				GarbageSettings:   []models.MitraGarbageSetting{},
			},
			returnLocationMock: &models.MitraLocation{
				Lat:     123,
				Lon:     321,
				Address: "dummy addresss",
			},
			returnErrorMitraData:    nil,
			returnErrorLocationSave: nil,
			returnErrorMitraUpdate:  errors.New(`violate constraint on column "phone"`),
			expectedErrorString:     `unable to update, phone already exists!`,
		}, {
			description: "Error on update mitra data",
			mitraId:     1,
			returnMitraData: MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: &MitraLocationData{
					Lat:     123,
					Lon:     321,
					Address: "dummy address",
				},
			},
			returnMitraMock: &models.Mitra{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				NotificationToken: "",
				GarbageSettings:   []models.MitraGarbageSetting{},
			},
			returnLocationMock: &models.MitraLocation{
				Lat:     123,
				Lon:     321,
				Address: "dummy addresss",
			},
			returnErrorMitraData:    nil,
			returnErrorLocationSave: nil,
			returnErrorMitraUpdate:  errors.New(`error cuy wtf`),
			expectedErrorString:     `error cuy wtf`,
		}, {
			description: "Update data success",
			mitraId:     1,
			returnMitraData: MitraData{
				Name:     "This is dummy",
				Phone:    "+6212345678",
				Email:    "test@gmail.com",
				Verified: true,
				Location: &MitraLocationData{
					Lat:     123,
					Lon:     321,
					Address: "dummy address",
				},
			},
			returnMitraMock: &models.Mitra{
				Model:             gorm.Model{ID: 1},
				Phone:             "+6212345678",
				Password:          "123password",
				Name:              "This is dummy",
				Email:             "test@gmail.com",
				NotificationToken: "",
				GarbageSettings:   []models.MitraGarbageSetting{},
			},
			returnLocationMock: &models.MitraLocation{
				Lat:     123,
				Lon:     321,
				Address: "dummy addresss",
			},
			returnErrorMitraData:    nil,
			returnErrorMitraUpdate:  nil,
			returnErrorLocationSave: nil,
			expectedErrorString:     "",
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByID", tc.mitraId).Return(tc.returnMitraMock, tc.returnErrorMitraData)
		mockedMitraRepository.On("Update", mock.Anything).Return(tc.returnMitraMock, tc.returnErrorMitraUpdate)

		mockedLocationRepository := new(mockRepositories.IMitraLocationRepository)
		mockedLocationRepository.On("Save", mock.Anything).Return(tc.returnLocationMock, tc.returnErrorLocationSave)

		manageAccountService := ManageMitraAccountService{
			MitraRepository:         mockedMitraRepository,
			MitraLocationRepository: mockedLocationRepository,
		}

		err := manageAccountService.UpdateMitraAccountData(tc.mitraId, tc.returnMitraData)

		if err != nil {
			assert.EqualError(err, tc.expectedErrorString, tc.description)
		} else {
			mockedMitraRepository.AssertCalled(t, "Update", mock.Anything)
			mockedLocationRepository.AssertCalled(t, "Save", mock.Anything)
		}
	}
}
