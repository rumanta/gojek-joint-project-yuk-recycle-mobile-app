package mitraservices

import (
	mockRepositories "PPLA4/backend/mocks/repositories"
	mockNotification "PPLA4/backend/mocks/services/notification"
	"PPLA4/backend/models"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"

	"github.com/stretchr/testify/assert"
)

func TestPickUpOrder(t *testing.T) {
	tests := []struct {
		description          string
		orderID              uint
		mitraID              uint
		returnOrderMock      *models.Order
		returnOrderErrorMock error
		returnMitraMock      *models.Mitra
		returnCustomerMock   *models.Customer
		expectedError        error
	}{
		{
			description:          "orderID doesn't exist in database",
			orderID:              uint(1),
			mitraID:              uint(2),
			returnOrderErrorMock: errors.New("Record doesn't exists"),
			expectedError:        errors.New("Record doesn't exists"),
		}, {
			description: "Invalid corresponding mitra",
			orderID:     uint(1),
			mitraID:     uint(2),
			returnOrderMock: &models.Order{
				MitraID: uint(3),
			},
			expectedError: errors.New(orderConstant.InvalidCorrespondingMitra),
		}, {
			description: "Invalid order status transition",
			orderID:     uint(1),
			mitraID:     uint(2),
			returnOrderMock: &models.Order{
				MitraID:      uint(2),
				LatestStatus: 1,
			},
			expectedError: errors.New(orderConstant.InvalidOrderStatusTransition),
		}, {
			description: "Success picking up order",
			orderID:     uint(1),
			mitraID:     uint(2),
			returnOrderMock: &models.Order{
				MitraID:      uint(2),
				LatestStatus: orderConstant.AcceptedAndScheduled,
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetByID", tc.orderID).Return(tc.returnOrderMock, tc.returnOrderErrorMock)

		mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
		mockedOrderStatusHistoryRepository.On("Save", mock.Anything).Return(nil, nil)

		mockedNotificationService := new(mockNotification.INotificationService)
		mockedNotificationService.On("SendNotification", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		mockedCustomerRepository.On("GetByID", mock.Anything).Return(&models.Customer{}, nil)

		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByID", mock.Anything).Return(&models.Mitra{}, nil)

		pickUpOrderService := PickUpOrderService{
			OrderRepository:     mockedOrderRepository,
			OrderStatusHistory:  mockedOrderStatusHistoryRepository,
			NotificationService: mockedNotificationService,
			CustomerRepository:  mockedCustomerRepository,
			MitraRepository:     mockedMitraRepository,
		}
		err := pickUpOrderService.PickUpOrder(tc.orderID, tc.mitraID)

		assert.Equal(tc.expectedError, err, tc.description)
	}
}
