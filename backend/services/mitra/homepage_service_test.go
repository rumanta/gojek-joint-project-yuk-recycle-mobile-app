package mitraservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	mockRepositories "PPLA4/backend/mocks/repositories"
)

func TestHomepageService_GetWeeklyOrderStatistics(t *testing.T) {
	tests := []struct {
		description                 string
		mitraID                     uint
		returnOrdersMock            []models.Order
		returnOrderDetailsMock      []models.OrderDetail
		returnErrorOrdersMock       error
		returnErrorOrderDetailsMock error
		expectedOrderStatistic      *OrderStatistic
		expectedError               error
	}{
		{
			description:                 "Failed get orders",
			mitraID:                     1,
			returnOrdersMock:            nil,
			returnOrderDetailsMock:      nil,
			returnErrorOrdersMock:       errors.New("Error orders"),
			returnErrorOrderDetailsMock: nil,
			expectedOrderStatistic:      nil,
			expectedError:               errors.New("Failed to get orders"),
		}, {
			description: "Failed get order details",
			mitraID:     1,
			returnOrdersMock: []models.Order{
				models.Order{
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       4,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
			},
			returnOrderDetailsMock:      nil,
			returnErrorOrdersMock:       nil,
			returnErrorOrderDetailsMock: errors.New("Error orders"),
			expectedOrderStatistic:      nil,
			expectedError:               errors.New("Failed to get order details"),
		}, {
			description: "Success get order statistics",
			mitraID:     1,
			returnOrdersMock: []models.Order{
				models.Order{
					Model:              gorm.Model{ID: 1},
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       2,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
				models.Order{
					Model:              gorm.Model{ID: 2},
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       3,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
				models.Order{
					Model:              gorm.Model{ID: 3},
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       8,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja sayah",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 0}, OrderID: 0, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 0}, OrderID: 0, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
			},
			returnErrorOrdersMock:       nil,
			returnErrorOrderDetailsMock: nil,
			expectedOrderStatistic: &OrderStatistic{
				OrderTotal:    uint(2),
				ActiveOrder:   uint(1),
				PickedUpOrder: uint(1),
				SpendingTotal: uint(12000),
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetOrderByDateRange", mock.Anything, mock.Anything, mock.Anything).Return(tc.returnOrdersMock, tc.returnErrorOrdersMock)

		mockedOrderDetailRepository := new(mockRepositories.IOrderDetailRepository)
		mockedOrderDetailRepository.On("GetByOrderID", mock.Anything).Return(tc.returnOrderDetailsMock, tc.returnErrorOrderDetailsMock)

		homepageService := HomepageService{
			OrderRepository:       mockedOrderRepository,
			OrderDetailRepository: mockedOrderDetailRepository,
		}

		orderStatistic, err := homepageService.GetWeeklyOrderStatistics(tc.mitraID)

		assert.Equal(tc.expectedOrderStatistic, orderStatistic, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}

func TestHomepageService_GetMitraName(t *testing.T) {
	tests := []struct {
		description          string
		mitraID              uint
		returnMitraMock      *models.Mitra
		returnErrorMitraMock error
		expectedMitraName    string
		expectedError        error
	}{
		{
			description:          "Get mitra name success",
			mitraID:              1,
			returnMitraMock:      &models.Mitra{Phone: "08123456789", Name: "william", Password: "123password", Email: "william@gmail.com"},
			returnErrorMitraMock: nil,
			expectedMitraName:    "william",
			expectedError:        nil,
		}, {
			description:          "Get mitra name failed",
			mitraID:              1,
			returnMitraMock:      nil,
			returnErrorMitraMock: errors.New("error"),
			expectedMitraName:    "",
			expectedError:        errors.New("Failed to get mitra"),
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByID", mock.Anything).Return(tc.returnMitraMock, tc.returnErrorMitraMock)

		homepageService := HomepageService{MitraRepository: mockedMitraRepository}
		mitraName, err := homepageService.GetMitraName(tc.mitraID)

		assert.Equal(tc.expectedMitraName, mitraName, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}

func TestHomepageService_GetOnlineMode(t *testing.T) {
	tests := []struct {
		description          string
		mitraID              uint
		returnMitraMock      *models.Mitra
		returnErrorMitraMock error
		expectedOnlineMode   bool
		expectedError        error
	}{
		{
			description:          "Get mitra name success online",
			mitraID:              1,
			returnMitraMock:      &models.Mitra{Phone: "08123456789", Name: "william", Password: "123password", Email: "william@gmail.com", NotificationToken: "asdasd123"},
			returnErrorMitraMock: nil,
			expectedOnlineMode:   true,
			expectedError:        nil,
		}, {
			description:          "Get mitra name success offline",
			mitraID:              1,
			returnMitraMock:      &models.Mitra{Phone: "08123456789", Name: "william", Password: "123password", Email: "william@gmail.com", NotificationToken: ""},
			returnErrorMitraMock: nil,
			expectedOnlineMode:   false,
			expectedError:        nil,
		}, {
			description:          "Get mitra name failed",
			mitraID:              1,
			returnMitraMock:      nil,
			returnErrorMitraMock: errors.New("error"),
			expectedOnlineMode:   false,
			expectedError:        errors.New("Failed to get mitra"),
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByID", mock.Anything).Return(tc.returnMitraMock, tc.returnErrorMitraMock)

		homepageService := HomepageService{MitraRepository: mockedMitraRepository}
		isOnline, err := homepageService.GetOnlineMode(tc.mitraID)

		assert.Equal(tc.expectedOnlineMode, isOnline, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}

func TestHomepageService_GetHomepageData(t *testing.T) {
	tests := []struct {
		description                 string
		mitraID                     uint
		returnMitraMock             *models.Mitra
		returnErrorMitraMock        error
		returnOrdersMock            []models.Order
		returnOrderDetailsMock      []models.OrderDetail
		returnErrorOrdersMock       error
		returnErrorOrderDetailsMock error
		expectedHomepageData        *HomepageData
		expectedError               error
	}{
		{
			description:          "Success get homepage data",
			mitraID:              1,
			returnMitraMock:      &models.Mitra{Phone: "08123456789", Name: "william", Password: "123password", Email: "william@gmail.com", NotificationToken: "asdasd123"},
			returnErrorMitraMock: nil,
			returnOrdersMock: []models.Order{
				models.Order{
					Model:              gorm.Model{ID: 1},
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       2,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
				models.Order{
					Model:              gorm.Model{ID: 2},
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       3,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 0}, OrderID: 0, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 0}, OrderID: 0, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
			},
			returnErrorOrdersMock: nil,
			expectedHomepageData: &HomepageData{
				WeeklyOrderStatistic: &OrderStatistic{
					OrderTotal:    uint(2),
					ActiveOrder:   uint(1),
					PickedUpOrder: uint(1),
					SpendingTotal: uint(12000),
				},
				MitraName: "william",
				IsOnline:  true,
			},
			expectedError: nil,
		}, {
			description:            "Failed get homepage data because order statistic",
			mitraID:                1,
			returnMitraMock:        &models.Mitra{Phone: "08123456789", Name: "william", Password: "123password", Email: "william@gmail.com", NotificationToken: "asdasd123"},
			returnErrorMitraMock:   nil,
			returnOrdersMock:       nil,
			returnOrderDetailsMock: nil,
			returnErrorOrdersMock:  errors.New("Failed to get order statistic"),
			expectedHomepageData:   nil,
			expectedError:          errors.New("Failed to get order statistic"),
		}, {
			description:          "Failed get homepage data because mitra name",
			mitraID:              1,
			returnMitraMock:      nil,
			returnErrorMitraMock: errors.New("Failed to get mitra name"),
			returnOrdersMock: []models.Order{
				models.Order{
					Model:              gorm.Model{ID: 1},
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       2,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
				models.Order{
					Model:              gorm.Model{ID: 2},
					MitraID:            1,
					CustomerID:         2,
					LatestStatus:       3,
					Address:            "UI Depok",
					CancellationReason: "Mau cancel aja",
					CustomerLat:        -6.30786100,
					CustomerLon:        106.83883300,
				},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 0}, OrderID: 0, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 0}, OrderID: 0, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
			},
			returnErrorOrdersMock: nil,
			expectedHomepageData:  nil,
			expectedError:         errors.New("Failed to get mitra name"),
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByID", mock.Anything).Return(tc.returnMitraMock, tc.returnErrorMitraMock)

		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetOrderByDateRange", mock.Anything, mock.Anything, mock.Anything).Return(tc.returnOrdersMock, tc.returnErrorOrdersMock)

		mockedOrderDetailRepository := new(mockRepositories.IOrderDetailRepository)
		mockedOrderDetailRepository.On("GetByOrderID", mock.Anything).Return(tc.returnOrderDetailsMock, tc.returnErrorOrderDetailsMock)

		homepageService := HomepageService{
			MitraRepository:       mockedMitraRepository,
			OrderRepository:       mockedOrderRepository,
			OrderDetailRepository: mockedOrderDetailRepository,
		}

		homepageData, err := homepageService.GetHomepageData(tc.mitraID)

		assert.Equal(tc.expectedHomepageData, homepageData, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
