package mitraservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/jinzhu/gorm"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestSettingsService_GetSettingsData(t *testing.T) {
	tests := []struct {
		description                         string
		mitraId                             uint
		returnGarbageTypesMock              []models.GarbageType
		returnMitraGarbageSettingsMock      []models.MitraGarbageSetting
		returnPickUpDistanceMock            *models.MitraPickUpDistance
		returnPickUpScheduleMock            *models.MitraPickUpSchedule
		returnErrorGarbageTypesMock         error
		returnErrorMitraGarbageSettingsMock error
		returnErrorPickUpDistanceMock       error
		returnErrorPickUpScheduleMock       error
		expectedSchedules                   *Schedules
		expectedGarbageSettings             []GarbageSetting
		expectedMaximumPickUpDistance       uint
		expectedError                       error
	}{
		{
			description:                         "Garbage types error",
			mitraId:                             uint(1),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         errors.New("Error"),
			returnErrorMitraGarbageSettingsMock: errors.New("Error"),
			returnErrorPickUpDistanceMock:       errors.New("Error"),
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedSchedules:                   nil,
			expectedGarbageSettings:             nil,
			expectedMaximumPickUpDistance:       uint(0),
			expectedError:                       errors.New(BackendError),
		}, {
			description:                         "Mitra garbage settings error",
			mitraId:                             uint(1),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: errors.New("Error"),
			returnErrorPickUpDistanceMock:       errors.New("Error"),
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedSchedules:                   nil,
			expectedGarbageSettings:             nil,
			expectedMaximumPickUpDistance:       uint(0),
			expectedError:                       errors.New(BackendError),
		}, {
			description:                         "Pick up distance error",
			mitraId:                             uint(1),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       errors.New("Error"),
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedSchedules:                   nil,
			expectedGarbageSettings:             nil,
			expectedMaximumPickUpDistance:       uint(0),
			expectedError:                       errors.New(BackendError),
		}, {
			description:                         "Pick up schedule error",
			mitraId:                             uint(1),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       nil,
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedSchedules:                   nil,
			expectedGarbageSettings:             nil,
			expectedMaximumPickUpDistance:       uint(0),
			expectedError:                       errors.New(BackendError),
		}, {
			description: "Get data success",
			mitraId:     uint(1),
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah", Description: "Sampah", QuantityType: "Kg", ImageBase64: ""},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", Description: "Sampah 2", QuantityType: "Kg", ImageBase64: ""},
			},
			returnMitraGarbageSettingsMock: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{Model: gorm.Model{ID: 1}, MitraID: 1, GarbageTypeID: 1, MinimumQuantity: 10, Price: 500},
			},
			returnPickUpDistanceMock: &models.MitraPickUpDistance{Model: gorm.Model{ID: 1}, MitraID: 1, MaximumDistance: 10},
			returnPickUpScheduleMock: &models.MitraPickUpSchedule{
				Model:     gorm.Model{ID: 1},
				MitraID:   1,
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       nil,
			returnErrorPickUpScheduleMock:       nil,
			expectedSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			expectedGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 500, QuantityType: "Kg"},
				GarbageSetting{Selected: false, Title: "Sampah 2", QuantityType: "Kg"},
			},
			expectedMaximumPickUpDistance: uint(10),
			expectedError:                 nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedGarbageTypeRepository := new(mockRepositories.IGarbageTypeRepository)
		mockedGarbageTypeRepository.On("GetAll").Return(tc.returnGarbageTypesMock, tc.returnErrorGarbageTypesMock)

		mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
		mockedMitraGarbageSettingRepository.On("GetByMitraId", tc.mitraId).Return(tc.returnMitraGarbageSettingsMock, tc.returnErrorMitraGarbageSettingsMock)

		mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
		mockedMitraPickUpDistanceRepository.On("GetByMitraId", tc.mitraId).Return(tc.returnPickUpDistanceMock, tc.returnErrorPickUpDistanceMock)

		mockedMitraPickUpScheduleRepository := new(mockRepositories.IMitraPickUpScheduleRepository)
		mockedMitraPickUpScheduleRepository.On("GetByMitraId", tc.mitraId).Return(tc.returnPickUpScheduleMock, tc.returnErrorPickUpScheduleMock)

		settingsService := SettingsService{
			GarbageTypeRepository:         mockedGarbageTypeRepository,
			MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
			MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
			MitraPickUpScheduleRepository: mockedMitraPickUpScheduleRepository,
		}
		schedules, garbageSettings, maximumPickUpDistance, err := settingsService.GetSettingsData(tc.mitraId)

		assert.Equal(tc.expectedSchedules, schedules, tc.description)
		assert.Equal(tc.expectedGarbageSettings, garbageSettings, tc.description)
		assert.Equal(tc.expectedMaximumPickUpDistance, maximumPickUpDistance, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}

func TestSettingsService_UpdateSettingsData(t *testing.T) {
	tests := []struct {
		description                         string
		mitraId                             uint
		requestSchedules                    *Schedules
		requestGarbageSettings              []GarbageSetting
		requestMaximumPickUpDistance        uint
		returnGarbageTypesMock              []models.GarbageType
		returnMitraGarbageSettingsMock      []models.MitraGarbageSetting
		returnPickUpDistanceMock            *models.MitraPickUpDistance
		returnPickUpScheduleMock            *models.MitraPickUpSchedule
		returnErrorGarbageTypesMock         error
		returnErrorMitraGarbageSettingsMock error
		returnErrorPickUpDistanceMock       error
		returnErrorPickUpScheduleMock       error
		expectedError                       error
	}{
		{
			description: "Garbage types error",
			mitraId:     uint(1),
			requestSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			requestGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 500},
				GarbageSetting{Selected: false, Title: "Sampah 2"},
			},
			requestMaximumPickUpDistance:        uint(10),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         errors.New("Error"),
			returnErrorMitraGarbageSettingsMock: errors.New("Error"),
			returnErrorPickUpDistanceMock:       errors.New("Error"),
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedError:                       errors.New(BackendError),
		}, {
			description: "Mitra garbage settings error",
			mitraId:     uint(1),
			requestSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			requestGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 500},
				GarbageSetting{Selected: false, Title: "Sampah 2"},
			},
			requestMaximumPickUpDistance:        uint(10),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: errors.New("Error"),
			returnErrorPickUpDistanceMock:       errors.New("Error"),
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedError:                       errors.New(BackendError),
		}, {
			description: "Pick up distance error",
			mitraId:     uint(1),
			requestSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			requestGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 500},
				GarbageSetting{Selected: false, Title: "Sampah 2"},
			},
			requestMaximumPickUpDistance:        uint(10),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       errors.New("Error"),
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedError:                       errors.New(BackendError),
		}, {
			description: "Pick up schedule error",
			mitraId:     uint(1),
			requestSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			requestGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 500},
				GarbageSetting{Selected: false, Title: "Sampah 2"},
			},
			requestMaximumPickUpDistance:        uint(10),
			returnGarbageTypesMock:              nil,
			returnMitraGarbageSettingsMock:      nil,
			returnPickUpDistanceMock:            nil,
			returnPickUpScheduleMock:            nil,
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       nil,
			returnErrorPickUpScheduleMock:       errors.New("Error"),
			expectedError:                       errors.New(BackendError),
		}, {
			description: "Not all garbage types exist",
			mitraId:     uint(1),
			requestSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			requestGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 500},
				GarbageSetting{Selected: false, Title: "Sampah 2"},
			},
			requestMaximumPickUpDistance: uint(10),
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah", Description: "Sampah", QuantityType: "Kg", ImageBase64: ""},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", Description: "Sampah 2", QuantityType: "Kg", ImageBase64: ""},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", Description: "Sampah 3", QuantityType: "Kg", ImageBase64: ""},
			},
			returnMitraGarbageSettingsMock: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{Model: gorm.Model{ID: 1}, MitraID: 1, GarbageTypeID: 1, MinimumQuantity: 10, Price: 1000},
				models.MitraGarbageSetting{Model: gorm.Model{ID: 2}, MitraID: 1, GarbageTypeID: 2, MinimumQuantity: 10, Price: 500},
				models.MitraGarbageSetting{Model: gorm.Model{ID: 3}, MitraID: 1, GarbageTypeID: 3, MinimumQuantity: 0, Price: 0},
			},
			returnPickUpDistanceMock: &models.MitraPickUpDistance{Model: gorm.Model{ID: 1}, MitraID: 1, MaximumDistance: 100},
			returnPickUpScheduleMock: &models.MitraPickUpSchedule{
				Model:     gorm.Model{ID: 1},
				MitraID:   1,
				Sunday:    false,
				Monday:    false,
				Tuesday:   false,
				Wednesday: false,
				Thursday:  true,
				Friday:    true,
				Saturday:  true,
			},
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       nil,
			returnErrorPickUpScheduleMock:       nil,
			expectedError:                       errors.New(GarbageTypeNotExist),
		}, {
			description: "Price below minimum price",
			mitraId:     uint(1),
			requestSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			requestGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 400},
				GarbageSetting{Selected: false, Title: "Sampah 2"},
			},
			requestMaximumPickUpDistance: uint(10),
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah", Description: "Sampah", QuantityType: "Kg", ImageBase64: ""},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", Description: "Sampah 2", QuantityType: "Kg", ImageBase64: ""},
			},
			returnMitraGarbageSettingsMock: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{Model: gorm.Model{ID: 1}, MitraID: 1, GarbageTypeID: 1, MinimumQuantity: 10, Price: 1000},
				models.MitraGarbageSetting{Model: gorm.Model{ID: 2}, MitraID: 1, GarbageTypeID: 2, MinimumQuantity: 10, Price: 500},
			},
			returnPickUpDistanceMock: &models.MitraPickUpDistance{Model: gorm.Model{ID: 1}, MitraID: 1, MaximumDistance: 100},
			returnPickUpScheduleMock: &models.MitraPickUpSchedule{
				Model:     gorm.Model{ID: 1},
				MitraID:   1,
				Sunday:    false,
				Monday:    false,
				Tuesday:   false,
				Wednesday: false,
				Thursday:  true,
				Friday:    true,
				Saturday:  true,
			},
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       nil,
			returnErrorPickUpScheduleMock:       nil,
			expectedError:                       errors.New(PriceBelowMinimumPriceErrorMessage),
		}, {
			description: "Update data success",
			mitraId:     uint(1),
			requestSchedules: &Schedules{
				Sunday:    true,
				Monday:    true,
				Tuesday:   true,
				Wednesday: true,
				Thursday:  false,
				Friday:    false,
				Saturday:  false,
			},
			requestGarbageSettings: []GarbageSetting{
				GarbageSetting{Selected: true, Title: "Sampah", MinimumQuantity: 10, Price: 500},
				GarbageSetting{Selected: false, Title: "Sampah 2"},
			},
			requestMaximumPickUpDistance: uint(10),
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah", Description: "Sampah", QuantityType: "Kg", ImageBase64: ""},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", Description: "Sampah 2", QuantityType: "Kg", ImageBase64: ""},
			},
			returnMitraGarbageSettingsMock: []models.MitraGarbageSetting{
				models.MitraGarbageSetting{Model: gorm.Model{ID: 1}, MitraID: 1, GarbageTypeID: 1, MinimumQuantity: 10, Price: 1000},
				models.MitraGarbageSetting{Model: gorm.Model{ID: 3}, MitraID: 1, GarbageTypeID: 2, MinimumQuantity: 10, Price: 500},
			},
			returnPickUpDistanceMock: &models.MitraPickUpDistance{Model: gorm.Model{ID: 1}, MitraID: 1, MaximumDistance: 100},
			returnPickUpScheduleMock: &models.MitraPickUpSchedule{
				Model:     gorm.Model{ID: 1},
				MitraID:   1,
				Sunday:    false,
				Monday:    false,
				Tuesday:   false,
				Wednesday: false,
				Thursday:  true,
				Friday:    true,
				Saturday:  true,
			},
			returnErrorGarbageTypesMock:         nil,
			returnErrorMitraGarbageSettingsMock: nil,
			returnErrorPickUpDistanceMock:       nil,
			returnErrorPickUpScheduleMock:       nil,
			expectedError:                       nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedGarbageTypeRepository := new(mockRepositories.IGarbageTypeRepository)
		mockedGarbageTypeRepository.On("GetAll").Return(tc.returnGarbageTypesMock, tc.returnErrorGarbageTypesMock)

		mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
		mockedMitraGarbageSettingRepository.On("GetByMitraId", tc.mitraId).Return(tc.returnMitraGarbageSettingsMock, tc.returnErrorMitraGarbageSettingsMock)
		if len(tc.returnMitraGarbageSettingsMock) > 0 {
			mockedMitraGarbageSettingRepository.On("Save", mock.Anything).Return(&tc.returnMitraGarbageSettingsMock[0], tc.expectedError)
		}

		mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
		mockedMitraPickUpDistanceRepository.On("GetByMitraId", tc.mitraId).Return(tc.returnPickUpDistanceMock, tc.returnErrorPickUpDistanceMock)
		mockedMitraPickUpDistanceRepository.On("Save", mock.Anything).Return(tc.returnPickUpDistanceMock, tc.expectedError)

		mockedMitraPickUpScheduleRepository := new(mockRepositories.IMitraPickUpScheduleRepository)
		mockedMitraPickUpScheduleRepository.On("GetByMitraId", tc.mitraId).Return(tc.returnPickUpScheduleMock, tc.returnErrorPickUpScheduleMock)
		mockedMitraPickUpScheduleRepository.On("Save", mock.Anything).Return(tc.returnPickUpScheduleMock, tc.expectedError)

		settingsService := SettingsService{
			GarbageTypeRepository:         mockedGarbageTypeRepository,
			MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
			MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
			MitraPickUpScheduleRepository: mockedMitraPickUpScheduleRepository,
		}
		err := settingsService.UpdateSettingsData(tc.mitraId, tc.requestSchedules, tc.requestGarbageSettings, tc.requestMaximumPickUpDistance)

		assert.Equal(tc.expectedError, err, tc.description)

		if tc.expectedError == nil {
			mockedMitraGarbageSettingRepository.AssertCalled(t, "Save", mock.Anything)
			mockedMitraPickUpDistanceRepository.AssertCalled(t, "Save", mock.Anything)
			mockedMitraPickUpScheduleRepository.AssertCalled(t, "Save", mock.Anything)
		}
	}
}
