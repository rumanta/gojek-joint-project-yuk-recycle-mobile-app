package mitraservices

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetTermsAndConditionsDetail(t *testing.T) {
	TermsAndConditionsService := TermsAndConditionsService{}
	TermsAndConditionsInfoDetails := TermsAndConditionsService.GetTermsAndConditionsDetail()
	assert := assert.New(t)
	assert.True(len(TermsAndConditionsInfoDetails) > 0)
}
