package mitraservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"
	"time"

	"github.com/jinzhu/gorm"

	"github.com/stretchr/testify/mock"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
)

func TestActiveOrderService_GetOrderDetailData(t *testing.T) {
	tests := []struct {
		description                       string
		orderID                           uint
		mitraID                           uint
		returnOrderMock                   *models.Order
		returnCustomerMock                *models.Customer
		returnMitraLocationMock           *models.MitraLocation
		returnGarbageTypesMock            []models.GarbageType
		returnOrderDetailsMock            []models.OrderDetail
		returnOrderStatusHistoryMock      *models.OrderStatusHistory
		returnErrorOrderMock              error
		returnErrorCustomerMock           error
		returnErrorMitraLocationMock      error
		returnErrorGarbageTypesMock       error
		returnErrorOrderDetailsMock       error
		returnErrorOrderStatusHistoryMock error
		expectedOrderDetailResponse       *OrderDetailResponse
		expectedError                     error
	}{
		{
			description:                       "Orders error",
			orderID:                           1,
			mitraID:                           2,
			returnOrderMock:                   nil,
			returnCustomerMock:                nil,
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              errors.New("Error"),
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(BackendError),
		}, {
			description:                       "There are mitra access other mitra order",
			orderID:                           1,
			mitraID:                           2,
			returnOrderMock:                   &models.Order{MitraID: 3},
			returnCustomerMock:                nil,
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(BackendError),
		}, {
			description:                       "Customers error",
			orderID:                           1,
			mitraID:                           2,
			returnOrderMock:                   &models.Order{MitraID: 2, CustomerID: 3, LatestStatus: 1},
			returnCustomerMock:                nil,
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           errors.New("Error"),
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(BackendError),
		}, {
			description:                       "Mitra locations error",
			orderID:                           1,
			mitraID:                           2,
			returnOrderMock:                   &models.Order{MitraID: 2, CustomerID: 3, LatestStatus: 1},
			returnCustomerMock:                &models.Customer{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock:           nil,
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      errors.New("Error"),
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(BackendError),
		}, {
			description:                       "Garbage types error",
			orderID:                           1,
			mitraID:                           2,
			returnOrderMock:                   &models.Order{MitraID: 2, CustomerID: 3, LatestStatus: 1},
			returnCustomerMock:                &models.Customer{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock:           &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock:            nil,
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       errors.New("Error"),
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(BackendError),
		}, {
			description:             "Order details error",
			orderID:                 1,
			mitraID:                 2,
			returnOrderMock:         &models.Order{MitraID: 2, CustomerID: 3, LatestStatus: 1},
			returnCustomerMock:      &models.Customer{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock:            nil,
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       errors.New("Error"),
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(BackendError),
		}, {
			description:             "Order status histories error",
			orderID:                 1,
			mitraID:                 2,
			returnOrderMock:         &models.Order{MitraID: 2, CustomerID: 3, LatestStatus: 1},
			returnCustomerMock:      &models.Customer{Model: gorm.Model{ID: 3}},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock:      nil,
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: errors.New("Error"),
			expectedOrderDetailResponse:       nil,
			expectedError:                     errors.New(BackendError),
		}, {
			description: "Searching detail",
			orderID:     1,
			mitraID:     2,
			returnOrderMock: &models.Order{
				MitraID:            2,
				CustomerID:         3,
				LatestStatus:       1,
				Address:            "UI Depok",
				CancellationReason: "Mau cancel aja",
				CustomerLat:        -6.30786100,
				CustomerLon:        106.83883300,
			},
			returnCustomerMock:      &models.Customer{Model: gorm.Model{ID: 3}, Name: "Endrawan", Phone: "081234567895"},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock: &models.OrderStatusHistory{
				Model:         gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
				OrderID:       1,
				OrderStatusID: 1,
			},
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse: &OrderDetailResponse{
				ID:       1,
				Name:     "Endrawan",
				Address:  "UI Depok",
				Distance: 11868.265705783926,
				Price:    6000,
				GarbageDetail: []GarbageDetail{
					GarbageDetail{Title: "Sampah 1", Quantity: 6, QuantityType: "Kg"},
					GarbageDetail{Title: "Sampah 2", Quantity: 6, QuantityType: "Kg"},
				},
				Status:    "Searching",
				Time:      "Monday, 22 April 2019, 20:14",
				Reason:    "Mau cancel aja",
				Phone:     "081234567895",
				Latitude:  -6.30786100,
				Longitude: 106.83883300,
			},
			expectedError: nil,
		}, {
			description: "Scheduled detail",
			orderID:     1,
			mitraID:     2,
			returnOrderMock: &models.Order{
				MitraID:            2,
				CustomerID:         3,
				LatestStatus:       2,
				Address:            "UI Depok",
				CancellationReason: "Mau cancel aja",
				CustomerLat:        -6.30786100,
				CustomerLon:        106.83883300,
			},
			returnCustomerMock:      &models.Customer{Model: gorm.Model{ID: 3}, Name: "Endrawan", Phone: "081234567895"},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock: &models.OrderStatusHistory{
				Model:         gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
				OrderID:       1,
				OrderStatusID: 1,
			},
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse: &OrderDetailResponse{
				ID:       1,
				Name:     "Endrawan",
				Address:  "UI Depok",
				Distance: 11868.265705783926,
				Price:    6000,
				GarbageDetail: []GarbageDetail{
					GarbageDetail{Title: "Sampah 1", Quantity: 6, QuantityType: "Kg"},
					GarbageDetail{Title: "Sampah 2", Quantity: 6, QuantityType: "Kg"},
				},
				Status:    "Scheduled",
				Time:      "Monday, 22 April 2019, 20:14",
				Reason:    "Mau cancel aja",
				Phone:     "081234567895",
				Latitude:  -6.30786100,
				Longitude: 106.83883300,
			},
			expectedError: nil,
		}, {
			description: "Pick up detail",
			orderID:     1,
			mitraID:     2,
			returnOrderMock: &models.Order{
				MitraID:            2,
				CustomerID:         3,
				LatestStatus:       3,
				Address:            "UI Depok",
				CancellationReason: "Mau cancel aja",
				CustomerLat:        -6.30786100,
				CustomerLon:        106.83883300,
			},
			returnCustomerMock:      &models.Customer{Model: gorm.Model{ID: 3}, Name: "Endrawan", Phone: "081234567895"},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock: &models.OrderStatusHistory{
				Model:         gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
				OrderID:       1,
				OrderStatusID: 1,
			},
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse: &OrderDetailResponse{
				ID:       1,
				Name:     "Endrawan",
				Address:  "UI Depok",
				Distance: 11868.265705783926,
				Price:    6000,
				GarbageDetail: []GarbageDetail{
					GarbageDetail{Title: "Sampah 1", Quantity: 6, QuantityType: "Kg"},
					GarbageDetail{Title: "Sampah 2", Quantity: 6, QuantityType: "Kg"},
				},
				Status:    "Pick Up",
				Time:      "Monday, 22 April 2019, 20:14",
				Reason:    "Mau cancel aja",
				Phone:     "081234567895",
				Latitude:  -6.30786100,
				Longitude: 106.83883300,
			},
			expectedError: nil,
		}, {
			description: "Completed detail",
			orderID:     1,
			mitraID:     2,
			returnOrderMock: &models.Order{
				MitraID:            2,
				CustomerID:         3,
				LatestStatus:       4,
				Address:            "UI Depok",
				CancellationReason: "Mau cancel aja",
				CustomerLat:        -6.30786100,
				CustomerLon:        106.83883300,
			},
			returnCustomerMock:      &models.Customer{Model: gorm.Model{ID: 3}, Name: "Endrawan", Phone: "081234567895"},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock: &models.OrderStatusHistory{
				Model:         gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
				OrderID:       1,
				OrderStatusID: 1,
			},
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse: &OrderDetailResponse{
				ID:       1,
				Name:     "Endrawan",
				Address:  "UI Depok",
				Distance: 11868.265705783926,
				Price:    6000,
				GarbageDetail: []GarbageDetail{
					GarbageDetail{Title: "Sampah 1", Quantity: 6, QuantityType: "Kg"},
					GarbageDetail{Title: "Sampah 2", Quantity: 6, QuantityType: "Kg"},
				},
				Status:    "Completed",
				Time:      "Monday, 22 April 2019, 20:14",
				Reason:    "Mau cancel aja",
				Phone:     "081234567895",
				Latitude:  -6.30786100,
				Longitude: 106.83883300,
			},
			expectedError: nil,
		}, {
			description: "Cancelled detail",
			orderID:     1,
			mitraID:     2,
			returnOrderMock: &models.Order{
				MitraID:            2,
				CustomerID:         3,
				LatestStatus:       5,
				Address:            "UI Depok",
				CancellationReason: "Mau cancel aja",
				CustomerLat:        -6.30786100,
				CustomerLon:        106.83883300,
			},
			returnCustomerMock:      &models.Customer{Model: gorm.Model{ID: 3}, Name: "Endrawan", Phone: "081234567895"},
			returnMitraLocationMock: &models.MitraLocation{MitraID: 2},
			returnGarbageTypesMock: []models.GarbageType{
				models.GarbageType{Model: gorm.Model{ID: 1}, Title: "Sampah 1", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 2}, Title: "Sampah 2", QuantityType: "Kg"},
				models.GarbageType{Model: gorm.Model{ID: 3}, Title: "Sampah 3", QuantityType: "Kg"},
			},
			returnOrderDetailsMock: []models.OrderDetail{
				models.OrderDetail{Model: gorm.Model{ID: 1}, OrderID: 1, GarbageTypeID: 1, TotalAmount: 3000, Quantity: 6},
				models.OrderDetail{Model: gorm.Model{ID: 2}, OrderID: 1, GarbageTypeID: 2, TotalAmount: 3000, Quantity: 6},
			},
			returnOrderStatusHistoryMock: &models.OrderStatusHistory{
				Model:         gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
				OrderID:       1,
				OrderStatusID: 1,
			},
			returnErrorOrderMock:              nil,
			returnErrorCustomerMock:           nil,
			returnErrorMitraLocationMock:      nil,
			returnErrorGarbageTypesMock:       nil,
			returnErrorOrderDetailsMock:       nil,
			returnErrorOrderStatusHistoryMock: nil,
			expectedOrderDetailResponse: &OrderDetailResponse{
				ID:       1,
				Name:     "Endrawan",
				Address:  "UI Depok",
				Distance: 11868.265705783926,
				Price:    6000,
				GarbageDetail: []GarbageDetail{
					GarbageDetail{Title: "Sampah 1", Quantity: 6, QuantityType: "Kg"},
					GarbageDetail{Title: "Sampah 2", Quantity: 6, QuantityType: "Kg"},
				},
				Status:    "Cancelled",
				Time:      "Monday, 22 April 2019, 20:14",
				Reason:    "Mau cancel aja",
				Phone:     "081234567895",
				Latitude:  -6.30786100,
				Longitude: 106.83883300,
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetByID", mock.Anything).Return(tc.returnOrderMock, tc.returnErrorOrderMock)

		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		mockedCustomerRepository.On("GetByID", mock.Anything).Return(tc.returnCustomerMock, tc.returnErrorCustomerMock)

		mockedMitraLocationRepository := new(mockRepositories.IMitraLocationRepository)
		mockedMitraLocationRepository.On("GetByMitraId", mock.Anything).Return(tc.returnMitraLocationMock, tc.returnErrorMitraLocationMock)

		mockedGarbageTypeRepository := new(mockRepositories.IGarbageTypeRepository)
		mockedGarbageTypeRepository.On("GetAll", mock.Anything).Return(tc.returnGarbageTypesMock, tc.returnErrorGarbageTypesMock)

		mockedOrderDetailRepository := new(mockRepositories.IOrderDetailRepository)
		mockedOrderDetailRepository.On("GetByOrderID", mock.Anything).Return(tc.returnOrderDetailsMock, tc.returnErrorOrderDetailsMock)

		mockedOrderStatusHistoryRepository := new(mockRepositories.IOrderStatusHistoryRepository)
		mockedOrderStatusHistoryRepository.On("GetByOrderIDAndOrderStatusID", mock.Anything, mock.Anything).Return(tc.returnOrderStatusHistoryMock, tc.returnErrorOrderStatusHistoryMock)

		mockedOrderStatusRepository := new(mockRepositories.IOrderStatusRepository)
		mockedOrderStatusRepository.On("GetByID", uint(1)).Return(&models.OrderStatus{Description: "Searching"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(2)).Return(&models.OrderStatus{Description: "Scheduled"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(3)).Return(&models.OrderStatus{Description: "Pick Up"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(4)).Return(&models.OrderStatus{Description: "Completed"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(5)).Return(&models.OrderStatus{Description: "Cancelled"}, nil)

		orderDetailService := OrderDetailService{
			OrderRepository:              mockedOrderRepository,
			CustomerRepository:           mockedCustomerRepository,
			MitraLocationRepository:      mockedMitraLocationRepository,
			OrderDetailRepository:        mockedOrderDetailRepository,
			GarbageTypeRepository:        mockedGarbageTypeRepository,
			OrderStatusHistoryRepository: mockedOrderStatusHistoryRepository,
			OrderStatusRepository:        mockedOrderStatusRepository,
		}

		orderDetailResponse, err := orderDetailService.GetOrderDetailData(tc.orderID, tc.mitraID)

		assert.Equal(tc.expectedOrderDetailResponse, orderDetailResponse, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
