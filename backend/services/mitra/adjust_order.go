package mitraservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"
)

type AdjustOrderService struct {
	OrderRepository               repositories.IOrderRepository
	OrderDetailRepository         repositories.IOrderDetailRepository
	MitraGarbageSettingRepository repositories.IMitraGarbageSettingRepository
}

type OrderDetailRequest struct {
	GarbageTypeID uint `json:"garbage_type_id" validate:"required"`
	Quantity      uint `json:"quantity" validate:"required"`
}

type AdjustOrderRequest struct {
	OrderDetail []OrderDetailRequest `json:"order_detail" validate:"required"`
}

type IAdjustOrderService interface {
	AdjustOrder(orderID, mitraID uint, adjustOrderRequest *AdjustOrderRequest) error
}

func (a AdjustOrderService) AdjustOrder(orderID, mitraID uint, adjustOrderRequest *AdjustOrderRequest) error {
	order, err := a.OrderRepository.GetByID(orderID)

	if err != nil {
		return errors.New(BackendError)
	}

	if order.MitraID != mitraID {
		return errors.New(orderConstant.InvalidCorrespondingMitra)
	}

	if order.LatestStatus != orderConstant.PickedUp {
		return errors.New(orderConstant.UnmatchedCurrentOrderStatus)
	}

	orderDetails, err := a.OrderDetailRepository.GetByOrderID(orderID)

	if err != nil {
		return errors.New(BackendError)
	}

	mitraSettings, err := a.MitraGarbageSettingRepository.GetByMitraId(mitraID)

	if err != nil {
		return errors.New(BackendError)
	}

	garbageSettingsByGarbageTypeId := make(map[uint]models.MitraGarbageSetting)

	for _, mitraSetting := range mitraSettings {
		garbageSettingsByGarbageTypeId[mitraSetting.GarbageTypeID] = mitraSetting
	}

	garbageIDDoesExist := make(map[uint]bool)
	garbageIDToOrder := make(map[uint]models.OrderDetail)

	for _, orderDetail := range orderDetails {
		garbageIDDoesExist[orderDetail.GarbageTypeID] = true
		garbageIDToOrder[orderDetail.GarbageTypeID] = orderDetail
	}

	for _, orderDetail := range adjustOrderRequest.OrderDetail {
		priceSetByMitra := garbageSettingsByGarbageTypeId[orderDetail.GarbageTypeID].Price
		if _, exist := garbageIDDoesExist[orderDetail.GarbageTypeID]; !exist && orderDetail.Quantity > 0 {
			a.OrderDetailRepository.Save(
				&models.OrderDetail{
					OrderID:       orderID,
					GarbageTypeID: orderDetail.GarbageTypeID,
					TotalAmount:   int(priceSetByMitra * orderDetail.Quantity),
					Quantity:      int(orderDetail.Quantity),
				},
			)
		} else { // if row is exists, then its either update or remove
			if orderDetail.Quantity <= 0 { // remove if quantity = 0. checking range < 0 for security
				a.OrderDetailRepository.DeleteByID(garbageIDToOrder[orderDetail.GarbageTypeID].ID)
			} else { // update
				orderDetailRow := garbageIDToOrder[orderDetail.GarbageTypeID]
				orderDetailRow.TotalAmount = int(priceSetByMitra * orderDetail.Quantity)
				orderDetailRow.Quantity = int(orderDetail.Quantity)
				a.OrderDetailRepository.Save(&orderDetailRow)
			}
		}
	}

	return nil
}
