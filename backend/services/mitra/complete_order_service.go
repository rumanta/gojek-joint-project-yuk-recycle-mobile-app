package mitraservices

import (
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"
	"PPLA4/backend/services/notification"
	notificationServices "PPLA4/backend/services/notification"
	orderConstant "PPLA4/backend/services/order_constant"
	"errors"

	"github.com/NaySoftware/go-fcm"
)

type CompleteOrderService struct {
	OrderRepository     repositories.IOrderRepository
	OrderStatusHistory  repositories.IOrderStatusHistoryRepository
	NotificationService notification.INotificationService
	CustomerRepository  repositories.ICustomerRepository
	MitraRepository     repositories.IMitraRepository
}

type ICompleteOrderService interface {
	CompleteOrder(orderID, mitraID uint) error
}

func (p CompleteOrderService) CompleteOrder(orderID, mitraID uint) error {
	order, err := p.OrderRepository.GetByID(orderID)

	if err != nil {
		return err
	}

	if mitraID != order.MitraID {
		return errors.New(orderConstant.InvalidCorrespondingMitra)
	}

	if !orderConstant.IsPrecededStatus[orderConstant.Completed][order.LatestStatus] {
		return errors.New(orderConstant.InvalidOrderStatusTransition)
	}

	p.OrderStatusHistory.Save(
		&models.OrderStatusHistory{
			OrderID:       orderID,
			OrderStatusID: orderConstant.Completed,
		},
	)

	customer, _ := p.CustomerRepository.GetByID(order.CustomerID)

	p.NotificationService.SendNotification(
		[]string{customer.NotificationToken},
		&fcm.NotificationPayload{
			Title: "Yeay!",
			Body:  "Your order is complete",
		},
		&notificationServices.NotificationData{
			ClickAction: "FLUTTER_NOTIFICATION_CLICK",
			TypeData:    "COMPLETE_ORDER",
			OrderID:     order.ID,
			Response:    "ORDER COMPLETED",
		},
	)

	return nil
}
