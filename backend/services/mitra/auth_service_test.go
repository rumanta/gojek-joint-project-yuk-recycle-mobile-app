package mitraservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"errors"
	"testing"

	mockMiddleware "PPLA4/backend/mocks/middleware"
	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"

	"github.com/stretchr/testify/assert"
)

func TestLogin(t *testing.T) {
	tests := []struct {
		description       string
		phone             string
		password          string
		inputPhoneMock    string
		returnMitraMock   *models.Mitra
		returnErrorMock   error
		inputUserIDMock   uint
		inputUserTypeMock string
		expectedToken     *middleware.Token
		expectedError     error
	}{
		{
			description:     "Login when no Connection",
			phone:           "081804886455",
			password:        "abcdefghi",
			inputPhoneMock:  "081804886455",
			returnMitraMock: nil,
			returnErrorMock: errors.New("connection error"),
			expectedToken:   nil,
			expectedError:   errors.New(UsernameAndPasswordDoesntMatch),
		}, {
			description:     "Login when username not found",
			phone:           "081804886455",
			password:        "abcdefghi",
			inputPhoneMock:  "081804886455",
			returnMitraMock: nil,
			returnErrorMock: gorm.ErrRecordNotFound,
			expectedToken:   nil,
			expectedError:   errors.New(UsernameAndPasswordDoesntMatch),
		}, {
			description:     "Login given wrong password",
			phone:           "081804886455",
			password:        "abcdefghi",
			inputPhoneMock:  "081804886455",
			returnMitraMock: &models.Mitra{Phone: "081804886455", Password: "pqrstuvwxyz"},
			returnErrorMock: nil,
			expectedToken:   nil,
			expectedError:   errors.New(UsernameAndPasswordDoesntMatch),
		}, {
			description:       "Login given correct username and password",
			phone:             "081804886455",
			password:          "abcdefghi",
			inputPhoneMock:    "081804886455",
			returnMitraMock:   &models.Mitra{Phone: "081804886455", Password: "abcdefghi"},
			returnErrorMock:   nil,
			inputUserIDMock:   0,
			inputUserTypeMock: "mitra",
			expectedToken:     &middleware.Token{Value: "qazwsxedcrfv", ExpiresAt: "12345678"},
			expectedError:     nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		if tc.returnMitraMock != nil {
			hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(tc.returnMitraMock.Password), bcrypt.DefaultCost)
			tc.returnMitraMock.Password = string(hashedPassword)
		}
		mockedMitraRepository.On("GetByPhoneNumber", tc.inputPhoneMock).Return(tc.returnMitraMock, tc.returnErrorMock)

		mockedTokenCreation := new(mockMiddleware.ITokenCreation)
		mockedTokenCreation.On("CreateToken", tc.inputUserIDMock, tc.inputUserTypeMock, middleware.NormalLoginType).Return(tc.expectedToken)

		authService := AuthService{MitraRepository: mockedMitraRepository, TokenCreation: mockedTokenCreation}
		token, err := authService.Login(tc.phone, tc.password)

		assert.Equal(token, tc.expectedToken, tc.description)
		assert.Equal(err, tc.expectedError, tc.description)
	}
}
