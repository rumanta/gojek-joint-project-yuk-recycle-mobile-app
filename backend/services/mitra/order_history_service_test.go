package mitraservices

import (
	"PPLA4/backend/models"
	"errors"
	"testing"
	"time"

	"github.com/jinzhu/gorm"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
)

func TestOrderHistoryService_GetOrderHistoriessData(t *testing.T) {
	tests := []struct {
		description              string
		mitraID                  uint
		returnOrdersMock         []models.Order
		returnCustomersMock      []*models.Customer
		returnErrorOrdersMock    error
		returnErrorCustomersMock error
		expectedOrderHistories   []OrderHistory
		expectedError            error
	}{
		{
			description:              "Orders error",
			mitraID:                  uint(1),
			returnOrdersMock:         nil,
			returnCustomersMock:      nil,
			returnErrorOrdersMock:    errors.New("Error"),
			returnErrorCustomersMock: nil,
			expectedOrderHistories:   nil,
			expectedError:            errors.New(CanNotAccessDatabase),
		}, {
			description: "Customers error",
			mitraID:     uint(1),
			returnOrdersMock: []models.Order{
				models.Order{
					Model:        gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					CustomerID:   1,
					LatestStatus: 1,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				},
			},
			returnCustomersMock: []*models.Customer{
				&models.Customer{
					Model: gorm.Model{ID: 1},
					Name:  "Endrawan",
				},
			},
			returnErrorOrdersMock:    nil,
			returnErrorCustomersMock: errors.New("Error"),
			expectedOrderHistories:   nil,
			expectedError:            errors.New(CanNotAccessDatabase),
		}, {
			description: "Get data success",
			mitraID:     uint(1),
			returnOrdersMock: []models.Order{
				models.Order{
					Model:        gorm.Model{ID: 1, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					CustomerID:   1,
					LatestStatus: 4,
					ScheduledAt:  time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
				}, models.Order{
					Model:              gorm.Model{ID: 2, CreatedAt: time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC)},
					CustomerID:         2,
					LatestStatus:       5,
					ScheduledAt:        time.Date(2019, 4, 25, 13, 14, 15, 16, time.UTC),
					CancellationReason: "Gpp cancel aja",
				},
			},
			returnCustomersMock: []*models.Customer{
				&models.Customer{
					Model: gorm.Model{ID: 1},
					Name:  "Endrawan",
				}, &models.Customer{
					Model: gorm.Model{ID: 2},
					Name:  "Endrawan Andika",
				}, &models.Customer{
					Model: gorm.Model{ID: 3},
					Name:  "Endrawan Andika Wicaksana",
				},
			},
			returnErrorOrdersMock:    nil,
			returnErrorCustomersMock: nil,
			expectedOrderHistories: []OrderHistory{
				OrderHistory{
					ID:     1,
					Name:   "Endrawan",
					Time:   "Monday, 22 April 2019, 20:14",
					Status: "Completed",
				}, OrderHistory{
					ID:     2,
					Name:   "Endrawan Andika",
					Time:   "Monday, 22 April 2019, 20:14",
					Status: "Cancelled",
					Reason: "Gpp cancel aja",
				},
			},
			expectedError: nil,
		},
	}

	assert := assert.New(t)
	for _, tc := range tests {
		mockedOrderRepository := new(mockRepositories.IOrderRepository)
		mockedOrderRepository.On("GetLatest20OrderHistoriesByMitraID", tc.mitraID).Return(tc.returnOrdersMock, tc.returnErrorOrdersMock)

		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		for _, customer := range tc.returnCustomersMock {
			mockedCustomerRepository.On("GetByID", customer.ID).Return(customer, tc.returnErrorCustomersMock)
		}

		mockedOrderStatusRepository := new(mockRepositories.IOrderStatusRepository)
		mockedOrderStatusRepository.On("GetByID", uint(4)).Return(&models.OrderStatus{Description: "Completed"}, nil)
		mockedOrderStatusRepository.On("GetByID", uint(5)).Return(&models.OrderStatus{Description: "Cancelled"}, nil)

		orderHistoryService := OrderHistoryService{
			OrderRepository:       mockedOrderRepository,
			CustomerRepository:    mockedCustomerRepository,
			OrderStatusRepository: mockedOrderStatusRepository,
		}

		activeOrders, err := orderHistoryService.GetOrderHistoriesData(tc.mitraID)

		assert.Equal(tc.expectedOrderHistories, activeOrders, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
	}
}
