package mitraservices

import (
	"PPLA4/backend/repositories"
	"errors"
	"time"
)

type OrderHistoryService struct {
	OrderRepository       repositories.IOrderRepository
	CustomerRepository    repositories.ICustomerRepository
	OrderStatusRepository repositories.IOrderStatusRepository
}

type IOrderHistoryService interface {
	GetOrderHistoriesData(mitraId uint) ([]OrderHistory, error)
}

type OrderHistory struct {
	ID     uint   `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Time   string `json:"time,omitempty"`
	Status string `json:"status,omitempty"`
	Reason string `json:"reason,omitempty"`
}

func (a OrderHistoryService) GetOrderHistoriesData(mitraID uint) ([]OrderHistory, error) {
	orders, err := a.OrderRepository.GetLatest20OrderHistoriesByMitraID(mitraID)

	if err != nil {
		return nil, errors.New(CanNotAccessDatabase)
	}

	orderHistories := make([]OrderHistory, len(orders))

	for i, order := range orders {
		customer, err := a.CustomerRepository.GetByID(order.CustomerID)

		if err != nil {
			return nil, errors.New(CanNotAccessDatabase)
		}

		status, _ := a.OrderStatusRepository.GetByID(uint(order.LatestStatus))

		orderHistories[i] = OrderHistory{
			ID:     order.ID,
			Name:   customer.Name,
			Time:   order.CreatedAt.Add(time.Hour * time.Duration(7)).Format("Monday, 02 January 2006, 15:04"),
			Status: status.Description,
			Reason: order.CancellationReason,
		}
	}

	return orderHistories, nil
}
