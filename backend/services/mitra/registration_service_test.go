package mitraservices

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"errors"
	"reflect"
	"testing"

	"github.com/jinzhu/gorm"

	mockMiddleware "PPLA4/backend/mocks/middleware"
	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestRegister(t *testing.T) {
	tests := []struct {
		description        string
		phone              string
		password           string
		name               string
		email              string
		returnErrorOnEmail error
		returnErrorOnPhone error
		returnMitraMock    *models.Mitra
		returnErrorMock    error
		inputUserIDMock    uint
		inputUserTypeMock  string
		expectedToken      *middleware.Token
		expectedError      error
	}{
		{
			description:        "Register error: email already exists",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnEmail: errors.New(emailAlreadyExists),
			returnErrorOnPhone: gorm.ErrRecordNotFound,
			expectedToken:      nil,
			expectedError:      errors.New(emailAlreadyExists),
		},
		{
			description:        "Register error: phone already exists",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnPhone: errors.New(phoneAlreadyExists),
			returnErrorOnEmail: gorm.ErrRecordNotFound,
			expectedToken:      nil,
			expectedError:      errors.New(phoneAlreadyExists),
		},
		{
			description:        "Register still error after checking email and phone",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnPhone: gorm.ErrRecordNotFound,
			returnErrorOnEmail: gorm.ErrRecordNotFound,
			returnMitraMock:    nil,
			returnErrorMock:    errors.New("Cannot create mitra record"),
			inputUserIDMock:    0,
			inputUserTypeMock:  "mitra",
			expectedToken:      nil,
			expectedError:      errors.New("Cannot create mitra record"),
		},
		{
			description:        "Register success",
			phone:              "08123456789",
			password:           "abcdefghij",
			name:               "Yusuf Sholeh",
			email:              "yusuf_sholeh@gmail.com",
			returnErrorOnPhone: gorm.ErrRecordNotFound,
			returnErrorOnEmail: gorm.ErrRecordNotFound,
			returnMitraMock:    &models.Mitra{Phone: "08123456789", Password: "abcdefghij", Email: "yusuf_sholeh@gmail.com"},
			returnErrorMock:    nil,
			inputUserIDMock:    0,
			inputUserTypeMock:  "mitra",
			expectedToken:      &middleware.Token{Value: "qazwsxedcrfv", ExpiresAt: "12345678"},
			expectedError:      nil,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedMitraRepository.On("GetByPhoneNumber", tc.phone).Return(&models.Mitra{}, tc.returnErrorOnPhone)
		mockedMitraRepository.On("GetByEmail", tc.email).Return(&models.Mitra{}, tc.returnErrorOnEmail)
		mockedMitraRepository.On("Add", mock.MatchedBy(func(m *models.Mitra) bool {
			return reflect.DeepEqual(m, &models.Mitra{Phone: tc.phone, Password: tc.password, Name: tc.name, Email: tc.email})
		})).Return(tc.returnMitraMock, tc.returnErrorMock)
		mockedGarbageTypeRepository := new(mockRepositories.IGarbageTypeRepository)
		mockedGarbageTypeRepository.On("GetAll").Return([]models.GarbageType{models.GarbageType{}}, nil)
		mockedMitraGarbageSettingRepository := new(mockRepositories.IMitraGarbageSettingRepository)
		mockedMitraGarbageSettingRepository.On("Save", mock.Anything).Return(&models.MitraGarbageSetting{}, nil)
		mockedMitraPickUpDistanceRepository := new(mockRepositories.IMitraPickUpDistanceRepository)
		mockedMitraPickUpDistanceRepository.On("Save", mock.Anything).Return(&models.MitraPickUpDistance{}, nil)
		mockedMitraPickUpScheduleRepository := new(mockRepositories.IMitraPickUpScheduleRepository)
		mockedMitraPickUpScheduleRepository.On("Save", mock.Anything).Return(&models.MitraPickUpSchedule{}, nil)
		mockedTokenCreation := new(mockMiddleware.ITokenCreation)
		mockedTokenCreation.On("CreateToken", tc.inputUserIDMock, tc.inputUserTypeMock, middleware.NormalLoginType).Return(tc.expectedToken)
		registrationService := RegistrationService{
			MitraRepository:               mockedMitraRepository,
			GarbageTypeRepository:         mockedGarbageTypeRepository,
			MitraGarbageSettingRepository: mockedMitraGarbageSettingRepository,
			MitraPickUpDistanceRepository: mockedMitraPickUpDistanceRepository,
			MitraPickUpScheduleRepository: mockedMitraPickUpScheduleRepository,
			TokenCreation:                 mockedTokenCreation,
		}
		token, err := registrationService.Register(tc.phone, tc.password, tc.name, tc.email)
		assert.Equal(tc.expectedToken, token, tc.description)
		assert.Equal(tc.expectedError, err, tc.description)
		if tc.expectedError == nil {
			mockedMitraGarbageSettingRepository.AssertCalled(t, "Save", mock.Anything)
			mockedMitraPickUpDistanceRepository.AssertCalled(t, "Save", mock.Anything)
			mockedMitraPickUpScheduleRepository.AssertCalled(t, "Save", mock.Anything)
		}
	}
}
