package notification

import (
	"testing"

	fcm "github.com/NaySoftware/go-fcm"
)

func TestNotificationService_SendNotification(t *testing.T) {
	type args struct {
		clientTokens []string
		payload      *fcm.NotificationPayload
		data         *NotificationData
	}
	tests := []struct {
		description string
		args        args
		want        *fcm.FcmResponseStatus
		wantErr     bool
	}{
		{
			description: "Send Notification Error",
			args: args{
				clientTokens: []string{},
				payload:      &fcm.NotificationPayload{},
				data:         &NotificationData{},
			},
			want:    nil,
			wantErr: true,
		},
	}
	n := new(NotificationService)
	for _, tt := range tests {
		status, _ := n.SendNotification(tt.args.clientTokens, tt.args.payload, tt.args.data)
		if status.Fail == 10 {
			t.Errorf("NotificationService.SendNotification() fail should be 0")
		}
	}
}
