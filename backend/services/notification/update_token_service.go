package notification

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"PPLA4/backend/repositories"

	"github.com/jinzhu/gorm"
)

type UpdateTokenService struct {
	MitraRepository    repositories.IMitraRepository
	CustomerRepository repositories.ICustomerRepository
}

type IUpdateTokenService interface {
	UpdateToken(userID uint, userType string, token string) error
}

func (u UpdateTokenService) UpdateToken(userID uint, userType string, token string) error {
	if userType == middleware.MitraUserType {
		mitra := &models.Mitra{Model: gorm.Model{ID: userID}, NotificationToken: token}
		_, err := u.MitraRepository.Update(mitra)
		return err
	} else {
		customer := &models.Customer{Model: gorm.Model{ID: userID}, NotificationToken: token}
		_, err := u.CustomerRepository.Update(customer)
		return err
	}
}
