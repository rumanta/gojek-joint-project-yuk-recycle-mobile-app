package notification

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"testing"

	mockRepositories "PPLA4/backend/mocks/repositories"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestUpdateToken(t *testing.T) {
	tests := []struct {
		description     string
		userID          uint
		userType        string
		token           string
		returnErrorMock error
		expectedError   error
	}{
		{
			description:     "Mitra Update Success",
			userID:          10,
			userType:        middleware.MitraUserType,
			token:           "abcdef",
			returnErrorMock: nil,
			expectedError:   nil,
		}, {
			description:     "Customer Update Success",
			userID:          10,
			userType:        middleware.CustomerUserType,
			token:           "abcdef",
			returnErrorMock: nil,
			expectedError:   nil,
		},
	}
	assert := assert.New(t)
	for _, tc := range tests {
		mockedMitraRepository := new(mockRepositories.IMitraRepository)
		mockedCustomerRepository := new(mockRepositories.ICustomerRepository)
		mitra := &models.Mitra{Model: gorm.Model{ID: tc.userID}, NotificationToken: tc.token}
		customer := &models.Customer{Model: gorm.Model{ID: tc.userID}, NotificationToken: tc.token}
		mockedMitraRepository.On("Update", mitra).Return(mitra, tc.returnErrorMock)
		mockedCustomerRepository.On("Update", customer).Return(customer, tc.returnErrorMock)
		updateTokenService := UpdateTokenService{
			MitraRepository:    mockedMitraRepository,
			CustomerRepository: mockedCustomerRepository,
		}
		err := updateTokenService.UpdateToken(tc.userID, tc.userType, tc.token)
		assert.Equal(err, tc.expectedError, tc.description)
	}
}
