package notification

import (
	"os"

	fcm "github.com/NaySoftware/go-fcm"
)

type NotificationService struct {
}

type INotificationService interface {
	SendNotification(clientTokens []string, payload *fcm.NotificationPayload, data *NotificationData) (*fcm.FcmResponseStatus, error)
}

// NotificationData our struct to configure data on notification
type NotificationData struct {
	ClickAction   string           `json:"click_action,omitempty"`
	TypeData      string           `json:"type,omitempty"`
	Token         uint             `json:"token"`
	OrderID       uint             `json:"order_id"`
	Name          string           `json:"name"`
	Address       string           `json:"address"`
	Distance      float64          `json:"distance"`
	GarbageDetail []*GarbageDetail `json:"garbage_detail"`
	Price         uint             `json:"price"`
	Response      string           `json:"response"`
	Notes         string           `json:"notes"`
}

type GarbageDetail struct {
	Title        string `json:"title"`
	Quantity     uint   `json:"quantity"`
	QuantityType string `json:"quantity_type"`
}

// SendNotification pass a slice, payload, and data
func (n NotificationService) SendNotification(clientTokens []string,
	payload *fcm.NotificationPayload,
	data *NotificationData) (*fcm.FcmResponseStatus, error) {

	fcmClient := fcm.NewFcmClient(os.Getenv("fcm_server_key"))
	fcmClient.SetNotificationPayload(payload)
	fcmClient.SetMsgData(data)
	fcmClient.AppendDevices(clientTokens)
	status, err := fcmClient.Send()
	status.PrintResults() // logging
	return status, err
}
