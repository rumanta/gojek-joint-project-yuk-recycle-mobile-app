package worker

import (
	"PPLA4/backend/models"
	services "PPLA4/backend/services/customer"
	notificationServices "PPLA4/backend/services/notification"
	"fmt"

	"github.com/NaySoftware/go-fcm"
)

type MakeOrderWorker struct {
	MakeOrderService     services.IMakeOrderServices
	NotificationServices notificationServices.INotificationService
}

type IMakeOrderWorker interface {
	Work(makeOrderRequest *services.MakeOrderRequest) (*models.Mitra, error)
}

func (m MakeOrderWorker) Work(makeOrderRequest *services.MakeOrderRequest) (*models.Mitra, error) {
	go func() {
		_, order, customer, err := m.MakeOrderService.MakeOrder(makeOrderRequest)
		if err != nil {
			// push notification
			fmt.Println(err.Error(), ". Customer's id: ", makeOrderRequest.CustomerID) // logging
			m.NotificationServices.SendNotification(
				[]string{customer.NotificationToken},
				&fcm.NotificationPayload{
					Title: "We're sorry!",
					Body:  err.Error(),
				},
				&notificationServices.NotificationData{
					ClickAction: "FLUTTER_NOTIFICATION_CLICK",
					TypeData:    "MAKE_ORDER",
					Response:    "YOUR LAST ORDER HAS FAILED",
				},
			)

		} else {
			// push notification
			m.NotificationServices.SendNotification(
				[]string{customer.NotificationToken},
				&fcm.NotificationPayload{
					Title: "We've found mitra for you!",
					Body:  "Your order has been scheduled to our mitra",
				},
				&notificationServices.NotificationData{
					ClickAction: "FLUTTER_NOTIFICATION_CLICK",
					TypeData:    "MAKE_ORDER",
					OrderID:     order.ID,
					Response:    "ORDER SUCCEDED",
				},
			)
		}

	}()
	return nil, nil
}
