package worker

import (
	mockServices "PPLA4/backend/mocks/services/customer"
	mockNotification "PPLA4/backend/mocks/services/notification"
	"PPLA4/backend/models"
	services "PPLA4/backend/services/customer"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestWorkWhenMakeOrderErrorReturnError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	makeOrderRequest := services.MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []services.MakeOrderDetailRequest{
			services.MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			services.MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mockedMakeOrderService := new(mockServices.IMakeOrderServices)
	mockedMakeOrderService.On("MakeOrder", mock.Anything).Return(
		nil,
		nil,
		&models.Customer{
			Model:             gorm.Model{ID: customerID},
			Name:              "muhammad yusuf sholeh",
			NotificationToken: "aab",
		},
		errors.New(services.NoServicesAvailable),
	)
	mockedNotificationServices := new(mockNotification.INotificationService)
	mockedNotificationServices.On("SendNotification", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

	makeOrderWorker := MakeOrderWorker{
		MakeOrderService:     mockedMakeOrderService,
		NotificationServices: mockedNotificationServices,
	}

	var err error
	func() {
		_, err := makeOrderWorker.Work(&makeOrderRequest)
		assert := assert.New(t)
		assert.Nil(err)
		time.Sleep(1500 * time.Millisecond)
	}()
	assert := assert.New(t)
	assert.Nil(err)
}

func TestWorkWhenMakeOrderErrorReturnNoError(t *testing.T) {
	customerID := uint(55)
	customerLat := float64(5)
	customerLon := float64(5)
	garbageTypeID1 := uint(1)
	quantity1 := uint(1)
	garbageTypeID2 := uint(2)
	quantity2 := uint(2)
	makeOrderRequest := services.MakeOrderRequest{
		CustomerID:  customerID,
		CustomerLat: customerLat,
		CustomerLon: customerLon,
		OrderDetailRequest: []services.MakeOrderDetailRequest{
			services.MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID1,
				Quantity:      quantity1,
			},
			services.MakeOrderDetailRequest{
				GarbageTypeID: garbageTypeID2,
				Quantity:      quantity2,
			},
		},
	}

	mockedNotificationServices := new(mockNotification.INotificationService)
	mockedNotificationServices.On("SendNotification", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

	mockedMakeOrderService := new(mockServices.IMakeOrderServices)
	mockedMakeOrderService.On("MakeOrder", mock.Anything).Return(
		&models.Mitra{
			Name: "endrawan andika wicaksana",
		},
		&models.Order{
			Model: gorm.Model{ID: 1},
		},
		&models.Customer{
			Model:             gorm.Model{ID: customerID},
			Name:              "muhammad yusuf sholeh",
			NotificationToken: "aab",
		},
		nil,
	)

	makeOrderWorker := MakeOrderWorker{
		MakeOrderService:     mockedMakeOrderService,
		NotificationServices: mockedNotificationServices,
	}

	var err error
	func() {
		_, err := makeOrderWorker.Work(&makeOrderRequest)
		assert := assert.New(t)
		assert.Nil(err)
		time.Sleep(1500 * time.Millisecond)
	}()
	assert := assert.New(t)
	assert.Nil(err)
}
