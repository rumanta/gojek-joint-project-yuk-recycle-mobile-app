package middleware

import (
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	ApplicationName         = "yuk-recycle"
	MitraUserType           = "mitra"
	CustomerUserType        = "customer"
	NormalLoginType         = 1
	OAuthLoginType          = 2
	LoginExpirationDuration = time.Duration(1) * time.Minute
)

type Claims struct {
	jwt.StandardClaims
	UserID    uint
	UserType  string
	LoginType int
}

// Token : for JSON response. Value is hashed jwt token
type Token struct {
	Value     string `json:"value"`
	ExpiresAt string `json:"expires_at"`
}

// TokenCreation A class that is responsible for creating token, implements ITokenCreation
type TokenCreation struct {
}

// CreateToken create an access token given UserID and UserType
func (t TokenCreation) CreateToken(userID uint, userType string, loginType int) *Token {
	expiresAt := time.Now().AddDate(1, 0, 0)
	// expiresAt := time.Now().Add(LoginExpirationDuration) TODO: expire token for front end
	tk := &Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    ApplicationName,
			ExpiresAt: expiresAt.Unix(),
		},
		UserID:    userID,
		UserType:  userType,
		LoginType: loginType,
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))

	return &Token{Value: tokenString, ExpiresAt: expiresAt.Format(time.RFC3339)}
}

// ITokenCreation interface for TokenCreation
type ITokenCreation interface {
	CreateToken(userID uint, userType string, loginType int) *Token
}
