package middleware

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
)

func TestJWTAuthenticationWhenNoAuthenticationNeededThenReturnStatusOK(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	req := httptest.NewRequest("GET", "/api/v1/mitra/new", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code, "Code should be 200")
}

func TestJWTAuthenticationWhenNoParamAuthorizationThenReturnStatusForbidden(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	req := httptest.NewRequest("GET", "/api/v1/mitra/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}

func TestJWTAuthenticationWhenNoBearerAtValueThenReturnStatusForbidden(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	req := httptest.NewRequest("GET", "/api/v1/mitra/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "ABCDEF")
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}

func TestJWTAuthenticationGivenWrongTokenThenReturnStatusForbidden(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	req := httptest.NewRequest("GET", "/api/v1/mitra/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer ABCDEF")
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}

func TestJWTAuthenticationGivenErrorAtClaimThenReturnStatusForbidden(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	req := httptest.NewRequest("GET", "/api/v1/mitra/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer ABCDEF")
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}

type testClaims struct {
	jwt.StandardClaims
}

func TestJWTAuthenticationGivenTokenIsExpiredThenReturnStatusForbidden(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	tk := &testClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    ApplicationName,
			ExpiresAt: time.Now().AddDate(-1, 0, 0).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	req := httptest.NewRequest("GET", "/api/v1/customer/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+tokenString)
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}

func TestJWTAuthenticationGivenWrongTokenPassword(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	tk := &testClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    ApplicationName,
			ExpiresAt: time.Now().AddDate(1, 0, 0).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte("qazwsxedcrfvtgbyhnujmiklop"))
	req := httptest.NewRequest("GET", "/api/v1/customer/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+tokenString)
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}

func TestJWTAuthenticationWhenMitraRequestToCustomerEndPointThenReturnStatusForbidden(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	tokenCreation := new(TokenCreation)
	token := tokenCreation.CreateToken(uint(123456789), "customer", 1)
	req := httptest.NewRequest("GET", "/api/v1/mitra/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token.Value)
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusForbidden, rr.Code)
}

func TestJWTAuthenticationGivenCorrectTokenThenReturnStatusOK(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	handlerToTest := JwtAuthentication(nextHandler)
	tokenCreation := new(TokenCreation)
	token := tokenCreation.CreateToken(uint(123456789), "customer", 1)
	req := httptest.NewRequest("GET", "/api/v1/customer/dummy", nil)
	rr := httptest.NewRecorder()
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token.Value)
	handlerToTest.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
}
