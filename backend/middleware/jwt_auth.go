package middleware

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

const (
	missingToken  = "Missing authorization token"
	invalidFormat = "Invalid token format"
)

type AuthError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

func writeMiddleware(w http.ResponseWriter, data *AuthError) {
	w.WriteHeader(http.StatusForbidden)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func JwtAuthentication(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		//List of endpoints that doesn't require auth
		notAuth := []string{
			"/api/v1/mitra/new", "/api/v1/mitra/login",
			"/api/v1/customer/new", "/api/v1/customer/login", "/api/v1/customer/oauth/google",
			"/api/v1/notification/dummy",
		}

		requestPath := r.URL.Path //current request path

		//check if request does not need authentication, serve the request if it doesn't need it
		for _, value := range notAuth {
			if value == requestPath {
				next.ServeHTTP(w, r)
				return
			}
		}

		tokenHeader := r.Header.Get("Authorization") // Grab the token from the header

		if tokenHeader == "" { // Token is missing, returns with error code 403 Unauthorized
			writeMiddleware(w, &AuthError{Code: "forbidden", Message: missingToken})
			return
		}

		splitted := strings.Split(tokenHeader, " ") // The token normally comes in format `Bearer {token-body}`, we check if the retrieved token matched this requirement
		if len(splitted) != 2 || strings.ToLower(splitted[0]) != "bearer" {
			writeMiddleware(w, &AuthError{Code: "forbidden", Message: invalidFormat})
			return
		}

		tokenPart := splitted[1] // Grab the token part, what we are truly interested in
		tk := &Claims{}

		token, err := jwt.ParseWithClaims(tokenPart, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("token_password")), nil
		})

		if token != nil && !token.Valid { //Token is invalid, maybe not signed on this server
			writeMiddleware(w, &AuthError{Code: "invalid_token", Message: "token is not valid or expired"})
			return
		}

		if err != nil { // Malformed token, returns with http code 403 as usual
			w.WriteHeader(http.StatusForbidden)
			writeMiddleware(w, &AuthError{Code: "malformed_token", Message: err.Error()})
			return
		}

		// invalid user type. Scenario: customer request to mitra, or mitra request to customer.
		if (strings.Contains(requestPath, MitraUserType) && tk.UserType == CustomerUserType) ||
			(strings.Contains(requestPath, CustomerUserType) && tk.UserType == MitraUserType) {
			writeMiddleware(w, &AuthError{Code: "malformed_token", Message: "malformed token"})
			return
		}

		// Everything went well, proceed to next step
		fmt.Println(requestPath) // logging
		fmt.Println("UserID : ", tk.UserID)
		fmt.Println("UserType : ", tk.UserType)
		r = r.WithContext(context.WithValue(r.Context(), "UserID", tk.UserID))
		r = r.WithContext(context.WithValue(r.Context(), "UserType", tk.UserType))
		r = r.WithContext(context.WithValue(r.Context(), "LoginType", tk.LoginType))
		next.ServeHTTP(w, r) //proceed in the middleware chain!
	})
}
