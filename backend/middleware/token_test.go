package middleware

import (
	"os"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
)

func TestCreateToken(t *testing.T) {
	userID := uint(12345678)
	userType := "mitra"
	loginType := 1
	tokenCreation := new(TokenCreation)
	token := tokenCreation.CreateToken(userID, userType, loginType)

	tk := &Claims{}
	_, err := jwt.ParseWithClaims(token.Value, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("token_password")), nil
	})

	if err != nil {
		t.Errorf("There is unexpected error, %s", err.Error())
		return
	}

	assert.Equal(t, tk.UserID, userID, "User id should be equal")
	assert.Equal(t, tk.UserType, userType, "User type should be equal")
	assert.Equal(t, tk.LoginType, loginType, "Login type should be equal")
}
