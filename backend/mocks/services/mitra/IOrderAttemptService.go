// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// IOrderAttemptService is an autogenerated mock type for the IOrderAttemptService type
type IOrderAttemptService struct {
	mock.Mock
}

// ApproveOrderAttempt provides a mock function with given fields: orderID, mitraID, token
func (_m *IOrderAttemptService) ApproveOrderAttempt(orderID uint, mitraID uint, token uint) error {
	ret := _m.Called(orderID, mitraID, token)

	var r0 error
	if rf, ok := ret.Get(0).(func(uint, uint, uint) error); ok {
		r0 = rf(orderID, mitraID, token)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CreateOrderAttempt provides a mock function with given fields: orderID, mitraID, token
func (_m *IOrderAttemptService) CreateOrderAttempt(orderID uint, mitraID uint, token uint) error {
	ret := _m.Called(orderID, mitraID, token)

	var r0 error
	if rf, ok := ret.Get(0).(func(uint, uint, uint) error); ok {
		r0 = rf(orderID, mitraID, token)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RejectOrderAttempt provides a mock function with given fields: orderID, mitraID, token
func (_m *IOrderAttemptService) RejectOrderAttempt(orderID uint, mitraID uint, token uint) error {
	ret := _m.Called(orderID, mitraID, token)

	var r0 error
	if rf, ok := ret.Get(0).(func(uint, uint, uint) error); ok {
		r0 = rf(orderID, mitraID, token)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
