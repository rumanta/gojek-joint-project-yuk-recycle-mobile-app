// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import mitraservices "PPLA4/backend/services/mitra"
import mock "github.com/stretchr/testify/mock"
import time "time"

// IHomepageService is an autogenerated mock type for the IHomepageService type
type IHomepageService struct {
	mock.Mock
}

// GetHomepageData provides a mock function with given fields: mitraID
func (_m *IHomepageService) GetHomepageData(mitraID uint) (*mitraservices.HomepageData, error) {
	ret := _m.Called(mitraID)

	var r0 *mitraservices.HomepageData
	if rf, ok := ret.Get(0).(func(uint) *mitraservices.HomepageData); ok {
		r0 = rf(mitraID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*mitraservices.HomepageData)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint) error); ok {
		r1 = rf(mitraID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetMitraName provides a mock function with given fields: mitraID
func (_m *IHomepageService) GetMitraName(mitraID uint) (string, error) {
	ret := _m.Called(mitraID)

	var r0 string
	if rf, ok := ret.Get(0).(func(uint) string); ok {
		r0 = rf(mitraID)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint) error); ok {
		r1 = rf(mitraID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetOneWeekFromDate provides a mock function with given fields: t
func (_m *IHomepageService) GetOneWeekFromDate(t time.Time) (time.Time, time.Time) {
	ret := _m.Called(t)

	var r0 time.Time
	if rf, ok := ret.Get(0).(func(time.Time) time.Time); ok {
		r0 = rf(t)
	} else {
		r0 = ret.Get(0).(time.Time)
	}

	var r1 time.Time
	if rf, ok := ret.Get(1).(func(time.Time) time.Time); ok {
		r1 = rf(t)
	} else {
		r1 = ret.Get(1).(time.Time)
	}

	return r0, r1
}

// GetOnlineMode provides a mock function with given fields: mitraID
func (_m *IHomepageService) GetOnlineMode(mitraID uint) (bool, error) {
	ret := _m.Called(mitraID)

	var r0 bool
	if rf, ok := ret.Get(0).(func(uint) bool); ok {
		r0 = rf(mitraID)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint) error); ok {
		r1 = rf(mitraID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWeeklyOrderStatistics provides a mock function with given fields: mitraID
func (_m *IHomepageService) GetWeeklyOrderStatistics(mitraID uint) (*mitraservices.OrderStatistic, error) {
	ret := _m.Called(mitraID)

	var r0 *mitraservices.OrderStatistic
	if rf, ok := ret.Get(0).(func(uint) *mitraservices.OrderStatistic); ok {
		r0 = rf(mitraID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*mitraservices.OrderStatistic)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint) error); ok {
		r1 = rf(mitraID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
