// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import middleware "PPLA4/backend/middleware"

import mock "github.com/stretchr/testify/mock"

// IAuthService is an autogenerated mock type for the IAuthService type
type IAuthService struct {
	mock.Mock
}

// Login provides a mock function with given fields: phone, password
func (_m *IAuthService) Login(phone string, password string) (*middleware.Token, error) {
	ret := _m.Called(phone, password)

	var r0 *middleware.Token
	if rf, ok := ret.Get(0).(func(string, string) *middleware.Token); ok {
		r0 = rf(phone, password)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*middleware.Token)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(phone, password)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
