// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import fcm "github.com/NaySoftware/go-fcm"
import mock "github.com/stretchr/testify/mock"
import notification "PPLA4/backend/services/notification"

// INotificationService is an autogenerated mock type for the INotificationService type
type INotificationService struct {
	mock.Mock
}

// SendNotification provides a mock function with given fields: clientTokens, payload, data
func (_m *INotificationService) SendNotification(clientTokens []string, payload *fcm.NotificationPayload, data *notification.NotificationData) (*fcm.FcmResponseStatus, error) {
	ret := _m.Called(clientTokens, payload, data)

	var r0 *fcm.FcmResponseStatus
	if rf, ok := ret.Get(0).(func([]string, *fcm.NotificationPayload, *notification.NotificationData) *fcm.FcmResponseStatus); ok {
		r0 = rf(clientTokens, payload, data)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*fcm.FcmResponseStatus)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]string, *fcm.NotificationPayload, *notification.NotificationData) error); ok {
		r1 = rf(clientTokens, payload, data)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
