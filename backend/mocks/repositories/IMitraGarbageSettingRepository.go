// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"
import models "PPLA4/backend/models"

// IMitraGarbageSettingRepository is an autogenerated mock type for the IMitraGarbageSettingRepository type
type IMitraGarbageSettingRepository struct {
	mock.Mock
}

// GetByMitraId provides a mock function with given fields: mitraId
func (_m *IMitraGarbageSettingRepository) GetByMitraId(mitraId uint) ([]models.MitraGarbageSetting, error) {
	ret := _m.Called(mitraId)

	var r0 []models.MitraGarbageSetting
	if rf, ok := ret.Get(0).(func(uint) []models.MitraGarbageSetting); ok {
		r0 = rf(mitraId)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.MitraGarbageSetting)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint) error); ok {
		r1 = rf(mitraId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Save provides a mock function with given fields: mitraGarbageSetting
func (_m *IMitraGarbageSettingRepository) Save(mitraGarbageSetting *models.MitraGarbageSetting) (*models.MitraGarbageSetting, error) {
	ret := _m.Called(mitraGarbageSetting)

	var r0 *models.MitraGarbageSetting
	if rf, ok := ret.Get(0).(func(*models.MitraGarbageSetting) *models.MitraGarbageSetting); ok {
		r0 = rf(mitraGarbageSetting)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.MitraGarbageSetting)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*models.MitraGarbageSetting) error); ok {
		r1 = rf(mitraGarbageSetting)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
