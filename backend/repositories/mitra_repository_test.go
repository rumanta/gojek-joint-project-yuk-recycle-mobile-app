package repositories

import (
	"PPLA4/backend/models"
	"database/sql/driver"
	"fmt"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

type AnyTime struct{} // time matching

func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

type AnyString struct{}

func (a AnyString) Match(v driver.Value) bool {
	_, ok := v.(string)
	return ok
}

func TestMitraGetByID(t *testing.T) {
	var mitraFieldNames = []string{"id", "phone", "password"}
	rows := sqlmock.NewRows(mitraFieldNames)
	rows.AddRow("1", "0812345678", "abcdefghi")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)

	// SELECT * FROM "mitra" WHERE "mitra"."deleted_at" IS NULL AND ((phone = $1)) ORDER BY "mitra"."id" ASC LIMIT 1
	mock.ExpectQuery("^SELECT (.+) FROM \"mitras\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	mitraRepo := new(MitraRepository)
	mitra, _ := mitraRepo.GetByID(1)

	assert := assert.New(t)
	assert.Equal("0812345678", mitra.Phone, "Username should be equal")
	assert.Equal("abcdefghi", mitra.Password, "Password should be equal")
}

func TestMitraGetByPhoneNumber(t *testing.T) {
	var mitraFieldNames = []string{"id", "phone", "password"}
	rows := sqlmock.NewRows(mitraFieldNames)
	rows.AddRow("1", "0812345678", "abcdefghi")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)

	// SELECT * FROM "mitra" WHERE "mitra"."deleted_at" IS NULL AND ((phone = $1)) ORDER BY "mitra"."id" ASC LIMIT 1
	mock.ExpectQuery("^SELECT (.+) FROM \"mitras\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	mitraRepo := new(MitraRepository)
	mitra, _ := mitraRepo.GetByPhoneNumber("0812345678")

	assert := assert.New(t)
	assert.Equal("0812345678", mitra.Phone, "Username should be equal")
	assert.Equal("abcdefghi", mitra.Password, "Password should be equal")
}

func TestMitraGetByEmail(t *testing.T) {
	var mitraFieldNames = []string{"id", "phone", "email", "password"}
	rows := sqlmock.NewRows(mitraFieldNames)
	rows.AddRow("1", "0812345678", "yusufsholeh@gmail.com", "abcdefghi")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)

	// SELECT * FROM "mitra" WHERE "mitra"."deleted_at" IS NULL AND ((email = $1)) ORDER BY "mitra"."id" ASC LIMIT 1
	mock.ExpectQuery("^SELECT (.+) FROM \"mitras\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	mitraRepo := new(MitraRepository)
	mitra, _ := mitraRepo.GetByEmail("yusufsholeh@gmail.com")

	assert := assert.New(t)
	assert.Equal("0812345678", mitra.Phone, "Username should be equal")
	assert.Equal("abcdefghi", mitra.Password, "Password should be equal")
	assert.Equal("yusufsholeh@gmail.com", mitra.Email, "Email should be equal")
}

func TestMitraAddSuccess(t *testing.T) {
	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("sqlite3", db)
	gormDb.LogMode(true)
	models.SetDB(gormDb)

	mock.ExpectExec("^INSERT (.+)$").WithArgs(AnyTime{}, AnyTime{}, nil, "08123456789", AnyString{}, AnyString{}, AnyString{}, AnyString{}).WillReturnResult(sqlmock.NewResult(1, 1))

	mitraRepo := new(MitraRepository)
	_, err := mitraRepo.Add(&models.Mitra{Phone: "08123456789", Password: "abcdefghi"})

	assert := assert.New(t)
	assert.Equal(nil, err, "success insert")

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestMitraAddFailed(t *testing.T) {
	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)
	models.SetDB(gormDb)

	mock.ExpectQuery("^INSERT (.+)$").WithArgs(AnyTime{}, AnyTime{}, nil, "08123456789", AnyString{}, AnyString{}, AnyString{}, AnyString{}).WillReturnError(fmt.Errorf("record error"))

	mitraRepo := new(MitraRepository)
	_, err := mitraRepo.Add(&models.Mitra{Phone: "08123456789", Password: "abcdefghi"})

	assert.NotNil(t, err)
	assert.Equal(t, fmt.Errorf("record error"), err, "record error")
}

func TestMitraUpdate(t *testing.T) {
	var fieldNames = []string{"id", "name", "email", "token"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "yusuf", "sholeh@gmail.com", "abcdef")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)
	models.SetDB(gormDb)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"mitras\".+$").WillReturnRows(rows)

	mitraRepo := new(MitraRepository)
	_, err := mitraRepo.Update(&models.Mitra{Phone: "08123456789", Password: "abcdefghi"})
	assert.NotNil(t, err)
}
