package repositories

import (
	"PPLA4/backend/models"
)

type MitraPickUpScheduleRepository struct {
}

type IMitraPickUpScheduleRepository interface {
	GetByMitraId(mitraId uint) (*models.MitraPickUpSchedule, error)
	Save(mitraPickUpSchedule *models.MitraPickUpSchedule) (*models.MitraPickUpSchedule, error)
}

func (m MitraPickUpScheduleRepository) GetByMitraId(mitraId uint) (*models.MitraPickUpSchedule, error) {
	mitraPickUpSchedule := &models.MitraPickUpSchedule{}
	err := models.GetDB().Where("mitra_id = ?", mitraId).Find(mitraPickUpSchedule).Error
	return mitraPickUpSchedule, err
}

func (m MitraPickUpScheduleRepository) Save(mitraPickUpSchedule *models.MitraPickUpSchedule) (*models.MitraPickUpSchedule, error) {
	err := models.GetDB().Save(&mitraPickUpSchedule).Error
	return mitraPickUpSchedule, err
}
