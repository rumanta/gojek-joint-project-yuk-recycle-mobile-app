package repositories

import (
	"PPLA4/backend/models"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestMitraPickUpDistanceRepository_GetByMitraId(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "maximum_distance"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "20")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_pick_up_distances\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraPickUpDistanceRepository)
	pickUpDistance, _ := repository.GetByMitraId(1)

	assert := assert.New(t)
	assert.Equal(uint(1), pickUpDistance.ID)
	assert.Equal(uint(20), pickUpDistance.MaximumDistance)
}

func TestMitraPickUpDistanceRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "maximum_distance"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "20")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"mitra_pick_up_distances\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraPickUpDistanceRepository)
	pickUpDistance, _ := repository.Save(&models.MitraPickUpDistance{MitraID: 1, MaximumDistance: 20})

	assert := assert.New(t)
	assert.Equal(uint(1), pickUpDistance.MitraID)
	assert.Equal(uint(20), pickUpDistance.MaximumDistance)
}
