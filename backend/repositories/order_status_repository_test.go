package repositories

import (
	"PPLA4/backend/models"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestOrderStatusRepository_GetByID(t *testing.T) {
	var fieldNames = []string{"id", "description"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "PPL MALES BANGET")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_statuses\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderStatusRepository)
	orderStatus, _ := repository.GetByID(1)

	assert := assert.New(t)
	assert.Equal(uint(1), orderStatus.ID)
}

func TestOrderStatusRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "description"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "PPL MALES BANGET")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"order_statuses\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderStatusRepository)
	obj, err := repository.Save(&models.OrderStatus{Description: "PPL MALES BANGET"})
	assert.NotNil(t, err)
	assert.Equal(t, "PPL MALES BANGET", obj.Description, "description should be equal")
}
