package repositories

import (
	"PPLA4/backend/models"

	"golang.org/x/crypto/bcrypt"
)

type CustomerRepository struct {
}

type ICustomerRepository interface {
	GetByID(id uint) (*models.Customer, error)
	GetByPhoneNumber(phone string) (*models.Customer, error)
	GetByEmail(email string) (*models.Customer, error)
	Add(customer *models.Customer) (*models.Customer, error)
	Update(customer *models.Customer) (*models.Customer, error)
}

func (m CustomerRepository) GetByID(id uint) (*models.Customer, error) {
	customer := &models.Customer{}
	err := models.GetDB().Where("id = ?", id).Find(customer).Error
	return customer, err
}

// GetByPhoneNumber possible output: gorm.ErrRecordNotFound
func (m CustomerRepository) GetByPhoneNumber(phone string) (*models.Customer, error) {
	customer := &models.Customer{}
	err := models.GetDB().Where("phone = ?", phone).First(customer).Error
	return customer, err
}

// GetByEmail possible output: gorm.ErrRecordNotFound
func (m CustomerRepository) GetByEmail(email string) (*models.Customer, error) {
	customer := &models.Customer{}
	err := models.GetDB().Where("email = ?", email).First(customer).Error
	return customer, err
}

func (m CustomerRepository) Add(customer *models.Customer) (*models.Customer, error) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(customer.Password), bcrypt.DefaultCost)
	customer.Password = string(hashedPassword)
	err := models.GetDB().Create(customer).Error
	if err != nil {
		return nil, err
	}
	return customer, nil
}

// Update : should specify customer.ID
func (m CustomerRepository) Update(customer *models.Customer) (*models.Customer, error) {
	if customer.Password != "" {
		hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(customer.Password), bcrypt.DefaultCost)
		customer.Password = string(hashedPassword)
	}
	err := models.GetDB().Model(&customer).Updates(&customer).Error
	return customer, err
}
