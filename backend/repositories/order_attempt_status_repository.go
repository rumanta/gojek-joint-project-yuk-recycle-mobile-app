package repositories

import (
	"PPLA4/backend/models"
)

type OrderAttemptStatusRepository struct {
}

type IOrderAttemptStatusRepository interface {
	GetByID(orderAttemptStatusID uint) (*models.OrderAttemptStatus, error)
	Save(orderAttemptStatus *models.OrderAttemptStatus) (*models.OrderAttemptStatus, error)
}

func (o OrderAttemptStatusRepository) GetByID(orderAttemptStatusID uint) (*models.OrderAttemptStatus, error) {
	orderAttemptStatus := &models.OrderAttemptStatus{}
	err := models.GetDB().Where("id = ?", orderAttemptStatusID).Find(orderAttemptStatus).Error
	return orderAttemptStatus, err
}

func (o OrderAttemptStatusRepository) Save(orderAttemptStatus *models.OrderAttemptStatus) (*models.OrderAttemptStatus, error) {
	err := models.GetDB().Save(&orderAttemptStatus).Error
	return orderAttemptStatus, err
}
