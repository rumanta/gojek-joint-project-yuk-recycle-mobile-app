package repositories

import (
	"PPLA4/backend/models"
)

type OrderStatusHistoryRepository struct {
}

type IOrderStatusHistoryRepository interface {
	GetByID(id uint) (*models.OrderStatusHistory, error)
	GetByOrderIDAndOrderStatusID(orderID, orderStatusID uint) (*models.OrderStatusHistory, error)
	Save(OrderStatusHistory *models.OrderStatusHistory) (*models.OrderStatusHistory, error)
}

func (m OrderStatusHistoryRepository) GetByID(id uint) (*models.OrderStatusHistory, error) {
	OrderStatusHistory := &models.OrderStatusHistory{}
	err := models.GetDB().Where("id = ?", id).Find(OrderStatusHistory).Error
	return OrderStatusHistory, err
}

func (m OrderStatusHistoryRepository) GetByOrderIDAndOrderStatusID(orderID, orderStatusID uint) (*models.OrderStatusHistory, error) {
	orderStatusHistory := &models.OrderStatusHistory{}
	err := models.GetDB().Where("order_id = ? AND order_status_id = ?", orderID, orderStatusID).Find(orderStatusHistory).Error
	return orderStatusHistory, err
}

func (m OrderStatusHistoryRepository) Save(OrderStatusHistory *models.OrderStatusHistory) (*models.OrderStatusHistory, error) {
	err := models.GetDB().Save(&OrderStatusHistory).Error
	return OrderStatusHistory, err
}
