package repositories

import (
	"PPLA4/backend/models"
	"sort"

	geo "github.com/kellydunn/golang-geo"
)

type MitraLocationRepository struct {
}

type IMitraLocationRepository interface {
	GetByMitraId(mitraId uint) (*models.MitraLocation, error)
	Save(mitraLocation *models.MitraLocation) (*models.MitraLocation, error)
	GetAllByLocationAndDistance(customerLat float64, customerLon float64, customerPreferenceDistance float64) ([]models.MitraLocation, error)
}

func (m MitraLocationRepository) GetByMitraId(mitraId uint) (*models.MitraLocation, error) {
	mitraLocation := &models.MitraLocation{}
	err := models.GetDB().Where("mitra_id = ?", mitraId).Find(mitraLocation).Error
	return mitraLocation, err
}

func (m MitraLocationRepository) Save(mitraLocation *models.MitraLocation) (*models.MitraLocation, error) {
	err := models.GetDB().Save(&mitraLocation).Error
	return mitraLocation, err
}

// GetAllByDistance description
/*
	Note:
		- distance processed in kilometres
		- customer preferences distance not more than DISTANCE, set INF to ignore this
		- there is no validation from MITRA PERSPECTIVE!
	Complexity: O(N lg N)
	Return: all valid rows, sorted by distance
*/
func (m MitraLocationRepository) GetAllByLocationAndDistance(customerLat float64, customerLon float64,
	customerPreferenceDistance float64) ([]models.MitraLocation, error) {
	var allRows []models.MitraLocation
	err := models.GetDB().Find(&allRows).Error

	if err != nil {
		return []models.MitraLocation{}, err
	}

	customer := geo.NewPoint(customerLat, customerLon)

	var result []models.MitraLocation

	for _, mitraLocation := range allRows {
		warehouse := geo.NewPoint(mitraLocation.Lat, mitraLocation.Lon)
		dist := customer.GreatCircleDistance(warehouse)
		if dist <= customerPreferenceDistance {
			result = append(result, mitraLocation)
		}
	}

	// Sort warehouse based on distance from customer
	sort.Slice(result, func(i, j int) bool { // Define less function
		warehouse1 := geo.NewPoint(result[i].Lat, result[i].Lon)
		warehouse2 := geo.NewPoint(result[j].Lat, result[j].Lon)
		distanceI := customer.GreatCircleDistance(warehouse1)
		distanceJ := customer.GreatCircleDistance(warehouse2)
		return distanceI < distanceJ
	})

	return result, nil
}
