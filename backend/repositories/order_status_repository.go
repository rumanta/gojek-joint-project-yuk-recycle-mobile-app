package repositories

import (
	"PPLA4/backend/models"
)

type OrderStatusRepository struct {
}

type IOrderStatusRepository interface {
	GetByID(orderStatusID uint) (*models.OrderStatus, error)
	Save(orderStatus *models.OrderStatus) (*models.OrderStatus, error)
}

func (m OrderStatusRepository) GetByID(orderStatusID uint) (*models.OrderStatus, error) {
	orderStatus := &models.OrderStatus{}
	err := models.GetDB().Where("id = ?", orderStatusID).Find(orderStatus).Error
	return orderStatus, err
}

func (m OrderStatusRepository) Save(orderStatus *models.OrderStatus) (*models.OrderStatus, error) {
	err := models.GetDB().Save(&orderStatus).Error
	return orderStatus, err
}
