package repositories

import (
	"PPLA4/backend/models"
	"errors"
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestMitraLocationRepository_GetByMitraId(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "3.1", "3.2")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraLocationRepository)
	mitraLocation, _ := repository.GetByMitraId(1)

	assert := assert.New(t)
	assert.Equal(uint(1), mitraLocation.ID)
	assert.Equal(3.1, mitraLocation.Lat)
	assert.Equal(3.2, mitraLocation.Lon)
}

func TestMitraLocationRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "3.1", "3.2")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"mitra_locations\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraLocationRepository)
	_, err := repository.Save(&models.MitraLocation{MitraID: 1, Lat: 3.1, Lon: 3.2})
	assert.NotNil(t, err)
}

func TestGetAllByDistance_Error(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "3.5", "3.5")
	rows.AddRow("2", "2", "3.1", "3.1")
	customerLat := 3.0
	customerLon := 3.0
	customerPreferenceDistance := 100.0

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnError(fmt.Errorf("connection database error"))
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, err := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(len(results), 0)
	assert.Equal(err, errors.New("connection database error"))
}

func TestGetAllByDistance_1(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "3.5", "3.5")
	rows.AddRow("2", "2", "3.1", "3.1")
	customerLat := 3.0
	customerLon := 3.0
	customerPreferenceDistance := 100.0

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(uint(2), results[0].ID)
	assert.Equal(uint(1), results[1].ID)
}

func TestGetAllByDistance_2(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "3.5", "3.5")
	rows.AddRow("2", "2", "3.1", "3.1")
	customerLat := 3.0
	customerLon := 3.0
	customerPreferenceDistance := 30.0

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(uint(2), results[0].ID)
	assert.Equal(len(results), 1)
}

func TestGetAllByDistance_3(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "10", "-6.173460", "106.816917") // monas
	customerLat := -6.185620                          // gondang dia
	customerLon := 106.834270
	customerPreferenceDistance := 2.2 // based on maps, distance should be 2.346984765922268 km

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(len(results), 0)
}

func TestGetAllByDistance_4(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "10", "-6.173460", "106.816917") // monas
	customerLat := -6.185620                          // gondang dia
	customerLon := 106.834270
	customerPreferenceDistance := 2.4 // based on maps, distance should be 2.346984765922268 km

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(len(results), 1)
}

func TestGetAllByDistance_5(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "10", "-6.173460", "106.816917") // monas
	customerLat := -6.185620                          // gondang dia
	customerLon := 106.834270
	customerPreferenceDistance := 2.4 // based on maps, distance should be 2.346984765922268 km.

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(len(results), 1)
}

func TestGetAllByDistance_6(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "10", "-6.173460", "106.816917") // monas
	customerLat := -6.209410                          // manggarai
	customerLon := 106.850010
	customerPreferenceDistance := 2.4 // based on maps, distance should be 5.418754337285638 km.

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(len(results), 0)
}

func TestGetAllByDistance_7(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "10", "-6.173460", "106.816917") // monas
	customerLat := -6.209410                          // manggarai
	customerLon := 106.850010
	customerPreferenceDistance := 5.42 // based on maps, distance should be 5.418754337285638 km.

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(len(results), 1)
	assert.Equal(uint(10), results[0].MitraID)
}

func TestGetAllByDistance_8(t *testing.T) {
	// if a customer currently located in monas
	// mitra 17 located in manggarai
	// mitra 311 located in gondang dia
	// function should return mitra 2 first because mitra 2 is closer to customer than mitra 1
	var fieldNames = []string{"id", "mitra_id", "lat", "lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "17", "-6.173460", "106.816917")  // manggarai
	rows.AddRow("2", "311", "-6.185620", "106.834270") // gondang dia
	customerLat := -6.209410                           // monas
	customerLon := 106.850010
	customerPreferenceDistance := 5.5 // based on maps, distance should be 5.418754337285638 km.

	db, mock, _ := sqlmock.New()
	gormDb, _ := gorm.Open("postgres", db)
	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_locations\".+$").WillReturnRows(rows)
	models.SetDB(gormDb)

	repository := new(MitraLocationRepository)
	results, _ := repository.GetAllByLocationAndDistance(customerLat, customerLon, customerPreferenceDistance)

	assert := assert.New(t)
	assert.Equal(len(results), 2)
	assert.Equal(uint(311), results[0].MitraID)
	assert.Equal(uint(17), results[1].MitraID)
}
