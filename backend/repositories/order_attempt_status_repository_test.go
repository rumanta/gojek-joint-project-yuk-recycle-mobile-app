package repositories

import (
	"PPLA4/backend/models"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestOrderAttemptStatusRepository_GetByID(t *testing.T) {
	var fieldNames = []string{"id", "description"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "not yet decided")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_attempt_statuses\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderAttemptStatusRepository)
	orderAttemptStatus, _ := repository.GetByID(1)

	assert := assert.New(t)
	assert.Equal(uint(1), orderAttemptStatus.ID)
}

func TestOrderAttemptStatusRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "description"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "not yet decided")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"order_attempt_statuses\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderAttemptStatusRepository)
	_, err := repository.Save(&models.OrderAttemptStatus{Description: "not yet decided"})
	assert.NotNil(t, err)
}
