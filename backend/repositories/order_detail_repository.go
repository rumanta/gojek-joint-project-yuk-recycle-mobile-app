package repositories

import (
	"PPLA4/backend/models"
)

type OrderDetailRepository struct {
}

type IOrderDetailRepository interface {
	GetByID(id uint) (*models.OrderDetail, error)
	GetByOrderID(orderID uint) ([]models.OrderDetail, error)
	Save(OrderDetail *models.OrderDetail) (*models.OrderDetail, error)
	DeleteByOrderIDAndGarbageTypeID(orderID, garbageID uint) error
	DeleteByID(id uint) error
}

func (m OrderDetailRepository) GetByID(id uint) (*models.OrderDetail, error) {
	OrderDetail := &models.OrderDetail{}
	err := models.GetDB().Where("id = ?", id).Find(OrderDetail).Error
	return OrderDetail, err
}

func (m OrderDetailRepository) GetByOrderID(orderID uint) ([]models.OrderDetail, error) {
	var orderDetails []models.OrderDetail
	err := models.GetDB().Where("order_id = ?", orderID).Find(&orderDetails).Error
	return orderDetails, err
}

func (m OrderDetailRepository) Save(OrderDetail *models.OrderDetail) (*models.OrderDetail, error) {
	err := models.GetDB().Save(&OrderDetail).Error
	return OrderDetail, err
}

func (m OrderDetailRepository) DeleteByOrderIDAndGarbageTypeID(orderID, garbageID uint) error {
	err := models.GetDB().Unscoped().Where("order_id = ? AND garbage_type_id = ?", orderID, garbageID).Delete(&models.OrderDetail{}).Error
	return err
}

func (m OrderDetailRepository) DeleteByID(id uint) error {
	err := models.GetDB().Unscoped().Where("id = ?", id).Delete(&models.OrderDetail{}).Error
	return err
}
