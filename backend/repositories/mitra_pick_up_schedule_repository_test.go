package repositories

import (
	"PPLA4/backend/models"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestMitraPickUpScheduleRepository_GetByMitraId(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "sunday", "monday",
		"tuesday", "wednesday", "thursday", "friday", "saturday"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", true, true, true, true, false, false, false)

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_pick_up_schedules\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraPickUpScheduleRepository)
	pickUpSchedule, _ := repository.GetByMitraId(1)

	assert := assert.New(t)
	assert.Equal(uint(1), pickUpSchedule.ID)
	assert.Equal(true, pickUpSchedule.Sunday)
	assert.Equal(false, pickUpSchedule.Saturday)
}

func TestMitraPickUpScheduleRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "sunday", "monday",
		"tuesday", "wednesday", "thursday", "friday", "saturday"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", true, true, true, true, false, false, false)

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"mitra_pick_up_schedules\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraPickUpScheduleRepository)
	pickUpSchedule, _ := repository.Save(&models.MitraPickUpSchedule{
		MitraID:   1,
		Sunday:    true,
		Monday:    true,
		Tuesday:   true,
		Wednesday: true,
		Thursday:  false,
		Friday:    false,
		Saturday:  false,
	})

	assert := assert.New(t)
	assert.Equal(uint(1), pickUpSchedule.MitraID)
	assert.Equal(true, pickUpSchedule.Sunday)
	assert.Equal(false, pickUpSchedule.Saturday)
}
