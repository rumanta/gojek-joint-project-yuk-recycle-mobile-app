package repositories

import (
	"PPLA4/backend/models"
	"time"
)

type OrderRepository struct {
}

type IOrderRepository interface {
	GetByID(orderID uint) (*models.Order, error)
	GetLatest20ActiveOrdersByMitraID(mitraID uint) ([]models.Order, error)
	GetLatest20ActiveOrdersByCustomerID(customerID uint) ([]models.Order, error)
	GetLatest20OrderHistoriesByMitraID(mitraID uint) ([]models.Order, error)
	GetLatest20OrderHistoriesByCustomerID(customerID uint) ([]models.Order, error)
	Save(order *models.Order) (*models.Order, error)
	GetOrderByDateRange(mitraID uint, firstDay time.Time, lastDay time.Time) ([]models.Order, error)
}

func (m OrderRepository) GetByID(orderID uint) (*models.Order, error) {
	order := &models.Order{}
	err := models.GetDB().Where("id = ?", orderID).Find(order).Error
	return order, err
}

func (m OrderRepository) GetLatest20ActiveOrdersByMitraID(mitraID uint) ([]models.Order, error) {
	var orders []models.Order
	err := models.GetDB().Where("mitra_id = ? AND latest_status <= 3", mitraID).Order("id desc").Limit(20).Find(&orders).Error
	return orders, err
}

func (m OrderRepository) GetLatest20ActiveOrdersByCustomerID(customerID uint) ([]models.Order, error) {
	var orders []models.Order
	err := models.GetDB().Where("customer_id = ? AND latest_status >= 1 AND latest_status <= 3", customerID).Order("id desc").Limit(20).Find(&orders).Error
	return orders, err
}

func (m OrderRepository) GetLatest20OrderHistoriesByMitraID(mitraID uint) ([]models.Order, error) {
	var orders []models.Order
	err := models.GetDB().Where("mitra_id = ? AND latest_status >= 4", mitraID).Order("id desc").Limit(20).Find(&orders).Error
	return orders, err
}

func (m OrderRepository) GetLatest20OrderHistoriesByCustomerID(customerID uint) ([]models.Order, error) {
	var orders []models.Order
	err := models.GetDB().Where("customer_id = ? AND latest_status >= 4 AND latest_status <= 6", customerID).Order("id desc").Limit(20).Find(&orders).Error
	return orders, err
}

func (m OrderRepository) Save(order *models.Order) (*models.Order, error) {
	err := models.GetDB().Save(&order).Error
	return order, err
}

func (m OrderRepository) GetOrderByDateRange(mitraID uint, firstDay time.Time, lastDay time.Time) ([]models.Order, error) {
	var orders []models.Order
	err := models.GetDB().Where("mitra_id = ? AND scheduled_at >= ? AND scheduled_at <= ?", mitraID, firstDay, lastDay).Find(&orders).Error
	return orders, err
}
