package repositories

import (
	"PPLA4/backend/models"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestOrderDetailRepository_GetByID(t *testing.T) {
	var fieldNames = []string{"id", "total_amount"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "14045")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_details\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderDetailRepository)
	orderDetail, _ := repository.GetByID(1)

	assert := assert.New(t)
	assert.Equal(uint(1), orderDetail.ID)
	assert.Equal(14045, orderDetail.TotalAmount)
}

func TestOrderDetailRepository_GetByOrderID(t *testing.T) {
	var fieldNames = []string{"id", "total_amount", "order_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "14045", "1")
	rows.AddRow("2", "14022", "1")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_details\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderDetailRepository)
	orderDetails, _ := repository.GetByOrderID(1)

	assert := assert.New(t)
	assert.Equal(14045, orderDetails[0].TotalAmount)
	assert.Equal(14022, orderDetails[1].TotalAmount)
}

func TestOrderDetailRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "total_amount"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "14045")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"order_details\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderDetailRepository)
	obj, err := repository.Save(&models.OrderDetail{TotalAmount: 14045})
	assert.NotNil(t, err)
	assert.Equal(t, 14045, obj.TotalAmount)
}

func TestOrderDetailRepository_Delete(t *testing.T) {
	var fieldNames = []string{"id", "total_amount", "garbage_type_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "14045", "2")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	// DELETE FROM "order_details"  WHERE "order_details"."id" = $1
	mock.ExpectQuery("^DELETE from \"order_details\".+$").WillReturnError(nil)

	models.SetDB(gormDb)
	repository := new(OrderDetailRepository)
	err := repository.DeleteByOrderIDAndGarbageTypeID(1, 2)
	assert.NotNil(t, err)
}

func TestOrderDetailRepository_DeleteID(t *testing.T) {
	var fieldNames = []string{"id", "total_amount", "garbage_type_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "14045", "2")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	// DELETE FROM "order_details"  WHERE "order_details"."id" = $1
	mock.ExpectQuery("^DELETE from \"order_details\".+$").WillReturnError(nil)

	models.SetDB(gormDb)
	repository := new(OrderDetailRepository)
	err := repository.DeleteByID(uint(1))

	assert.NotNil(t, err)
}
