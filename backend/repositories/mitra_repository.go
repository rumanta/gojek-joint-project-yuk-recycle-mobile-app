package repositories

import (
	"PPLA4/backend/models"

	"golang.org/x/crypto/bcrypt"
)

type MitraRepository struct {
}

type IMitraRepository interface {
	GetByID(id uint) (*models.Mitra, error)
	GetByPhoneNumber(phone string) (*models.Mitra, error)
	GetByEmail(email string) (*models.Mitra, error)
	Add(mitra *models.Mitra) (*models.Mitra, error)
	Update(mitra *models.Mitra) (*models.Mitra, error)
}

func (m MitraRepository) GetByID(id uint) (*models.Mitra, error) {
	mitra := &models.Mitra{}
	err := models.GetDB().Where("id = ?", id).Find(mitra).Error
	return mitra, err
}

// GetByPhoneNumber possible output: gorm.ErrRecordNotFound
func (m MitraRepository) GetByPhoneNumber(phone string) (*models.Mitra, error) {
	mitra := &models.Mitra{}
	err := models.GetDB().Where("phone = ?", phone).First(mitra).Error
	return mitra, err
}

// GetByEmail possible output: gorm.ErrRecordNotFound
func (m MitraRepository) GetByEmail(email string) (*models.Mitra, error) {
	mitra := &models.Mitra{}
	err := models.GetDB().Where("email = ?", email).First(mitra).Error
	return mitra, err
}

func (m MitraRepository) Add(mitra *models.Mitra) (*models.Mitra, error) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(mitra.Password), bcrypt.DefaultCost)
	mitra.Password = string(hashedPassword)
	err := models.GetDB().Create(mitra).Error
	if err != nil {
		return nil, err
	}
	return mitra, nil
}

// Update : should specify mitra.ID to update
func (m MitraRepository) Update(mitra *models.Mitra) (*models.Mitra, error) {
	err := models.GetDB().Model(&mitra).Updates(&mitra).Error
	return mitra, err
}
