package repositories

import (
	"PPLA4/backend/models"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestMitraGarbageSettingRepository_GetByMitraId(t *testing.T) {
	var fieldNames = []string{"id", "mitra_id", "garbage_type_id", "minimum_quantity", "price"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "1", "5", "500")
	rows.AddRow("2", "1", "2", "6", "600")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"mitra_garbage_settings\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraGarbageSettingRepository)
	garbageSettings, _ := repository.GetByMitraId(1)

	assert := assert.New(t)
	assert.Equal(2, len(garbageSettings))
	assert.Equal(uint(1), garbageSettings[0].ID)
	assert.Equal(uint(500), garbageSettings[0].Price)
	assert.Equal(uint(2), garbageSettings[1].ID)
	assert.Equal(uint(600), garbageSettings[1].Price)
}

func TestMitraGarbageSettingRepository_Save(t *testing.T) {
	var fieldNames = []string{"mitra_id", "garbage_type_id", "minimum_quantity", "price"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "1", "5", "500")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"mitra_garbage_settings\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(MitraGarbageSettingRepository)
	garbageSetting, _ := repository.Save(&models.MitraGarbageSetting{MitraID: 1, GarbageTypeID: 1, MinimumQuantity: 5, Price: 500})

	assert := assert.New(t)
	assert.Equal(uint(1), garbageSetting.MitraID)
	assert.Equal(uint(500), garbageSetting.Price)
}
