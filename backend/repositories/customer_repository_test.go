package repositories

import (
	"PPLA4/backend/middleware"
	"PPLA4/backend/models"
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestCustomerGetByID(t *testing.T) {
	var customerFieldNames = []string{"id", "phone", "password"}
	rows := sqlmock.NewRows(customerFieldNames)
	rows.AddRow("1", "0812345678", "abcdefghi")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)

	// SELECT * FROM "customer" WHERE "customer"."deleted_at" IS NULL AND ((phone = $1)) ORDER BY "customer"."id" ASC LIMIT 1
	mock.ExpectQuery("^SELECT (.+) FROM \"customers\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	customerRepo := new(CustomerRepository)
	customer, _ := customerRepo.GetByID(1)

	assert := assert.New(t)
	assert.Equal("0812345678", customer.Phone, "Username should be equal")
	assert.Equal("abcdefghi", customer.Password, "Password should be equal")
}

func TestCustomerGetByPhoneNumber(t *testing.T) {
	var customerFieldNames = []string{"id", "phone", "password"}
	rows := sqlmock.NewRows(customerFieldNames)
	rows.AddRow("1", "0812345678", "abcdefghi")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)

	// SELECT * FROM "customer" WHERE "customer"."deleted_at" IS NULL AND ((phone = $1)) ORDER BY "customer"."id" ASC LIMIT 1
	mock.ExpectQuery("^SELECT (.+) FROM \"customers\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	customerRepo := new(CustomerRepository)
	customer, _ := customerRepo.GetByPhoneNumber("0812345678")

	assert := assert.New(t)
	assert.Equal("0812345678", customer.Phone, "Username should be equal")
	assert.Equal("abcdefghi", customer.Password, "Password should be equal")
}

func TestCustomerGetByEmail(t *testing.T) {
	var customerFieldNames = []string{"id", "phone", "email", "password"}
	rows := sqlmock.NewRows(customerFieldNames)
	rows.AddRow("1", "0812345678", "yusufsholeh@gmail.com", "abcdefghi")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)

	// SELECT * FROM "customer" WHERE "customer"."deleted_at" IS NULL AND ((email = $1)) ORDER BY "customer"."id" ASC LIMIT 1
	mock.ExpectQuery("^SELECT (.+) FROM \"customers\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	customerRepo := new(CustomerRepository)
	customer, _ := customerRepo.GetByEmail("yusufsholeh@gmail.com")

	assert := assert.New(t)
	assert.Equal("0812345678", customer.Phone, "Username should be equal")
	assert.Equal("abcdefghi", customer.Password, "Password should be equal")
	assert.Equal("yusufsholeh@gmail.com", customer.Email, "Email should be equal")
}

func TestCustomerAddSuccess(t *testing.T) {
	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("sqlite3", db)
	gormDb.LogMode(true)
	models.SetDB(gormDb)

	mock.ExpectExec("^INSERT (.+)$").WithArgs(AnyTime{}, AnyTime{}, nil, "08123456789", AnyString{}, AnyString{}, AnyString{}, middleware.OAuthLoginType, AnyString{}).WillReturnResult(sqlmock.NewResult(1, 1))

	customerRepo := new(CustomerRepository)
	_, err := customerRepo.Add(&models.Customer{Phone: "08123456789", Password: "abcdefghi", LoginType: middleware.OAuthLoginType})

	assert := assert.New(t)
	assert.Equal(nil, err, "success insert")

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestCustomerAddFailed(t *testing.T) {
	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)
	models.SetDB(gormDb)

	mock.ExpectQuery("^INSERT (.+)$").WithArgs(AnyTime{}, AnyTime{}, nil, "08123456789", AnyString{}, AnyString{}, AnyString{}, middleware.OAuthLoginType, AnyString{}).WillReturnError(fmt.Errorf("record error"))

	customerRepo := new(CustomerRepository)
	_, err := customerRepo.Add(&models.Customer{Phone: "08123456789", Password: "abcdefghi", LoginType: middleware.OAuthLoginType})

	assert.NotNil(t, err)
	assert.Equal(t, fmt.Errorf("record error"), err, "record error")
}

func TestCustomerUpdate(t *testing.T) {
	var fieldNames = []string{"id", "name", "email", "token"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "yusuf", "sholeh@gmail.com", "abcdef")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)
	models.SetDB(gormDb)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"customers\".+$").WillReturnRows(rows)

	customerRepo := new(CustomerRepository)
	_, err := customerRepo.Update(&models.Customer{Phone: "08123456789", Password: "abcdefghi"})
	assert.NotNil(t, err)
}

func TestCustomerUpdateWithoutPassword(t *testing.T) {
	var fieldNames = []string{"id", "name", "email", "token"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "yusuf", "sholeh@gmail.com", "abcdef")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)
	gormDb.LogMode(true)
	models.SetDB(gormDb)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"customers\".+$").WillReturnRows(rows)

	customerRepo := new(CustomerRepository)
	_, err := customerRepo.Update(&models.Customer{Phone: "08123456789", Password: ""})
	assert.NotNil(t, err)
}
