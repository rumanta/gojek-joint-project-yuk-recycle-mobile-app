package repositories

import (
	"PPLA4/backend/models"
)

type MitraGarbageSettingRepository struct {
}

type IMitraGarbageSettingRepository interface {
	GetByMitraId(mitraId uint) ([]models.MitraGarbageSetting, error)
	Save(mitraGarbageSetting *models.MitraGarbageSetting) (*models.MitraGarbageSetting, error)
}

func (m MitraGarbageSettingRepository) GetByMitraId(mitraId uint) ([]models.MitraGarbageSetting, error) {
	var mitraGarbageSettings []models.MitraGarbageSetting
	err := models.GetDB().Where("mitra_id = ?", mitraId).Find(&mitraGarbageSettings).Error
	return mitraGarbageSettings, err
}

func (m MitraGarbageSettingRepository) Save(mitraGarbageSetting *models.MitraGarbageSetting) (*models.MitraGarbageSetting, error) {
	err := models.GetDB().Save(&mitraGarbageSetting).Error
	return mitraGarbageSetting, err
}
