package repositories

import (
	"PPLA4/backend/models"
)

type MitraPickUpDistanceRepository struct {
}

type IMitraPickUpDistanceRepository interface {
	GetByMitraId(mitraId uint) (*models.MitraPickUpDistance, error)
	Save(mitraPickUpDistance *models.MitraPickUpDistance) (*models.MitraPickUpDistance, error)
}

func (m MitraPickUpDistanceRepository) GetByMitraId(mitraId uint) (*models.MitraPickUpDistance, error) {
	mitraPickUpDistance := &models.MitraPickUpDistance{}
	err := models.GetDB().Where("mitra_id = ?", mitraId).Find(mitraPickUpDistance).Error
	return mitraPickUpDistance, err
}

func (m MitraPickUpDistanceRepository) Save(mitraPickUpDistance *models.MitraPickUpDistance) (*models.MitraPickUpDistance, error) {
	err := models.GetDB().Save(&mitraPickUpDistance).Error
	return mitraPickUpDistance, err
}
