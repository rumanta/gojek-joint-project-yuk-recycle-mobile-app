package repositories

import (
	"PPLA4/backend/models"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestOrderStatusHistoryRepository_GetByID(t *testing.T) {
	var fieldNames = []string{"id", "order_id", "order_status_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "2", "3")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_status_histories\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderStatusHistoryRepository)
	orderStatusHistory, _ := repository.GetByID(1)

	assert := assert.New(t)
	assert.Equal(uint(1), orderStatusHistory.ID)
	assert.Equal(uint(2), orderStatusHistory.OrderID)
	assert.Equal(uint(3), orderStatusHistory.OrderStatusID)
}

func TestOrderStatusHistoryRepository_GetByOrderIDAndOrderStatusID(t *testing.T) {
	var fieldNames = []string{"id", "order_id", "order_status_id", "created_at"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "2", "3", time.Date(2019, 4, 22, 13, 14, 15, 16, time.UTC))

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	gormDb.LogMode(true)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_status_histories\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderStatusHistoryRepository)
	orderStatusHistory, _ := repository.GetByOrderIDAndOrderStatusID(2, 3)

	assert := assert.New(t)
	assert.Equal(uint(1), orderStatusHistory.ID)
	assert.Equal("2019-04-22 13:14:15", orderStatusHistory.CreatedAt.Format("2006-01-02 15:04:05"))
}

func TestOrderStatusHistoryRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "order_id", "order_status_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "2", "3")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"order_status_histories\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderStatusHistoryRepository)
	obj, err := repository.Save(&models.OrderStatusHistory{OrderID: 2, OrderStatusID: 3})
	assert.NotNil(t, err)
	assert.Equal(t, uint(2), obj.OrderID)
}
