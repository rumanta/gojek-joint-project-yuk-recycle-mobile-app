package repositories

import (
	"PPLA4/backend/models"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestOrderRepository_GetByID(t *testing.T) {
	var fieldNames = []string{"id", "customer_lat", "customer_lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "3.1", "3.2")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"orders\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderRepository)
	order, _ := repository.GetByID(1)

	assert := assert.New(t)
	assert.Equal(uint(1), order.ID)
}

func TestOrderRepository_GetLatest20ActiveOrdersByMitraID(t *testing.T) {
	var fieldNames = []string{"id", "customer_lat", "customer_lon", "mitra_id", "latest_status"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "3.1", "3.2", "1", "1")
	rows.AddRow("1", "3.1", "3.2", "1", "2")
	rows.AddRow("1", "3.1", "3.2", "1", "3")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"orders\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderRepository)
	orders, _ := repository.GetLatest20ActiveOrdersByMitraID(1)

	assert := assert.New(t)
	assert.Equal(1, orders[0].LatestStatus)
	assert.Equal(2, orders[1].LatestStatus)
	assert.Equal(3, orders[2].LatestStatus)
}

func TestOrderRepository_GetLatest20ActiveOrdersByCustomerID(t *testing.T) {
	var fieldNames = []string{"id", "customer_lat", "customer_lon", "customer_id", "latest_status"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "3.1", "3.2", "1", "1")
	rows.AddRow("1", "3.1", "3.2", "1", "2")
	rows.AddRow("1", "3.1", "3.2", "1", "3")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"orders\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderRepository)
	orders, _ := repository.GetLatest20ActiveOrdersByCustomerID(1)

	assert := assert.New(t)
	assert.Equal(1, orders[0].LatestStatus)
	assert.Equal(2, orders[1].LatestStatus)
	assert.Equal(3, orders[2].LatestStatus)
}

func TestOrderRepository_GetLatest20OrderHistoriesByMitraID(t *testing.T) {
	var fieldNames = []string{"id", "customer_lat", "customer_lon", "mitra_id", "latest_status"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "3.1", "3.2", "1", "4")
	rows.AddRow("1", "3.1", "3.2", "1", "5")
	rows.AddRow("1", "3.1", "3.2", "1", "4")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"orders\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderRepository)

	orders, _ := repository.GetLatest20OrderHistoriesByMitraID(1)

	assert := assert.New(t)
	assert.Equal(4, orders[0].LatestStatus)
	assert.Equal(5, orders[1].LatestStatus)
	assert.Equal(4, orders[2].LatestStatus)
}

func TestOrderRepository_GetLatest20OrderHistoriesByCustomerID(t *testing.T) {
	var fieldNames = []string{"id", "customer_lat", "customer_lon", "customer_id", "latest_status"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "3.1", "3.2", "1", "4")
	rows.AddRow("1", "3.1", "3.2", "1", "5")
	rows.AddRow("1", "3.1", "3.2", "1", "4")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"orders\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderRepository)

	orders, _ := repository.GetLatest20OrderHistoriesByCustomerID(1)

	assert := assert.New(t)
	assert.Equal(4, orders[0].LatestStatus)
	assert.Equal(5, orders[1].LatestStatus)
	assert.Equal(4, orders[2].LatestStatus)
}

func TestOrderRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "customer_lat", "customer_lon"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "3.1", "3.2")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"orders\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderRepository)
	_, err := repository.Save(&models.Order{CustomerLat: 3.1, CustomerLon: 3.2})
	assert.NotNil(t, err)
}

func TestOrderRepository_GetOrderOneWeekByMitraID(t *testing.T) {
	var fieldNames = []string{"id", "customer_lat", "customer_lon", "mitra_id", "latest_status", "scheduled_at"}

	schedule_now := time.Now()
	schedule_month_ago := time.Now().AddDate(0, 0, -30)

	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "3.1", "3.2", "1", "1", schedule_now)
	rows.AddRow("1", "3.1", "3.2", "1", "2", schedule_now)
	rows.AddRow("1", "3.1", "3.2", "1", "3", schedule_month_ago)

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"orders\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderRepository)
	now := time.Now()
	orders, _ := repository.GetOrderByDateRange(1, now, now)

	assert := assert.New(t)
	assert.Equal(1, orders[0].LatestStatus)
	assert.Equal(2, orders[1].LatestStatus)
	assert.Equal(3, orders[2].LatestStatus)
}
