package repositories

import (
	"PPLA4/backend/models"
)

type GarbageTypeRepository struct {
}

type IGarbageTypeRepository interface {
	GetAll() ([]models.GarbageType, error)
}

func (g GarbageTypeRepository) GetAll() ([]models.GarbageType, error) {
	var garbageTypes []models.GarbageType
	err := models.GetDB().Find(&garbageTypes).Error
	return garbageTypes, err
}
