package repositories

import (
	"PPLA4/backend/models"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestOrderAttemptRepository_GetByID(t *testing.T) {
	var fieldNames = []string{"id", "order_id", "mitra_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "2", "3")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_attempts\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderAttemptRepository)
	orderAttempt, _ := repository.GetByID(1)

	assert := assert.New(t)
	assert.Equal(uint(1), orderAttempt.ID)
}

func TestOrderAttemptRepository_GetByOrderIDAndMitraID(t *testing.T) {
	var fieldNames = []string{"id", "order_id", "mitra_id", "order_attempt_status_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "2", "3", "4")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^SELECT (.+) FROM \"order_attempts\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderAttemptRepository)
	orderAttempt, _ := repository.GetByOrderIDAndMitraID(2, 3)

	assert := assert.New(t)
	assert.Equal(uint(4), orderAttempt.OrderAttemptStatusID)
}

func TestOrderAttemptRepository_Save(t *testing.T) {
	var fieldNames = []string{"id", "order_id", "mitra_id"}
	rows := sqlmock.NewRows(fieldNames)
	rows.AddRow("1", "2", "3")

	db, mock, _ := sqlmock.New()

	gormDb, _ := gorm.Open("postgres", db)

	mock.ExpectQuery("^(UPDATE|INSERT INTO) \"order_attempts\".+$").WillReturnRows(rows)

	models.SetDB(gormDb)
	repository := new(OrderAttemptRepository)
	_, err := repository.Save(&models.OrderAttempt{OrderID: 2, MitraID: 3})
	assert.NotNil(t, err)
}
