package repositories

import (
	"PPLA4/backend/models"
)

type OrderAttemptRepository struct {
}

type IOrderAttemptRepository interface {
	GetByID(orderAttemptID uint) (*models.OrderAttempt, error)
	GetByOrderIDAndMitraID(orderID uint, mitraID uint) (*models.OrderAttempt, error)
	Save(orderAttempt *models.OrderAttempt) (*models.OrderAttempt, error)
}

func (o OrderAttemptRepository) GetByID(orderAttemptID uint) (*models.OrderAttempt, error) {
	orderAttempt := &models.OrderAttempt{}
	err := models.GetDB().Where("id = ?", orderAttemptID).Find(orderAttempt).Error
	return orderAttempt, err
}

func (o OrderAttemptRepository) GetByOrderIDAndMitraID(orderID uint, mitraID uint) (*models.OrderAttempt, error) {
	orderAttempt := &models.OrderAttempt{}
	err := models.GetDB().Where("order_id = ? AND mitra_id = ?", orderID, mitraID).Find(orderAttempt).Error
	return orderAttempt, err
}

func (o OrderAttemptRepository) Save(orderAttempt *models.OrderAttempt) (*models.OrderAttempt, error) {
	err := models.GetDB().Save(&orderAttempt).Error
	return orderAttempt, err
}
