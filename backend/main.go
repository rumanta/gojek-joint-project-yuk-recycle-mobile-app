package main

import (
	customerControllers "PPLA4/backend/controllers/customer"
	mitraControllers "PPLA4/backend/controllers/mitra"
	notificationControllers "PPLA4/backend/controllers/notification"
	"PPLA4/backend/middleware"
	"PPLA4/backend/repositories"
	customerServices "PPLA4/backend/services/customer"
	makeOrderCheckInterval "PPLA4/backend/services/customer/make_order_database_checking_interval"
	oauthConfiguration "PPLA4/backend/services/customer/oauth_configuration"
	mitraServices "PPLA4/backend/services/mitra"
	notificationServices "PPLA4/backend/services/notification"
	workerServices "PPLA4/backend/services/worker"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()

	mitraRepository := repositories.MitraRepository{}
	garbageTypeRepository := repositories.GarbageTypeRepository{}
	mitraGarbageSettingRepository := repositories.MitraGarbageSettingRepository{}
	mitraPickUpDistanceRepository := repositories.MitraPickUpDistanceRepository{}
	mitraPickUpScheduleRepository := repositories.MitraPickUpScheduleRepository{}
	customerRepository := repositories.CustomerRepository{}
	tokenCreation := middleware.TokenCreation{}
	customerOAuthConfiguration := oauthConfiguration.GoogleOAuthConfiguration{}

	mitraRegistrationService := mitraServices.RegistrationService{
		MitraRepository:               mitraRepository,
		GarbageTypeRepository:         garbageTypeRepository,
		MitraGarbageSettingRepository: mitraGarbageSettingRepository,
		MitraPickUpDistanceRepository: mitraPickUpDistanceRepository,
		MitraPickUpScheduleRepository: mitraPickUpScheduleRepository,
		TokenCreation:                 tokenCreation,
	}
	mitraRegistrationController := mitraControllers.Registration{RegistrationService: mitraRegistrationService}
	mitraAuthService := mitraServices.AuthService{MitraRepository: mitraRepository, TokenCreation: tokenCreation}
	settingServices := mitraServices.SettingsService{
		GarbageTypeRepository:         garbageTypeRepository,
		MitraGarbageSettingRepository: mitraGarbageSettingRepository,
		MitraPickUpDistanceRepository: mitraPickUpDistanceRepository,
		MitraPickUpScheduleRepository: mitraPickUpScheduleRepository,
	}

	mitraAuthController := mitraControllers.Authentication{AuthService: mitraAuthService}
	mitraSettingsController := mitraControllers.Settings{SettingsService: settingServices}

	customerRegistrationService := customerServices.RegistrationService{CustomerRepository: customerRepository}
	customerRegistrationController := customerControllers.Registration{RegistrationService: customerRegistrationService}
	customerAuthService := customerServices.AuthService{CustomerRepository: customerRepository, TokenCreation: tokenCreation}
	customerAuthController := customerControllers.Authentication{AuthService: customerAuthService}
	customerGoogleOAuthService := customerServices.GoogleOAuthService{CustomerRepository: customerRepository, Config: customerOAuthConfiguration, TokenCreation: tokenCreation}
	customerGoogleOAuthController := customerControllers.GoogleOAuth{GoogleOAuthService: customerGoogleOAuthService}

	updateTokenNotificationService := notificationServices.UpdateTokenService{
		MitraRepository:    mitraRepository,
		CustomerRepository: customerRepository,
	}
	updateTokenNotificationController := notificationControllers.UpdateTokenController{
		UpdateTokenService: updateTokenNotificationService,
	}

	notificationService := notificationServices.NotificationService{}
	dummySendNotificationController := notificationControllers.DummyNotificationController{
		NotificationService: notificationService,
	}
	customerHomepageService := customerServices.HomepageService{}
	customerHomepageController := customerControllers.Homepage{HomepageService: customerHomepageService}

	customerManageAccountService := customerServices.ManageAccountService{CustomerRepository: customerRepository}
	customerManageAccountController := customerControllers.ManageAccount{ManageAccountService: customerManageAccountService}

	orderRepository := repositories.OrderRepository{}
	orderDetailRepository := repositories.OrderDetailRepository{}
	orderStatusHistoryRepository := repositories.OrderStatusHistoryRepository{}
	orderStatusRepository := repositories.OrderStatusRepository{}

	mitraLocationRepository := repositories.MitraLocationRepository{}

	orderAttemptRepository := repositories.OrderAttemptRepository{}

	orderAttemptService := mitraServices.OrderAttemptService{OrderAttemptRepository: orderAttemptRepository}

	orderAttemptController := mitraControllers.OrderAttempt{OrderAttemptService: orderAttemptService}

	makeOrderCheckIntervalService := makeOrderCheckInterval.CheckingInterval{
		OrderAttemptRepository: orderAttemptRepository,
	}

	makeOrderServices := customerServices.MakeOrderServices{
		OrderRepository:               orderRepository,
		MitraRepository:               mitraRepository,
		MitraLocationRepository:       mitraLocationRepository,
		MitraPickUpDistanceRepository: mitraPickUpDistanceRepository,
		MitraGarbageSettingRepository: mitraGarbageSettingRepository,
		NotificationServices:          notificationService,
		MakeOrderCheckInterval:        makeOrderCheckIntervalService,
		OrderAttemptRepository:        orderAttemptRepository,
		CustomerRepository:            customerRepository,
		GarbageTypeRepository:         garbageTypeRepository,
		OrderDetailRepository:         orderDetailRepository,
		OrderStatusHistoryRepository:  orderStatusHistoryRepository,
		MitraPickUpScheduleRepository: mitraPickUpScheduleRepository,
	}

	makeOrderWorker := workerServices.MakeOrderWorker{
		MakeOrderService:     makeOrderServices,
		NotificationServices: notificationService,
	}

	makeOrderController := customerControllers.MakeOrderController{
		MakeOrderWorker: makeOrderWorker,
	}

	ManageMitraAccountService := mitraServices.ManageMitraAccountService{
		MitraRepository:         mitraRepository,
		MitraLocationRepository: mitraLocationRepository,
	}

	manageAccountMitraController := mitraControllers.ManageAccount{ManageMitraAccountService: ManageMitraAccountService}

	// orderAttemptStatusRepository := new(repositories.OrderAttemptStatusRepository)
	mitraOrderDetailService := mitraServices.OrderDetailService{
		OrderRepository:              orderRepository,
		CustomerRepository:           customerRepository,
		MitraLocationRepository:      mitraLocationRepository,
		OrderDetailRepository:        orderDetailRepository,
		GarbageTypeRepository:        garbageTypeRepository,
		OrderStatusHistoryRepository: orderStatusHistoryRepository,
		OrderStatusRepository:        orderStatusRepository,
	}

	mitraOrderDetailController := mitraControllers.OrderDetail{
		OrderDetailService: mitraOrderDetailService,
	}

	mitraActiveOrderService := mitraServices.ActiveOrderService{
		OrderRepository:       orderRepository,
		CustomerRepository:    customerRepository,
		OrderStatusRepository: orderStatusRepository,
	}

	mitraActiveOrderController := mitraControllers.ActiveOrder{
		ActiveOrderService: mitraActiveOrderService,
	}

	pickUpOrderService := mitraServices.PickUpOrderService{
		OrderRepository:     orderRepository,
		OrderStatusHistory:  orderStatusHistoryRepository,
		NotificationService: notificationService,
		CustomerRepository:  customerRepository,
		MitraRepository:     mitraRepository,
	}

	pickUpOrderController := mitraControllers.PickUpOrderController{
		PickUpOrderService: pickUpOrderService,
	}

	completeOrderService := mitraServices.CompleteOrderService{
		OrderRepository:     orderRepository,
		OrderStatusHistory:  orderStatusHistoryRepository,
		NotificationService: notificationService,
		CustomerRepository:  customerRepository,
		MitraRepository:     mitraRepository,
	}

	completeOrderController := mitraControllers.CompleteOrderController{
		CompleteOrderService: completeOrderService,
	}

	mitraCancelOrderService := mitraServices.CancelOrderService{
		OrderRepository:     orderRepository,
		OrderStatusHistory:  orderStatusHistoryRepository,
		NotificationService: notificationService,
		CustomerRepository:  customerRepository,
		MitraRepository:     mitraRepository,
	}

	mitraCancelOrderController := mitraControllers.CancelOrderController{
		CancelOrderService: mitraCancelOrderService,
	}

	customerCancelOrderService := customerServices.CancelOrderService{
		OrderRepository:     orderRepository,
		OrderStatusHistory:  orderStatusHistoryRepository,
		NotificationService: notificationService,
		CustomerRepository:  customerRepository,
		MitraRepository:     mitraRepository,
	}

	customerCancelOrderController := customerControllers.CancelOrderController{
		CancelOrderService: customerCancelOrderService,
	}
	mitraOrderHistoryService := mitraServices.OrderHistoryService{
		OrderRepository:       orderRepository,
		CustomerRepository:    customerRepository,
		OrderStatusRepository: orderStatusRepository,
	}

	mitraOrderHistoryController := mitraControllers.OrderHistory{
		OrderHistoryService: mitraOrderHistoryService,
	}

	customerActiveOrderService := customerServices.ActiveOrderService{
		OrderRepository:       orderRepository,
		MitraRepository:       mitraRepository,
		OrderStatusRepository: orderStatusRepository,
	}

	customerActiveOrderController := customerControllers.ActiveOrder{
		ActiveOrderService: customerActiveOrderService,
	}

	customerOrderHistoryService := customerServices.OrderHistoryService{
		OrderRepository:       orderRepository,
		MitraRepository:       mitraRepository,
		OrderStatusRepository: orderStatusRepository,
	}

	customerOrderHistoryController := customerControllers.OrderHistory{
		OrderHistoryService: customerOrderHistoryService,
	}

	customerOrderDetailService := customerServices.OrderDetailService{
		OrderRepository:              orderRepository,
		MitraRepository:              mitraRepository,
		MitraLocationRepository:      mitraLocationRepository,
		OrderDetailRepository:        orderDetailRepository,
		GarbageTypeRepository:        garbageTypeRepository,
		OrderStatusHistoryRepository: orderStatusHistoryRepository,
		OrderStatusRepository:        orderStatusRepository,
	}

	customerOrderDetailController := customerControllers.OrderDetail{
		OrderDetailService: customerOrderDetailService,
	}

	garbageTypeService := customerServices.GarbageTypeService{GarbageTypeRepository: garbageTypeRepository}
	garbageTypeController := customerControllers.GarbageType{GarbageTypeService: garbageTypeService}
	customerFAQService := customerServices.FAQService{}
	customerFAQController := customerControllers.FAQ{
		FAQService: customerFAQService,
	}
	mitraFAQService := mitraServices.FAQService{}
	mitraFAQController := mitraControllers.FAQ{
		FAQService: mitraFAQService,
	}

	mitraHomepageService := mitraServices.HomepageService{MitraRepository: mitraRepository, OrderRepository: orderRepository, OrderDetailRepository: orderDetailRepository}
	mitraHomepageController := mitraControllers.Homepage{HomepageService: mitraHomepageService}

	adjustOrderService := mitraServices.AdjustOrderService{
		OrderRepository:               orderRepository,
		OrderDetailRepository:         orderDetailRepository,
		MitraGarbageSettingRepository: mitraGarbageSettingRepository,
	}
	adjustOrderController := mitraControllers.AdjustOrderController{
		AdjustOrderService: adjustOrderService,
	}
	customerTermsAndConditionsService := customerServices.TermsAndConditionsService{}
	customerTermsAndConditionsController := customerControllers.TermsAndConditions{
		TermsAndConditionsService: customerTermsAndConditionsService,
	}
	mitraTermsAndConditionsService := mitraServices.TermsAndConditionsService{}
	mitraTermsAndConditionsController := mitraControllers.TermsAndConditions{
		TermsAndConditionsService: mitraTermsAndConditionsService,
	}

	router.HandleFunc("/api/v1/mitra/login", mitraAuthController.Authenticate).Methods("POST")
	router.HandleFunc("/api/v1/mitra/new", mitraRegistrationController.Register).Methods("POST")
	router.HandleFunc("/api/v1/mitra/dummy", mitraControllers.Dummy).Methods("POST")
	router.HandleFunc("/api/v1/mitra/account", manageAccountMitraController.GetMitraData).Methods("GET")
	router.HandleFunc("/api/v1/mitra/account", manageAccountMitraController.UpdateMitraData).Methods("POST")
	router.HandleFunc("/api/v1/mitra/settings", mitraSettingsController.GetSettings).Methods("GET")
	router.HandleFunc("/api/v1/mitra/settings", mitraSettingsController.UpdateSettings).Methods("PUT")
	router.HandleFunc("/api/v1/customer/login", customerAuthController.Authenticate).Methods("POST")
	router.HandleFunc("/api/v1/customer/new", customerRegistrationController.Register).Methods("POST")
	router.HandleFunc("/api/v1/customer/dummy", customerControllers.Dummy).Methods("POST")
	router.HandleFunc("/api/v1/customer/oauth/google", customerGoogleOAuthController.GoogleOAuthHandler).Methods("POST")
	router.HandleFunc("/api/v1/customer/home", customerHomepageController.GetHomepageData).Methods("GET")
	router.HandleFunc("/api/v1/customer/privacy-policy", customerManageAccountController.GetPrivacyPolicyDetail).Methods("GET")
	router.HandleFunc("/api/v1/mitra/privacy-policy", customerManageAccountController.GetPrivacyPolicyDetail).Methods("GET")
	router.HandleFunc("/api/v1/customer/account", customerManageAccountController.GetCustomerData).Methods("GET")
	router.HandleFunc("/api/v1/customer/account", customerManageAccountController.UpdateCustomerData).Methods("POST")
	router.HandleFunc("/api/v1/notification/token", updateTokenNotificationController.UpdateTokenHandler).Methods("PUT")
	router.HandleFunc("/api/v1/notification/dummy", dummySendNotificationController.NotifyHandler).Methods("POST")
	router.HandleFunc("/api/v1/order", makeOrderController.MakeOrderHandler).Methods("POST")
	router.HandleFunc("/api/v1/mitra/order/attempt/approve", orderAttemptController.ApproveOrderAttempt).Methods("POST")
	router.HandleFunc("/api/v1/mitra/order/attempt/reject", orderAttemptController.RejectOrderAttempt).Methods("POST")
	router.HandleFunc("/api/v1/customer/garbage-types", garbageTypeController.GetGarbageTypeList).Methods("GET")
	router.HandleFunc("/api/v1/mitra/garbage-types", garbageTypeController.GetGarbageTypeList).Methods("GET")
	router.HandleFunc("/api/v1/mitra/order/active", mitraActiveOrderController.GetActiveOrders).Methods("GET")
	router.HandleFunc("/api/v1/mitra/order/pickup/{orderID}", pickUpOrderController.PickUpOrderHandler).Methods("POST")
	router.HandleFunc("/api/v1/mitra/order/complete/{orderID}", completeOrderController.CompleteOrderHandler).Methods("POST")
	router.HandleFunc("/api/v1/mitra/order/cancel/{orderID}", mitraCancelOrderController.CancelOrderHandler).Methods("POST")
	router.HandleFunc("/api/v1/customer/order/cancel/{orderID}", customerCancelOrderController.CancelOrderHandler).Methods("POST")
	router.HandleFunc("/api/v1/mitra/order/history", mitraOrderHistoryController.GetOrderHistories).Methods("GET")
	router.HandleFunc("/api/v1/mitra/order/{orderID}", mitraOrderDetailController.GetOrderDetail).Methods("GET")
	router.HandleFunc("/api/v1/customer/order/active", customerActiveOrderController.GetActiveOrders).Methods("GET")
	router.HandleFunc("/api/v1/mitra/home", mitraHomepageController.GetMitraHomepage).Methods("GET")
	router.HandleFunc("/api/v1/customer/order/history", customerOrderHistoryController.GetOrderHistories).Methods("GET")
	router.HandleFunc("/api/v1/customer/order/{orderID}", customerOrderDetailController.GetOrderDetail).Methods("GET")
	router.HandleFunc("/api/v1/customer/FAQ", customerFAQController.GetFAQData).Methods("GET")
	router.HandleFunc("/api/v1/mitra/FAQ", mitraFAQController.GetFAQData).Methods("GET")
	router.HandleFunc("/api/v1/mitra/order/adjust/{orderID}", adjustOrderController.AdjustOrderHandler).Methods("PUT")
	router.HandleFunc("/api/v1/customer/TNC", customerTermsAndConditionsController.GetTermsAndConditionsData).Methods("GET")
	router.HandleFunc("/api/v1/mitra/TNC", mitraTermsAndConditionsController.GetTermsAndConditionsData).Methods("GET")
	router.Use(middleware.JwtAuthentication) //attach JWT auth middleware

	port := os.Getenv("PORT")
	if port == "" {
		port = "8000" //localhost
	}

	fmt.Println(port)

	err := http.ListenAndServe(":"+port, router) //Launch the app, visit localhost:8000/api
	if err != nil {
		fmt.Print(err)
	}
}
